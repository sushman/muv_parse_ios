 //
//  LocationDetail.m
//  MuV
//
//  Created by Hing Huynh on 2/5/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "LocationDetail.h"
#import "BookingInformation.h"
#import "AdminManager.h"

#define PAST_ADDRESS_TABLE @"pastAddress"
#define PAST_ADDRESS_COL_USER @"user"
#define PAST_ADDRESS_COL_ADDRESS @"searchedAddress"
#define PAST_ADDRESS_COL_NUMTIMESUSED @"numOfTimesUsed"

@implementation LocationDetail

AdminManager *adminManager;

@synthesize pictureName, addressType, address, placemark;

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:pictureName forKey:@"pictureName"];
    [encoder encodeObject:addressType forKey:@"addressType"];
    [encoder encodeObject:address forKey:@"address"];
    [encoder encodeObject:placemark forKey:@"placemark"];
}


- (id)initWithCoder:(NSCoder *)decoder {
    if (self = [super init]) {
        self.pictureName = [decoder decodeObjectForKey:@"pictureName"];
        self.addressType = [decoder decodeObjectForKey:@"addressType"];
        self.address = [decoder decodeObjectForKey:@"address"];
        self.placemark = [decoder decodeObjectForKey:@"placemark"];
    }
    return self;
}



- (void)getPossibilitiesFromAddress:(NSString *)searchAddress forRegion:(CLLocationCoordinate2D)placemarkCurrent withCompletionBLock:(void (^) (NSMutableArray *validAddresses, NSError *error))completionBLock {
    
    CLLocationCoordinate2D centerCoordinate = placemarkCurrent;
    
    CLCircularRegion *region = [[CLCircularRegion alloc] initWithCenter:centerCoordinate
                                                                 radius:80000 // Metres
                                                             identifier:@"typedLocations"];
    
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    
    [geocoder geocodeAddressString:searchAddress inRegion:region completionHandler:^(NSArray *placemarks, NSError *error) {
        
        if (!error) {
            NSMutableArray *validAddresses = [NSMutableArray new];
            for (CLPlacemark *location in placemarks){
                if (location.subThoroughfare.length){
                    LocationDetail *locationDetail = [LocationDetail new];
                    locationDetail.pictureName = @"locatorIcon.png";
                    if (location.name){
                        locationDetail.addressType = location.name;
                    } else {
                        locationDetail.addressType = @"Search Result";
                    }
                    locationDetail.address = [NSString stringWithFormat:@"%@ %@, %@, %@, %@", location.subThoroughfare, location.thoroughfare, location.locality,location.administrativeArea, location.postalCode];
                    locationDetail.placemark = placemark;
                    [validAddresses addObject:locationDetail];
                }
            }
            completionBLock(validAddresses, nil);
        } else {
            completionBLock(nil, error);
        }
    }];
}


- (void)getPlacemarkFromAddress:(NSString *)validAddress withCompletionBLock:(void (^) (CLPlacemark *placemark, NSError *error))completionBLock {
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder geocodeAddressString:validAddress completionHandler:^(NSArray *placemarks, NSError *error) {
        if (placemarks){
            completionBLock([placemarks firstObject], nil);
        } else {
            completionBLock(nil, error);
        }
    }];
}



- (void)getUserLocationsWithCompletionBlock:(void (^) (NSMutableArray *locationsArray, NSError *error))completionBLock {
    
    _userInfo = [UserProfileInformation sharedMySingleton];
    
    __block NSMutableArray *arrLocationDetail = [[NSMutableArray alloc] init];
    __block LocationDetail *locationDetail;
    
    //home location
    
    if (![[NSString stringWithFormat:@"%@, %@, %@, %@", _userInfo.user[@"homeAddress"], _userInfo.user[@"homeCity"], _userInfo.user[@"homeState"], _userInfo.user[@"homeZip"]] isEqualToString:@"(null), (null), (null), (null)"]) {
       
        locationDetail = [LocationDetail new];
        locationDetail.pictureName = @"MuV_HomeIcon.png";
        locationDetail.addressType = @"Home";
        locationDetail.address = [NSString stringWithFormat:@"%@, %@, %@, %@", _userInfo.user[@"homeAddress"], _userInfo.user[@"homeCity"], _userInfo.user[@"homeState"], _userInfo.user[@"homeZip"]];
        [arrLocationDetail addObject:locationDetail];
        
    }
    
    //work location
    if (![[NSString stringWithFormat:@"%@, %@, %@, %@", _userInfo.user[@"workAddress"], _userInfo.user[@"workCity"], _userInfo.user[@"workState"], _userInfo.user[@"workZip"]] isEqualToString:@"(null), (null), (null), (null)"]) {
        
        locationDetail = [LocationDetail new];
        locationDetail.pictureName = @"MuV_WorkIcon.png";
        locationDetail.addressType = @"Work";
        locationDetail.address = [NSString stringWithFormat:@"%@, %@, %@, %@", _userInfo.user[@"workAddress"], _userInfo.user[@"workCity"], _userInfo.user[@"workState"], _userInfo.user[@"workZip"]];
        [arrLocationDetail addObject:locationDetail];
    }
    
    //current
    locationDetail = [LocationDetail new];
    locationDetail.pictureName = @"MuV_LocatorSearchIcon.png";
    locationDetail.addressType = @"Current";
    locationDetail.address = [LocationManager sharedInstance].currentLocation;
    [arrLocationDetail addObject:locationDetail];

    [self getPastFiveAddressesForUserWithCompletionBlock:^(NSArray *pastAddresses, NSError *error) {
        for (NSString *pastAddress in pastAddresses) {
            locationDetail = [LocationDetail new];
            locationDetail.pictureName = @"MuV_PastTripsIcon.png";
            locationDetail.addressType = @"Past Location";
            locationDetail.address = pastAddress;
            [arrLocationDetail addObject:locationDetail];
        }
         completionBLock(arrLocationDetail, nil);
    }];
}

- (NSMutableArray *)getUserLocations {
    
    _userInfo = [UserProfileInformation sharedMySingleton];
    
    NSMutableArray *arrLocationDetail = [[NSMutableArray alloc] init];
    
    LocationDetail *locationDetail = [LocationDetail new];
    locationDetail.pictureName = @"MuV_HomeIcon.png";
    locationDetail.addressType = @"Home";
    locationDetail.address = [NSString stringWithFormat:@"%@ %@, %@ %@", _userInfo.user[@"homeAddress"], _userInfo.user[@"homeCity"], _userInfo.user[@"homeState"], _userInfo.user[@"homeZip"]];
    
    [arrLocationDetail addObject:locationDetail];
    
    locationDetail = [LocationDetail new];
    locationDetail.pictureName = @"MuV_WorkIcon.png";
    locationDetail.addressType = @"Work";
    locationDetail.address = [NSString stringWithFormat:@"%@ %@, %@ %@", _userInfo.user[@"workAddress"], _userInfo.user[@"workCity"], _userInfo.user[@"workState"], _userInfo.user[@"workZip"]];
    [arrLocationDetail addObject:locationDetail];
    
    locationDetail = [LocationDetail new];
    locationDetail.pictureName = @"MuV_LocatorSearchIcon.png";
    locationDetail.addressType = @"Current";
    locationDetail.address = [LocationManager sharedInstance].currentLocation;
    [arrLocationDetail addObject:locationDetail];
    
    return arrLocationDetail;
    
}


- (void)addPastAddressIfNotDuplicate:(NSString *)searchedAddress {
    
    NSString *usersHomeAddress = [NSString stringWithFormat:@"%@, %@, %@, %@", _userInfo.user[@"homeAddress"], _userInfo.user[@"homeCity"], _userInfo.user[@"homeState"], _userInfo.user[@"homeZip"]];
    
    NSString *usersWorkAddress = [NSString stringWithFormat:@"%@, %@, %@, %@", _userInfo.user[@"workAddress"], _userInfo.user[@"workCity"], _userInfo.user[@"workState"], _userInfo.user[@"workZip"]];
    
    
    if ([searchedAddress isEqualToString:usersHomeAddress] || [searchedAddress isEqualToString:usersWorkAddress]) {
        return;
    }
    if ([searchedAddress isEqualToString:@"(null), (null), (null), (null)"]){
        return;
    }
    if ([searchedAddress rangeOfString:@"null" options:NSCaseInsensitiveSearch].location != NSNotFound) {
        return;
    }
    
    [self checkIfAddressExists:searchedAddress withCompletionBlock:^(PFObject *object, NSError *error) {
        PFObject *pastAddress;
        if (error.code == 101) {
            if (!object) {
                pastAddress = [PFObject objectWithClassName:PAST_ADDRESS_TABLE];
                pastAddress[PAST_ADDRESS_COL_ADDRESS] = searchedAddress;
                pastAddress[PAST_ADDRESS_COL_USER] =  [UserProfileInformation sharedMySingleton].user;
                pastAddress[PAST_ADDRESS_COL_NUMTIMESUSED] =  [NSNumber numberWithInt:0];
            }
        }
        else if (object) {
            pastAddress = [self updatePastAddressDetails:object];
        }
        else {
             NSLog(@"error  %@ saving past addresses %@ ", error, searchedAddress);
        }
        if (pastAddress) {
            [pastAddress saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                
            }];
        }
    }];
}

- (void)checkIfAddressExists:(NSString *)searchedAddress withCompletionBlock:(void (^)(PFObject *object, NSError *error))completionBlock {
    PFQuery *pastAddressQuery = [self queryIsPastAddress:searchedAddress];
    
    [pastAddressQuery getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        if (!error) {
            if (object != nil) {
               // [self updatePastAddressDetails:object];
                completionBlock (object, nil);
            }
        }
        else {
            completionBlock (nil, error);
        }
    }];
}

- (PFObject *)updatePastAddressDetails:(PFObject *)pastAddress {
    
    NSNumber *numTimesUsed = pastAddress[PAST_ADDRESS_COL_NUMTIMESUSED];
    
    int value = [numTimesUsed intValue];
    numTimesUsed = [NSNumber numberWithInt:value + 1];
    pastAddress[PAST_ADDRESS_COL_NUMTIMESUSED] = numTimesUsed;
    return pastAddress;
}

- (void)updateRideObject:(PFObject *)ride withCompletionBlock:(void (^)(BOOL succeeded, NSError *error))completionBlock {
    [ride saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (succeeded) {
            completionBlock (succeeded, nil);
        }
        else {
            completionBlock (NO, error);
        }
    }];
}

- (PFQuery *)queryIsPastAddress:(NSString *)searchedAddress {
    PFQuery *usersPastAddress = [PFQuery queryWithClassName:PAST_ADDRESS_TABLE];
    
    [usersPastAddress whereKey:PAST_ADDRESS_COL_USER equalTo: [UserProfileInformation sharedMySingleton].user];
    [usersPastAddress whereKey:PAST_ADDRESS_COL_ADDRESS equalTo:searchedAddress];
   
    return usersPastAddress;
}

- (void)getPastFiveAddressesForUserWithCompletionBlock:(void (^)(NSArray *pastAddresses, NSError *error))completionBlock {
    
    PFQuery *pastAddressQuery = [self queryPastFiveAddressesForUser];
    
    [pastAddressQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (objects) {
            NSMutableArray *arrayOfAddresses = [NSMutableArray new];
            for (PFObject *pastAddress in objects) {
                if ([pastAddress[PAST_ADDRESS_COL_ADDRESS] rangeOfString:@"null" options:NSCaseInsensitiveSearch].location == NSNotFound) {
                    [arrayOfAddresses addObject:pastAddress[PAST_ADDRESS_COL_ADDRESS]];
                }
            }
            completionBlock (arrayOfAddresses, nil);
        }
        else {
            completionBlock (nil, error);
        }
    }];
}

- (PFQuery *)queryPastFiveAddressesForUser {
    adminManager = [AdminManager sharedInstance];
    PFQuery *usersPastAddress = [PFQuery queryWithClassName:PAST_ADDRESS_TABLE];
    [usersPastAddress whereKey:PAST_ADDRESS_COL_USER equalTo: [UserProfileInformation sharedMySingleton].user];
    [usersPastAddress orderByDescending:@"updatedAt"];
    usersPastAddress.limit = [adminManager.adminSettings.numberOfRecentLocations integerValue];

    return usersPastAddress;
}

@end
