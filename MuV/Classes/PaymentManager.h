//
//  PaymentManager.h
//  MuV
//
//  Created by Hing Huynh on 3/2/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>
#import "Ride.h"

@interface PaymentManager : NSObject

- (void)createPaymentWithPaymentDetail:(PFObject *)paymentDetail
                      andAmountCharged:(NSNumber *)amount
                   andCostWithNoCoupon:(NSNumber *)originalFareCost
                       andDriverDetail:(PFObject *)driverDetail
                   withCompletionBlock:(void (^)(PFObject *payment, NSError *error))completionBlock;

- (PFObject *)createPaymentObject:(PFObject *)paymentDetail
                 andAmountCharged:(NSNumber *)amount
              andCostWithNoCoupon:(NSNumber *)originalFareCost
                  andDriverDetail:(PFObject *)driverDetail
                      andCouponId:(PFObject *)couponId
                  andCouponAmount:(NSNumber *)couponAmount;

- (void)savePaymentObject:(NSArray *)paymentObjs
      withCompletionBlock:(void (^)(BOOL succeeded, NSError *error))completionBlock;

// use this for full payment charge including last minute cancellation
- (void)chargePaymentWithRide:(PFObject *)ride withCompletionBlock:(void (^)(BOOL succeeded, NSError *error))completionBlock;

// used only when charging the cancellation fee amount from admin settings
- (void)createCancellationChargeWithRide:(PFObject *)ride withCompletionBlock:(void (^)(BOOL succeeded, NSError *error))completionBlock;

- (void)addVenmoAccountWithFirstName:(NSString *)firstName
                            lastName:(NSString *)lastName
                               email:(NSString *)email
                         dateOfBirth:(NSString *)dateOfBirth
                                 ssn:(NSString *)taxId
                       streetAddress:(NSString *)streetAddress
                                city:(NSString *)city
                               state:(NSString *)state
                             zipCode:(NSString *)zipCode
                       subMerchantId:(NSString *)subMerchantId;
@end
