//
//  CouponManager.m
//  MuV
//
//  Created by SushmaN on 2/23/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "CouponManager.h"
#import "UserProfileInformation.h"
#import "Helper.h"


#define COUPON_TABLE @"coupon"

@implementation CouponManager

+ (instancetype)sharedInstance {
    
    static CouponManager *sharedInstance = nil;
    static dispatch_once_t dispatchOnce;
    
    dispatch_once(&dispatchOnce, ^{
        sharedInstance = [[self alloc]init];
        // sharedInstance.couponName = [NSString new];
    });
    
    return sharedInstance;
}


- (void)createCouponCodeRecordforReferralForAmount:(NSNumber *)amount {
    
    PFObject *couponCode = [PFObject objectWithClassName:COUPON_TABLE];
    couponCode[@"amount"] = amount;
    couponCode[@"user"] =  [UserProfileInformation sharedMySingleton].user;
    couponCode[@"type"] =  @"amount";
    couponCode[@"unusedCoupons"] = [NSNumber numberWithInt:0];
    couponCode[@"active"] = [NSNumber numberWithBool:1];
    [couponCode saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (succeeded) {
            couponCode[@"name"] = couponCode.objectId;
            _couponName =  couponCode.objectId;
            [couponCode saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (!succeeded) {
                    NSLog(@"error saving coupon code name");
                }
            }];
        }
        else {
            NSLog(@"error saving coupon code for referral");
        }
    }];
}

- (void)applyCoupon:(PFObject *)coupon andCouponAmount:(NSNumber *)couponAmount withCompletionBlock:(void (^)(PFObject *couponObj, NSError *error))completionBlock {
    
    if (coupon) {
        PFUser *user = coupon[@"admin"];
        if (user) {
            //this is an admin generated coupon
            NSNumber *maxRedemptions = coupon[@"maxRedemptions"];
            int value = [maxRedemptions intValue];
            maxRedemptions = [NSNumber numberWithInt:value - 1];
            coupon[@"maxRedemptions"] = maxRedemptions;
        }
        else {
            //referral earned coupon
            user = coupon[@"user"];
            
            if (user) {
                //check if the users are same
                if ([user.objectId isEqualToString:[UserProfileInformation sharedMySingleton].user.objectId]) {
                    
                    //if same user reduce the num of unused coupon by 1
                    NSNumber *numOfUnusedCoupons = coupon[@"unusedCoupons"];
                    int value = [numOfUnusedCoupons intValue];
                    numOfUnusedCoupons = [NSNumber numberWithInt:value - 1];
                    coupon[@"unusedCoupons"] = numOfUnusedCoupons;
                }
                else {
                    //increase the unsed coupon by 1 and update the friend user
                    // friend referral coupon
                    NSNumber *numOfUnusedCoupons = coupon[@"unusedCoupons"];
                    int value = [numOfUnusedCoupons intValue];
                    numOfUnusedCoupons = [NSNumber numberWithInt:value + 1];
                    coupon[@"unusedCoupons"] = numOfUnusedCoupons;
                    [coupon addObject:[UserProfileInformation sharedMySingleton].user.objectId forKey:@"usedBy"];
                    user = [UserProfileInformation sharedMySingleton].user;
                    user[@"referralUsed"] = [NSNumber numberWithBool:YES];
                    [user saveInBackground];
                }
            }
            // this is promotional coupon
            else {
                
                NSNumber *maxRedemptions = coupon[@"maxRedemptions"];
                int value = [maxRedemptions intValue];
                maxRedemptions = [NSNumber numberWithInt:value - 1];
                coupon[@"maxRedemptions"] = maxRedemptions;
                [coupon addObject:[UserProfileInformation sharedMySingleton].user.objectId forKey:@"usedBy"];
                
            }
        }
        
        [coupon saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
            if (!error){
                completionBlock (coupon, nil);
            }
            else {
                completionBlock (nil, error);
            }
        }];
    }
}

- (void)validateAndGetCoupon:(NSString *)couponName withCompletionBlock:(void (^)(Coupon *coupon, NSError *error)) completionBlock {
    
    PFUser *currentUser = [UserProfileInformation sharedMySingleton].user;
    //if user has used his referral coupon
    
    if (currentUser[@"referralUsed"]) {
        [self getEarnedPromoAndAdminCoupons:(NSString *)couponName WithCompletionBlock:^(PFObject *coupon, NSError *error) {
            if (!error && coupon) {
                //is a valid coupon
                completionBlock ([self createCouponObject:coupon], nil);
            }
            else {
                completionBlock (nil, error);
            }
        }];
    }
    else {
        [self getReferralEarnedPromoAndAdminCoupons:couponName WithCompletionBlock:^(PFObject *coupon, NSError *error) {
            if (!error && coupon) {
                completionBlock ([self createCouponObject:coupon], nil);
            }
            else {
                completionBlock (nil, error);
            }
        }];
    }
}


- (Coupon *)createCouponObject:(PFObject *)couponObj{
    Coupon *coupon = [Coupon new];
    coupon.coupon = couponObj;
    coupon.couponName = couponObj[@"name"];
    coupon.couponAmount = couponObj[@"amount"];
    coupon.couponType = couponObj[@"type"];
   
    if (couponObj[@"user"]) {
        coupon.numOfCoupons = couponObj[@"unusedCoupons"];
    }
    else if (couponObj[@"admin"]){
        coupon.numOfCoupons = couponObj[@"maxRedemptions"];
    }
    return coupon;
}

- (PFQuery *)getCouponsEarnedQuery:(NSString *)couponName showAll:(BOOL)showAllCoupons {
    PFQuery *couponEarned = [PFQuery queryWithClassName:COUPON_TABLE];
    
    if (couponName) {
        [couponEarned whereKey:@"name" equalTo: couponName];
    }
    
    [couponEarned whereKey:@"active" equalTo: [NSNumber numberWithBool:YES]];
    
    [couponEarned whereKey:@"user" equalTo: [UserProfileInformation sharedMySingleton].user];
    if (!showAllCoupons) {
        [couponEarned whereKey:@"unusedCoupons" greaterThan:[NSNumber numberWithInt:0]];
    }
   
    return couponEarned;
}

- (PFQuery *)getAdminCouponsQuery:(NSString *)couponName showAll:(BOOL)showAllCoupons {
    PFQuery *adminCoupon = [PFQuery queryWithClassName:COUPON_TABLE];
    if (couponName) {
        [adminCoupon whereKey:@"name" equalTo: couponName];
    }
    
    [adminCoupon whereKey:@"admin" equalTo: [UserProfileInformation sharedMySingleton].user];
    [adminCoupon whereKey:@"active" equalTo: [NSNumber numberWithBool:YES]];
    [adminCoupon whereKey:@"expiresOn" lessThanOrEqualTo:[Helper lastSecondOfTheDay:[NSDate date]]];
    
    if (!showAllCoupons) {
        [adminCoupon whereKey:@"maxRedemptions" greaterThan:[NSNumber numberWithInt:0]];
    }
    return adminCoupon;
}


- (PFQuery *)gePromotionalCouponsQuery:(NSString *)couponName showAll:(BOOL)showAllCoupons {
    PFQuery *promoCoupon = [PFQuery queryWithClassName:COUPON_TABLE];
    if (couponName) {
        [promoCoupon whereKey:@"name" equalTo: couponName];
    }
    
    [promoCoupon whereKey:@"active" equalTo: [NSNumber numberWithBool:YES]];
    [promoCoupon whereKey:@"expiresOn" greaterThanOrEqualTo:[Helper lastSecondOfTheDay:[NSDate date]]];
    [promoCoupon whereKey:@"maxRedemptions" greaterThan:[NSNumber numberWithInt:0]];

    
    NSArray *list=[[NSArray alloc] initWithObjects:[UserProfileInformation sharedMySingleton].user.objectId,nil];
    [promoCoupon whereKey:@"usedBy" notContainedIn:list];
    
    
    return promoCoupon;
}

- (PFQuery *)getReferredCouponsQuery:(NSString *)couponName {
    PFQuery *referredCoupon = [PFQuery queryWithClassName:COUPON_TABLE];
    [referredCoupon whereKey:@"name" equalTo: couponName];
    
    [referredCoupon whereKey:@"user" notEqualTo: [UserProfileInformation sharedMySingleton].user];
    [referredCoupon whereKeyDoesNotExist:@"admin"];
    
    return referredCoupon;
}

- (PFQuery *)getMyCouponCodeForReferringFriendsQuery {
    PFQuery *couponEarned = [PFQuery queryWithClassName:COUPON_TABLE];
    [couponEarned whereKey:@"user" equalTo: [UserProfileInformation sharedMySingleton].user];
    [couponEarned whereKey:@"active" equalTo: [NSNumber numberWithBool:YES]];
    return couponEarned;
}

- (void)getReferralCoupon:(NSString *)couponName withCompletionBlock:(void (^)(PFObject *object, NSError *error)) completionBlock {
    PFQuery *referralCouponQuery = [self getReferredCouponsQuery:couponName];
    
    [referralCouponQuery getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        if (!error) {
            if (object != nil) {
                NSLog(@"coupouns are %@", object);
            }
            completionBlock (object, nil);
        }
        else {
            completionBlock (nil, error);
        }
    }];
}

- (void)getEarnedPromoAndAdminCoupons:(NSString *)couponName WithCompletionBlock:(void (^)(PFObject *coupon, NSError *error)) completionBlock {
    
    PFQuery *earnedCoupon = [self getCouponsEarnedQuery:(NSString *)couponName showAll:NO];
    
    PFQuery *adminCoupon = [self getAdminCouponsQuery:(NSString *)couponName showAll:NO];
    
    PFQuery *promoCoupon = [self gePromotionalCouponsQuery:(NSString *)couponName showAll:NO];
    
    PFQuery *getMyCouponQuery = [PFQuery orQueryWithSubqueries:@[earnedCoupon, adminCoupon, promoCoupon]];
    
    [getMyCouponQuery getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        if (!error && object) {
            completionBlock (object, nil);
        }
        else {
            completionBlock (nil, error);
            NSLog(@"error getting earned & admin coupons from DB %@", error);
        }
    }];
    
}


- (void)getMyCouponCodeForReferringFriendsWithCompletionBlock:(void (^)(NSString *couponCode, NSError *error)) completionBlock  {
    PFQuery *couponCodeQuery = [self getMyCouponCodeForReferringFriendsQuery];
    [couponCodeQuery getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        if (!error && object) {
            _couponName = object[@"name"];
            completionBlock (object[@"name"], nil);
        }
        else {
            completionBlock (nil, error);
        }
    }];
}



- (void)getReferralEarnedPromoAndAdminCoupons:(NSString *)couponName WithCompletionBlock:(void (^)(PFObject *coupon, NSError *error)) completionBlock {
    
    PFQuery *referralCoupon = [self getReferredCouponsQuery:couponName];
    
    PFQuery *earnedCoupon = [self getCouponsEarnedQuery:(NSString *)couponName showAll:NO];
    
    PFQuery *adminCoupon = [self getAdminCouponsQuery:(NSString *)couponName showAll:NO];
    
    PFQuery *promoCoupon = [self gePromotionalCouponsQuery:(NSString *)couponName showAll:NO];
    
    PFQuery *getMyCouponQuery = [PFQuery orQueryWithSubqueries:@[referralCoupon, earnedCoupon, adminCoupon, promoCoupon]];
    
    [getMyCouponQuery getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        if (!error && object) {
            completionBlock (object, nil);
        }
        else {
            completionBlock (nil, error);
        }
    }];
}



- (void)findMyEarnedPromoAndAdminCoupons:(BOOL)showValidCoupons WithCompletionBlock:(void (^)(NSArray *coupons, NSError *error)) completionBlock {
    
    PFQuery *earnedCoupon = [self getCouponsEarnedQuery:nil showAll:!showValidCoupons];
    
    PFQuery *adminCoupon = [self getAdminCouponsQuery:nil showAll:!showValidCoupons];
    
    PFQuery *promoCoupon = [self gePromotionalCouponsQuery:nil showAll:!showValidCoupons];
    
    PFQuery *getMyCouponQuery = [PFQuery orQueryWithSubqueries:@[earnedCoupon, adminCoupon, promoCoupon]];
    
    [getMyCouponQuery findObjectsInBackgroundWithBlock:^(NSArray *coupons, NSError *error) {
        if (!error) {
            NSMutableArray *arrCoupons;
            if (coupons.count > 0) {
                arrCoupons = [NSMutableArray new];
                
                Coupon *coupon;
                for (PFObject *couponObj in coupons) {
                    coupon = [Coupon new];
                    coupon.couponName = couponObj[@"name"];
                    coupon.couponAmount = couponObj[@"amount"];
                    coupon.couponType = couponObj[@"type"];
                    if (couponObj[@"user"]) {
                        if (couponObj[@"unusedCoupons"]) {
                            coupon.numOfCoupons = [NSString stringWithFormat:@"%d",[couponObj[@"unusedCoupons"] intValue]];
                        }
                        else {
                            coupon.numOfCoupons = @"0";
                        }
                    }
                    else if (couponObj[@"admin"]){
                        if (couponObj[@"maxRedemptions"]) {
                            coupon.numOfCoupons = [NSString stringWithFormat:@"%d",[couponObj[@"maxRedemptions"] intValue]];
                        }
                        else {
                            coupon.numOfCoupons = @"0";
                        }
                    }
                    else {
                        coupon.numOfCoupons = @"1";
                    }
                    [arrCoupons addObject:coupon];
                }
            }
            completionBlock (arrCoupons, nil);
        }
        else {
            completionBlock (nil, error);
        }
    }];
    
}


@end
