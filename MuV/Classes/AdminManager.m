//
//  AdminManager.m
//  MuV
//
//  Created by SushmaN on 3/10/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "AdminManager.h"


@implementation AdminManager

#pragma mark Singleton

+ (instancetype)sharedInstance {
    
    static dispatch_once_t dispatchOnce;
    static AdminManager *sharedInstance = nil;
    
    dispatch_once(&dispatchOnce, ^{
        sharedInstance = [[self alloc]init];
        sharedInstance.adminSettings = [[AdminSettings alloc]init];
    });
    
    return sharedInstance;
}

- (void)getAdminSettingsWithCompletionBlock: (void (^)(AdminSettings *adminSetting, NSError *error))completionBlock {
    [self getAdminSettingRecordWithCompletionBlock:^(PFObject *adminSettingObj, NSError *error) {
        if (adminSettingObj) {
            AdminSettings *adminSettings = [AdminSettings new];
            adminSettings.userPromoAmount = adminSettingObj[@"userPromoAmount"];
            adminSettings.costPerMile = adminSettingObj[@"costPerMile"];
            adminSettings.driverPaymentPercentage = adminSettingObj[@"driverPaymentPercentage"];
            adminSettings.hoursBeforeArrivalForFee = adminSettingObj[@"hoursBeforeArrivalForFee"];
            adminSettings.hoursBeforeDepartureForFee = adminSettingObj[@"hoursBeforeDepartureForFee"];
            adminSettings.cancellationFeeAmount = adminSettingObj[@"cancellationFeeAmount"];
            adminSettings.numberOfRecentLocations = adminSettingObj[@"numberOfRecentLocations"];
            adminSettings.geoFenceInMiles = adminSettingObj[@"geoFenceInMiles"];
            adminSettings.currentLocationPingIntervalInSeconds = adminSettingObj[@"currentLocationPingIntervalInSeconds"];
            adminSettings.adminEmailAddress = adminSettingObj[@"adminEmailAddress"];
            _adminSettings = adminSettings;
            completionBlock (adminSettings, nil);
        }
        else {
           
            if (error.code == -100 || error.code == -1005 || error.code == -1009) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please check your network connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
                 completionBlock (nil, error);
            }
        }
    }];
}


- (void)getAdminSettingRecordWithCompletionBlock:(void (^)(PFObject *object, NSError *error))completionBlock {
    
    PFQuery *query = [PFQuery queryWithClassName:@"adminSettings"];
    
    [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        if (!error) {
            completionBlock (object, nil);
        }
        else {
            NSLog(@"error is  %ld", (long)error.code);
            completionBlock (nil, error);
        }
    }];
    
}

@end

