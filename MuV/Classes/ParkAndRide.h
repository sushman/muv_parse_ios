//
//  ParkAndRide.h
//  MuV
//
//  Created by SushmaN on 4/10/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface ParkAndRide : NSObject

@property (nonatomic, strong) NSString *arrivalTime;
@property (nonatomic, strong) NSString *departureTime;
@property (nonatomic, strong) NSString *date;
@property (nonatomic, assign) BOOL isOneWay;
@property (nonatomic, strong) NSString *dropOffAddress;
@property (nonatomic, strong) CLLocation *dropOffLocation;
@property (nonatomic, strong) CLLocation *pickUpLocation;

@end
