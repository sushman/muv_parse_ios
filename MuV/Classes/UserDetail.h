//
//  UserDetail.h
//  MuV
//
//  Created by SushmaN on 1/23/15
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>

@interface UserDetail : NSObject

@property (nonatomic, strong) PFObject *userId;
@property (nonatomic, strong) NSString *userFirstName;
@property (nonatomic, strong) NSString *userLastName;
@property (nonatomic, strong) NSString *displayName;
@property (nonatomic, strong) PFFile *profileImage;
@property (nonatomic, strong) NSData *image;
@property (nonatomic, strong) NSString *startLocation;
@property (nonatomic, strong) NSString *endLocation;
@property (nonatomic, strong) NSString *startAddress;
@property (nonatomic, strong) NSString *endAddress;
@property (nonatomic, strong) NSDate *startTime;
@property (nonatomic, strong) NSDate *endTime;
//@property (nonatomic, strong) NSDate *pickUpTime;
//@property (nonatomic, strong) NSDate *dropOffTime;

@property (nonatomic, assign) BOOL isNextInLineForPickUp;
@property (nonatomic, assign) BOOL isNextInLineForDropOff;
@property (nonatomic, assign) BOOL isNoShow;
@property (nonatomic, assign) BOOL isPickedUp;
@property (nonatomic, assign) BOOL isDroppedOff;
@property (nonatomic, assign) NSInteger rank;

@property (nonatomic, strong) PFObject *rideObj;

- (UserDetail *)createUserDetail:(PFObject *) userObject
                withStartAddress:(NSString *)startAddress
                   andEndAddress:(NSString *)endAddress
                andStartLocation:(NSString *)startLocation
                  andEndLocation:(NSString *)endLocation
                   andRideObject:(PFObject *)rideObject
                    andStartTime:(NSDate *)startTime
                      andEndTime:(NSDate *)endTime;

@end
