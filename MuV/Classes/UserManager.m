//
//  User.m
//  MuV
//
//  Created by SushmaN on 1/19/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "UserManager.h"

@implementation UserManager



-(BOOL)isDriver {
//    PFUser *user = [PFUser currentUser];
//    NSLog(@"user: %@", [[PFUser currentUser] driver]);
//
//    NSNumber *driver = user.dr;
//    bool isDriver = [driver boolValue];
//    
//    if (isDriver) return YES;
    return YES;
}

-(PFObject *)hasDriverAbilities {
    PFUser *user = [PFUser currentUser];
    PFObject *driverDetail = user[@"driverdetail"];
    [driverDetail fetchIfNeeded];
    return driverDetail;
}

@end
