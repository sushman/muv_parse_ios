//
//  CouponManager.h
//  MuV
//
//  Created by SushmaN on 2/23/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>
#import "Coupon.h"

@interface CouponManager : NSObject

@property (strong, nonatomic) NSString *couponName;

+ (instancetype)sharedInstance;

- (void)createCouponCodeRecordforReferralForAmount:(NSNumber *) amount;
- (void)validateAndGetCoupon:(NSString *)couponName withCompletionBlock:(void (^)(Coupon *coupon, NSError *error)) completionBlock;
- (void)findMyEarnedPromoAndAdminCoupons:(BOOL)showValidCoupons WithCompletionBlock:(void (^)(NSArray *coupons, NSError *error)) completionBlock;
- (void)getMyCouponCodeForReferringFriendsWithCompletionBlock:(void (^)(NSString *couponCode, NSError *error)) completionBlock;
- (void)applyCoupon:(PFObject *)coupon andCouponAmount:(NSNumber *)couponAmount withCompletionBlock:(void (^)(PFObject *coupon, NSError *error))completionBlock;


@end
