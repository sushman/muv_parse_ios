//
//  LocationManager.h
//  MuV
//
//  Created by SushmaN on 1/14/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GoogleMaps/GoogleMaps.h>

@interface LocationManager : NSObject

@property (strong, nonatomic) NSString *currentLocation;

+ (instancetype)sharedInstance;
- (CLLocation *)getCurrentLocation;

@end
