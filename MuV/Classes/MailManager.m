//
//  MailManager.m
//  MuV
//
//  Created by Hing Huynh on 2/5/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "MailManager.h"
#import <AFNetworking/AFNetworking.h>
#import <Parse/Parse.h>
#import "AdminManager.h"

#define ADMIN_EMAIL @"bookings@muv2work.com"

@implementation MailManager

AdminManager *adminManager;
NSString *adminEmail = ADMIN_EMAIL;

- (void) getAdminEmail {
   
    AdminManager *adminManager = [AdminManager sharedInstance];
    if (!adminManager.adminSettings.adminEmailAddress) {
        [adminManager getAdminSettingsWithCompletionBlock:^(AdminSettings *adminSetting, NSError *error) {
            if (adminSetting) {
                adminEmail = adminManager.adminSettings.adminEmailAddress;
            }
        }];
    }
    else {
        adminEmail = adminManager.adminSettings.adminEmailAddress;
    }
}

// DEFAULT MAILING METHOD
- (void)sendEmailsWithSubject:(NSString *)subject AndTemplate:(NSString *)template AndVariables:(NSDictionary *)variables To:(NSMutableArray *)recipients withCompletionBlock:(void (^)(BOOL succeeded, NSError *error))completionBlock {
    
    NSArray *recipientsArray = [self setupRecipients:recipients];
    NSArray *mergeVars = [self setupVars:variables ForRecipients:recipients];
//    NSLog(@"recipients: %@", recipientsArray);
//    NSLog(@"mergeVars: %@", mergeVars);
//    NSLog(@"subject %@", subject);
//    NSLog(@"template %@", template);
    
    [self getAdminEmail];
  
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    AFHTTPResponseSerializer *serializer = [AFHTTPResponseSerializer serializer];
    
    manager.responseSerializer = serializer;
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    NSString *urlString = @"https://mandrillapp.com/api/1.0/messages/send-template.json";
    
    [manager POST:urlString parameters:@{
                                         @"key": @"pmUQBGN8VMzJqfAJ5rpBmQ",
                                         @"template_name": template,
                                         @"template_content": @[
                                                 @{
                                                     }
                                                 ],
                                         @"message": @{
                                                 @"subject": subject,
                                                 @"from_email": adminEmail,
                                                 @"from_name": @"MUV Technologies, Inc",
                                                 @"to": recipientsArray,
                                                 @"headers": @{
                                                         @"Reply-To": adminEmail
                                                         },
                                                 @"important": @YES,
                                                 @"merge": @YES,
                                                 @"merge_language": @"mailchimp",
                                                 @"merge_vars": mergeVars,
                                                 @"attachments": @[
                                                         @{}
                                                         ]
                                                 },
                                         @"async": @NO,
                                         @"ip_pool": @"Main Pool"
                                         }          success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                             
                                             completionBlock(YES, nil);
                                         }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"error: %@", error);
              
              completionBlock(NO, error);
              
          }];
    
}

// CUSTOM MAILING METHOD WITH CUSTOM RECIPIENTS

- (void)sendEmailsWithSubject:(NSString *)subject AndTemplate:(NSString *)template AndVariables:(NSDictionary *)variables ToCustomName:(NSMutableArray *)recipients withCompletionBlock:(void (^)(BOOL succeeded, NSError *error))completionBlock {
    
    NSArray *recipientsArray = [self setupRecipients:recipients];
    NSArray *mergeVars = [self setupCustomVars:variables WithCustomNamesForRecipients:recipients];
//    NSLog(@"recipients: %@", recipientsArray);
//    NSLog(@"mergeVars: %@", mergeVars);
    
     [self getAdminEmail];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    AFHTTPResponseSerializer *serializer = [AFHTTPResponseSerializer serializer];
    
    manager.responseSerializer = serializer;
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    NSString *urlString = @"https://mandrillapp.com/api/1.0/messages/send-template.json";
    
    [manager POST:urlString parameters:@{
                                         @"key": @"pmUQBGN8VMzJqfAJ5rpBmQ",
                                         @"template_name": template,
                                         @"template_content": @[
                                                 @{
                                                     }
                                                 ],
                                         @"message": @{
                                                 @"subject": subject,
                                                 @"from_email": adminEmail,
                                                 @"from_name": @"MUV Technologies, Inc",
                                                 @"to": recipientsArray,
                                                 @"headers": @{
                                                         @"Reply-To": adminEmail
                                                         },
                                                 @"important": @YES,
                                                 @"merge": @YES,
                                                 @"merge_language": @"mailchimp",
                                                 @"merge_vars": mergeVars,
                                                 @"attachments": @[
                                                         @{}
                                                         ]
                                                 },
                                         @"async": @NO,
                                         @"ip_pool": @"Main Pool"
                                         }          success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                             
                                             NSError* error = nil;
                                             NSArray* json = [NSJSONSerialization
                                                              JSONObjectWithData:responseObject
                                                              
                                                              options:kNilOptions
                                                              error:&error];
                                            // NSLog(@"json data = %@",json);
                                             
                                             completionBlock(YES, nil);
                                         }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"error: %@", error);
              
              completionBlock(NO, error);
              
          }];
    
}

// CUSTOM MAILING METHOD TO SEND TO ADMIN

- (void)sendEmailsWithSubject:(NSString *)subject AndTemplate:(NSString *)template AndVariables:(NSDictionary *)variables ToAdminWithCompletionBlock:(void (^)(BOOL succeeded, NSError *error))completionBlock {
    
     [self getAdminEmail];
    // SEND TO ADMIN
    NSArray *recipientsArray = @[@{@"email":   adminEmail,
                                   @"name":    @"MuV Admin",
                                   @"type":    @"to"}];
    NSArray *mergeVars = [self setupVarsForAdmin:variables];
    NSLog(@"recipients: %@", recipientsArray);
    NSLog(@"mergeVars: %@", mergeVars);
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    AFHTTPResponseSerializer *serializer = [AFHTTPResponseSerializer serializer];
    
    manager.responseSerializer = serializer;
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    NSString *urlString = @"https://mandrillapp.com/api/1.0/messages/send-template.json";
    
    [manager POST:urlString parameters:@{
                                         @"key": @"pmUQBGN8VMzJqfAJ5rpBmQ",
                                         @"template_name": template,
                                         @"template_content": @[
                                                 @{
                                                     }
                                                 ],
                                         @"message": @{
                                                 @"subject": subject,
                                                 @"from_email": adminEmail,
                                                 @"from_name": @"MUV Technologies, Inc",
                                                 @"to": recipientsArray,
                                                 @"headers": @{
                                                         @"Reply-To": adminEmail
                                                         },
                                                 @"important": @YES,
                                                 @"merge": @YES,
                                                 @"merge_language": @"mailchimp",
                                                 @"merge_vars": mergeVars,
                                                 @"attachments": @[
                                                         @{}
                                                         ]
                                                 },
                                         @"async": @NO,
                                         @"ip_pool": @"Main Pool"
                                         }          success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                             
                                             NSError* error = nil;
                                             NSArray* json = [NSJSONSerialization
                                                              JSONObjectWithData:responseObject
                                                              
                                                              options:kNilOptions
                                                              error:&error];
                                            // NSLog(@"json data = %@",json);
                                             
                                             completionBlock(YES, nil);
                                         }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"error: %@", error);
              
              completionBlock(NO, error);
              
          }];
    
}

- (NSArray *)setupVarsForAdmin:(NSDictionary *)vars  {
    
    NSMutableArray *mergeVars = [NSMutableArray new];
    NSMutableArray *varsArray = [NSMutableArray new];
    
    [self getAdminEmail];
    for(id key in vars) {
        
        [varsArray addObject:
         @{@"name":    key,
           @"content":    [vars objectForKey:key]}];
    }
    [mergeVars addObject:@{@"rcpt":   adminEmail,
                           @"vars":[varsArray mutableCopy]}];
    
    
    return [mergeVars mutableCopy];
}

- (NSArray *)setupVars:(NSDictionary *)vars ForRecipients: (NSMutableArray *)recipients {
    
    NSMutableArray *mergeVars = [NSMutableArray new];
    if ([recipients[0] isKindOfClass:[PFUser class]]){
        for (PFUser *user in recipients){
            
            NSMutableArray *varsArray = [NSMutableArray new];
            
            for(id key in vars) {
                
                [varsArray addObject:
                 @{@"name":    key,
                   @"content":    [vars objectForKey:key]}];
            }
            
            [mergeVars addObject:@{@"rcpt":   user.email,
                                   @"vars":[varsArray mutableCopy]}];
        }
    } else {
        
    }
    
    return [mergeVars mutableCopy];
}

- (NSArray *)setupRecipients:(NSMutableArray *)recipients {
    NSMutableArray *recipientsArray = [NSMutableArray new];
    if ([recipients[0] isKindOfClass:[PFUser class]]){
        for (PFUser *user in recipients){
            [recipientsArray addObject:@{@"email":   user.email,
                                         @"name":    user[@"firstName"],
                                         @"type":    @"to"}];
        }
    } else {
        for (NSDictionary *user in recipients){
            [recipientsArray addObject:user];
        }
    }
    
    return [NSArray arrayWithArray:recipientsArray];
}

- (NSArray *)setupCustomVars:(NSDictionary *)vars WithCustomNamesForRecipients: (NSMutableArray *)recipients {
    
    NSMutableArray *mergeVars = [NSMutableArray new];
    if ([recipients[0] isKindOfClass:[PFUser class]]){
        for (PFUser *user in recipients){
            
            NSMutableArray *varsArray = [NSMutableArray new];
            
            [varsArray addObject:
             @{@"name":    @"fname",
               @"content": user[@"firstName"]}];
            
            for(id key in vars) {
                
                [varsArray addObject:
                 @{@"name":    key,
                   @"content":    [vars objectForKey:key]}];
            }
            
            [mergeVars addObject:@{@"rcpt":   user.email,
                                   @"vars":[varsArray mutableCopy]}];
        }
    } else {
        
    }
    
    return [mergeVars mutableCopy];
}


// EMAIL TEMPLATES !!!

- (void) sendDriverCancelsAdminAlertWithRoute:(PFObject *)route  {
    
    NSDictionary *variablesDict = [NSDictionary new];
    variablesDict = @{@"ridenumber":route.objectId};
    
     [self getAdminEmail];
    NSMutableArray *recipients = [NSMutableArray new];
    
    [recipients addObject:@{@"email":   adminEmail,
                            @"name":    @"Admin",
                            @"type":    @"to"}];
    
    [self sendEmailsWithSubject:@"MüV Driver Cancellation" AndTemplate:@"Driver cancels Admin alert" AndVariables:variablesDict ToAdminWithCompletionBlock:^(BOOL succeeded, NSError *error) {
        if (succeeded) {
            NSLog(@"Success  admin alert");
        }
        else {
            NSLog(@"error %@", error);
        }
    }];
}


- (void) sendDriverNotificationForRiderCancelationWithRide:(PFObject *)ride  {
    [ride[@"driver"] fetchIfNeededInBackgroundWithBlock:^(PFObject *driver, NSError *error){
        [driver[@"userdetail"] fetchIfNeededInBackgroundWithBlock:^(PFObject *driverUser, NSError *error){
            
          //  NSLog(@"error: %@", error);
           // NSLog(@"driver: %@", driver);
            
            NSString *driverName = driverUser[@"firstName"];
            NSString *riderName = [UserProfileInformation sharedMySingleton].user[@"firstName"];
            
            NSLog(@"vars: %@", driverName);
            NSLog(@"vars: %@", riderName);
            
            NSDate *date = ride[@"rideDate"];
            NSDateFormatter *formatter = [NSDateFormatter new];
            [formatter setDateFormat:@"MM/dd/yyyy"];
            NSString *stringFromDate = [formatter stringFromDate:date];
            
            NSDictionary *variablesDict = [NSDictionary new];
            variablesDict = @{@"fname":driverName,
                              @"ridername":riderName,
                              @"date": stringFromDate
                              };
            
            NSMutableArray *recipients = [NSMutableArray new];
            [recipients addObject:driverUser];
            
            [self sendEmailsWithSubject:@"MüV Passenger Ride Cancellation" AndTemplate:@"Driver notification for rider cancellation" AndVariables:variablesDict To:recipients withCompletionBlock:^(BOOL succeeded, NSError *error){
            }];
        }];
    }];
}

- (void)sendDriverNotificationForRiderCancelationWithRides:(PFObject *)arrivalRide
                                             DepartureRide:(PFObject *)departureRide
                                                 isArrival:(BOOL)isArrival
                                               isDeparture:(BOOL)isDeparture {
    
    if (isArrival && isDeparture) {
        [arrivalRide[@"driver"] fetchIfNeededInBackgroundWithBlock:^(PFObject *arrivalDriver, NSError *error) {
            [departureRide[@"driver"] fetchIfNeededInBackgroundWithBlock:^(PFObject *departureDriver, NSError *error) {
                if ([arrivalDriver isEqual:departureDriver]) {
                    [arrivalDriver[@"userdetail"] fetchIfNeededInBackgroundWithBlock:^(PFObject *driverUserDetails, NSError *error) {
                        [self sendEmailToDriverOnPassengerBooking:arrivalDriver andDriverUserDetails:driverUserDetails andRide:arrivalRide withTripDetails:@"Round Trip"];
                    }];
                }
                else {
                    [arrivalDriver[@"userdetail"] fetchIfNeededInBackgroundWithBlock:^(PFObject *driverUserDetails, NSError *error) {
                        [self sendEmailToDriverOnPassengerBooking:arrivalDriver andDriverUserDetails:driverUserDetails andRide:arrivalRide withTripDetails:@"One Way Arrival"];
                    }];
                    
                    [departureDriver[@"userdetail"] fetchIfNeededInBackgroundWithBlock:^(PFObject *driverUserDetails, NSError *error) {
                        [self sendEmailToDriverOnPassengerBooking:departureDriver andDriverUserDetails:driverUserDetails andRide:departureRide withTripDetails:@"One Way Departure"];
                    }];
                }
            }];
        }];
    }
    else if (isArrival) {
        [arrivalRide[@"driver"] fetchIfNeededInBackgroundWithBlock:^(PFObject *arrivalDriver, NSError *error) {
            [arrivalDriver[@"userdetail"] fetchIfNeededInBackgroundWithBlock:^(PFObject *driverUserDetails, NSError *error) {
                [self sendEmailToDriverOnPassengerBooking:arrivalDriver andDriverUserDetails:driverUserDetails andRide:arrivalRide withTripDetails:@"One Way Arrival"];
            }];
        }];
    }
    else if (isDeparture) {
        [departureRide[@"driver"] fetchIfNeededInBackgroundWithBlock:^(PFObject *departureDriver, NSError *error) {
            [departureDriver[@"userdetail"] fetchIfNeededInBackgroundWithBlock:^(PFObject *driverUserDetails, NSError *error) {
                [self sendEmailToDriverOnPassengerBooking:departureDriver andDriverUserDetails:driverUserDetails andRide:departureRide withTripDetails:@"One Way Departure"];
            }];
        }];
    }
    else {
        NSLog(@"Information missing");
    }
}

- (void)sendEmailToDriverOnPassengerBooking:(PFObject *)driver
                       andDriverUserDetails:(PFObject *)driverUserDetails
                                    andRide:(PFObject *)ride
                            withTripDetails:(NSString *)tripDetail {
   // NSLog(@"driver: %@", driver);
    
    NSString *driverName = driverUserDetails[@"firstName"];
    NSString *riderName = [UserProfileInformation sharedMySingleton].user[@"firstName"];
    
    NSLog(@"vars: %@", driverName);
    NSLog(@"vars: %@", riderName);
    
    NSDate *date = ride[@"rideDate"];
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"MM/dd/yyyy"];
    NSString *stringFromDate = [formatter stringFromDate:date];
    
    NSDictionary *variablesDict = [NSDictionary new];
    variablesDict = @{@"drivername":driverName,
                      @"ridername":riderName,
                      @"tripDetails":tripDetail,
                      @"date": stringFromDate
                      };
    
    NSMutableArray *recipients = [NSMutableArray new];
    [recipients addObject:driverUserDetails];
    
    [self sendEmailsWithSubject:@"MüV Passenger Ride Booked" AndTemplate:@"Rider Books Drivers Muv" AndVariables:variablesDict To:recipients withCompletionBlock:^(BOOL succeeded, NSError *error){
    }];
}

- (void) sendDriverSchedulesMUVConfirmationWithRoute:(PFObject *)route isArrive:(BOOL)arrival isDepart:(BOOL)departure {
    
    NSDate *date = route[@"rideDate"];
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"MM/dd/yyyy"];
    NSString *stringFromDate = [formatter stringFromDate:date];
    
    NSString *firstName = [UserProfileInformation sharedMySingleton].user[@"firstName"];
    
    NSString *tripDetails = [NSString new];
    if (arrival && departure) {
        
        tripDetails = @"Round Trip";
        
    } else if (arrival && !departure) {
        
        tripDetails = @"One Way to Work";
        
    } else if (!arrival && departure) {
        
        tripDetails = @"One Way Departing from Work";
        
    } else { // not servicing area yet
        
    }
    
    NSDictionary *variablesDict = [NSDictionary new];
    variablesDict = @{@"fname":firstName,
                      @"tripdetails": tripDetails,
                      @"date": stringFromDate
                      };
    
    NSMutableArray *recipients = [NSMutableArray new];
    [recipients addObject:[UserProfileInformation sharedMySingleton].user];
    
    [self sendEmailsWithSubject:@"MüV Ride Confirmation" AndTemplate:@"Driver schedules MUV : Confirmation" AndVariables:variablesDict To:recipients withCompletionBlock:^(BOOL succeeded, NSError *error){
    }];
}

//
// Friend Invitation should be sent by friend not by MUV
//

- (void)sendRideConfirmationForRiderWithRide:(PFObject *)ride
                                     isArrive:(BOOL)arrival
                                     isDepart:(BOOL)departure
                          withCompletionBlock:(void (^)(BOOL succeeded, NSError *error))completionBlock {
    
    NSDate *date = ride[@"rideDate"];
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"MM/dd/yyyy"];
    NSString *stringFromDate = [formatter stringFromDate:date];
    
    NSLog(@"1:%@",stringFromDate);
    
    NSString *firstName = [UserProfileInformation sharedMySingleton].user[@"firstName"];
    
    NSString *tripDetails = [NSString new];
    if (arrival && departure) {
        
        tripDetails = @"Round Trip";
        
    } else if (arrival && !departure) {
        
        tripDetails = @"One Way to Work";
        
    } else if (!arrival && departure) {
        
        tripDetails = @"One Way Departing from Work";
        
    } else {
        
    }
    
    NSLog(@"2:%@",tripDetails);
    NSLog(@"3:%@", firstName);
    
    NSDictionary *variablesDict = [NSDictionary new];
    variablesDict = @{@"fname":firstName,
                      @"tripdetails": tripDetails,
                      @"date": stringFromDate
                      };
    
    NSMutableArray *recipients = [NSMutableArray new];
    [recipients addObject:[UserProfileInformation sharedMySingleton].user];
    
    [self sendEmailsWithSubject:@"MüV Ride Confirmation" AndTemplate:@"Ride booking confirmation (for rider)" AndVariables:variablesDict To:recipients withCompletionBlock:^(BOOL succeeded, NSError *error) {
        if (succeeded) {
            completionBlock (YES, nil);
        }
        else {
            completionBlock (NO, error);
        }
    }];
}

- (void) sendRideCancelingByDriver1WithRoute:(PFObject *)route {
    NSDictionary *variablesDict = [NSDictionary new];
    
    NSDate *date = route[@"rideDate"];
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"MM/dd/yyyy"];
    NSString *stringFromDate = [formatter stringFromDate:date];
    
    NSString *firstName = [UserProfileInformation sharedMySingleton].user[@"firstName"];
    
    variablesDict = @{@"fname":firstName,
                      @"date": stringFromDate
                      };
    NSMutableArray *recipients = [NSMutableArray new];
    [recipients addObject:[UserProfileInformation sharedMySingleton].user];
    
    [self sendEmailsWithSubject:@"MüV Ride Cancellation" AndTemplate:@"Ride cancellation (by driver) : 1" AndVariables:variablesDict To:recipients withCompletionBlock:^(BOOL succeeded, NSError *error){
    }];
}

- (void) sendRideCancelingByDriver2WithRoute:(PFObject *)route {
    NSDictionary *variablesDict = [NSDictionary new];
    
    NSDate *date = route[@"rideDate"];
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"MM/dd/yyyy"];
    NSString *stringFromDate = [formatter stringFromDate:date];
    NSString *firstName = [UserProfileInformation sharedMySingleton].user[@"firstName"];
    variablesDict = @{@"fname":firstName,
                      @"date": stringFromDate
                      };
    NSMutableArray *recipients = [NSMutableArray new];
    [recipients addObject:[UserProfileInformation sharedMySingleton].user];
    
    [self sendEmailsWithSubject:@"MüV Ride Cancellation" AndTemplate:@"Ride cancellation (by driver) : 2" AndVariables:variablesDict To:recipients withCompletionBlock:^(BOOL succeeded, NSError *error){
    }];
}

- (void) sendRideCancelingByDriver3WithRoute:(PFObject *)route  {
    
    NSDictionary *variablesDict = [NSDictionary new];
    
    NSDate *date = route[@"rideDate"];
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"MM/dd/yyyy"];
    NSString *stringFromDate = [formatter stringFromDate:date];
    NSString *firstName = [UserProfileInformation sharedMySingleton].user[@"firstName"];
    variablesDict = @{@"fname":firstName,
                      @"date": stringFromDate
                      };
    NSMutableArray *recipients = [NSMutableArray new];
    [recipients addObject:[UserProfileInformation sharedMySingleton].user];
    
    [self sendEmailsWithSubject:@"MüV Ride Cancellation" AndTemplate:@"Ride cancellation (by driver) : 3" AndVariables:variablesDict To:recipients withCompletionBlock:^(BOOL succeeded, NSError *error){
    }];
}

- (void) sendRiderCancelsMuVGetsRefundWithRide:(PFObject *)ride {
    NSDictionary *variablesDict = [NSDictionary new];
    
    NSDate *date = ride[@"rideDate"];
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"MM/dd/yyyy"];
    NSString *stringFromDate = [formatter stringFromDate:date];
    
    NSString *firstName = [UserProfileInformation sharedMySingleton].user[@"firstName"];
    
    variablesDict = @{@"fname":firstName,
                      @"date": stringFromDate
                      };
    NSMutableArray *recipients = [NSMutableArray new];
    [recipients addObject:[UserProfileInformation sharedMySingleton].user];
    
    [self sendEmailsWithSubject:@"MüV Ride Cancellation" AndTemplate:@"Rider cancels MUV - gets refund" AndVariables:variablesDict To:recipients withCompletionBlock:^(BOOL succeeded, NSError *error){
    }];
}

- (void) sendRiderCancelsMuVDoesNotGetRefundWithRide:(PFObject *)ride {
    NSDictionary *variablesDict = [NSDictionary new];
    
    NSDate *date = ride[@"rideDate"];
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"MM/dd/yyyy"];
    NSString *stringFromDate = [formatter stringFromDate:date];
    
    NSString *firstName = [UserProfileInformation sharedMySingleton].user[@"firstName"];
    
    variablesDict = @{@"fname":firstName,
                      @"date": stringFromDate
                      };
    NSMutableArray *recipients = [NSMutableArray new];
    [recipients addObject:[UserProfileInformation sharedMySingleton].user];
    
    [self sendEmailsWithSubject:@"MüV Ride Cancellation" AndTemplate:@"Rider cancels MUV - Does not get refund (copy 01)" AndVariables:variablesDict To:recipients withCompletionBlock:^(BOOL succeeded, NSError *error){
    }];
}

- (void) sendRiderCancelsMuVGetsChargedFiveWithRide:(PFObject *)ride {
    NSDictionary *variablesDict = [NSDictionary new];
    
    NSDate *date = ride[@"rideDate"];
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"MM/dd/yyyy"];
    NSString *stringFromDate = [formatter stringFromDate:date];
    
    NSString *firstName = [UserProfileInformation sharedMySingleton].user[@"firstName"];
    
    variablesDict = @{@"fname":firstName,
                      @"date": stringFromDate
                      };
    NSMutableArray *recipients = [NSMutableArray new];
    [recipients addObject:[UserProfileInformation sharedMySingleton].user];
    
    [self sendEmailsWithSubject:@"MüV Ride Cancellation" AndTemplate:@"Rider cancels MUV - is charged $ 5 fee" AndVariables:variablesDict To:recipients withCompletionBlock:^(BOOL succeeded, NSError *error){
    }];
}


- (void) sendRiderNoShowWithRide:(PFObject *)ride {
    
    NSDate *date = ride[@"rideDate"];
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"MM/dd/yyyy"];
    NSString *stringFromDate = [formatter stringFromDate:date];
    
    [ride[@"rider"]fetchIfNeededInBackgroundWithBlock:^(PFObject *rider, NSError *error){
        NSString *firstName = rider[@"firstName"];
        
        NSDictionary *variablesDict = [NSDictionary new];
        
        variablesDict = @{@"fname":firstName,
                          @"date": stringFromDate
                          };
        NSMutableArray *recipients = [NSMutableArray new];
        [recipients addObject:rider];
        
        [self sendEmailsWithSubject:@"MüV Ride Cancellation" AndTemplate:@"Rider No Show" AndVariables:variablesDict To:recipients withCompletionBlock:^(BOOL succeeded, NSError *error){
        }];
    }];
    
    
}

- (void) sendRiderNotificationWhenDriverCancelsWithRoute:(PFObject *)route {
    
    NSDate *date = route[@"rideDate"];
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"MM/dd/yyyy"];
    NSString *stringFromDate = [formatter stringFromDate:date];
    
    [route[@"driver"] fetchIfNeededInBackgroundWithBlock:^(PFObject *driver, NSError *error){
        [driver[@"userdetail"] fetchIfNeededInBackgroundWithBlock:^(PFObject *driverUser, NSError *error){
            
            NSDictionary *variablesDict = [NSDictionary new];
            
            variablesDict = @{@"drivername":    driverUser[@"firstName"],
                              @"date": stringFromDate
                              };
            
            NSMutableArray *recipients = [NSMutableArray new];
            for (PFObject *ride in route[@"rides"]){
                
                [ride[@"rider"] fetchIfNeededInBackgroundWithBlock:^(PFObject *rider, NSError *error){
                    [recipients addObject:rider];
                }];
            }
            
            [self sendEmailsWithSubject:@"MüV Driver Cancellation" AndTemplate:@"Rider notification - Driver cancels" AndVariables:variablesDict ToCustomName:recipients withCompletionBlock:^(BOOL succeeded, NSError *error){
            }];
        }];
    }];
}

- (void) sendRiderTripCompletionRoute:(PFObject *)ride {
    
    NSDate *date = ride[@"rideDate"];
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"MM/dd/yyyy"];
    NSString *stringFromDate = [formatter stringFromDate:date];
    
    NSString *firstName = ride[@"rider"][@"firstName"];
    
    [ride[@"payment"] fetchIfNeededInBackgroundWithBlock:^(PFObject *payment, NSError *error){
        NSString *paymentAmount = [NSString stringWithFormat:@"$%@", [payment[@"AmountCharged"]stringValue]];
        
        NSDictionary *variablesDict = [NSDictionary new];
        
        variablesDict = @{@"fname":firstName,
                          @"fare": paymentAmount,
                          @"date": stringFromDate
                          };
        NSMutableArray *recipients = [NSMutableArray new];
        
        [ride[@"rider"] fetchIfNeededInBackgroundWithBlock:^(PFObject *rider, NSError *error){
            
            [recipients addObject:rider];
            
            [self sendEmailsWithSubject:@"MüV Ride Receipt" AndTemplate:@"Rider trip completion" AndVariables:variablesDict To:recipients withCompletionBlock:^(BOOL succeeded, NSError *error){
            }];
        }];
    }];
    
}

- (void) sendAdminWhenRiderWantsToBecomeADriverWithUser:(PFObject *)user {
    NSDictionary *variablesDict = [NSDictionary new];
    
    NSString *firstName = user[@"firstName"];
    NSString *email = user[@"email"];
    NSString *phone = @"";
    if (user[@"phone"]){
        phone = user[@"phone"];
    }
    
    variablesDict = @{@"rider":         firstName,
                      @"rideremail":    email,
                      @"riderdetails":  phone
                      };
    
    [self sendEmailsWithSubject:@"Rider wants to become a driver" AndTemplate:@"Rider wants to become a driver" AndVariables:variablesDict ToAdminWithCompletionBlock:^(BOOL succeeded, NSError *error){
    }];
}

- (void) sendRiderRequestsReceiptWithRide:(PFObject *)ride {
    
    NSDate *date = ride[@"rideDate"];
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"MM/dd/yyyy"];
    NSString *stringFromDate = [formatter stringFromDate:date];
    
    NSString *firstName = ride[@"rider"][@"firstName"];
    
    NSString *pickupLocation = [NSString stringWithFormat:@"%@, %@, %@, %@", ride[@"startAddress"],ride[@"startCity"],ride[@"startState"],ride[@"startZip"]];
    NSString *dropoffLocation = [NSString stringWithFormat:@"%@, %@, %@, %@", ride[@"endAddress"],ride[@"endCity"],ride[@"endState"],ride[@"endZip"]];
    
    NSLog(@"pickup: %@", pickupLocation);
    [ride[@"payment"] fetchIfNeededInBackgroundWithBlock:^(PFObject *payment, NSError *error){
        NSString *paymentAmount = [NSString stringWithFormat:@"$%@", [payment[@"AmountCharged"]stringValue]];
        

        NSDictionary *variablesDict = [NSDictionary new];
        
        variablesDict = @{@"ridername":firstName,
                          @"fare": paymentAmount,
                          @"date": stringFromDate,
                          @"pickuplocation": pickupLocation,
                          @"dropoflocation": dropoffLocation,
                              
                          @"typeoftrip": @"One Way"
                          };
        
        NSLog(@"vars: %@", variablesDict);

        NSMutableArray *recipients = [NSMutableArray new];
        
        [ride[@"rider"] fetchIfNeededInBackgroundWithBlock:^(PFObject *rider, NSError *error){
            
            [recipients addObject:rider];
            
            [self sendEmailsWithSubject:@"MüV Ride Receipt" AndTemplate:@"Ride Receipt" AndVariables:variablesDict To:recipients withCompletionBlock:^(BOOL succeeded, NSError *error){
            }];
        }];
    }];
    
    
}


@end
