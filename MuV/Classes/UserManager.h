//
//  User.h
//  MuV
//
//  Created by SushmaN on 1/19/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>

@interface UserManager : NSObject

@property (strong, nonatomic) PFUser *currentUser;

-(BOOL)isDriver;
-(PFObject *)hasDriverAbilities;




//+ (instancetype)sharedInstance;
//- (CLLocation *)getCurrentUser;


@end
