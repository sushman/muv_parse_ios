//
//  LocationManager.m
//  MuV
//
//  Created by SushmaN on 1/14/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "LocationManager.h"

@implementation LocationManager

#pragma mark Singleton
+ (instancetype)sharedInstance {
    
    static LocationManager *sharedInstance = nil;
    static dispatch_once_t dispatchOnce;
    
    dispatch_once(&dispatchOnce, ^{
        sharedInstance = [[self alloc]init];
    });
    
    return sharedInstance;
}

- (id)init {
    if (self = [super init]) {
    }
    return self;
}

- (NSString *)getCurrentLocation {
    return _currentLocation;
}


@end
