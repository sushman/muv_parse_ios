//
//  Route.h
//  MuV
//
//  Created by SushmaN on 1/23/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>

@interface Trip : NSObject

@property (strong, nonatomic) PFObject *route;
@property (assign, nonatomic) BOOL isCurrent;
@property (assign, nonatomic) BOOL hasEnded;
@property (strong, nonatomic) PFUser *driverDetails;
@property (strong, nonatomic) NSString *startAddress;
@property (strong, nonatomic) NSString *startLocation;

@property (strong, nonatomic) NSString *endAddress;
@property (strong, nonatomic) NSString *endLocation;


@property (strong, nonatomic) NSString *startTime;
@property (strong, nonatomic) NSString *endTime;
@property (strong, nonatomic) NSDate *tripDate;
@property (strong, nonatomic) NSMutableArray *wayPointStartAddress;
@property (strong, nonatomic) NSMutableArray *wayPointEndAddress;
@property (strong, nonatomic) NSMutableArray *userDetails;
@property (strong, nonatomic) NSMutableDictionary *dictOfPassengersAndAddresses;
@property (strong, nonatomic) NSMutableArray *rideObjects;


@end
