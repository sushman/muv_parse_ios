//
//  TripManager.h
//  MuV
//
//  Created by SushmaN on 1/28/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Ride.h"
#import "Trip.h"
#import "ParkAndRide.h"

@interface TripManager : NSObject

@property (nonatomic, retain) NSDictionary *rides;
@property (nonatomic, retain) NSArray *tripDates;
@property (nonatomic, retain) NSString *dateLoaded;
@property (nonatomic, retain) NSDate *monthLoaded;
@property (nonatomic, retain) Ride *editedRide;
@property (nonatomic, retain) Trip *currentTrip;
@property (nonatomic, retain) Trip *currentTripDriver;
@property (nonatomic, retain) Trip *currentTripRider;
@property (nonatomic, retain) ParkAndRide *parkAndRide;

+ (NSString *)dateLoaded;
+ (id)sharedManager;

@end
