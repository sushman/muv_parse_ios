//
//  UserDetail.m
//  MuV
//
//  Created by SushmaN on 1/23/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "UserDetail.h"

@implementation UserDetail

- (UserDetail *)createUserDetail:(PFObject *) userObject
                withStartAddress:(NSString *)startAddress
                   andEndAddress:(NSString *)endAddress
                andStartLocation:(NSString *)startLocation
                  andEndLocation:(NSString *)endLocation
                   andRideObject:(PFObject *)rideObject
                    andStartTime:(NSDate *)startTime
                      andEndTime:(NSDate *)endTime {
    
    UserDetail *userDetail = [UserDetail new];
    userDetail.userId = userObject;
    userDetail.displayName = [NSString stringWithFormat:@"%@ %@.", userObject[@"firstName"], [userObject[@"lastName"] substringToIndex:1]];
    userDetail.startLocation = startLocation;
    userDetail.endLocation = endLocation;
    userDetail.startAddress = startAddress;
    userDetail.endAddress = endAddress;
    userDetail.rideObj = rideObject;
    userDetail.startTime = startTime;
    userDetail.endTime = endTime;
    
    NSDate *pickUpTime = rideObject[@"pickUpTime"];
    if (pickUpTime) {
        userDetail.isPickedUp = YES;
    }
    
    NSDate *dropOffTime = rideObject[@"dropOffTime"];
    if (dropOffTime) {
        userDetail.isDroppedOff = YES;
    }
    
    if (rideObject[@"noShow"] == [NSNumber numberWithBool:YES]) {
        userDetail.isNoShow = YES;
    }
    if (userObject[@"profileImage"] != nil) {
        userDetail.profileImage = userObject[@"profileImage"];
    }
    return userDetail;
}

@end
