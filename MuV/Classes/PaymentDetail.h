//
//  PaymentDetails.h
//  MuV
//
//  Created by SushmaN on 4/9/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>

@interface PaymentDetail : NSObject

@property (nonatomic, strong) NSString *objectId;
@property (nonatomic, strong) NSString *paymentType;
@property (nonatomic, strong) NSNumber *cardType;
@property (nonatomic, strong) NSString *nameOnCard;
@property (nonatomic, strong) NSString *lastFourDigits;
@property (nonatomic, strong) NSString *cardToken;
@property (nonatomic, strong) NSNumber *venmoEmail;

- (void)addPaymentDetail:(NSString *)cardType
              nameOnCard:(NSString *)nameOnCard
          lastFourDigits:(NSString *)lastFourDigits
               cardToken:(NSString *)cardToken
              venmoEmail:(NSString *)venmoEmail
     withCompletionBlock:(void (^)(PFObject *paymentDetail, NSError *error))completionBlock;

@end
