//
//  Coupon.h
//  MuV
//
//  Created by SushmaN on 2/24/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>

@interface Coupon : NSObject

@property (nonatomic, strong) NSString *couponName;
@property (nonatomic, strong) NSNumber *couponAmount;
@property (nonatomic, strong) NSString *couponType;
@property (nonatomic, strong) NSString *numOfCoupons;
@property (nonatomic, assign) BOOL active;
@property (nonatomic, strong) PFObject *coupon;

@end
