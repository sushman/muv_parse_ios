//
//  LocationDetail.h
//  MuV
//
//  Created by Hing Huynh on 2/5/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserProfileInformation.h"
#import "LocationManager.h"

@interface LocationDetail : NSObject

@property (nonatomic, retain) NSString *pictureName;
@property (nonatomic, retain) NSString *addressType;
@property (nonatomic, retain) NSString *address;
@property (nonatomic, retain) CLPlacemark *placemark;
@property (nonatomic, retain) CLLocationManager *locationManager;
@property (nonatomic, retain) CLPlacemark *currentLocation;

@property (nonatomic, strong) UserProfileInformation *userInfo;

//- (void)updateHomeLocation;
//
//- (void)updateWorkLocation;

- (void)getUserLocationsWithCompletionBlock:(void (^) (NSMutableArray *locationsArray, NSError *error))completionBLock;

//- (void)addPastLocationIfNotDuplicate:(NSString *)address;

- (void)getPossibilitiesFromAddress:(NSString *)searchAddress forRegion:(CLLocationCoordinate2D)placemarkCurrent withCompletionBLock:(void (^) (NSMutableArray *validAddresses, NSError *error))completionBLock;

- (void)getPlacemarkFromAddress:(NSString *)address withCompletionBLock:(void (^) (CLPlacemark *placemark, NSError *error))completionBLock;

- (void)addPastAddressIfNotDuplicate:(NSString *)newAddress;

@end
