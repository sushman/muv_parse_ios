//
//  SearchManager.h
//  MuV
//
//  Created by Hing Huynh on 2/10/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SearchManager : NSObject

- (void)searchForDate:(NSDate *)date WithStartAddress:(NSString *)startAddress AndEndAddress:(NSString *)endAddress AndArrival:(NSString *)boolean withCompletionBlock:(void (^)(NSArray *bestMatches, NSError *error))completionBlock;


@end
