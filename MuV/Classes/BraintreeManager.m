//
//  BraintreeManager.m
//  MuV
//
//  Created by SushmaN on 4/9/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "BraintreeManager.h"
#import "UserProfileInformation.h"

@implementation BraintreeManager

Braintree *braintree;

- (void)addCreditCardDetailsWithCardNumber:(NSString *)cardNumber
                               andExpMonth:(NSString *)expMonth
                                andExpYear:(NSString *)expYear
                                    andCvv:(NSString *)cvv
                          nameonCreditCard:(NSString *)nameOnCreditCard {
    
    [self getClientTokenWithCompletionBlock:^(NSString *clientToken, NSError *error) {
        
        if (!error){
            
            PFObject *user = [UserProfileInformation sharedMySingleton].user;
            NSString *customerId = user[@"customerId"];
            braintree = [Braintree braintreeWithClientToken:clientToken];
            
            
            BTClientCardRequest *request = [[BTClientCardRequest alloc] init];
            request.cvv = cvv;
            request.number = cardNumber;
            request.expirationMonth = expMonth;
            request.expirationYear = expYear;
            //            request.postalCode = _zipCodeTextField.text;
            
            [braintree tokenizeCard:request completion:^(NSString *nonce, NSError *error) {
                               //   [self.navigationItem.rightBarButtonItem setEnabled:YES];
                  if (error) {
                      NSLog(@"Error: %@", error);
                      [[[UIAlertView alloc] initWithTitle:@"Error"
                                                  message:[error localizedDescription]
                                                 delegate:nil
                                        cancelButtonTitle:@"OK"
                                        otherButtonTitles:nil] show];
                  }
                  
                  if (nonce) {
                      NSLog(@"Card tokenized -> Nonce Received: %@", nonce);
                      
                      // CHECKING IF customerId exists didn't work
                      if (customerId.length > 6){
                          [self addCardToExistingCustomerIdWithNonce:nonce andCustomerId:customerId andNameOnCreditOCard:nameOnCreditCard andCreditCardNumber:cardNumber];
                      } else {
                          [self addCardWithNonce:nonce userFirstName:user[@"firstName"] andLastName:user[@"lastName"] andNameOnCreditCard:nameOnCreditCard andCreditCardNumber:cardNumber];
                      }
                  }
            }];
        }
    }];
}

//- (void)addVenmoDetailsWithCardNumber:(NSString *)venmoEmail {
//    
//    [self getClientTokenWithCompletionBlock:^(NSString *clientToken, NSError *error) {
//        
//        if (!error){
//            
//            PFObject *user = [UserProfileInformation sharedMySingleton].user;
//            NSString *customerId = user[@"customerId"];
//            braintree = [Braintree braintreeWithClientToken:clientToken];
//            
//            BTClientCardTokenizationRequest
//            BTClientCardRequest *request = [[BTClientCardRequest alloc] init];
//
//            
//            [braintree tokenizeCard:request completion:^(NSString *nonce, NSError *error) {
//                //   [self.navigationItem.rightBarButtonItem setEnabled:YES];
//                if (error) {
//                    NSLog(@"Error: %@", error);
//                    [[[UIAlertView alloc] initWithTitle:@"Error"
//                                                message:[error localizedDescription]
//                                               delegate:nil
//                                      cancelButtonTitle:@"OK"
//                                      otherButtonTitles:nil] show];
//                }
//                
//                if (nonce) {
//                    NSLog(@"Card tokenized -> Nonce Received: %@", nonce);
//                    
//                    // CHECKING IF customerId exists didn't work
//                    if (customerId.length > 6){
//                        //[self addCardToExistingCustomerIdWithNonce:nonce andCustomerId:customerId andNameOnCreditOCard:nameOnCreditCard andCreditCardNumber:cardNumber];
//                    } else {
//                       // [self addCardWithNonce:nonce userFirstName:user[@"firstName"] andLastName:user[@"lastName"] andNameOnCreditCard:nameOnCreditCard andCreditCardNumber:cardNumber];
//                    }
//                }
//            }];
//        }
//    }];
//}

- (void)getClientTokenWithCompletionBlock:(void (^)(NSString *clientToken, NSError *error))completionBlock {
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    AFHTTPResponseSerializer *serializer = [AFHTTPResponseSerializer serializer];
    
    serializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = serializer;
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager GET:@"http://muv2work.com/api/payserSaveClient"
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             
             NSString *responseText = [[NSString alloc] initWithData:responseObject encoding: NSASCIIStringEncoding];
             NSString *stringWithoutQuotation = [responseText
                                                 stringByReplacingOccurrencesOfString:@"\"" withString:@""];
             // Setup braintree with responseObject[@"client_token"]
             completionBlock(stringWithoutQuotation, nil);
             
         }
     
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             // Handle failure communicating with your server
             NSLog(@"error: %@", error);
             completionBlock(nil, error);
             
    }];
}



- (void) addCardWithNonce:(NSString *)nonce userFirstName:(NSString *)firstName andLastName:(NSString *)lastName andNameOnCreditCard:(NSString *)nameOnCreditCard andCreditCardNumber:(NSString *) creditCardName {
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    AFHTTPResponseSerializer *serializer = [AFHTTPResponseSerializer serializer];
    
    serializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = serializer;
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager POST:@"http://muv2work.com/api/payserSaveClient"
       parameters:@{ @"fname": firstName,
                     @"lname": lastName,
                     @"nonce":nonce}
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSError* error = nil;
              
              NSString *responseText = [[NSString alloc] initWithData:responseObject encoding: NSASCIIStringEncoding];
              
              NSData* data = [responseText dataUsingEncoding:NSASCIIStringEncoding];
              
              NSArray* json = [NSJSONSerialization
                               JSONObjectWithData:data
                               
                               options:kNilOptions
                               error:&error];
              
              //              NSLog(@"json data1 = %@",json);
              NSDictionary *responseDict = (NSDictionary*)json;
              if (![responseDict[@"cardToken"]isKindOfClass:[NSNull class]]) {
                  
                PaymentDetail *paymentDetail = [PaymentDetail new];
                NSString *cardType = [self getCardTypeFromResponseURL:responseDict[@"rslt"][@"Target"][@"CreditCards"][0][@"ImageUrl"]];
                  
                [paymentDetail addPaymentDetail:cardType nameOnCard:nameOnCreditCard lastFourDigits:[creditCardName substringFromIndex: [creditCardName length] - 4] cardToken:responseDict[@"cardToken"] venmoEmail:nil withCompletionBlock:^(PFObject *paymentDetailObj, NSError *error) {
                    if (paymentDetailObj) {
                        PFObject *user = [UserProfileInformation sharedMySingleton].user;
                        user[@"customerId"] = responseDict[@"customerId"];
                        user[@"defaultToken"] = paymentDetailObj;
                        [user saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
                         //   [_paymentAlertDelegate updatePaymentMethodsWithCardandSender:nil];
                         //   [[self navigationController] popViewControllerAnimated:YES];
                        }];
                    }
                }];
                  
              } else {
                  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                  message:@"Invalid Card, please try again."
                                                                 delegate:nil
                                                        cancelButtonTitle:nil
                                                        otherButtonTitles:@"Ok", nil];
                  [alert show];
              }
          }
     
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              // Handle failure communicating with your server
              NSLog(@"error: %@", error);
          }];
}


- (void) addCardToExistingCustomerIdWithNonce:(NSString *)nonce andCustomerId:(NSString *)customerId andNameOnCreditOCard:(NSString *)nameOnCard andCreditCardNumber:(NSString *)creditCardNumber  {
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    AFHTTPResponseSerializer *serializer = [AFHTTPResponseSerializer serializer];
    
    serializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = serializer;
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager POST:@"http://muv2work.com/api/payserAddCard"
       parameters:@{ @"customerId": customerId,
                     @"nonce":nonce}
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSError* error = nil;
              
              NSString *responseText = [[NSString alloc] initWithData:responseObject encoding: NSASCIIStringEncoding];
              
              NSData* data = [responseText dataUsingEncoding:NSASCIIStringEncoding];
              
              NSArray* json = [NSJSONSerialization
                               JSONObjectWithData:data
                               
                               options:kNilOptions
                               error:&error];
              
              NSLog(@"json data2 = %@",json);
              NSDictionary *responseDict = (NSDictionary*)json;
              
              if ([responseDict[@"Message"]isKindOfClass:[NSNull class]]) {
                  
                  NSString *cardType = [self getCardTypeFromResponseURL:responseDict[@"Target"][@"ImageUrl"]];

                  PaymentDetail *paymentDetail = [PaymentDetail new];
                  [paymentDetail addPaymentDetail:cardType nameOnCard:nameOnCard lastFourDigits:[creditCardNumber substringFromIndex: [creditCardNumber length] - 4] cardToken:responseDict[@"Target"][@"Token"] venmoEmail:nil withCompletionBlock:^(PFObject *paymentDetailObj, NSError *error) {
                      if (paymentDetailObj) {
                          PFObject *user = [UserProfileInformation sharedMySingleton].user;
                          user[@"defaultToken"] = paymentDetailObj;
                          [user addUniqueObject:paymentDetailObj forKey:@"paymentTokens"];
                          [user saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
                              //[_paymentAlertDelegate updatePaymentMethodsWithCardandSender:nil];
                            //  [[self navigationController] popViewControllerAnimated:YES];
                          }];
                      }
                  }];
                  
              } else {
                  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                  message:@"Invalid Card, please try again."
                                                                 delegate:nil
                                                        cancelButtonTitle:nil
                                                        otherButtonTitles:@"Ok", nil];
                  [alert show];
              }
              
          }
     
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              // Handle failure communicating with your server
              NSLog(@"error: %@", error);
          }];
}


- (NSString *)getCardTypeFromResponseURL:(NSString *)cardURL {
    
    if ([cardURL rangeOfString:@"visa"].location != NSNotFound) {
        return @"visa";
    } else if ([cardURL rangeOfString:@"mastercard"].location != NSNotFound){
        return @"mastercard";
    } else if ([cardURL rangeOfString:@"discover"].location != NSNotFound){
        return @"discover";
    } else if ([cardURL rangeOfString:@"american_express"].location != NSNotFound){
        return @"american_express";
    } else if ([cardURL rangeOfString:@"jcb"].location != NSNotFound){
        return @"jcb";
    } else {
        return @"other";
    }

}

@end
