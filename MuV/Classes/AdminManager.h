//
//  AdminManager.h
//  MuV
//
//  Created by SushmaN on 3/10/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>
#import "AdminSettings.h"


@interface AdminManager : NSObject

@property (strong, nonatomic) AdminSettings *adminSettings;

+ (instancetype)sharedInstance;

- (void)getAdminSettingsWithCompletionBlock: (void (^)(AdminSettings *adminSetting, NSError *error))completionBlock;

@end
