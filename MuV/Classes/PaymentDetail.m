//
//  PaymentDetails.m
//  MuV
//
//  Created by SushmaN on 4/9/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "PaymentDetail.h"
#define PAYMENT_DETAIL_TABLE @"paymentDetail"

@implementation PaymentDetail



- (void)addPaymentDetail:(NSString *)cardType
              nameOnCard:(NSString *)nameOnCard
          lastFourDigits:(NSString *)lastFourDigits
               cardToken:(NSString *)cardToken
              venmoEmail:(NSString *)venmoEmail
     withCompletionBlock:(void (^)(PFObject *paymentDetail, NSError *error))completionBlock {
    
    
    PFObject *paymentDetail = [PFObject objectWithClassName:PAYMENT_DETAIL_TABLE];
   
    if (venmoEmail.length > 0) {
         paymentDetail[@"venmoEmail"] = venmoEmail;
         paymentDetail[@"paymentType"] = @"venmo";
    }
    else {
        paymentDetail[@"cardType"] = cardType;
        paymentDetail[@"nameOnCard"] =  nameOnCard;
        paymentDetail[@"lastFourDigits"] =  lastFourDigits;
        paymentDetail[@"cardToken"] = cardToken;
        paymentDetail[@"paymentType"] = @"credit card";
    }
    
    [paymentDetail saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (succeeded) {
            completionBlock (paymentDetail, nil);
        }
        else {
            NSLog(@"error saving PAYMENT MEHTOD for referral");
            completionBlock (nil, error);
        }
    }];

}
@end
