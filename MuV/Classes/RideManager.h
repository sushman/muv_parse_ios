//
//  RideManager.h
//  MuV
//
//  Created by SushmaN on 1/6/15.
//  Copyright (c) 2015 SushmaN. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Ride.h"
#import <Parse/Parse.h>
#import "UserProfileInformation.h"
#import "Trip.h"
#import "BookingInformation.h"


@interface RideManager : NSObject


@property (strong,nonatomic) PFObject *routeDetails;
@property (strong,nonatomic) PFObject *rideDetails;
@property (strong, nonatomic) UserProfileInformation* userInfo;
@property (strong, nonatomic) BookingInformation* bookingInfo;

- (void)checkIfUserIsNotDriver:(NSString *)objectId forMatchingRoutes:(NSArray *)arrayBestRoutes withCompletionBlock:(void (^)(BOOL exists, NSError *error))completionBlock;

- (void)scheduleDriversMuV:(NSArray *)arrRides withCompletionBlock:(void (^)(NSArray *arrRouteObjs, NSError *error))completionBlock;

- (void)bookMuV:(NSArray *)arrRides withCompletionBlock:(void (^)(NSArray *arrRouteObjs, NSError *error))completionBlock;
- (void)getDriversForRide:(Ride *)ride withCompletionBlock:(void (^)(NSMutableArray *arrRides, NSError *error))completionBlock;

- (void)getCurrentRideForDriverwithCompletionBlock:(void (^)(Trip *trip, NSError *error))completionBlock;
- (void)getCurrentRideForRiderwithCompletionBlock:(void (^)(Trip *trip, NSError *error))completionBlock;


- (void)getTripsForDriver:(NSInteger) tripViewType forDate:(NSDate *)reqDate withCompletionBlock:(void (^)(NSDictionary *trips, NSArray *tripDates, NSError *error))completionBlock;


- (void)updateDriverLocation: (CLLocationCoordinate2D)coordinate;
- (void)getDriversCurrentLocationWithCompletionBlock:(void (^)(PFGeoPoint *geoPoint, NSError *error))completionBlock;

- (void)getRidesForRiderForTripView: (NSInteger) tripViewType forDate:(NSDate *)reqDate withCompletionBlock:(void (^)(NSDictionary *trips, NSArray *tripDates, NSError *error))completionBlock;


- (void)updateRideObject:(PFObject *)ride;
- (void)updateRideObject:(PFObject *)ride withCompletionBlock:(void (^)(BOOL succeeded, NSError *error))completionBlock;

- (void)cancelRide:(PFObject *)ride withCompletionBlock:(void (^)(BOOL succeeded, NSError *error))completionBlock;

- (void)updatePFObject:(PFObject *)object;
- (void)updatePFObject:(PFObject *)object withCompletionBlock:(void (^)(BOOL succeeded, NSError *error))completionBlock;

- (void)setDriversLocationToNull;

- (void)getBestMatchedRoutesWithoutMyRoutes:(NSArray *)bestMatchedRoutes withCompletionBlock:(void (^)(NSArray *objectIds, NSError *error))completionBlock;
- (void)getBestMatchedRoutesExcludingCurrentUserForRides:(NSArray *)bestMatchedRoutes withCompletionBlock:(void (^)(NSArray *filteredResults, NSError *error))completionBlock;

- (void)updateTripTimes:(BOOL)isStartTime :(PFObject *)route;

- (void)updateTotalDriverSeatsAvailable:(NSNumber *)totalSeats;

@end
