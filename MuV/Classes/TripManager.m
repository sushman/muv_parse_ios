//
//  TripManager.m
//  MuV
//
//  Created by SushmaN on 1/28/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "TripManager.h"

@implementation TripManager

#pragma mark Singleton Methods

+ (id)sharedManager {
    static TripManager *sharedTripManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedTripManager = [[self alloc] init];
    });
    return sharedTripManager;
}

+ (NSString *)dateLoaded {
    return [[self sharedManager] dateLoaded];
}

- (void)setDateLoaded :(NSString *)date{
    _dateLoaded = date;
}

- (void)setMonthLoaded :(NSDate *)date{
    _monthLoaded = date;
}

- (void)setEditedRide:(Ride *)editedRide {
    _editedRide = editedRide;
}

- (void)setCurrentTrip:(Trip *)currentTrip {
    _currentTrip = currentTrip;
}

- (void)setCurrentTripDriver:(Trip *)currentTripDriver {
    _currentTripDriver = currentTripDriver;
}

- (void)setCurrentTripRider:(Trip *)currentTripRider {
    _currentTripRider = currentTripRider;
}

- (void)setParkAndRide:(ParkAndRide *)parkAndRide {
    _parkAndRide = parkAndRide;
}


@end
