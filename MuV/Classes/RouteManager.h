//
//  RouteManager.h
//  MuV
//
//  Created by SushmaN on 1/30/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>

@interface RouteManager : NSObject


@property (strong, nonatomic) PFObject *currentRide;

+ (instancetype)sharedInstance;

@end
