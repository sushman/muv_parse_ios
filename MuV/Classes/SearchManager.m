//
//  SearchManager.m
//  MuV
//
//  Created by Hing Huynh on 2/10/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "SearchManager.h"
#import <AFNetworking/AFNetworking.h>

@implementation SearchManager

- (void)searchForDate:(NSDate *)date WithStartAddress:(NSString *)startAddress AndEndAddress:(NSString *)endAddress AndArrival:(NSString *)boolean withCompletionBlock:(void (^)(NSArray *bestMatches, NSError *error))completionBlock {
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    AFHTTPResponseSerializer *serializer = [AFHTTPResponseSerializer serializer];
    
    serializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    
    manager.responseSerializer = serializer;
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    NSDateFormatter *formatter = [NSDateFormatter new];
    NSTimeZone *GMT = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [formatter setTimeZone:GMT];
    [formatter setDateFormat:@"MM/dd/yyyy%20hh:mma"];
    NSString *stringFromDate = [formatter stringFromDate:date];
    NSLog(@"date: %@", stringFromDate);
    
    
    NSString *urlString = [NSString stringWithFormat:@"http://muv2work.com/api/getdrivers/?pckdrpdtme=%@&startaddr=%@&endaddr=%@&arrival=%@", stringFromDate, startAddress, endAddress, boolean];
    
    NSLog(@"url: %@", urlString);
    
    [manager GET:urlString parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             
             NSError* error = nil;
             
             NSString *responseText = [[NSString alloc] initWithData:responseObject encoding: NSASCIIStringEncoding];
             
             NSString *stringWithoutBackSlashes = [responseText
                                                   stringByReplacingOccurrencesOfString:@"\\" withString:@""];
             
             NSString *stringWithoutQuotations = [stringWithoutBackSlashes substringWithRange:NSMakeRange(1, [stringWithoutBackSlashes length]-2)];
             
             NSData* data = [stringWithoutQuotations dataUsingEncoding:NSASCIIStringEncoding];
             
             NSArray* json = [NSJSONSerialization
                              JSONObjectWithData:data
                              
                              options:kNilOptions
                              error:&error];
             NSLog(@"json data = %@",json);
             
             completionBlock(json, nil);
             
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             if (error) {
                 NSLog(@"error: %@", error);
                 completionBlock(nil, error);
             }
         }];
    
}

@end
