//
//  Trips.h
//  MuV
//
//  Created by SushmaN on 1/25/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Trips : NSObject

@property (nonatomic, strong) NSString *tripId;
@property (nonatomic, strong) NSString *tripDate;
@property (nonatomic, strong) NSString *tripTime;
@property (nonatomic, strong) NSString *tripRouteType;
@property (nonatomic, assign) bool isScheduled;



@end
