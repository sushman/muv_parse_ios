//
//  AdminSettings.h
//  MuV
//
//  Created by SushmaN on 3/10/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AdminSettings :  NSObject

@property (nonatomic, strong) NSNumber *userPromoAmount;
@property (nonatomic, strong) NSNumber *costPerMile;
@property (nonatomic, strong) NSNumber *driverPaymentPercentage;
@property (nonatomic, strong) NSNumber *hoursBeforeDepartureForFee;
@property (nonatomic, strong) NSNumber *hoursBeforeArrivalForFee;
@property (nonatomic, strong) NSNumber *cancellationFeeAmount;
@property (nonatomic, strong) NSNumber *numberOfRecentLocations;
@property (nonatomic, strong) NSNumber *geoFenceInMiles;
@property (nonatomic, strong) NSNumber *currentLocationPingIntervalInSeconds;
@property (nonatomic, strong) NSString *adminEmailAddress;


@end
