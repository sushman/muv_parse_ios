//
//  RatingManager.m
//  MuV
//
//  Created by Hing Huynh on 2/25/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "RatingManager.h"
#import "Ride.h"
#import <Parse/Parse.h>
#import "UserDetail.h"
#import "Trip.h"

@implementation RatingManager

-(void) createRatingWithReview:(NSString *)review andRating:(NSNumber *)rating ForRide:(Ride *)ride {
    
    PFObject *userRating = [PFObject objectWithClassName:@"rating"];
    userRating[@"comment"]= review;
    userRating[@"rating"] = rating;
    userRating[@"ride"] = ride.rideNo;
    userRating[@"driver"] = ride.driverDetail;
    userRating[@"rider"] = ride.rideNo[@"rider"];
    
    [userRating saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
        if (!error){
            [ride.driverDetail addUniqueObject:rating forKey:@"ratings"];
            ride.rideNo[@"hasBeenRated"] = [NSNumber numberWithBool:YES];
            NSArray *arrayOfObjects = [[NSArray alloc] initWithObjects:ride.driverDetail, ride.rideNo, nil];
            [PFObject saveAllInBackground:arrayOfObjects];
        }
    }];
    
}

-(NSNumber *) getAverageRatingFromArrayOfRatings:(NSArray *)ratings {
    
    NSNumber *ratingSum = @0;
    for (NSNumber *rating in ratings){
        ratingSum = @([ratingSum floatValue] + [rating floatValue]);
    }
    
    NSNumber *averageRating = @([ratingSum floatValue]/ ratings.count);
    return @(roundf([averageRating floatValue] * 2.0f) / 2.0f);
}

@end
