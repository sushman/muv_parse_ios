//
//  BraintreeManager.h
//  MuV
//
//  Created by SushmaN on 4/9/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Braintree/Braintree.h>
#import <AFNetworking/AFNetworking.h>
#import <Parse/Parse.h>
#import "PaymentDetail.h"

@interface BraintreeManager : NSObject

- (void)addCreditCardDetailsWithCardNumber:(NSString *)cardNumber
                               andExpMonth:(NSString *)expMonth
                                andExpYear:(NSString *)expYear
                                    andCvv:(NSString *)cvv
                          nameonCreditCard:(NSString *)nameOnCreditCard;

@end
