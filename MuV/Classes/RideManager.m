
//
//  RideManager.m
//  MuV
//
//  Created by SushmaN on 1/6/15.
//  Copyright (c) 2015 SushmaN. All rights reserved.
//

#import "RideManager.h"
#import "Helper.h"
#import "BestMatchedRides.h"
#import "ControlVariables.h"
#import "UserProfileInformation.h"
#import "UserDetail.h"
#import "RouteManager.h"
#import <AFNetworking/AFNetworking.h>
#import "CouponManager.h"
#import "RatingManager.h"

#define ARRIVAL @"arrival"
#define DEPARTURE @"departure"

@implementation RideManager

enum RangeEnum
{
    Today,
    Day,
    Week,
    Month,
    MonthAndHalf,
    CurrentMonth
};


- (void)updateTripTimes:(BOOL) isStartTime :(PFObject *)route {
    BOOL hasChanged = NO;
    if (isStartTime) {
        NSLog(@"route %@",route[@"tripStartedTime"]);
        if (!route[@"tripStartedTime"]) {
            route[@"tripStartedTime"] = [NSDate date];
            route[@"hasStarted"] = [NSNumber numberWithBool:YES];
            hasChanged = YES;
        }
    }
    else {
        if (route[@"tripEndedTime"]) {
            route[@"tripEndedTime"] = [NSDate date];
            hasChanged = YES;
        }
    }
    
    if (hasChanged) {
        [route saveInBackground];
    }
}

- (void)updateRideObject:(PFObject *)ride withCompletionBlock:(void (^)(BOOL succeeded, NSError *error))completionBlock {
   [ride saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
       if (succeeded) {
           completionBlock (succeeded, nil);
       }
       else {
           completionBlock (NO, error);
       }
   }];
}

- (void)getBestMatchedRoutesExcludingCurrentUserForRides:(NSArray *)bestMatchedRoutes withCompletionBlock:(void (^)(NSArray *filteredResults, NSError *error))completionBlock {
    
   // NSLog(@"bestMatched Roytes %@", bestMatchedRoutes);
    [self getBestMatchedRoutesWithoutMyRoutes:bestMatchedRoutes withCompletionBlock:^(NSArray *objectIds, NSError *error) {
        NSMutableArray *filtereredResults = [NSMutableArray new];
        if (objectIds) {
            for (NSString *objectId in objectIds) {
                for (NSDictionary *dict in bestMatchedRoutes) {
              //      NSLog(@"Dict Object id %@ & %@objectId", dict[@"objectId"], objectId);
                    if ([dict[@"objectid"] isEqualToString:objectId]){
                        [filtereredResults addObject:dict];
                        break;
                    }
                }
            }
            completionBlock (filtereredResults, nil);
        }
        else {
            completionBlock (nil, error);

        }
    }];
}

- (void)getBestMatchedRoutesWithoutMyRoutes:(NSArray *)bestMatchedRoutes withCompletionBlock:(void (^)(NSArray *objectIds, NSError *error))completionBlock {
    
    NSMutableArray *bestRoutes = [NSMutableArray new];
    
    for (NSDictionary *result in bestMatchedRoutes){
        [bestRoutes addObject:result[@"objectid"]];
    }
  
    PFObject *driver = [UserProfileInformation sharedMySingleton].driverDetails;
    
    if (!driver) {
        completionBlock (bestRoutes, nil);
        return;
    }
    
    PFQuery *query = [PFQuery queryWithClassName:@"routes"];
   
    
    [query whereKey:@"objectId" containedIn:[NSArray arrayWithArray:bestRoutes]];
    [query whereKey:@"driver" notEqualTo:driver];

    [query findObjectsInBackgroundWithBlock:^(NSArray *routes, NSError *error) {
        if (routes) {
            NSMutableArray *arrayRoutes = [NSMutableArray new];
            for (PFObject *route in routes) {
                [arrayRoutes addObject:route.objectId];
            }
            completionBlock (arrayRoutes, nil);
        }
        else {
            completionBlock (nil, error);
        }
    }];
    
}

- (void)updateRideObject:(PFObject *)ride {
    [ride saveInBackground];
}

- (void)updatePFObject:(PFObject *)object {
    [object saveInBackground];
}

- (void)updatePFObject:(PFObject *)object withCompletionBlock:(void (^)(BOOL succeeded, NSError *error))completionBlock {
    [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (succeeded) {
            completionBlock (succeeded, nil);
        }
        else {
            completionBlock (NO, error);
        }
    }];
}

- (void)updateDriverLocation:(CLLocationCoordinate2D)coordinate {
    NSLog(@"location: %f, %f", coordinate.latitude, coordinate.longitude);
    PFGeoPoint *geoPoint = [PFGeoPoint geoPointWithLatitude:coordinate.latitude longitude:coordinate.longitude];
    
    PFObject *route = [RouteManager sharedInstance].currentRide;
    
    if (geoPoint.latitude == 0 && geoPoint.longitude == 0) {
        CLLocationCoordinate2D cord = [Helper getPositionFromCommaSeperatedLatitude:route[@"startLocation"]];
        geoPoint = [PFGeoPoint geoPointWithLatitude:cord.latitude longitude:cord.longitude];
    }
    route[@"currentLocation"] = geoPoint;
    [route saveInBackground];
}

- (void)setDriversLocationToNull {
    
    PFObject *route = [RouteManager sharedInstance].currentRide;
    
    // Now let's update it with some new data. In this case, only cheatMode and score
    // will get sent to the cloud. playerName hasn't changed.
    [route setObject:[NSNull null] forKey:@"currentLocation"];
    route[@"hasEnded"] = [NSNumber numberWithBool:YES];
    route[@"tripEndedTime"] = [NSDate date];
    [route saveInBackground];
}

- (void)cancelRide:(PFObject *)ride withCompletionBlock:(void (^)(BOOL succeeded, NSError *error))completionBlock {
    
    [ride saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (succeeded) {
            completionBlock(succeeded, nil);
        }
        else {
            completionBlock (NO, error);
        }
    }];
}


//schedulewithCompletionBlocks
- (void)scheduleDriversMuV:(NSArray *)arrRides withCompletionBlock:(void (^)(NSArray *arrRouteObjs, NSError *error))completionBlock {
    
    _userInfo = [UserProfileInformation sharedMySingleton];
    NSMutableArray *routeObjects = [NSMutableArray new];
    
    for (Ride *ride in arrRides) {
        //create PF ride Object
        _routeDetails = [PFObject objectWithClassName:@"routes"];
        _routeDetails[@"driver"] = ride.driverDetail;
        
        NSNumber *carSeats = ride.driverDetail[@"carSeatCount"];
        
        //[May change later, for now limiting the number of car seats to 5]
        if ([carSeats intValue] > 5) {
            carSeats = @5;
        }
        
        _routeDetails[@"TotalSeats"] = carSeats;
        _routeDetails[@"occupiedSeats"] = @0;
        _routeDetails[@"mileage"] = ride.mileage;
        
        if (ride.startAddress) {
            _routeDetails[@"startAddress"] = ride.startAddress;
        }
        
        if (ride.startCity) {
             _routeDetails[@"startCity"] = ride.startCity;
        }
        
        if (ride.startState) {
            _routeDetails[@"startState"] = ride.startState;
        }
        
        if (ride.startZip) {
            _routeDetails[@"startZip"] = ride.startZip;
        }
       
        _routeDetails[@"startLocation"] = ride.startLocation;
        
        
        if (ride.endAddress) {
            _routeDetails[@"endAddress"] = ride.endAddress;
        }
        
        if (ride.endCity) {
            _routeDetails[@"endCity"] = ride.endCity;
        }
        
        if (ride.endState) {
            _routeDetails[@"endState"] = ride.endState;
        }
        
        if (ride.endZip) {
            _routeDetails[@"endZip"] = ride.endZip;
        }

    
        _routeDetails[@"endLocation"] = ride.endLocation;
        _routeDetails[@"routeType"] = ride.routeType;
        _routeDetails[@"isRoundTrip"] = [NSNumber numberWithBool:ride.isRoundTrip];
        
        // ALL DRIVER VARIABLES ARE NECESSARY TO BE FILLED and VALID
        if (_userInfo.driverDetails[@"maxExtraDistance"]) {
            _routeDetails[@"maxExtraDistance"] = _userInfo.driverDetails[@"maxExtraDistance"];
        }
        else {
            _routeDetails[@"maxExtraDistance"] = [NSNumber numberWithInt:0];
        }
        _routeDetails[@"orgRouteDistance"] = ride.mileage;
        
        
        if ([ride.routeType isEqualToString:ARRIVAL]) {
            _routeDetails[@"endDate"] = ride.rideDate;
        }
        else {
            _routeDetails[@"startDate"] = ride.rideDate;
        }
        
        _routeDetails[@"rideDate"] = ride.rideDate;
        
        [routeObjects addObject:_routeDetails];
    }
    
    [PFObject saveAllInBackground:routeObjects block:^(BOOL succeeded,NSError *error){
        if (error) {
            completionBlock (nil, error);
        } else if (!succeeded){
            completionBlock (nil, nil);
        } else{
            completionBlock (routeObjects, nil);
        }
    }];
}


- (void)bookMuV:(NSArray *)arrRides withCompletionBlock:(void (^)(NSArray *arrRouteObjs, NSError *error))completionBlock {
    
    //NSLog(@"SMN BOOK MUV");
    _userInfo = [UserProfileInformation sharedMySingleton];
    NSMutableArray *rideObjects = [NSMutableArray new];
    
    BOOL isArrival = NO;
    BOOL isDeparture = NO;
    NSDate *rideDate;
    
    for (Ride *ride in arrRides) {
        _rideDetails = [PFObject objectWithClassName:@"ride"];
        
        _rideDetails[@"rider"] = [[UserProfileInformation sharedMySingleton] user];
        _rideDetails[@"mileage"] = ride.mileage;
        if (ride.startTime) {
            _rideDetails[@"startTime"] = ride.startTime;
            _rideDetails[@"rideDate"] = ride.startTime;
            
            isDeparture = YES;
        }
        if (ride.endTime) {
            _rideDetails[@"endTime"] = ride.endTime;
            _rideDetails[@"rideDate"] = ride.endTime;
            
            isArrival = YES;
        }
        
        rideDate = _rideDetails[@"rideDate"];
         _rideDetails[@"fare"] = ride.totalFareCost;
        
        if (ride.startAddress) {
            _rideDetails[@"startAddress"] = ride.startAddress;
        }
        
        if (ride.startCity) {
            _rideDetails[@"startCity"] = ride.startCity;
        }
        
        if (ride.startState) {
            _rideDetails[@"startState"] = ride.startState;
        }
        
        if (ride.startZip) {
            _rideDetails[@"startZip"] = ride.startZip;
        }
        
        _rideDetails[@"startLocation"] = ride.startLocation;
        
        
        if (ride.endAddress) {
            _rideDetails[@"endAddress"] = ride.endAddress;
        }
        
        if (ride.endCity) {
            _rideDetails[@"endCity"] = ride.endCity;
        }
        
        if (ride.endState) {
            _rideDetails[@"endState"] = ride.endState;
        }
        
        if (ride.endZip) {
            _rideDetails[@"endZip"] = ride.endZip;
        }
        
       
        _rideDetails[@"endLocation"] = ride.endLocation;
        
        if (ride.payment) {
            _rideDetails[@"payment"] = ride.payment;
        }
        
        //add below fields if driver is found
        if (ride.rideNo) {
            NSLog(@" Ride had ride no");
            _rideDetails[@"rideNo"] = ride.rideNo;
            _rideDetails[@"driver"] = ride.driverDetail;
        }
        
        [rideObjects addObject:_rideDetails];
    }
    
    
    [self getRequestedRidesByUserAndDate:rideDate andRouteTypeIsArrival:isArrival andRouteTypeIsDeparture:isDeparture withCompletionBlock:^(NSArray *rideObjs, NSError *error) {
        if (rideObjs) {
           // NSLog(@"Get Requested Rides ");
          //  NSMutableArray *arrRideObjsEdited = [NSMutableArray new];
            for (PFObject *rideObj in rideObjs) {
                rideObj[@"isEdited"] = [NSNumber numberWithBool:YES];
                [rideObjects addObject:rideObj];
            }
            //[PFObject saveAllInBackground:arrRideObjsEdited];
        }
        
        [PFObject saveAllInBackground:rideObjects block:^(BOOL succeeded,NSError *error) {
            if (error) {
                completionBlock (nil, error);
            } else if (!succeeded){
                completionBlock (nil, nil);
            } else {
                NSMutableArray *arrRouteObjs = [NSMutableArray new];
                PFObject *route;
                for (PFObject *rideObj in rideObjects) {
                    if (rideObj[@"rideNo"]) {
                        route = rideObj[@"rideNo"];
                        NSArray *ridesArray = rideObj[@"rideNo"][@"rides"];
                        route[@"occupiedSeats"] = [NSNumber numberWithInteger: ridesArray.count + 1];
                        [route addObject:rideObj forKey:@"rides"];
                        [arrRouteObjs addObject:route];
                    }
                }
                [PFObject saveAllInBackground:rideObjects block:^(BOOL succeeded,NSError *error)
                 {
                     completionBlock (rideObjects, nil);
                 }];
                
            }
        }];
    }];
}


//smn test
- (void)getDriversCurrentLocationWithCompletionBlock:(void (^)(PFGeoPoint *geoPoint, NSError *error))completionBlock {
    //my current ride
    _userInfo = [UserProfileInformation sharedMySingleton];
    //get my current ride
    enum RangeEnum day = Today;
    PFQuery *query = [self getQueryMyCurrentRideforDateAsRider:[Helper getFirstSecondOfTheDay:[NSDate date]] andRange:day];
    [query getFirstObjectInBackgroundWithBlock:^(PFObject *ride, NSError *error){
        if (error) {
            NSLog(@"unable to find user data %@",error );
            completionBlock(nil, error);
            return;
        }
        else {
            if (ride[@"rideNo"][@"hasStarted"] && !ride[@"rideNo"][@"hasEnded"]) {
                if (ride[@"rideNo"][@"currentLocation"] != nil) {
                    //get currentLocation
                    completionBlock(ride[@"rideNo"][@"currentLocation"], error);
                }
                else {
                    completionBlock(nil, error);
                }
            }
            else {
                completionBlock(nil, error);
            }
        }
    }];
}


//update users previous requested seraches as edited when the user wants to search for a ride.
- (void)getRequestedRidesByUserAndDate:(NSDate *)dateRequested andRouteTypeIsArrival:(BOOL)isArrival andRouteTypeIsDeparture:(BOOL)isDeparture withCompletionBlock:(void (^)(NSArray *rides, NSError *error))completionBlock {
    
    _userInfo = [UserProfileInformation sharedMySingleton];
    PFQuery *query = [PFQuery queryWithClassName:@"ride"];
    [query whereKey:@"rider" equalTo:_userInfo.user];
    [query whereKeyDoesNotExist:@"driver"];
    [query whereKeyDoesNotExist:@"rideNo"];

    [query whereKey:@"isEdited" notEqualTo:[NSNumber numberWithBool:YES]];
    
    [query whereKey:@"rideDate" greaterThan:[Helper getFirstSecondOfTheDay:dateRequested]];
    [query whereKey:@"rideDate" lessThan:[Helper lastSecondOfTheDay:dateRequested]];
    
    //do below only if arrival or departire trip is false, else get all trips
    if (!isArrival || !isDeparture) {
        if (isArrival) {
            //ensure departure rides don't exist
            [query whereKeyDoesNotExist:@"startTime"];
        }
        if (isDeparture) {
            //ensure arrival rides don't exist
            [query whereKeyDoesNotExist:@"endTime"];
        }
    }
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *rides, NSError *error) {
        if (rides) {
            completionBlock (rides, nil);
        }
        else {
            completionBlock (nil, nil);
        }
        if (error) {
            completionBlock (0, error);
        }
    }];
}



- (void)checkIfUserIsNotDriver:(NSString *)objectId forMatchingRoutes:(NSArray *)arrayBestRoutes withCompletionBlock:(void (^)(BOOL exists, NSError *error))completionBlock {
    
    if (_userInfo.driverDetails == nil) {
        completionBlock (NO, nil);
        return;
    }
    PFQuery *query = [PFQuery queryWithClassName:@"routes"];
    [query whereKey:@"objectId" containedIn:[NSArray arrayWithArray:arrayBestRoutes]];
    [query whereKey:@"driver" notEqualTo:_userInfo.driverDetails];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (objects) {
            completionBlock (NO, nil);
        }
        else {
            completionBlock (YES, nil);
        }
    }];
}

//hing
- (void)getDriversForRide:(Ride *)ride withCompletionBlock:(void (^)(NSMutableArray *arrRides, NSError *error))completionBlock{
    
    NSMutableArray *arrRides = [NSMutableArray new];
    PFQuery *query = [PFQuery queryWithClassName:@"routes"];
    NSMutableArray *bestRoutes = [NSMutableArray new];
    
    for (NSDictionary *result in ride.searchResults){
        [bestRoutes addObject:result[@"objectid"]];
    }
    
    [query whereKey:@"objectId" containedIn:[NSArray arrayWithArray:bestRoutes]];
    // shouldn't need this
  //  [query whereKey:@"routeType" equalTo:ride.routeType];
    [query includeKey:@"driver"];
    [query includeKey:@"driver.userdetail"];
    [query includeKey:@"rides"];
    [query includeKey:@"rides.rider"];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *routes, NSError *error){
        
        if (!error){
            for (PFObject *route in routes) {
                
                BestMatchedRides *bestMatch = [BestMatchedRides new];
                bestMatch.route = route;
                bestMatch.rideType = route[@"routeType"];
                bestMatch.totalSeats = route[@"TotalSeats"];
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setDateFormat:@"hh:mm a"];
                [dateFormat setTimeZone:[NSTimeZone systemTimeZone]];
                
                if ([bestMatch.rideType isEqualToString:ARRIVAL]) {
                    bestMatch.rideTime = [dateFormat stringFromDate:route[@"endDate"]];
                }
                else {
                    bestMatch.rideTime = [dateFormat stringFromDate:route[@"startDate"]];
                }
                
                bestMatch.driverDetails = route[@"driver"];
                bestMatch.carMake = [NSString stringWithFormat:@"%@ %@ %@", bestMatch.driverDetails[@"carYear"], bestMatch.driverDetails[@"carModel"], bestMatch.driverDetails[@"carMake"]];
                bestMatch.userDetails = bestMatch.driverDetails[@"userdetail"];
                PFFile *driverPhoto = bestMatch.userDetails[@"profileImage"];
                bestMatch.driverPicture = driverPhoto.url;
                
                // GETTING RATING OF DRIVER
                RatingManager *ratingManager = [RatingManager new];
                bestMatch.driverRating = [ratingManager getAverageRatingFromArrayOfRatings:route[@"driver"][@"ratings"]];
                
                NSString *lastInitial = [[bestMatch.userDetails[@"lastName"] substringToIndex:1]uppercaseString];
                bestMatch.driverName = [NSString stringWithFormat:@"%@ %@.",bestMatch.userDetails[@"firstName"],lastInitial];
                
                bestMatch.otherPassengers = route[@"rides"];
                
                // LOGIC GETTING CLOSEST TIMES BEFORE SORTING
                NSDate *idealDate = [NSDate new];
                
                if (ride.startTime){
                    idealDate = ride.startTime;
                }
                if (ride.endTime) {
                    idealDate = ride.endTime;
                }
                
                NSInteger idealDateInterval = [idealDate timeIntervalSinceNow];
                
                NSDate *resultDate = bestMatch.route[@"rideDate"];
                NSInteger resultDateInterval = [resultDate timeIntervalSinceNow];
                
                // Get difference in time and conver to absolute number
                bestMatch.differenceInTimeFromIdeal = [NSNumber numberWithDouble:fabs([@(idealDateInterval - resultDateInterval) doubleValue])];
                NSLog(@"time difference: %@", bestMatch.differenceInTimeFromIdeal);
                
                // Don't like using another for loop
                for (NSDictionary *result in ride.searchResults){
                    if ([result[@"objectid"]isEqualToString:route.objectId]){
                        bestMatch.distance = result[@"newdistance"];
                        break;
                    }
                }
                
                [arrRides addObject:bestMatch];
                
            }
            
            NSSortDescriptor *sortDescriptor;
            sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"differenceInTimeFromIdeal"
                                                         ascending:YES];
            NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
            NSArray *sortedArray;
            sortedArray = [arrRides sortedArrayUsingDescriptors:sortDescriptors];
            
            completionBlock([sortedArray mutableCopy],nil);
            
        }
        else {
            completionBlock(nil, error);
        }
    }];
}

- (void)updateTotalDriverSeatsAvailable:(NSNumber *)totalSeats {
    PFQuery *query = [self createQueryForFutureDriverRoutes];
    [query findObjectsInBackgroundWithBlock:^(NSArray *routes, NSError *error) {
        if (routes) {
            for (PFObject *route in routes) {
                route[@"TotalSeats"] = totalSeats;
            }
            [PFObject saveAllInBackground:routes];
        }
    }];
}



- (void)getCurrentRideForDriverwithCompletionBlock:(void (^)(Trip* trip, NSError *error))completionBlock {
    _userInfo = [UserProfileInformation sharedMySingleton];
    
    NSDate *startDate = [Helper getFirstSecondOfTheDay:[NSDate date]];
    NSDate *endDate = [Helper lastSecondOfTheDay:[NSDate date]];
    
    PFQuery *query = [self createQueryForRoutesAndRidesForStartDate:startDate andDateEnd:endDate andTripViewType:Current];
    
    if (!query) {
        completionBlock (nil, nil);
        return;
    }
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *routes, NSError *error) {
        if (routes) {
            Ride *ride;
            for (PFObject *route in routes) {
                if (![Helper isPastDay:route[@"rideDate"]]) {
                    
                    if ([route[@"hasStarted"] boolValue] && !route[@"hasEnded"]) {
                        ride = [Ride new];
                        
                        if (routes.count > 1) {
                            if ([routes[1][@"hasStarted"] boolValue] && !routes[1][@"hasEnded"]) {
                                ride.rideDate = routes[1][@"rideDate"];
                                if (routes[1] != nil) {
                                    ride.trip = [self createTripObject:routes[1] andIsDriver:YES];
                                    ride.trip.isCurrent = YES;
                                }
                            }
                            else {
                                ride.rideDate = route[@"rideDate"];
                                if (route != nil) {
                                    ride.trip = [self createTripObject:route andIsDriver:YES];
                                    ride.trip.isCurrent = YES;
                                }
                                break;
                            }
                        }
                        else {
                            ride.rideDate = route[@"rideDate"];
                            if (route != nil) {
                                ride.trip = [self createTripObject:route andIsDriver:YES];
                                ride.trip.isCurrent = YES;
                            }
                            break;
                        }
                    }
                }
            }
            if (ride) {
                completionBlock (ride.trip, nil);
            }
            else {
                completionBlock (nil, nil);
            }
        }
        else {
            completionBlock (nil, error);
        }
    }];
}

- (void)getCurrentRideForRiderwithCompletionBlock:(void (^)(Trip* route, NSError *error))completionBlock {
    _userInfo = [UserProfileInformation sharedMySingleton];
    NSDate *startDate = [Helper getFirstSecondOfTheDay:[NSDate date]];
    NSDate *endDate = [Helper lastSecondOfTheDay:[NSDate date]];
    
    PFQuery *query = [self createQueryForRideForStartDate:startDate andDateEnd:endDate andTripViewType:Current];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *rides, NSError *error) {
        if (rides) {
            Ride *ride;
            for (PFObject *rideObj in rides) {
                if (![Helper isPastDay:rideObj[@"rideDate"]]) {
                    
                    ride = [Ride new];
                    ride.rideDate = rideObj[@"rideDate"];
                    if (rideObj != nil && rideObj[@"rideNo"][@"hasStarted"] && !rideObj[@"rideNo"][@"hasEnded"] && !rideObj[@"dropOffTime"]) {
                        ride.trip = [self createTripObject:rideObj[@"rideNo"] andIsDriver:NO];
                        ride.trip.isCurrent = YES;
                         break;
                    }
                   
                }
            }
            if (ride) {
                completionBlock (ride.trip, nil);
            }
            else {
                completionBlock (nil, nil);
            }
        }
        else {
            completionBlock (nil, error);
        }
    }];
}



#pragma Riders Trips


- (void)getRidesForRiderForTripView: (NSInteger) tripViewType forDate:(NSDate *)reqDate withCompletionBlock:(void (^)(NSDictionary *trips, NSArray *tripDates, NSError *error))completionBlock{
    
    NSDate *dateStart = [self getStartDateForTripViewType:tripViewType forDate:reqDate];
    NSDate *dateEnd = [Helper lastSecondOfTheMonth:reqDate];
    
    if (tripViewType == Past) {
        dateEnd = [Helper getFirstSecondOfTheDay:[NSDate date]];
    }

    
    _userInfo = [UserProfileInformation sharedMySingleton];
    
    PFQuery *query = [self createQueryForRideForStartDate:dateStart andDateEnd:dateEnd andTripViewType:tripViewType];
    
    NSMutableDictionary *dictOfDateAndTrips = [NSMutableDictionary new];
    NSMutableArray *arrayOfDatesInOrder = [NSMutableArray new];

    
    [query findObjectsInBackgroundWithBlock:^(NSArray *rides, NSError *error) {
        if (!error) {
            // The find succeeded.
            Ride *ride;
            //found a route
            int routeCount = 0;
            NSMutableArray *trips;
            while (routeCount < rides.count) {
                if ((routeCount + 1 == rides.count) || (![Helper isSameDay:rides[routeCount][@"rideDate"] otherDay:rides[routeCount + 1][@"rideDate"]])) {
                    
                    trips = [NSMutableArray new];
                    ride = [self setUpRideDetailsForRider:rides[routeCount] andDriver:_userInfo.user];
                    
                    [trips addObject:ride];
                    [arrayOfDatesInOrder addObject:[Helper getStringFromDate:rides[routeCount][@"rideDate"]]];
                    [dictOfDateAndTrips setObject:trips forKey:[Helper getStringFromDate:rides[routeCount][@"rideDate"]]];
                    routeCount++;
                }
                else {
                    //add 2 events
                    trips = [NSMutableArray new];
                    
                    //add arrival and departure trips
                    [trips addObject:[self setUpRideDetailsForRider:rides[routeCount] andDriver:_userInfo.user]];
                    [trips addObject:[self setUpRideDetailsForRider:rides[routeCount + 1] andDriver:_userInfo.user]];
                    
                     [arrayOfDatesInOrder addObject:[Helper getStringFromDate:rides[routeCount][@"rideDate"]]];
                    //add trips to the dictionary of date-trips
                    [dictOfDateAndTrips setObject:trips forKey:[Helper getStringFromDate:rides[routeCount][@"rideDate"]]];
                    
                    routeCount =  routeCount + 2;
                }
            }
            completionBlock(dictOfDateAndTrips, arrayOfDatesInOrder, error);
        }
        else {
            completionBlock(nil, arrayOfDatesInOrder, error);
        }
    }];
}



- (Ride *)setUpRideDetailsForRider:(PFObject *)rideDetail andDriver:(PFObject *)userDetail {
    Ride *ride = [Ride new];
    ride.rideDate = rideDetail[@"rideDate"];
    ride.rideNo = rideDetail;
    
    if (rideDetail[@"hasBeenRated"]) {
        ride.hasBeenRated = [rideDetail[@"hasBeenRated"] boolValue];
    }
    
    if (rideDetail[@"endTime"] != nil) {
        ride.routeType = ARRIVAL;
    }
    else {
        ride.routeType = DEPARTURE;
    }
    
    // if the ride table has driver details column filled in, the ride is scheduled
    //else the ride is a requested ride
    if (rideDetail[@"driver"]) {
        ride.isRequested = NO;
        ride.driverDetail = rideDetail[@"driver"];
        
        // GETTING RATING OF DRIVER
        RatingManager *ratingManager = [RatingManager new];
        ride.driverRating = [ratingManager getAverageRatingFromArrayOfRatings:rideDetail[@"driver"][@"ratings"]];
        
        if (rideDetail != nil) {
           ride.trip = [self createTripObject:rideDetail[@"rideNo"] andIsDriver:NO];
        }
    }
    else {
        ride.isRequested = YES;
    }
    
    return ride;
}

#pragma Driver



- (void)getTripsForDriver:(NSInteger) tripViewType forDate:(NSDate *)reqDate withCompletionBlock:(void (^)(NSDictionary *trips, NSArray *tripDates, NSError *error))completionBlock {
    
    NSDate *dateStart = [self getStartDateForTripViewType:tripViewType forDate:reqDate];
    NSDate *dateEnd = [self getEndDateForTripViewType:tripViewType forDate:reqDate];
    
    _userInfo = [UserProfileInformation sharedMySingleton];
    NSMutableDictionary *dictOfDateAndTrips = [NSMutableDictionary new];
    NSMutableArray *arrayOfDatesInOrder = [NSMutableArray new];
    
    [self getTripsForDriver:dateStart andEndDate:dateEnd andTripViewType:tripViewType withACompletionBlock:^(NSArray *rideObjects, NSError *error) {
        if (error == nil) {
            // The find succeeded.
            Ride *ride;
            Ride *rideNext;
            //found a route
            int routeCount = 0;
            NSMutableArray *trips;
            while (routeCount < rideObjects.count) {
                ride = (Ride *)rideObjects[routeCount];
                if (routeCount + 1 != rideObjects.count) {
                     rideNext = (Ride *)rideObjects[routeCount+1];
                }
                
                if ((routeCount + 1 == rideObjects.count) || (![Helper isSameDay:ride.rideDate otherDay:rideNext.rideDate])) {
                    trips = [NSMutableArray new];
                    if ([Helper isSameDay:ride.rideDate otherDay:[NSDate date]]) {
                        if (![Helper isPastDay:ride.rideDate]) {
                            if (!ride.trip.hasEnded) {
                                ride.trip.isCurrent = YES;
                            }
                        }
                        else {
                            ride.trip.isCurrent = NO;
                        }
                    }
                    else {
                        ride.trip.isCurrent = NO;
                    }
                    [trips addObject:ride];
                  
                    routeCount++;
                }
                else {
                    //add 2 events
                    trips = [NSMutableArray new];
                    //add arrival and departure trips
                    if ([Helper isSameDay:ride.rideDate otherDay:[NSDate date]]) {
                        if ([Helper isSameDay:ride.rideDate otherDay:[NSDate date]]) {
                            if (![Helper isPastDay:ride.rideDate]) {
                                if (!ride.trip.hasEnded) {
                                    ride.trip.isCurrent = YES;
                                }
                            }
                            else {
                                ride.trip.isCurrent = NO;
                            }
                        }
                        else {
                            ride.trip.isCurrent = NO;
                        }
                    }
                    
                    [trips addObject:ride];

                    if ([Helper isSameDay:rideNext.rideDate otherDay:[NSDate date]]) {
                        if (ride.trip.isCurrent == NO) {
                            if (![Helper isPastDay:rideNext.rideDate]) {
                                if (!ride.trip.hasEnded) {
                                    ride.trip.isCurrent = YES;
                                }
                            }
                            else {
                                rideNext.trip.isCurrent = NO;
                            }
                        }
                        else {
                            rideNext.trip.isCurrent = NO;
                        }
                    }
                    
                    [trips addObject:rideNext];
                    routeCount =  routeCount + 2;
                }
                [arrayOfDatesInOrder addObject:[Helper getStringFromDate:ride.rideDate]];
                [dictOfDateAndTrips setObject:trips forKey:[Helper getStringFromDate:ride.rideDate]];
            }
            completionBlock(dictOfDateAndTrips, arrayOfDatesInOrder, error);
        }
        else {
            completionBlock(nil, nil, error);
        }
    }];
}



//get trips for driver - i.e get routes + rides for that route
- (void) getTripsForDriver:(NSDate *)dateStart andEndDate:(NSDate *)dateEnd andTripViewType:(NSInteger) tripViewType withACompletionBlock:(void (^)(NSArray *rides, NSError *error))completionBlockForArrivalTripsForDriver {
    
    PFQuery *query = [self createQueryForRoutesAndRidesForStartDate:dateStart andDateEnd:dateEnd andTripViewType:tripViewType];
    //get arrival trips
    
    if (_userInfo.driverDetails == nil) {
        completionBlockForArrivalTripsForDriver (nil, nil);
        return;
    }
    
    //query to get the routes
    [query findObjectsInBackgroundWithBlock:^(NSArray *routes, NSError *error) {
        if (!error) {
            NSMutableArray *trips = [NSMutableArray new];
            Ride *ride;
            for (PFObject *route in routes) {
                ride = [Ride new];
                ride.rideDate = route[@"rideDate"];
                ride.totalSeats = route[@"TotalSeats"];
                if ([route[@"routeType"] isEqualToString:ARRIVAL]) {
                    ride.routeType = ARRIVAL;
                }
                else {
                    ride.routeType = DEPARTURE;
                }
                
                if (route != nil) {
                   ride.trip = [self createTripObject:route andIsDriver:YES];
                }
               
                
                if (ride.trip.userDetails.count > 0) {
                    ride.isRequested = NO;
                }
                else {
                    ride.isRequested = YES;
                }
                
                if (ride.isRequested == NO && tripViewType == PendingForDriver) {
                    
                }
                else if (ride.isRequested == YES && tripViewType == ScheduledForDriver) {
                    
                }
                else {
                      [trips addObject:ride];
                }
            }
            completionBlockForArrivalTripsForDriver(trips, nil);
        }
        else {
            completionBlockForArrivalTripsForDriver(nil, error);
        }
    }];
}



- (Trip *)createTripObject:(PFObject *)route andIsDriver:(BOOL) isDriver {
    Trip *trip = [Trip new];
    trip.route = route;
    trip.tripDate = route[@"rideDate"];
    
    if (route[@"currentLocation"] != nil && route[@"hasStarted"] && !route[@"hasEnded"]) {
        trip.isCurrent = YES;
    }
    
    if (route[@"hasEnded"]) {
        trip.hasEnded = YES;
    }
    
    
    trip.driverDetails = route[@"driver"][@"userdetail"];
    trip.startAddress =  [NSString stringWithFormat:@"%@ %@ %@ %@",route[@"startAddress"], route[@"startCity"], route[@"startState"], route[@"startZip"]];
    trip.startLocation = route[@"startLocation"];
    trip.endAddress    = [NSString stringWithFormat:@"%@ %@ %@ %@",route[@"endAddress"], route[@"endCity"], route[@"endState"], route[@"endZip"]];
    trip.endLocation =   route[@"endLocation"];
    
    trip.wayPointStartAddress = [NSMutableArray new];
    trip.wayPointEndAddress = [NSMutableArray new];
    trip.rideObjects = [NSMutableArray new];
    trip.userDetails = [NSMutableArray new];
    
    UserDetail *userDetail = [UserDetail new];
    NSString *startAddress;
    NSString *endAddress;
    for(PFObject *ride in route[@"rides"]) {
        [trip.wayPointStartAddress addObject:ride[@"startLocation"]];
        [trip.wayPointEndAddress addObject:ride[@"endLocation"]];
        
        startAddress =  [NSString stringWithFormat:@"%@ %@ %@ %@",ride[@"startAddress"], ride[@"startCity"], ride[@"startState"], ride[@"startZip"]];
        endAddress    = [NSString stringWithFormat:@"%@ %@ %@ %@",ride[@"endAddress"], ride[@"endCity"], ride[@"endState"], ride[@"endZip"]];

        [trip.userDetails addObject:[userDetail createUserDetail:ride[@"rider"] withStartAddress:startAddress andEndAddress:endAddress andStartLocation:ride[@"startLocation"] andEndLocation:ride[@"endLocation"] andRideObject:ride andStartTime:ride[@"startTime"]
            andEndTime:ride[@"endTime"]]];
    }
    
    return trip;
}



#pragma Queries
//query for ride dates
- (PFQuery *)getQueryMyCurrentRideforDateAsRider:(NSDate *)date andRange:(enum RangeEnum) range {
    PFQuery *query = [PFQuery queryWithClassName:@"ride"];
    //decide arrival or departure based on time interval
    [query whereKey:@"rider" equalTo:[UserProfileInformation sharedMySingleton].user];
    [query whereKey:@"rideDate" greaterThan:[Helper getFirstSecondOfTheDay:date]];
    [query whereKey:@"rideDate" lessThan:[Helper lastSecondOfTheDay:date]];
    
    [query whereKey:@"cancelled" notEqualTo:[NSNumber numberWithBool:YES]];
    [query whereKey:@"isCancelledByDriver" notEqualTo:[NSNumber numberWithBool:YES]];
    [query whereKey:@"isEdited" notEqualTo:[NSNumber numberWithBool:YES]];
    
    [query includeKey:@"rideNo"];
    [query orderByAscending:@"rideDate"];
    return query;
}

-(PFQuery *)createQueryForRideForStartDate:(NSDate *)dateStart andDateEnd:(NSDate *)dateEnd andTripViewType:(NSInteger) tripViewType {
    
    _userInfo = [UserProfileInformation sharedMySingleton];
    PFQuery *query = [PFQuery queryWithClassName:@"ride"];
    [query whereKey:@"rider" equalTo:_userInfo.user];
    [query includeKey:@"payment"];
    [query includeKey:@"payment.paymentDetail"];
    [query includeKey:@"payment.driverDetail"];
    [query includeKey:@"payment.driverDetail.merchantAccount"];
    
    [query includeKey:@"driver"];
    [query includeKey:@"driver.userdetail"];
    [query includeKey:@"rideNo"];
    [query includeKey:@"rideNo.rides.rider"];
    
    [query whereKey:@"cancelled" notEqualTo:[NSNumber numberWithBool:YES]];
    [query whereKey:@"isCancelledByDriver" notEqualTo:[NSNumber numberWithBool:YES]];
    [query whereKey:@"isEdited" notEqualTo:[NSNumber numberWithBool:YES]];
    
    [query whereKey:@"rideDate" greaterThan:dateStart];
    [query whereKey:@"rideDate" lessThan:dateEnd];

    
    if (tripViewType == PendingForRider) {
        [query whereKeyDoesNotExist:@"rideNo"];
    }
    if (tripViewType == ScheduledForRider) {
        [query whereKeyExists:@"rideNo"];
    }
    
    [query orderByAscending:@"rideDate"];
    
    return query;
}

-(PFQuery *)createQueryForRoutesAndRidesForStartDate:(NSDate *)dateStart andDateEnd:(NSDate *)dateEnd andTripViewType:(NSInteger)tripViewType {
    
     _userInfo = [UserProfileInformation sharedMySingleton];
    
    if (!_userInfo.driverDetails) {
        return nil;
    }
    PFQuery *query = [PFQuery queryWithClassName:@"routes"];
    [query whereKey:@"driver" equalTo:_userInfo.driverDetails];
    [query includeKey:@"driver"];
    [query includeKey:@"driver.userdetail"];
    [query includeKey:@"rides"];
    [query includeKey:@"rides.rider"];
    
    [query includeKey:@"rides.payment"];
    [query includeKey:@"rides.payment.paymentDetail"];
    [query includeKey:@"rides.payment.driverDetail"];
    [query includeKey:@"rides.payment.driverDetail.merchantAccount"];
    
    [query whereKey:@"isCancelled" notEqualTo:[NSNumber numberWithBool:YES]];
    [query whereKey:@"isEdited" notEqualTo:[NSNumber numberWithBool:YES]];
    
    [query whereKey:@"rideDate" greaterThan:dateStart];
    [query whereKey:@"rideDate" lessThan:dateEnd];
    
    
    if (tripViewType == PendingForDriver) {
        [query whereKeyDoesNotExist:@"rides"];
    }
    
    if (tripViewType == ScheduledForRider) {
        [query whereKeyExists:@"rides"];
    }
    
    [query orderByAscending:@"rideDate"];
    
    return query;
}

- (PFQuery *)createQueryForFutureDriverRoutes {
    
    _userInfo = [UserProfileInformation sharedMySingleton];
    PFQuery *query = [PFQuery queryWithClassName:@"routes"];
    [query whereKey:@"driver" equalTo:_userInfo.driverDetails];
    
    [query whereKey:@"isCancelled" notEqualTo:[NSNumber numberWithBool:YES]];
    [query whereKey:@"isEdited" notEqualTo:[NSNumber numberWithBool:YES]];
    
    [query whereKey:@"rideDate" greaterThan:[Helper getFirstSecondOfTheDay:[NSDate date]]];
    [query orderByAscending:@"rideDate"];
    
    return query;
}


#pragma Helpers
- (NSDate *) getStartDateForDate:(NSDate *)reqDate andRange:(enum RangeEnum) dateRange {
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    NSDate *dateStart;
    int daysToAdd = -1;
    
    if (dateRange == Month || dateRange == MonthAndHalf ) {
        daysToAdd = -15;
        [components setDay:daysToAdd];
        dateStart = [gregorian dateByAddingComponents:components toDate:reqDate options:0];
        
    }
    else if (dateRange == Week || dateRange == Day) {
        //getting trips weekly for TripView
        //set up startDate
        //for tripview this should be one day less than the date passed
        daysToAdd = -1;
        [components setDay:daysToAdd];
        dateStart = [gregorian dateByAddingComponents:components toDate:reqDate options:0];
    }
    else {
        //for todays trip get only the trips after datetime now;
        dateStart = [gregorian dateByAddingComponents:components toDate:[NSDate date] options:0];
    }
    
    return dateStart;
}

- (NSDate *) getEndDateForDate:(NSDate *)reqDate andRange:(enum RangeEnum) dateRange {
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    NSDate *dateEnd;
    int daysToAdd;
    
    if (dateRange == Month) {
        daysToAdd = 15;
        [components setDay:daysToAdd];
        dateEnd = [gregorian dateByAddingComponents:components toDate:reqDate options:0];    }
    else if (dateRange == Week) {
        //getting trips weekly for TripView
        //set up startDate
        //for tripview this should be one day less than the date passed
        daysToAdd = 7;
        [components setDay:daysToAdd];
        dateEnd = [gregorian dateByAddingComponents:components toDate:reqDate options:0];
    }
    else if (dateRange == MonthAndHalf) {
        daysToAdd = 30;
        [components setDay:daysToAdd];
        dateEnd = [gregorian dateByAddingComponents:components toDate:reqDate options:0];
    }
    else {
        daysToAdd = 1;
        [components setDay:daysToAdd];
        dateEnd = [gregorian dateByAddingComponents:components toDate:reqDate options:0];
    }
    return dateEnd;
}


- (NSDate *)getStartDateForTripViewType:(NSInteger) tripViewType forDate:(NSDate *)reqDate {
    //assigning tripViewType = 1
    NSDate *startDate;
    if (tripViewType == Agenda) {
        if ([Helper isCurrentMonth:reqDate]) {
            startDate = [Helper getFirstSecondOfTheDay:[NSDate date]];
        }
        else {
            startDate = [Helper firstSecondOfTheMonth:reqDate];
        }
    }
    else if (tripViewType == Past) {
        //startDate = [self getStartDateForDate:[NSDate date] andRange:MonthAndHalf];
        startDate = [Helper firstSecondOfTheMonth:reqDate];
    }
    else if (tripViewType == PendingForDriver || tripViewType == PendingForRider || tripViewType == ScheduledForDriver || tripViewType == ScheduledForRider) {
        if ([Helper isCurrentMonth:reqDate]) {
            startDate = [Helper getFirstSecondOfTheDay:[NSDate date]];
        }
        else {
            startDate = [Helper firstSecondOfTheMonth:reqDate];
        }
    }
    else { //calendarViewType
        startDate = [Helper firstSecondOfTheMonth:reqDate];
        //startDate = [self getStartDateForDate:reqDate andRange:Month];
    }
    return startDate;
}



- (NSDate *)getEndDateForTripViewType:(NSInteger) tripViewType forDate:(NSDate *)reqDate {
    NSDate *endDate;
    //assigning tripViewType = 1
    if (tripViewType == Agenda) {
        
        endDate = [Helper lastSecondOfTheMonth:reqDate];
    }
    else if (tripViewType == Past) {
        
        if ([Helper isCurrentMonth:reqDate]) {
            endDate = [Helper getFirstSecondOfTheDay:[NSDate date]];;
        }
        else {
            endDate = [Helper firstSecondOfTheMonth:reqDate];
        }
       // endDate = [NSDate date];
    }
    else if (tripViewType == PendingForDriver || tripViewType == ScheduledForDriver) {
        //endDate = [self getEndDateForDate:reqDate andRange:MonthAndHalf];
        endDate =  [Helper lastSecondOfTheMonth:reqDate];
    }
    else if (tripViewType == PendingForRider || tripViewType == ScheduledForRider) {
       // endDate = [self getEndDateForDate:reqDate andRange:Month];
        endDate =  [Helper lastSecondOfTheMonth:reqDate];
    }
    else if (tripViewType == Calendar) {
        endDate = [Helper lastSecondOfTheMonth:reqDate];
    }
    else { //calendarViewType
        endDate = [self getEndDateForDate:reqDate andRange:Month];
    }
    return endDate;
}

- (void)getMyTop5LatestPickUpLocationsAsDriverWithCompletionBlock:(void (^)(NSArray *locations, NSError *error))completionBlock {
    
    [self getTop5LocationsForDriversForPickUp:YES WithCompletionBlock:^(NSArray *locations, NSError *error) {
        if (locations) {
            completionBlock (locations, nil);
        }
        else {
            completionBlock (nil, error);
        }
    }];
}

- (void)getTop5LocationsForDriversForPickUp:(BOOL)isPickUp WithCompletionBlock:(void (^)(NSArray *locations, NSError *error))completionBlock {
    PFQuery *query = [self getQueryForTop5SearchesForDriver];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *routes, NSError *error) {
        if (routes) {
            NSMutableDictionary *dictOfLocations = [NSMutableDictionary new];
            NSString *address;
            for (PFObject *route in routes) {
                
                if (isPickUp) {
                     address = [NSString stringWithFormat:@"%@ %@ %@ %@",route[@"startAddress"], route[@"startCity"], route[@"startState"], route[@"startZip"]];
                }
                else {
                     address = [NSString stringWithFormat:@"%@ %@ %@ %@",route[@"endAddress"], route[@"endCity"], route[@"endState"], route[@"endZip"]];
                }
               
                [dictOfLocations setObject:address forKey:address];
                
            }
            completionBlock (dictOfLocations.allKeys, nil);
        }
        else {
            if (error) {
                NSLog(@"Error in getting the top 5 driver locations %@", error);
            }
            completionBlock (nil, error);
        }
    }];

}

- (void)getTop5LocationsForRidersForPickUp:(BOOL)isPickUp WithCompletionBlock:(void (^)(NSArray *locations, NSError *error))completionBlock {
    PFQuery *query = [self getQueryForTop5SearchesForRider];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *rides, NSError *error) {
        if (rides) {
            NSMutableDictionary *dictOfLocations = [NSMutableDictionary new];
            NSString *address;
            for (PFObject *ride in rides) {
                
                if (isPickUp) {
                    address = [NSString stringWithFormat:@"%@ %@ %@ %@",ride[@"startAddress"], ride[@"startCity"], ride[@"startState"], ride[@"startZip"]];
                }
                else {
                    address = [NSString stringWithFormat:@"%@ %@ %@ %@",ride[@"endAddress"], ride[@"endCity"], ride[@"endState"], ride[@"endZip"]];
                }
                
                [dictOfLocations setObject:address forKey:address];
                
            }
            completionBlock (dictOfLocations.allKeys, nil);
        }
        else {
            if (error) {
                NSLog(@"Error in getting the top 5 driver locations %@", error);
            }
            completionBlock (nil, error);
        }
    }];
    
}


- (void)getMyTop5LatestPickUpLocationsAsRiderWithCompletionBlock:(void (^)(NSArray *locations, NSError *error))completionBlock {
    [self getTop5LocationsForRidersForPickUp:YES WithCompletionBlock:^(NSArray *locations, NSError *error) {
        if (locations) {
            completionBlock (locations, nil);
        }
        else {
            completionBlock (nil, error);
        }
    }];
}

- (void)getMyTop5LatestDropOffLocationsAsDriverWithCompletionBlock:(void (^)(NSArray *locations, NSError *error))completionBlock {
    
    [self getTop5LocationsForDriversForPickUp:NO WithCompletionBlock:^(NSArray *locations, NSError *error) {
        if (locations) {
            completionBlock (locations, nil);
        }
        else {
            completionBlock (nil, error);
        }
    }];
}

- (void)getMyTop5LatestDropOffLocationsAsRiderWithCompletionBlock:(void (^)(NSArray *locations, NSError *error))completionBlock {
    [self getTop5LocationsForRidersForPickUp:NO WithCompletionBlock:^(NSArray *locations, NSError *error) {
        if (locations) {
            completionBlock (locations, nil);
        }
        else {
            completionBlock (nil, error);
        }
    }];
}

- (PFQuery *)getQueryForTop5SearchesForDriver {
    
    _userInfo.driverDetails = [UserProfileInformation sharedMySingleton].driverDetails;
    PFQuery *query = [PFQuery queryWithClassName:@"route"];
    [query whereKey:@"driver" equalTo:_userInfo.driverDetails];
    [query orderByDescending:@"createdAt"];
    query.limit = 10;
    return query;
    
}

- (PFQuery *)getQueryForTop5SearchesForRider {
    
    _userInfo.user = [UserProfileInformation sharedMySingleton].user;
    PFQuery *query = [PFQuery queryWithClassName:@"ride"];
    [query whereKey:@"rider" equalTo:_userInfo.user];
    [query orderByDescending:@"createdAt"];
    query.limit = 10;
    return query;
    
}


@end



