//
//  DriverManager.h
//  MuV
//
//  Created by SushmaN on 1/14/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DriverManager : NSObject

- (double)getDriverFare;

@end
