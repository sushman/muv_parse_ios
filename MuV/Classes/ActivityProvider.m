//
//  ActivityProvider.m
//  MuV
//
//  Created by SushmaN on 6/17/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "ActivityProvider.h"
#import "CouponManager.h"

@implementation ActivityProvider {
    
}

- (id) activityViewController:(UIActivityViewController *)activityViewController
          itemForActivityType:(NSString *)activityType {
    
    if ( [activityType isEqualToString:UIActivityTypePostToTwitter] )
        return [NSString stringWithFormat:@"Best #commute experience today with the #MüV #app download free iOS in @AppStore Use this coupon:%@ #ridesharing @letsmuvtogether",[CouponManager sharedInstance].couponName];
    else if ([activityType isEqualToString:@"Gmail"])
        return [NSString stringWithFormat:@"Best #commute experience today with the #MüV #app download free iOS in @AppStore Use this coupon:%@ #ridesharing @letsmuvtogether",[CouponManager sharedInstance].couponName];
    else if ( [activityType isEqualToString:UIActivityTypePostToFacebook] )
        return [NSString stringWithFormat: @"I thought you’d like to know about MüV – a safe, reliable and affordable ride share service that offers a great alternative for your daily commute.  Here is a coupon code to use when you MüV: \n%@\n Download the free app \nhttp://www.muvtogether.com/",[CouponManager sharedInstance].couponName];
    else if ( [activityType isEqualToString:UIActivityTypeMessage] )
        return [NSString stringWithFormat: @"I thought you’d like to know about MüV – a safe, reliable and affordable ride share service that offers a great alternative for your daily commute.  Here is a coupon code to use when you MüV: \n%@\n Download the free app \nhttp://www.muvtogether.com/",[CouponManager sharedInstance].couponName];
    else if ( [activityType isEqualToString:UIActivityTypeMail] )
        return [NSString stringWithFormat: @"I thought you’d like to know about MüV – a safe, reliable and affordable ride share service that offers a great alternative for your daily commute.  Here is a coupon code to use when you MüV: \n%@\n Download the free app \nhttp://www.muvtogether.com/",[CouponManager sharedInstance].couponName];
    else {
         return [NSString stringWithFormat: @"I thought you’d like to know about MüV – a safe, reliable and affordable ride share service that offers a great alternative for your daily commute.  Here is a coupon code to use when you MüV: \n%@\n Download the free app \nhttp://www.muvtogether.com/",[CouponManager sharedInstance].couponName];
    }
    return nil;
}


- (id) activityViewControllerPlaceholderItem:(UIActivityViewController *)activityViewController {
    return @"Get the MüV App";
}


- (id) activityViewController:(UIActivityViewController *)activityViewController
          subjectForActivityType:(NSString *)activityType {
    return @"Get the MüV App";

}
@end