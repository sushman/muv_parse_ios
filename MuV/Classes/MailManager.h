//
//  MailManager.h
//  MuV
//
//  Created by Hing Huynh on 2/5/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserProfileInformation.h"

@interface MailManager : NSObject
@property (nonatomic, strong) UserProfileInformation *userInfo;

// SENDS MESSAGE TO ADMIN WHEN DRIVER CANCELS WITH RIDERS
- (void) sendDriverCancelsAdminAlertWithRoute:(PFObject *)route;

// SEND WHEN RIDER HITS CANCEL FOR A RIDE DAY
- (void) sendDriverNotificationForRiderCancelationWithRide:(PFObject *)ride;

// ANY ROUTE OBJECT FOR ROUND TRIP IS FINE I JUST NEED DATE FROM RIDE
- (void) sendDriverSchedulesMUVConfirmationWithRoute:(PFObject *)route isArrive:(BOOL)arrival isDepart:(BOOL)departure;

// ANY RIDE OBJECT FOR ROUND TRIP IS FINE I JUST NEED DATE FROM RIDE
- (void)sendRideConfirmationForRiderWithRide:(PFObject *)ride
                                    isArrive:(BOOL)arrival
                                    isDepart:(BOOL)departure
                         withCompletionBlock:(void (^)(BOOL succeeded, NSError *error))completionBlock;

// NO RIDERS FOR ROUTE
- (void) sendRideCancelingByDriver1WithRoute:(PFObject *)route;

// HAS RIDERS GETS CHARGED CANCELATION FEE
- (void) sendRideCancelingByDriver2WithRoute:(PFObject *)route;

// THIS ONE IS FOR WHEN DRIVER IS NO SHOW?
//- (void) sendRideCancelingByDriver3WithRoute:(PFObject *)route;

- (void) sendRiderCancelsMuVGetsRefundWithRide:(PFObject *)ride;

// DRIVER SENDS WHEN HE HITS NO SHOW
- (void) sendRiderNoShowWithRide:(PFObject *)ride;

// SEND RIDERS WHEN DRIVER CANCELS
- (void) sendRiderNotificationWhenDriverCancelsWithRoute:(PFObject *)route;

// DRIVER SENDS ON END MOVE FOR PASSENGER
- (void) sendRiderTripCompletionRoute:(PFObject *)ride;

// RIDER WANTS TO BECOME A DRIVER SEND TO ADMIN
- (void) sendAdminWhenRiderWantsToBecomeADriverWithUser:(PFObject *)user;

- (void)sendDriverNotificationForRiderCancelationWithRides:(PFObject *)arrivalRide
                                             DepartureRide:(PFObject *)departureRide
                                                 isArrival:(BOOL)isArrival
                                               isDeparture:(BOOL)isDeparture;



/// NEW ONES
- (void) sendRiderCancelsMuVGetsChargedFiveWithRide:(PFObject *)ride;

- (void) sendRiderCancelsMuVDoesNotGetRefundWithRide:(PFObject *)ride;

// RIDER REQUESTS RECEIPT
- (void) sendRiderRequestsReceiptWithRide:(PFObject *)ride;
@end
