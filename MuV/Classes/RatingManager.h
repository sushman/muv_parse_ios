//
//  RatingManager.h
//  MuV
//
//  Created by Hing Huynh on 2/25/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Ride.h"

@interface RatingManager : NSObject

-(void) createRatingWithReview:(NSString *)review andRating:(NSNumber *)rating ForRide:(Ride *)ride;

-(NSNumber *) getAverageRatingFromArrayOfRatings:(NSArray *)ratings;

@end
