//
//  ActivityViewCustomActivity.m
//  MuV
//
//  Created by SushmaN on 6/17/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "ActivityViewCustomActivity.h"
#import "CouponManager.h"

@implementation ActivityViewCustomActivity

- (NSString *)activityType {
    return @"Gmail";
}

- (NSString *)activityTitle {
    return @"Gmail";
}

- (UIImage *)activityImage {
    // Note: These images need to have a transparent background and I recommend these sizes:
    // iPadShare@2x should be 126 px, iPadShare should be 53 px, iPhoneShare@2x should be 100
    // px, and iPhoneShare should be 50 px. I found these sizes to work for what I was making.
    
   
        return [UIImage imageNamed:@"gmail_icon.png"];
}

- (BOOL)canPerformWithActivityItems:(NSArray *)activityItems {
    NSLog(@"%s", __FUNCTION__);
    return YES;
}

- (void)prepareWithActivityItems:(NSArray *)activityItems {
    NSLog(@"%s",__FUNCTION__);
}

- (UIViewController *)activityViewController {
    NSLog(@"%s",__FUNCTION__);
    return nil;
}

- (void)performActivity {
    
    NSString *email = [NSString stringWithFormat:@"googlegmail:///co?subject=Get The MüV App&body=I thought you’d like to know about MüV – a safe, reliable and affordable ride share service that offers a great alternative for your daily commute.  Here is a coupon code to use when you MüV: \n%@\n Download the free app \nhttp://www.muvtogether.com/",[CouponManager sharedInstance].couponName];
    email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
    
    [self activityDidFinish:YES];
}

@end
