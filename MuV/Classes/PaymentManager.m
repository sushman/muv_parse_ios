//
//  PaymentManager.m
//  MuV
//
//  Created by Hing Huynh on 3/2/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "PaymentManager.h"
#import "UserProfileInformation.h"
#import <AFNetworking/AFNetworking.h>
#import "AdminManager.h"

@implementation PaymentManager

- (void)createPaymentWithPaymentDetail:(PFObject *)paymentDetail
                      andAmountCharged:(NSNumber *)amount
                   andCostWithNoCoupon:(NSNumber *)originalFareCost
                       andDriverDetail:(PFObject *)driverDetail
                   withCompletionBlock:(void (^)(PFObject *payment, NSError *error))completionBlock {
    
    UserProfileInformation *userInfo = [UserProfileInformation sharedMySingleton];
    
    PFObject *payment = [PFObject objectWithClassName:@"payment"];
    payment[@"amountToBeCharged"] = amount;
    payment[@"totalFare"] = originalFareCost;
    payment[@"userId"] = userInfo.user;
    payment[@"paymentDetail"] = paymentDetail;
    payment[@"driverDetail"] = driverDetail;
    
    [payment saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
        if (!error){
            completionBlock(payment, nil);
        } else {
            completionBlock(nil, error);
        }
    }];
}

- (PFObject *)createPaymentObject:(PFObject *)paymentDetail
           andAmountCharged:(NSNumber *)amount
        andCostWithNoCoupon:(NSNumber *)originalFareCost
            andDriverDetail:(PFObject *)driverDetail
                 andCouponId:(PFObject *)couponId
              andCouponAmount:(NSNumber *)couponAmount {

    UserProfileInformation *userInfo = [UserProfileInformation sharedMySingleton];
    
    PFObject *payment = [PFObject objectWithClassName:@"payment"];
    payment[@"amountToBeCharged"] = amount;
    payment[@"totalFare"] = originalFareCost;
    payment[@"userId"] = userInfo.user;
    payment[@"paymentDetail"] = paymentDetail;
    payment[@"driverDetail"] = driverDetail;
    if (couponId) {
        payment[@"couponId"] = couponId;
        payment[@"couponAmount"] = couponAmount;
    }
    
    return payment;
}

- (void)savePaymentObject:(NSArray *)paymentObjs
            withCompletionBlock:(void (^)(BOOL succeeded, NSError *error))completionBlock {
    
   [PFObject saveAllInBackground:paymentObjs block:^(BOOL succeeded,NSError *error) {
        if (!error) {
            completionBlock(succeeded , nil);
        } else {
            completionBlock(NO, error);
        }
    }];
}



- (void)chargePaymentWithRide:(PFObject *)ride withCompletionBlock:(void (^)(BOOL succeeded, NSError *error))completionBlock {
    
    // PAYMENT OBJECT
    PFObject *payment = ride[@"payment"];
    
    if (!payment) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Payment detail is not added" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }

    
    if (payment[@"driverDetail"][@"merchantAccount"] == [NSNull null] || payment[@"driverDetail"][@"merchantAccount"] == nil) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Merchant Bank Acoount not set up" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    // CHECKING IF PAYMENT HAS BEEN MADE BEFORE
    if (payment[@"transactionId"] == nil || payment[@"transactionId"] == [NSNull null]) {
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        AFHTTPResponseSerializer *serializer = [AFHTTPResponseSerializer serializer];
        
        serializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
        
        manager.responseSerializer = serializer;
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        
        AdminManager *adminManager = [AdminManager sharedInstance];
        
        NSNumber *driverPaymentPercentage =  adminManager.adminSettings.driverPaymentPercentage;
        NSNumber *serviceFeePercentage = @([@1 floatValue] - [driverPaymentPercentage floatValue]);
        
        NSNumber *amount = payment[@"amountToBeCharged"];
        NSNumber *totalFare = payment[@"totalFare"];

        NSNumber *serviceFee = @([amount floatValue] * [serviceFeePercentage floatValue]);
        serviceFee = [self convertToDollar:serviceFee];
        
        
        NSNumber *amountPaidToDriver = @([amount floatValue] - [serviceFee floatValue]);
        amountPaidToDriver  = [self convertToDollar:amountPaidToDriver];
        
        NSNumber *fullAmountOwedToDriver = @([totalFare floatValue] * [driverPaymentPercentage floatValue]);
        fullAmountOwedToDriver = [self convertToDollar:fullAmountOwedToDriver];
        
        NSNumber *remainingAmountOwedToDriver = @([fullAmountOwedToDriver floatValue] - [amountPaidToDriver floatValue]);
        remainingAmountOwedToDriver = [self convertToDollar:remainingAmountOwedToDriver];
        
        if ([amount floatValue] > 0 ) {
            [manager POST:@"http://muv2work.com/api/payserAddSubMerTran"
               parameters:@{                               @"MerchantAccountId":    payment[@"driverDetail"][@"merchantAccount"][@"subMerchantId"], //driver merchant id
                                                           @"customerid":           payment[@"userId"][@"customerId"],
                                                           @"amount":               amount,
                                                           @"ServiceFeeAmount":     serviceFee}
                  success:^(AFHTTPRequestOperation *operation, id responseObject) {
                      NSError* error = nil;
                      
                      NSString *responseText = [[NSString alloc] initWithData:responseObject encoding: NSASCIIStringEncoding];
                      
                      NSData* data = [responseText dataUsingEncoding:NSASCIIStringEncoding];
                      
                      NSArray* json = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       
                                       options:kNilOptions
                                       error:&error];
                      
                      NSDictionary *responseDict = (NSDictionary*)json;
                      
                     // NSLog(@"reponseDict %@", responseDict);
                     // NSLog(@"id data = %@",responseDict[@"Target"][@"Id"]);
                      NSString *message = responseDict[@"Message"];
                     // NSLog(@"message data = %@",message);
                      
                      if ([message isKindOfClass:[NSNull class]]){
                          // UPDATING PAYMENT OBJECT WITH AMOUNTS
                          payment[@"transactionId"] = responseDict[@"Target"][@"Id"];
                          payment[@"AmountCharged"] = responseDict[@"Target"][@"Amount"];
                          payment[@"amountPaidToDriver"] = amountPaidToDriver;
                          payment[@"serviceFeePaid"] = responseDict[@"Target"][@"ServiceFeeAmount"];
                          
                          // ADD DRIVER CREDIT IF NECESSARY
                          if (![totalFare isEqualToNumber:amount]){
                              NSNumber *currentCreditAmount = payment[@"driverDetail"][@"driverCreditBalance"];
                              if (currentCreditAmount){
                                  payment[@"driverDetail"][@"driverCreditBalance"] = @([currentCreditAmount floatValue] + [remainingAmountOwedToDriver floatValue]);
                              } else {
                                  payment[@"driverDetail"][@"driverCreditBalance"] = remainingAmountOwedToDriver;
                              }
                          }
                          
                          [payment saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
                              if (!error){
                                  completionBlock(YES, nil);
                              } else {
                                  completionBlock(NO, error);
                              }
                          }];
                      } else {
                          NSLog(@"error: %@", responseDict[@"Message"]);
                          completionBlock(NO, responseDict[@"Message"]);
                      }
                  }
                  failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                     // NSLog(@"Error is %@", error.description);
                      completionBlock(NO, error);
                  }
             ];
        }
        else {
            payment[@"transactionId"] = @"Amount 0 - no transaction";
            payment[@"AmountCharged"] = amount;
            payment[@"amountPaidToDriver"] = amountPaidToDriver;
            payment[@"serviceFeePaid"] = amount;
            
            // ADD DRIVER CREDIT IF NECESSARY
            if (![totalFare isEqualToNumber:amount]){
                NSNumber *currentCreditAmount = payment[@"driverDetail"][@"driverCreditBalance"];
                if (currentCreditAmount){
                    payment[@"driverDetail"][@"driverCreditBalance"] = @([currentCreditAmount floatValue] + [remainingAmountOwedToDriver floatValue]);
                } else {
                    payment[@"driverDetail"][@"driverCreditBalance"] = remainingAmountOwedToDriver;
                }
            }
            
            [payment saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
                if (!error){
                    completionBlock(YES, nil);
                } else {
                    completionBlock(NO, error);
                }
                
            }];
        }
        
    } else {
        completionBlock(NO, @"Transaction has already been created");
    }
    
}


- (void)createCancellationChargeWithRide:(PFObject *)ride withCompletionBlock:(void (^)(BOOL succeeded, NSError *error))completionBlock {
    
    // PAYMENT OBJECT
    PFObject *payment = ride[@"payment"];
    
    if (!payment) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Payment detail is not added" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    if (payment[@"driverDetail"][@"merchantAccount"] == [NSNull null] || payment[@"driverDetail"][@"merchantAccount"] == nil) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Merchant Bank Acoount not set up" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    // CHECKING IF PAYMENT HAS BEEN MADE BEFORE
    if (!payment[@"transactionId"]){
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        AFHTTPResponseSerializer *serializer = [AFHTTPResponseSerializer serializer];
        
        serializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
        
        manager.responseSerializer = serializer;
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        
        AdminManager *adminManager = [AdminManager sharedInstance];
        
        NSNumber *driverPaymentPercentage =  adminManager.adminSettings.driverPaymentPercentage;
        NSNumber *serviceFeePercentage = @([@1 floatValue] - [driverPaymentPercentage floatValue]);
        
        NSNumber *cancellationFee = adminManager.adminSettings.cancellationFeeAmount;
        
        NSNumber *serviceFee = @([cancellationFee floatValue] * [serviceFeePercentage floatValue]);
        serviceFee = [self convertToDollar:serviceFee];
        NSLog(@"service fee: %@", serviceFee);
        
        NSNumber *amountPaidToDriver = @([cancellationFee floatValue] - [serviceFee floatValue]);
        amountPaidToDriver = [self convertToDollar:amountPaidToDriver];
        
        [manager POST:@"http://muv2work.com/api/payserAddSubMerTran"
           parameters:@{                               @"MerchantAccountId":    payment[@"driverDetail"][@"merchantAccount"][@"subMerchantId"],
                                                       @"customerid":           payment[@"userId"][@"customerId"],
                                                       @"amount":               cancellationFee,
                                                       @"ServiceFeeAmount":     serviceFee}
              success:^(AFHTTPRequestOperation *operation, id responseObject) {
                  NSError* error = nil;
                  
                  NSString *responseText = [[NSString alloc] initWithData:responseObject encoding: NSASCIIStringEncoding];
                  
                  NSData* data = [responseText dataUsingEncoding:NSASCIIStringEncoding];
                  
                  NSArray* json = [NSJSONSerialization
                                   JSONObjectWithData:data
                                   
                                   options:kNilOptions
                                   error:&error];
                  
             //     NSLog(@"json data = %@",json);
                  
                  NSDictionary *responseDict = (NSDictionary*)json;
                  
                  NSString *message = responseDict[@"Message"];
                  NSLog(@"message data = %@",message);
                  
                  if ([message isKindOfClass:[NSNull class]]){
                      // UPDATING PAYMENT OBJECT WITH AMOUNTS
                      NSLog(@"here");
                      payment[@"transactionId"] = responseDict[@"Target"][@"Id"];
                      payment[@"AmountCharged"] = responseDict[@"Target"][@"Amount"];
                      payment[@"amountPaidToDriver"] = amountPaidToDriver;
                      payment[@"serviceFeePaid"] = responseDict[@"Target"][@"ServiceFeeAmount"];
                      
                      [payment saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
                          if (!error){
                              completionBlock(YES, nil);
                          } else {
                              completionBlock(NO, error);
                          }
                          
                      }];
                  } else {
                      NSLog(@"error: %@", responseDict[@"Message"]);
                      completionBlock(NO, responseDict[@"Message"]);
                      
                  }
              }
              failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  completionBlock(NO, error);
              }];
        
    } else {
        
        completionBlock(NO, @"Transaction has already been created");
    }
    
}

- (void)addVenmoAccountWithFirstName:(NSString *)firstName
                            lastName:(NSString *)lastName
                               email:(NSString *)email
                         dateOfBirth:(NSString *)dateOfBirth
                                 ssn:(NSString *)taxId
                       streetAddress:(NSString *)streetAddress
                                city:(NSString *)city
                               state:(NSString *)state
                             zipCode:(NSString *)zipCode
                       subMerchantId:(NSString *)subMerchantId {
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    AFHTTPResponseSerializer *serializer = [AFHTTPResponseSerializer serializer];
    
    serializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = serializer;
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager POST:@"http://muv2work.com/api/payserAddSMerchantVenmo"
        parameters:@{
                     @"FirstName":      firstName,
                     @"LastName":       lastName,
                     @"Email":          email,
                     @"DateOfBirth":    dateOfBirth,
                     @"Ssn":            taxId,
                     @"StreetAddress":  streetAddress,
                     @"Locality":       city,
                     @"Region":         state,
                     @"PostalCode":     zipCode,
                     @"TosAccepted":    @"true",
                     @"submerchantid": subMerchantId
                }
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSError* error = nil;
              
              NSString *responseText = [[NSString alloc] initWithData:responseObject encoding: NSASCIIStringEncoding];
              
              NSData* data = [responseText dataUsingEncoding:NSASCIIStringEncoding];
              
              NSArray* json = [NSJSONSerialization
                               JSONObjectWithData:data
                               
                               options:kNilOptions
                               error:&error];
              
         //     NSLog(@"json data = %@",json);
              NSDictionary *responseDict = (NSDictionary*)json;
              
              
              if (![responseDict[@"Message"]isKindOfClass:[NSNull class]]) {
                  
                  PFObject *merchantDetail = [PFObject objectWithClassName:@"merchantDetail"];
                  
                  merchantDetail[@"accountType"] = @"Direct Bank";
                 // merchantDetail[@"lastFourDigits"] = [_accountNumberTextField.text substringFromIndex: [_accountNumberTextField.text length] - 4];
                  merchantDetail[@"subMerchantId"] = [UserProfileInformation sharedMySingleton].driverDetails.objectId;
                  
                  [merchantDetail saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
                      [UserProfileInformation sharedMySingleton].driverDetails[@"merchantAccount"] = merchantDetail;
                      [[UserProfileInformation sharedMySingleton].driverDetails saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
                     //     [_paymentAlertDelegate updatePaymentMethodsWithCardandSender:nil];
                     //     [[self navigationController] popViewControllerAnimated:YES];
                      }];
                  }];
                  
              } else {
                  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                  message:@"Invalid Inputs, please try again."
                                                                 delegate:nil
                                                        cancelButtonTitle:nil
                                                        otherButtonTitles:@"Ok", nil];
                  [alert show];
              }
          }
     
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              // Handle failure communicating with your server
              NSLog(@"error: %@", error);
          }];
}




- (NSNumber *)convertToDollar:(NSNumber *)number{
    NSString *string = [NSString stringWithFormat:@"%0.2f", [number floatValue]];
    return @([string floatValue]);
}

@end
