//
//  Ride.h
//  MuV
//
//  Created by SushmaN on 1/6/15.
//  Copyright (c) 2015 SushmaN. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>
#import "Trip.h"

@interface Ride : NSObject




@property (nonatomic, assign) bool isRequested;

@property (nonatomic, strong) PFObject *rideNo;
@property (nonatomic, strong) NSString *fare;
@property (nonatomic, strong) NSString *totalFare;
@property (nonatomic, strong) PFObject *driverDetail;
@property (nonatomic, strong) PFObject *userDetail;
@property (nonatomic, strong) NSNumber *mileage;
@property (nonatomic, strong) NSDate *rideDate;
@property (nonatomic, assign) BOOL isRoundTrip;

@property (nonatomic, strong) NSDate *startTime;
@property (nonatomic, strong) NSDate *endTime;

@property (nonatomic, strong) NSString *startAddress;
@property (nonatomic, strong) NSString *startAddress1;
@property (nonatomic, strong) NSString *startCity;
@property (nonatomic, strong) NSString *startState;
@property (nonatomic, strong) NSString *startZip;
//storing latitude / longitude as (latitude, longitude)
@property (nonatomic, strong) NSString *startLocation;

@property (nonatomic, assign) BOOL hasBeenRated;



@property (nonatomic, strong) NSString *endAddress;
@property (nonatomic, strong) NSString *endAddress1;
@property (nonatomic, strong) NSString *endCity;
@property (nonatomic, strong) NSString *endState;
@property (nonatomic, strong) NSString *endZip;
//storing latitude / longitude as (latitude, longitude)
@property (nonatomic, strong) NSString *endLocation;
@property (nonatomic, strong) NSString *routeType;


@property (nonatomic, strong) NSString *dateSelected;
@property (nonatomic, strong) NSString *arriveTime;
@property (nonatomic, strong) NSString *departTime;
@property (nonatomic, strong) NSString *driverName;
@property (nonatomic, strong) NSMutableArray *passengers;

@property (nonatomic, strong) NSNumber *totalSeats;
@property (nonatomic, strong) NSDecimalNumber *totalFareCost;
@property (nonatomic, strong) Trip *trip;

@property (nonatomic, strong) NSArray *searchResults;
@property (nonatomic, strong) NSNumber *driverRating;

@property (nonatomic, strong) PFObject *coupon;
@property (nonatomic, strong) PFObject *payment;

@end
