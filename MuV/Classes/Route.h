//
//  Route.h
//  MuV
//
//  Created by SushmaN on 1/23/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>

@interface Route : NSObject

@property (strong, nonatomic) PFObject *route;
@property (strong, nonatomic) NSString *startAddress;
@property (strong, nonatomic) NSString *endAddress;
@property (strong, nonatomic) NSMutableArray *wayPointStartAddress;
@property (strong, nonatomic) NSMutableArray *wayPointEndAddress;
@property (strong, nonatomic) NSMutableArray *userDetails;


@end
