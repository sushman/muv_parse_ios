//
//  MuVCouponsViewController.h
//  MuV
//
//  Created by SushmaN on 2/24/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MuVCouponsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic,strong) NSArray *inputArray;

@end
