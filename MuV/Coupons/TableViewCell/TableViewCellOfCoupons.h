//
//  TableViewCellOfCoupons.h
//  MuV
//
//  Created by SushmaN on 2/24/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewCellOfCoupons : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *couponName;
@property (weak, nonatomic) IBOutlet UILabel *couponAmount;
@property (weak, nonatomic) IBOutlet UILabel *numOfCoupons;

@end
