//
//  MuVCouponsViewController.m
//  MuV
//
//  Created by SushmaN on 2/24/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "MuVCouponsViewController.h"
#import "TableViewCellOfCoupons.h"
#import "CouponManager.h"
#import "Coupon.h"

#define AMOUNT @"amount"
#define PERCENTAGE @"percentage"

@interface MuVCouponsViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableViewOfCoupons;
@property (weak, nonatomic) IBOutlet UIView *noCouponView;

@end

@implementation MuVCouponsViewController
CouponManager *couponManager;
//NSArray *myCoupons;

- (void)viewDidLoad {
    [super viewDidLoad];
    _noCouponView.hidden = YES;
    
    CouponManager *couponManager = [CouponManager new];
    [couponManager findMyEarnedPromoAndAdminCoupons:NO WithCompletionBlock:^(NSArray *coupons, NSError *error) {
        _inputArray = coupons;
        
        if (_inputArray.count > 0) {
            _tableViewOfCoupons.dataSource = self;
            _tableViewOfCoupons.delegate = self;
            [_tableViewOfCoupons reloadData];
            _noCouponView.hidden = YES;
            _tableViewOfCoupons.hidden = NO;
        }
        else {
            _noCouponView.hidden = NO;
            _tableViewOfCoupons.hidden = YES;
        }
    }];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TableViewCellOfCoupons *newCell = (TableViewCellOfCoupons *)[tableView dequeueReusableCellWithIdentifier: @"couponCell" forIndexPath:indexPath];
    if (newCell != nil) {
        
        Coupon *coupon = [Coupon new];
        coupon = (Coupon *) _inputArray[indexPath.row];
        
        newCell.couponName.text = coupon.couponName;
        NSString *couponAmount;
        if ([coupon.couponType isEqualToString: AMOUNT]) {
            couponAmount = [NSString stringWithFormat:@"$ %@", coupon.couponAmount];
            newCell.numOfCoupons.text = coupon.numOfCoupons;
        }
        else {
            couponAmount = [NSString stringWithFormat:@"%@ %%", coupon.couponAmount];
            newCell.numOfCoupons.text = coupon.numOfCoupons;
        }
        
        newCell.couponAmount.text = couponAmount;
        
    }
    return newCell;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_inputArray count];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //  [_tableAlertDelegate tableView:self didSelectRowAtIndexPath:indexPath];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
    
}



@end
