//
//  PaymentDetailsTableViewCell.h
//  MuV
//
//  Created by Hing Huynh on 2/23/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaymentDetailsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *selectButton;
@property (weak, nonatomic) IBOutlet UIImageView *visaImageView;
@property (weak, nonatomic) IBOutlet UILabel *creditCardNumberLabel;
@property (weak, nonatomic) IBOutlet UIButton *deletButton;

@end
