//
//  MuVDriverPaymentMethodsViewController.h
//  MuV
//
//  Created by Hing Huynh on 2/23/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MuVDriverPaymentSetupViewController.h"

@interface MuVDriverPaymentMethodsViewController : UIViewController<DriverPaymentMethodsProtocol>

@end
