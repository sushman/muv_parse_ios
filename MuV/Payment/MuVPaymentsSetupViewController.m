//
//  MuVPaymentsSetupViewController.m
//  MuV
//
//  Created by Hing Huynh on 2/23/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "MuVPaymentsSetupViewController.h"
#import "TPKeyboardAvoidingScrollView.h"
#import <Braintree/Braintree.h>
#import <AFNetworking/AFNetworking.h>
#import "UserProfileInformation.h"
#import "UITextField+MuVTextFieldAdditions.h"
#import "UIButton+MuVUIButtonAdditions.h"

@interface MuVPaymentsSetupViewController ()

@property (weak, nonatomic) IBOutlet UITextField *zipCodeTextField;
@property (weak, nonatomic) IBOutlet UITextField *nameOnCardTextField;
@property (weak, nonatomic) IBOutlet UITextField *creditCardNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *monthTextField;
@property (weak, nonatomic) IBOutlet UITextField *yearTextField;
@property (weak, nonatomic) IBOutlet UITextField *cvvTextField;
@property (weak, nonatomic) IBOutlet UIButton *addPaymentButton;
@property (strong, nonatomic) Braintree *braintree;
@property (strong, nonatomic) UserProfileInformation *userInfo;

@end

@implementation MuVPaymentsSetupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [_scrollView contentSizeToFit];
    _userInfo = [UserProfileInformation sharedMySingleton];
    
    _zipCodeTextField = [UITextField muv_textFieldUpdate:_zipCodeTextField];
    _nameOnCardTextField = [UITextField muv_textFieldUpdate:_nameOnCardTextField];
    _creditCardNumberTextField = [UITextField muv_textFieldUpdate:_creditCardNumberTextField];
    _monthTextField = [UITextField muv_textFieldUpdate:_monthTextField];
    _yearTextField = [UITextField muv_textFieldUpdate:_yearTextField];
    _cvvTextField = [UITextField muv_textFieldUpdate:_cvvTextField];
    
    _addPaymentButton = [UIButton muv_buttonUpdate:_addPaymentButton];
    
//    self.creditCardNumberTextField.text = @"5555555555554444";
//    self.monthTextField.text = @"12";
//    self.yearTextField.text = @"2020";
}

- (void)viewDidUnload
{
    [self setScrollView:nil];
    [super viewDidUnload];
}

- (IBAction)addPaymentButtonTapped:(id)sender {
    
    [self getClientTokenWithCompletionBlock:^(NSString *clientToken, NSError *error){
        
        if (!error){
            _braintree = [Braintree braintreeWithClientToken:clientToken];
            
            BTClientCardRequest *request = [[BTClientCardRequest alloc] init];
            request.cvv = _cvvTextField.text;
            request.number = _creditCardNumberTextField.text;
            request.expirationMonth = _monthTextField.text;
            request.expirationYear = _yearTextField.text;
            //            request.postalCode = _zipCodeTextField.text;
            
            [self.braintree tokenizeCard:request
                              completion:^(NSString *nonce, NSError *error) {
                                  [self.navigationItem.rightBarButtonItem setEnabled:YES];
                                  if (error) {
                                      NSLog(@"Error: %@", error);
                                      [[[UIAlertView alloc] initWithTitle:@"Error"
                                                                  message:[error localizedDescription]
                                                                 delegate:nil
                                                        cancelButtonTitle:@"OK"
                                                        otherButtonTitles:nil] show];
                                  }
                                  
                                  if (nonce) {
                                      NSLog(@"Card tokenized -> Nonce Received: %@", nonce);
                                      
                                      // CHECKING IF customerId exists didn't work
                                      NSString *customerId = _userInfo.user[@"customerId"];
                                      if (customerId.length > 6){
                                          [self addCardToExistingCustomerIdWithNonce:nonce];
                                      } else {
                                          [self addCardWithNonce:nonce];
                                      }
                                      
                                  }
                              }];
        }
    }];
}


- (void) getClientTokenWithCompletionBlock:(void (^)(NSString *clientToken, NSError *error))completionBlock {
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    AFHTTPResponseSerializer *serializer = [AFHTTPResponseSerializer serializer];
    
    serializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = serializer;
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager GET:@"http://muv2work.com/api/payserSaveClient"
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             NSString *responseText = [[NSString alloc] initWithData:responseObject encoding: NSASCIIStringEncoding];
             NSString *stringWithoutQuotation = [responseText
                                                 stringByReplacingOccurrencesOfString:@"\"" withString:@""];
             // Setup braintree with responseObject[@"client_token"]
             completionBlock(stringWithoutQuotation, nil);
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             // Handle failure communicating with your server
             NSLog(@"error: %@", error);
             completionBlock(nil, error);
             
         }];
}


- (void) addCardWithNonce:(NSString *)nonce {
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    AFHTTPResponseSerializer *serializer = [AFHTTPResponseSerializer serializer];
    
    serializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = serializer;
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager POST:@"http://muv2work.com/api/payserSaveClient"
       parameters:@{ @"fname": _userInfo.user[@"firstName"],
                     @"lname": _userInfo.user[@"lastName"],
                     @"nonce":nonce}
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSError* error = nil;
              
              NSString *responseText = [[NSString alloc] initWithData:responseObject encoding: NSASCIIStringEncoding];
              
              NSData* data = [responseText dataUsingEncoding:NSASCIIStringEncoding];
              
              NSArray* json = [NSJSONSerialization
                               JSONObjectWithData:data
                               
                               options:kNilOptions
                               error:&error];
              
              //              NSLog(@"json data1 = %@",json);
              NSDictionary *responseDict = (NSDictionary*)json;
              if (![responseDict[@"cardToken"]isKindOfClass:[NSNull class]]) {
                  _userInfo.user[@"customerId"] = responseDict[@"customerId"];
                  PFObject *paymentDetail = [PFObject objectWithClassName:@"paymentDetail"];
                  paymentDetail[@"paymentType"] = @"credit card";
                  paymentDetail[@"nameOnCard"] = _nameOnCardTextField.text;
                  paymentDetail[@"lastFourDigits"] = [_creditCardNumberTextField.text substringFromIndex: [_creditCardNumberTextField.text length] - 4];
                  paymentDetail[@"cardToken"] = responseDict[@"cardToken"];
                  
                  NSString *cardType = responseDict[@"rslt"][@"Target"][@"CreditCards"][0][@"ImageUrl"];
                  
                  if ([cardType rangeOfString:@"visa"].location != NSNotFound) {
                      paymentDetail[@"cardType"] = @"visa";
                  } else if ([cardType rangeOfString:@"mastercard"].location != NSNotFound){
                      paymentDetail[@"cardType"] = @"mastercard";
                  } else if ([cardType rangeOfString:@"discover"].location != NSNotFound){
                      paymentDetail[@"cardType"] = @"discover";
                  } else if ([cardType rangeOfString:@"american_express"].location != NSNotFound){
                      paymentDetail[@"cardType"] = @"american_express";
                  } else if ([cardType rangeOfString:@"jcb"].location != NSNotFound){
                      paymentDetail[@"cardType"] = @"jcb";
                  } else {
                      paymentDetail[@"cardType"] = @"other";
                  }
                  
                  [paymentDetail saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
                      if (!error){
                          _userInfo.user[@"defaultToken"] = paymentDetail;
                          [_userInfo.user saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
                              [_paymentAlertDelegate updatePaymentMethodsWithCardandSender:nil];
                              [[self navigationController] popViewControllerAnimated:YES];
                          }];
                      }
                      
                  }];
                  
              } else {
                  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                  message:@"Invalid Card, please try again."
                                                                 delegate:nil
                                                        cancelButtonTitle:nil
                                                        otherButtonTitles:@"Ok", nil];
                  [alert show];
              }
          }
     
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              // Handle failure communicating with your server
              NSLog(@"error: %@", error);
          }];
}

- (void) addCardToExistingCustomerIdWithNonce:(NSString *)nonce {
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    AFHTTPResponseSerializer *serializer = [AFHTTPResponseSerializer serializer];
    
    serializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = serializer;
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager POST:@"http://muv2work.com/api/payserAddCard"
       parameters:@{ @"customerId": _userInfo.user[@"customerId"],
                     @"nonce":nonce}
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSError* error = nil;
              
              NSString *responseText = [[NSString alloc] initWithData:responseObject encoding: NSASCIIStringEncoding];
              
              NSData* data = [responseText dataUsingEncoding:NSASCIIStringEncoding];
              
              NSArray* json = [NSJSONSerialization
                               JSONObjectWithData:data
                               
                               options:kNilOptions
                               error:&error];
              
            //  NSLog(@"json data2 = %@",json);
              NSDictionary *responseDict = (NSDictionary*)json;
              
              if ([responseDict[@"Message"]isKindOfClass:[NSNull class]]){
                  PFObject *paymentDetail = [PFObject objectWithClassName:@"paymentDetail"];
                  paymentDetail[@"paymentType"] = @"credit card";
                  paymentDetail[@"lastFourDigits"] = [_creditCardNumberTextField.text substringFromIndex: [_creditCardNumberTextField.text length] - 4];
                  paymentDetail[@"cardToken"] = responseDict[@"Target"][@"Token"];
                  NSString *cardType = responseDict[@"Target"][@"ImageUrl"];
                  
                  if ([cardType rangeOfString:@"visa"].location != NSNotFound) {
                      paymentDetail[@"cardType"] = @"visa";
                  } else if ([cardType rangeOfString:@"mastercard"].location != NSNotFound){
                      paymentDetail[@"cardType"] = @"mastercard";
                  } else if ([cardType rangeOfString:@"discover"].location != NSNotFound){
                      paymentDetail[@"cardType"] = @"discover";
                  } else if ([cardType rangeOfString:@"american_express"].location != NSNotFound){
                      paymentDetail[@"cardType"] = @"american_express";
                  } else if ([cardType rangeOfString:@"jcb"].location != NSNotFound){
                      paymentDetail[@"cardType"] = @"jcb";
                  } else {
                      paymentDetail[@"cardType"] = @"other";
                  }
                  
                  [paymentDetail saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
                      if (!error){
                          _userInfo.user[@"defaultToken"] = paymentDetail;
                          [_userInfo.user addUniqueObject:paymentDetail forKey:@"paymentTokens"];
                          [_userInfo.user saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
                              [_paymentAlertDelegate updatePaymentMethodsWithCardandSender:nil];
                              [[self navigationController] popViewControllerAnimated:YES];
                          }];
                      }
                  }];
                  
              } else {
                  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                  message:@"Invalid Card, please try again."
                                                                 delegate:nil
                                                        cancelButtonTitle:nil
                                                        otherButtonTitles:@"Ok", nil];
                  [alert show];
              }
              
          }
     
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              // Handle failure communicating with your server
              NSLog(@"error: %@", error);
          }];
}

@end
