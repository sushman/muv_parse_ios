//
//  MuVPaymentsSetupViewController.h
//  MuV
//
//  Created by Hing Huynh on 2/23/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@class TPKeyboardAvoidingScrollView;

@protocol PaymentMethodsProtocol;


@interface MuVPaymentsSetupViewController : UIViewController

@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@property (assign, nonatomic) id <PaymentMethodsProtocol> paymentAlertDelegate;

@end

@protocol PaymentMethodsProtocol <NSObject>

- (void)updatePaymentMethodsWithCardandSender:(id)sender;

@end
