//
//  MuVPaymentMethodsViewController.m
//  MuV
//
//  Created by Hing Huynh on 2/23/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "MuVPaymentMethodsViewController.h"
#import "MuVPaymentsSetupViewController.h"
#import "UserProfileInformation.h"
#import "PaymentDetailsTableViewCell.h"
#import "UIButton+MuVUIButtonAdditions.h"

@interface MuVPaymentMethodsViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *addPaymentButton;
@property (strong, nonatomic) NSArray *paymentTokens;
@property (strong, nonatomic) UserProfileInformation *userInfo;
@property (strong, nonatomic) PFObject *defaultToken;
@end

@implementation MuVPaymentMethodsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _userInfo = [UserProfileInformation sharedMySingleton];
    if (_userInfo.user[@"defaultToken"]){
        _paymentTokens = @[_userInfo.user[@"defaultToken"]];
        _defaultToken = _userInfo.user[@"defaultToken"];
        [_addPaymentButton setTitle:@"Replace Credit Card" forState:UIControlStateNormal];
    }
    
    _addPaymentButton = [UIButton muv_buttonUpdate:_addPaymentButton];
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [_tableView reloadData];
    
    _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _paymentTokens.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PaymentDetailsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"PaymentDetailsTableViewCell" forIndexPath:indexPath];
    PFObject *paymentDetail = _paymentTokens[indexPath.row];
    cell.creditCardNumberLabel.text = [NSString stringWithFormat:@"Credit Card xxxx%@", paymentDetail[@"lastFourDigits"]];
    NSString *imageName = [NSString stringWithFormat:@"%@.png", paymentDetail[@"cardType"]];
    [cell.visaImageView setImage:[UIImage imageNamed:imageName]];
    [cell.selectButton setImage:[UIImage imageNamed:@"MuV_Button_Green"] forState:UIControlStateNormal];
 
    return cell;
    
}

//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//
//}

- (void)updatePaymentMethodsWithCardandSender:(id)sender {
    _paymentTokens = @[_userInfo.user[@"defaultToken"]];
    _defaultToken = _userInfo.user[@"defaultToken"];
    [_addPaymentButton setTitle:@"Replace Credit Card" forState:UIControlStateNormal];
    [_tableView reloadData];
    [_bookingsAlertDelegate updateBookingsWithPaymentDetails:_defaultToken andSender:sender];
}


- (IBAction)addPaymentButtonTapped:(id)sender {
    [self performSegueWithIdentifier:@"addPaymentSegue" sender:sender];
}

- (IBAction)deleteButtonTapped:(id)sender {
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:_tableView];
    NSIndexPath *indexPath = [_tableView indexPathForRowAtPoint:buttonPosition];
    
    PFObject *tokenToBeDeleted = _paymentTokens[indexPath.row];
    [_userInfo.user[@"paymentTokens"] removeObject:tokenToBeDeleted];
    [_userInfo.user removeObjectForKey: @"defaultToken"];
    _paymentTokens = @[];
    _defaultToken = nil;
    [_bookingsAlertDelegate updateBookingsWithPaymentDetails:nil andSender:sender];
    [_addPaymentButton setTitle:@"Add New Credit Card" forState:UIControlStateNormal];
    [_userInfo.user saveInBackground];
    [_tableView reloadData];
    
}

- (IBAction)selectButtonTapped:(id)sender {
    // get indexPath
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:_tableView];
    NSIndexPath *indexPath = [_tableView indexPathForRowAtPoint:buttonPosition];
    [_bookingsAlertDelegate updateBookingsWithPaymentDetails:_paymentTokens[indexPath.row] andSender:sender];
    [[self navigationController] popViewControllerAnimated:YES];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString: @"addPaymentSegue"]) {
        MuVPaymentsSetupViewController *paymentSetupViewController = [segue destinationViewController];
        paymentSetupViewController.paymentAlertDelegate = self;
    }
}
@end
