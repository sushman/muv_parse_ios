//
//  MuVDriverPaymentSetupViewController.h
//  MuV
//
//  Created by Hing Huynh on 2/23/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TPKeyboardAvoidingScrollView;
@protocol DriverPaymentMethodsProtocol;

@interface MuVDriverPaymentSetupViewController : UIViewController

@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;

@property (assign, nonatomic) id <DriverPaymentMethodsProtocol> paymentAlertDelegate;

@end

@protocol DriverPaymentMethodsProtocol <NSObject>

- (void)updatePaymentMethodsWithCardandSender:(id)sender;

@end

