//
//  MuVDriverPaymentSetupViewController.m
//  MuV
//
//  Created by Hing Huynh on 2/23/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "MuVDriverPaymentSetupViewController.h"
#import "UIButton+MuVUIButtonAdditions.h"
#import "TPKeyboardAvoidingScrollView.h"
#import <Braintree/Braintree.h>
#import <AFNetworking/AFNetworking.h>
#import "UserProfileInformation.h"
#import "PaymentManager.h"
#import "MuVSidePanelController.h"

@interface MuVDriverPaymentSetupViewController ()

@property (weak, nonatomic) IBOutlet UITextField *businessNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *addressTextField;
@property (weak, nonatomic) IBOutlet UITextField *cityTextField;
@property (weak, nonatomic) IBOutlet UITextField *stateTextField;
@property (weak, nonatomic) IBOutlet UITextField *zipCodeTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailAddressTextField;
@property (weak, nonatomic) IBOutlet UITextField *dateOfBirthTextField;
@property (weak, nonatomic) IBOutlet UITextField *routingNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *accountNumberTextField;
@property (weak, nonatomic) IBOutlet UIButton *addAccountButton;
@property (strong, nonatomic) Braintree *braintree;
@property (strong, nonatomic) UserProfileInformation *userInfo;
@property (strong, nonatomic) PFObject *merchantAccount;

@end

@implementation MuVDriverPaymentSetupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [_scrollView contentSizeToFit];
    _addAccountButton = [UIButton muv_buttonUpdate:_addAccountButton];
    
//    _firstNameTextField.text = @"Steve";
//    _lastNameTextField.text = @"Bourne";
//    _emailAddressTextField.text = @"stevebourne@gmail.com";
//    _dateOfBirthTextField.text = @"1988-11-19";
//    _taxIdTextField.text = @"456454561";
//    _addressTextField.text = @"2754 Coltwood Dr";
//    _cityTextField.text = @"San Jose";
//    _stateTextField.text = @"CA";
//    _zipCodeTextField.text = @"95148";
//    _businessNameTextField.text = @"Steve Freelance";
//    _routingNumberTextField.text = @"071101307";
//    _accountNumberTextField.text = @"1123581321";
    
    _userInfo = [UserProfileInformation sharedMySingleton];
    _merchantAccount = _userInfo.driverDetails[@"merchantAccount"];
   
        
    if ([_merchantAccount isKindOfClass:[PFObject class]]){
        [_addAccountButton setTitle:@"EDIT BANK ACCOUNT" forState:UIControlStateNormal];
        _accountNumberTextField.text = [NSString stringWithFormat:@" xxxx%@", _merchantAccount[@"lastFourDigits"]];
    }
}

- (void)viewDidUnload
{
    [self setScrollView:nil];
    [super viewDidUnload];
}

- (IBAction)addAccountButtonTapped:(id)sender {
    
    if ([_addAccountButton.titleLabel.text isEqualToString:@"EDIT BANK ACCOUNT"]) {
        _addAccountButton.enabled = YES;
        [self editBankAccount];
        
        
    }
    else {
        _addAccountButton.enabled = NO;
        [self addBankAccount];
    }
}

- (void)editBankAccount {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    AFHTTPResponseSerializer *serializer = [AFHTTPResponseSerializer serializer];
    
    serializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = serializer;
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager POST:@"http://muv2work.com/api/payserEditSMerchant"
       parameters:@{ @"FirstName":      _firstNameTextField.text,
                     @"LastName":       _lastNameTextField.text,
                     @"Email":          _emailAddressTextField.text,
                     @"DateOfBirth":    _dateOfBirthTextField.text,
                     @"Ssn":            @"",
                     @"StreetAddress":  _addressTextField.text,
                     @"Locality":       _cityTextField.text,
                     @"Region":         _stateTextField.text,
                     @"PostalCode":     _zipCodeTextField.text,
                     @"AccountNumber":  _accountNumberTextField.text,
                     @"RoutingNumber":  _routingNumberTextField.text,
                     @"TosAccepted":    @"true",
                     @"submerchantid": [UserProfileInformation sharedMySingleton].driverDetails.objectId
                     }
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSError* error = nil;
              
              NSString *responseText = [[NSString alloc] initWithData:responseObject encoding: NSASCIIStringEncoding];
              
              NSData* data = [responseText dataUsingEncoding:NSASCIIStringEncoding];
              
              NSArray* json = [NSJSONSerialization
                               JSONObjectWithData:data
                               
                               options:kNilOptions
                               error:&error];
              
         //     NSLog(@"json data = %@",json);
              NSDictionary *responseDict = (NSDictionary*)json;
              
              if ([responseDict[@"Errors"]isKindOfClass:[NSNull class]]) {
                  PFObject *merchantDetail = [PFObject objectWithClassName:@"merchantDetail"];
                  
                  merchantDetail[@"accountType"] = @"Direct Bank";
                  merchantDetail[@"lastFourDigits"] = [_accountNumberTextField.text substringFromIndex: [_accountNumberTextField.text length] - 4];
                  merchantDetail[@"subMerchantId"] = [UserProfileInformation sharedMySingleton].driverDetails.objectId;
                  
                  [merchantDetail saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
                      [UserProfileInformation sharedMySingleton].driverDetails[@"merchantAccount"] = merchantDetail;
                      [[UserProfileInformation sharedMySingleton].driverDetails saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
                          [_paymentAlertDelegate updatePaymentMethodsWithCardandSender:nil];
                          [[self navigationController] popViewControllerAnimated:YES];
                      }];
                  }];
                  [[self navigationController] popViewControllerAnimated:YES];
                  
              } else {
                  NSString *errorMsg;
                  if (![responseDict[@"Message"]isKindOfClass:[NSNull class]]) {
                      errorMsg = responseDict[@"Message"];
                  }
                  else {
                      errorMsg = @"Invalid inputs, please try again.";
                  }

                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                      message:errorMsg
                                                                     delegate:nil
                                                            cancelButtonTitle:nil
                                                            otherButtonTitles:@"Ok", nil];
                    [alert show];
                   _addAccountButton.enabled = YES;
              }
          }
     
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              // Handle failure communicating with your server
              NSLog(@"error: %@", error);
          }];
}

- (void)addBankAccount {
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    AFHTTPResponseSerializer *serializer = [AFHTTPResponseSerializer serializer];
    
    serializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = serializer;
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    NSLog(@"Driver id %@", [UserProfileInformation sharedMySingleton].driverDetails.objectId );
    [manager POST:@"http://muv2work.com/api/payserAddSMerchant"
       parameters:@{ @"FirstName":      _firstNameTextField.text,
                     @"LastName":       _lastNameTextField.text,
                     @"Email":          _emailAddressTextField.text,
                     //                     @"Phone":          @"",
                     @"DateOfBirth":    _dateOfBirthTextField.text,
                     @"Ssn":            @"",
                     @"StreetAddress":  _addressTextField.text,
                     @"Locality":       _cityTextField.text,
                     @"Region":         _stateTextField.text,
                     @"PostalCode":     _zipCodeTextField.text,
                     //                     @"Descriptor":     @"",
                     //                     @"fEmail":         @"",
                     //                     @"MobilePhone":    @"",
                     @"AccountNumber":  _accountNumberTextField.text,
                     @"RoutingNumber":  _routingNumberTextField.text,
                     @"TosAccepted":    @"true",
                     @"submerchantid": [UserProfileInformation sharedMySingleton].driverDetails.objectId
                     }
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSError* error = nil;
              
              NSString *responseText = [[NSString alloc] initWithData:responseObject encoding: NSASCIIStringEncoding];
              
              NSData* data = [responseText dataUsingEncoding:NSASCIIStringEncoding];
              
              NSArray* json = [NSJSONSerialization
                               JSONObjectWithData:data
                               
                               options:kNilOptions
                               error:&error];
              
              NSLog(@"json data = %@",json);
              NSDictionary *responseDict = (NSDictionary*)json;
              
               if ([responseDict[@"Errors"]isKindOfClass:[NSNull class]]) {
                  
                  PFObject *merchantDetail = [PFObject objectWithClassName:@"merchantDetail"];
                  
                  merchantDetail[@"accountType"] = @"Direct Bank";
                  merchantDetail[@"lastFourDigits"] = [_accountNumberTextField.text substringFromIndex: [_accountNumberTextField.text length] - 4];
                  merchantDetail[@"subMerchantId"] = [UserProfileInformation sharedMySingleton].driverDetails.objectId;
                  
                  [merchantDetail saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
                      [UserProfileInformation sharedMySingleton].driverDetails[@"merchantAccount"] = merchantDetail;
                      [[UserProfileInformation sharedMySingleton].driverDetails saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
                          [_paymentAlertDelegate updatePaymentMethodsWithCardandSender:nil];
                          //                          [[self navigationController] popViewControllerAnimated:YES];
                          MuVSidePanelController *sidePanelViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MuVSidePanelViewController"];
                          sidePanelViewController.centerPanel = [self.storyboard instantiateViewControllerWithIdentifier:@"recievePaymentsNav"];
                          [self presentViewController:sidePanelViewController animated:YES completion:nil];
                      }];
                  }];
              } else {
                  if ([responseDict[@"Message"] isEqualToString: @"Applicant merchant id is in use."]) {
                      PFObject *merchantDetail = [PFObject objectWithClassName:@"merchantDetail"];
                      
                      merchantDetail[@"accountType"] = @"Direct Bank";
                      merchantDetail[@"lastFourDigits"] = [_accountNumberTextField.text substringFromIndex: [_accountNumberTextField.text length] - 4];
                      merchantDetail[@"subMerchantId"] = [UserProfileInformation sharedMySingleton].driverDetails.objectId;
                      
                      [merchantDetail saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
                          [UserProfileInformation sharedMySingleton].driverDetails[@"merchantAccount"] = merchantDetail;
                          [[UserProfileInformation sharedMySingleton].driverDetails saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
                              [_paymentAlertDelegate updatePaymentMethodsWithCardandSender:nil];
                              //                          [[self navigationController] popViewControllerAnimated:YES];
                              MuVSidePanelController *sidePanelViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MuVSidePanelViewController"];
                              sidePanelViewController.centerPanel = [self.storyboard instantiateViewControllerWithIdentifier:@"recievePaymentsNav"];
                              [self presentViewController:sidePanelViewController animated:YES completion:nil];
                          }];
                      }];
                  }
                  else {
                      NSString *errorMsg;
                      if (![responseDict[@"Message"]isKindOfClass:[NSNull class]]) {
                          errorMsg = responseDict[@"Message"];
                      }
                      else {
                          errorMsg = @"Invalid inputs, please try again.";
                      }
        
                      UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                      message:errorMsg
                                                                     delegate:nil
                                                            cancelButtonTitle:nil
                                                            otherButtonTitles:@"Ok", nil];
                      _addAccountButton.enabled = YES;
                      [alert show];
                  }
              }
          }
     
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              // Handle failure communicating with your server
              NSLog(@"error: %@", error);
              UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Something went wrong, please contact MüV."
                                                             delegate:nil
                                                    cancelButtonTitle:nil
                                                    otherButtonTitles:@"Ok", nil];
              [alert show];

          }];
}


@end
