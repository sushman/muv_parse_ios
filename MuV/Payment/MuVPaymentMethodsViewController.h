//
//  MuVPaymentMethodsViewController.h
//  MuV
//
//  Created by Hing Huynh on 2/23/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MuVPaymentsSetupViewController.h"
#import <Parse/Parse.h>

@protocol UpdateBookingsProtocol;

@interface MuVPaymentMethodsViewController : UIViewController <PaymentMethodsProtocol>

@property (assign, nonatomic) id <UpdateBookingsProtocol> bookingsAlertDelegate;

@end

@protocol UpdateBookingsProtocol <NSObject>

- (void)updateBookingsWithPaymentDetails:(PFObject *)paymentDetail andSender:(id)sender;

@end