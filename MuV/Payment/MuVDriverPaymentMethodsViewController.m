//
//  MuVDriverPaymentMethodsViewController.m
//  MuV
//
//  Created by Hing Huynh on 2/23/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "MuVDriverPaymentMethodsViewController.h"
#import "UserProfileInformation.h"
#import "AccountDetailsTableViewCell.h"
#import <Parse/Parse.h>
#import "UIButton+MuVUIButtonAdditions.h"

@interface MuVDriverPaymentMethodsViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *addVenmoAccount;
@property (weak, nonatomic) IBOutlet UIButton *addBankAccount;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) UserProfileInformation *userInfo;
@property (strong, nonatomic) PFObject *merchantAccount;
@end

@implementation MuVDriverPaymentMethodsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _userInfo = [UserProfileInformation sharedMySingleton];
    
    if (!_userInfo.driverDetails) {
        return;
    }
    _merchantAccount = _userInfo.driverDetails[@"merchantAccount"];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [_tableView reloadData];
    
    _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    _addBankAccount = [UIButton muv_buttonUpdate:_addBankAccount];
    _addVenmoAccount = [UIButton muv_buttonUpdate:_addVenmoAccount];
    
    if ([_merchantAccount isKindOfClass:[PFObject class]]){
        _addBankAccount.enabled = YES;
        
        _addVenmoAccount.enabled = NO;
        [_addBankAccount setTitle:@"EDIT BANK ACCOUNT" forState:UIControlStateNormal];
        _addVenmoAccount.backgroundColor = [UIColor grayColor];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_merchantAccount){
        return 1;
    } else {
        return 0;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AccountDetailsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"AccountDetailsTableViewCell" forIndexPath:indexPath];
    if ([_merchantAccount[@"accountType"]isEqualToString:@"Direct Bank"]){
        cell.bankAccountLabel.text = [NSString stringWithFormat:@"Direct Bank xxxx%@", _merchantAccount[@"lastFourDigits"]];
    } else {
        cell.bankAccountLabel.text = [NSString stringWithFormat:@"Venmo Act xxxx%@", _merchantAccount[@"lastFourDigits"]];
    }
    
    //    NSString *imageName = [NSString stringWithFormat:@"%@.png", paymentDetail[@"cardType"]];
    //    [cell.visaImageView setImage:[UIImage imageNamed:imageName]];
    
    [cell.selectButton setImage:[UIImage imageNamed:@"MuV_Button_Green"] forState:UIControlStateNormal];
    
    return cell;
    
}

//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//
//}

- (void)updatePaymentMethodsWithCardandSender:(id)sender {
    _merchantAccount = _userInfo.driverDetails[@"merchantAccount"];
    [_tableView reloadData];
}

- (IBAction)deleteButtonTapped:(id)sender {
    
    //    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:_tableView];
    //    NSIndexPath *indexPath = [_tableView indexPathForRowAtPoint:buttonPosition];
    //
    //    PFObject *tokenToBeDeleted = _paymentTokens[indexPath.row];
    //    [_userInfo.user[@"paymentTokens"] removeObject:tokenToBeDeleted];
    //    if ([_defaultToken.objectId isEqualToString:tokenToBeDeleted.objectId]){
    //        if (_paymentTokens.count > 0){
    //            _userInfo.user[@"defaultToken"] = _paymentTokens[0];
    //            [_bookingsAlertDelegate updateBookingsWithPaymentDetails:_userInfo.user[@"defaultToken"] andSender:sender];
    //
    //        } else {
    //            [_userInfo.user removeObjectForKey: @"defaultToken"];
    //            [_bookingsAlertDelegate updateBookingsWithPaymentDetails:nil andSender:sender];
    //        }
    //    }
    //    [tokenToBeDeleted deleteInBackground];
    //    [_userInfo.user saveInBackground];
    //    [_tableView reloadData];
    
}

- (IBAction)selectButtonTapped:(id)sender {
    // get indexPath
    //    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:_tableView];
    //    NSIndexPath *indexPath = [_tableView indexPathForRowAtPoint:buttonPosition];
    //
    //    _userInfo.user[@"defaultToken"] = _paymentTokens[indexPath.row];
    //    [_userInfo.user saveInBackground];
    //    [_bookingsAlertDelegate updateBookingsWithPaymentDetails:_paymentTokens[indexPath.row] andSender:sender];
    //    [[self navigationController] popViewControllerAnimated:YES];
}

- (IBAction)addVenmoAccountButtonTapped:(id)sender {
    [self performSegueWithIdentifier:@"venmoSegue" sender:sender];
}

- (IBAction)addBankAccountButtonTapped:(id)sender {
    [self performSegueWithIdentifier:@"bankSegue" sender:sender];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString: @"paymentSetupSegue"]) {
        MuVDriverPaymentSetupViewController *paymentSetupViewController = [segue destinationViewController];
        paymentSetupViewController.paymentAlertDelegate = self;
    }
}

@end
