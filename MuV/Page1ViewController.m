//
//  Page1ViewController.m
//  MuV
//
//  Created by Hing Huynh on 6/11/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "Page1ViewController.h"

@interface Page1ViewController ()

@end

@implementation Page1ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)actionSkip:(id)sender {
    [[NSUserDefaults standardUserDefaults]setBool:YES forKey :@"doneTutorial"];
    [self.rootViewController performSegueWithIdentifier:@"loginSegue" sender:self];}

@end
