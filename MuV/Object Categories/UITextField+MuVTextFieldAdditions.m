//
//  UITextField+MuVTextFieldAdditions.m
//  MuV
//
//  Created by Hing Huynh on 1/9/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "UITextField+MuVTextFieldAdditions.h"

@implementation UITextField (MuVTextFieldAdditions)

- (void)addChangeListenerTarget:(id)target action:(SEL)action {
    [self addTarget:target
             action:action
   forControlEvents:UIControlEventEditingChanged];
}

+ (UITextField *)muv_textFieldUpdate:(UITextField *)textfield {
    textfield.layer.cornerRadius = 1.0f;
    textfield.layer.masksToBounds = NO;
    textfield.layer.shadowColor = [UIColor grayColor].CGColor;
    textfield.layer.shadowOpacity = 0.8;
    textfield.layer.shadowRadius = 2;
    textfield.layer.shadowOffset = CGSizeMake(2.0f, 1.0f);
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    textfield.leftView = paddingView;
    textfield.leftViewMode = UITextFieldViewModeAlways;
    return textfield;
}

+ (UITextField *)muv_searchBarUpdate:(UITextField *)textfield {
    textfield.layer.cornerRadius = 1.0f;
    textfield.layer.masksToBounds = NO;
    textfield.layer.shadowColor = [UIColor grayColor].CGColor;
    textfield.layer.shadowOpacity = 0.8;
    textfield.layer.shadowRadius = 2;
    textfield.layer.shadowOffset = CGSizeMake(2.0f, 1.0f);
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 35, 20)];
    textfield.leftView = paddingView;
    textfield.leftViewMode = UITextFieldViewModeAlways;
    return textfield;
}

@end
