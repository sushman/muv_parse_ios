//
//  NSString+MuVStringAdditions.m
//  MuV
//
//  Created by Hing Huynh on 1/8/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "NSString+MuVStringAdditions.h"

@implementation NSString (MuVStringAdditions)

- (BOOL)muv_isEmpty {
    return (self.length == 0);
}


- (BOOL)muv_isValidEmailAddress {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *testStringForEmail = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [testStringForEmail evaluateWithObject:self];
}



- (BOOL)muv_isValidZip {
    NSString *zipRegex = @"^[0-9]{5}(?:-[0-9]{4})?$";
    NSPredicate *testStringForZip = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", zipRegex];
    return [testStringForZip evaluateWithObject:self];;
}



- (BOOL)muv_isValidPhoneNumber {
    NSError *error = nil;
    NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypePhoneNumber
                                                               error:&error];
    
    NSUInteger numberOfMatches = [detector numberOfMatchesInString:self
                                                           options:0
                                                             range:NSMakeRange(0, self.length)];
    
    return (numberOfMatches != 0);
}


+ (NSString *)muv_currencyStringFromNumber:(NSNumber *)number {
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    
    numberFormatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US"];
    
    return [numberFormatter stringFromNumber:number];
}





@end
