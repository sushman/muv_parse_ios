//
//  UIImage+MuVImageAdditions.h
//  MuV
//
//  Created by SushmaN on 2/16/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (MuVImageAdditions)

+ (UIImage *)muv_imageCircle:(UIImage*)image;
+ (UIImage *)roundedCornerImage:(NSInteger)cornerSize borderSize:(NSInteger)borderSize forImage:(UIImage*)image;
+ (UIImage*)circularScaleNCrop:(UIImage*)image withColor:(UIColor *)color;

+ (UIImage*)imageWithBorderFromImage:(UIImage*)source;


@end
