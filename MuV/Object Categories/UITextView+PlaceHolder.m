//
//  UITextView+PlaceHolder.m
//  MuV
//
//  Created by SushmaN on 3/19/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <objc/runtime.h>
#import "UITextView+Placeholder.h"

@interface DeallocHooker : NSObject

@property (nonatomic, strong) void (^willDealloc)(void);

@end

@implementation DeallocHooker

- (void)dealloc {
    if (self.willDealloc) {
        self.willDealloc();
    }
}

@end

@implementation UITextView (PlaceHolder)

#pragma mark - Class Methods
#pragma mark `defaultPlaceholderColor`

+ (UIColor *)defaultPlaceholderColor {
    static UIColor *color = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        UITextField *textField = [[UITextField alloc] init];
        textField.placeholder = @" ";
        color = [textField valueForKeyPath:@"_placeholderLabel.textColor"];
    });
    return color;
}


#pragma mark - Properties
#pragma mark `placeholderLabel`

- (UILabel *)placeholderLabel {
    UILabel *label = objc_getAssociatedObject(self, @selector(placeholderLabel));
    if (!label) {
        NSAttributedString *originalText = self.attributedText;
        self.text = @" "; // lazily set font of `UITextView`.
        self.attributedText = originalText;
        
        label = [[UILabel alloc] init];
        label.textColor = [self.class defaultPlaceholderColor];
        label.numberOfLines = 0;
        label.userInteractionEnabled = NO;
        objc_setAssociatedObject(self, @selector(placeholderLabel), label, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(updatePlaceholderLabel)
                                                     name:UITextViewTextDidChangeNotification
                                                   object:self];
        
        NSArray *observingKeys = @[@"attributedText",
                                   @"bounds",
                                   @"font",
                                   @"frame",
                                   @"text",
                                   @"textAlignment",
                                   @"textContainerInset"];
        for (NSString *key in observingKeys) {
            [self addObserver:self forKeyPath:key options:NSKeyValueObservingOptionNew context:nil];
        }
        
        DeallocHooker *hooker = [[DeallocHooker alloc] init];
        hooker.willDealloc = ^{
            [[NSNotificationCenter defaultCenter] removeObserver:self];
            for (NSString *key in observingKeys) {
                [self removeObserver:self forKeyPath:key];
            }
        };
        objc_setAssociatedObject(self, @"deallocHooker", hooker, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    return label;
}


#pragma mark `placeholder`

- (NSString *)placeholder {
    return self.placeholderLabel.text;
}

- (void)setPlaceholder:(NSString *)placeholder {
    self.placeholderLabel.text = placeholder;
    [self updatePlaceholderLabel];
}


#pragma mark `placeholderColor`

- (UIColor *)placeholderColor {
    return self.placeholderLabel.textColor;
}

- (void)setPlaceholderColor:(UIColor *)placeholderColor {
    self.placeholderLabel.textColor = placeholderColor;
}


#pragma mark - KVO

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    [self updatePlaceholderLabel];
}


#pragma mark - Update

- (void)updatePlaceholderLabel {
    if (self.text.length) {
        [self.placeholderLabel removeFromSuperview];
        return;
    }
    
    [self insertSubview:self.placeholderLabel atIndex:0];
    
    self.placeholderLabel.font = self.font;
    self.placeholderLabel.textAlignment = self.textAlignment;
    
    CGFloat x = self.textContainer.lineFragmentPadding + self.textContainerInset.left;
    CGFloat y = self.textContainerInset.top;
    CGFloat width = (CGRectGetWidth(self.bounds) - x - self.textContainer.lineFragmentPadding
                     - self.textContainerInset.right);
    CGFloat height = [self.placeholderLabel sizeThatFits:CGSizeMake(width, 0)].height;
    self.placeholderLabel.frame = CGRectMake(x, y, width, height);
}
@end
