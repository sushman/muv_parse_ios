//
//  UIImageView+MuVUIImageViewAdditions.h
//  MuV
//
//  Created by Hing Huynh on 1/28/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (MuVUIImageViewAdditions)

+ (UIImageView *)muv_imageCircle:(UIImageView*)image;

@end
