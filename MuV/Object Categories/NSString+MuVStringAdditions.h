//
//  NSString+MuVStringAdditions.h
//  MuV
//
//  Created by Hing Huynh on 1/8/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (MuVStringAdditions)

- (BOOL)muv_isEmpty;
- (BOOL)muv_isValidEmailAddress;
- (BOOL)muv_isValidZip;
- (BOOL)muv_isValidPhoneNumber;
+ (NSString *)muv_currencyStringFromNumber:(NSNumber *)number;

@end
