//
//  UIView+UIViewAdditions.h
//  MuV
//
//  Created by Hing Huynh on 1/22/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (UIViewAdditions)

+ (UIView *)muv_viewUpdate:(UIView*)view;

@end
