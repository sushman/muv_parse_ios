//
//  UILabel+MuVFontColorSize.h
//  MuV
//
//  Created by SushmaN on 2/4/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (MuVFontColorSize)

+ (UILabel *)muv_CustomFontColorSize:(UILabel*)label;

+ (UILabel *)muv_QuicksandFontWithSize:(CGFloat)size andColor:(UIColor *)color forLabel:(UILabel*)label;

@end
