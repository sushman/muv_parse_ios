//
//  UITextField+MuVTextFieldAdditions.h
//  MuV
//
//  Created by Hing Huynh on 1/9/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (MuVTextFieldAdditions)

- (void)addChangeListenerTarget:(id)target action:(SEL)action;

+ (UITextField *)muv_textFieldUpdate:(UITextField *)textfield;
+ (UITextField *)muv_searchBarUpdate:(UITextField *)textfield;

@end
