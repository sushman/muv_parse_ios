//
//  UIImageView+MuVUIImageViewAdditions.m
//  MuV
//
//  Created by Hing Huynh on 1/28/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "UIImageView+MuVUIImageViewAdditions.h"

@implementation UIImageView (MuVUIImageViewAdditions)

+(UIImageView *)muv_imageCircle:(UIImageView*)image {
    image.layer.masksToBounds = YES;
    image.layer.cornerRadius = image.frame.size.width/2;
    return image;
}

@end
