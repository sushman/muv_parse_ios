//
//  UIColor+MuVColorAdditions.m
//  MuV
//
//  Created by Hing Huynh on 1/8/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "UIColor+MuVColorAdditions.h"

@implementation UIColor (MuVColorAdditions)

+ (UIColor *)muv_colorWithRed:(float)red green:(float)green blue:(float)blue alpha:(float)alpha {
    return [UIColor colorWithRed:(  red / 255.0f)
                           green:(green / 255.0f)
                            blue:( blue / 255.0f)
                           alpha:alpha];
}



+ (UIColor *)muv_viewBackgroundColor {
    return [UIColor whiteColor];
}


#pragma mark - Buttons
+ (UIColor *)muv_buttonBackgroundColor {
    return [UIColor darkGrayColor];
}



+ (UIColor *)muv_buttonTextColor {
    return [UIColor whiteColor];
}



+ (UIColor *)muv_buttonDisabledTextColor {
    return [UIColor grayColor];
}



#pragma mark - TableViews and Cells
+ (UIColor *)muv_tableCellViewBackgroundColor {
    return [UIColor whiteColor];
}



#pragma mark - Table Views and Cells
+ (UIColor *)muv_tableCellViewTextColor {
    return [UIColor whiteColor];
}



#pragma mark - Navigation Bar and Items
+ (UIColor *)muv_navigationBarTintColor {
    return [UIColor whiteColor];
}



+ (UIColor *)muv_navigationBarBackgroundColor {
    return [UIColor muv_colorWithRed:0.0f green:106.0f blue:255.0f alpha:1.0f];
}



#pragma mark - TextFields (WNTextField)
+ (UIColor *)muv_textFieldBackgroundColor {
    return [UIColor clearColor];
}



+ (UIColor *)muv_textFieldTextColor {
    return [UIColor blackColor];
}



+ (UIColor *)muv_textFieldPlaceholderTextColor {
    return [UIColor lightGrayColor];
}


#pragma mark - UILabels
+ (UIColor *)muv_labelTextColor {
    return [UIColor blackColor];
}
@end
