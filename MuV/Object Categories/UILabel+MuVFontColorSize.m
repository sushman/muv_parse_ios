//
//  UILabel+MuVFontColorSize.m
//  MuV
//
//  Created by SushmaN on 2/4/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "UILabel+MuVFontColorSize.h"
#import "ControlVariables.h"

@implementation UILabel (MuVFontColorSize)

+ (UILabel *)muv_CustomFontColorSize:(UILabel*)label {
    label.font =  MUV_BOLD_FONT(7);
    label.textColor =[UIColor grayColor];
    label.textAlignment = NSTextAlignmentCenter;
    return label;
}

+ (UILabel *)muv_QuicksandFontWithSize:(CGFloat)size andColor:(UIColor *)color forLabel:(UILabel*)label  {
    label.font =  MUV_BOLD_FONT(size);
    label.textColor =color;
    label.textAlignment = NSTextAlignmentLeft;
    return label;
}



@end
