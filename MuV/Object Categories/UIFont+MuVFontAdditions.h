//
//  UIFont+MuVFontAdditions.h
//  MuV
//
//  Created by Hing Huynh on 1/8/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (MuVFontAdditions)

+ (UIFont *)muv_buttonTextFontWithSize:(CGFloat)fontSize;

@end
