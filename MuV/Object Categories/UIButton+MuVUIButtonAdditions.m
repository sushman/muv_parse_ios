//
//  UIButton+MuVUIButtonAdditions.m
//  MuV
//
//  Created by Hing Huynh on 1/20/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "UIButton+MuVUIButtonAdditions.h"
#import "ControlVariables.h"

@implementation UIButton (MuVUIButtonAdditions)

+ (UIButton *)muv_buttonUpdate:(UIButton*)button {
    button.layer.cornerRadius = 3.0f;
    button.layer.masksToBounds = NO;
    button.layer.shadowColor = [UIColor blackColor].CGColor;
    button.layer.shadowOpacity = 0.8;
    button.layer.shadowRadius = 2;
    button.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
    button.titleLabel.font = MUV_BOLD_FONT(15);
    
    return button;
}

+ (UIButton *)muv_smallButtonUpdate:(UIButton*)button {
    button.layer.cornerRadius = 3.0f;
    button.layer.masksToBounds = NO;
    button.layer.shadowColor = [UIColor blackColor].CGColor;
    button.layer.shadowOpacity = 0.8;
    button.layer.shadowRadius = 2;
    button.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
    button.titleLabel.font =  MUV_BOLD_FONT(10);
    return button;
}

+ (UIButton *)muv_bigButton:(UIButton*)button {
    button.layer.cornerRadius = 3.0f;
    button.layer.masksToBounds = NO;
    button.layer.shadowColor = [UIColor blackColor].CGColor;
    button.layer.shadowOpacity = 0.8;
    button.layer.shadowRadius = 2;
    button.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [button.titleLabel setTextAlignment: NSTextAlignmentCenter];
    
    return button;
}

+ (UIButton *)muv_buttonCircle:(UIButton*)button {
    button.layer.masksToBounds = YES;
    button.layer.cornerRadius = button.frame.size.width/2;
//    button.layer.borderColor = [UIColor blackColor].CGColor;
//    button.layer.borderWidth = 1.0f;
    
    return button;
}

+ (UIButton *)muv_buttonrNormalState:(UIButton *)button SetImage:(NSString *)imageName AndTitleColor:(UIColor *)color {
    
    [button setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    [button setTitleColor:color forState:UIControlStateNormal];
    
    return button;
}

@end
