//
//  UIColor+MuVColorAdditions.h
//  MuV
//
//  Created by Hing Huynh on 1/8/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (MuVColorAdditions)

+ (UIColor *)muv_colorWithRed:(float)red green:(float)green blue:(float)blue alpha:(float)alpha;

+ (UIColor *)muv_viewBackgroundColor;

// Buttons
+ (UIColor *)muv_buttonTextColor;
+ (UIColor *)muv_buttonBackgroundColor;
+ (UIColor *)muv_buttonDisabledTextColor;

// TableViews and Cells
+ (UIColor *)muv_tableCellViewBackgroundColor;
+ (UIColor *)muv_tableCellViewTextColor;

// Navigation Bar and Items
+ (UIColor *)muv_navigationBarTintColor;
+ (UIColor *)muv_navigationBarBackgroundColor;

// TextFields (WNTextFields)
+ (UIColor *)muv_textFieldBackgroundColor;
+ (UIColor *)muv_textFieldTextColor;
+ (UIColor *)muv_textFieldPlaceholderTextColor;

// Labels
+ (UIColor *)muv_labelTextColor;


@end
