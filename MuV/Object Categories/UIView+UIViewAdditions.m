//
//  UIView+UIViewAdditions.m
//  MuV
//
//  Created by Hing Huynh on 1/22/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "UIView+UIViewAdditions.h"

@implementation UIView (UIViewAdditions)

+ (UIView *)muv_viewUpdate:(UIView*)view {
    view.layer.cornerRadius = 1.0f;
    view.layer.masksToBounds = NO;
    view.layer.shadowColor = [UIColor grayColor].CGColor;
    view.layer.shadowOpacity = 0.8;
    view.layer.shadowRadius = 2;
    view.layer.shadowOffset = CGSizeMake(2.0f, 1.0f);

    return view;
}

@end
