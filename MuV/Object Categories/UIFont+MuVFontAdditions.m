//
//  UIFont+MuVFontAdditions.m
//  MuV
//
//  Created by Hing Huynh on 1/8/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "UIFont+MuVFontAdditions.h"
#import "ControlVariables.h"

@implementation UIFont (MuVFontAdditions)

+ (UIFont *)muv_buttonTextFontWithSize:(CGFloat)fontSize {
    return MUV_REGULAR_FONT(fontSize);
}

@end
