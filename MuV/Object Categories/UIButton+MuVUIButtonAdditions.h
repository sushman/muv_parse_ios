//
//  UIButton+MuVUIButtonAdditions.h
//  MuV
//
//  Created by Hing Huynh on 1/20/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (MuVUIButtonAdditions)

+ (UIButton *)muv_buttonUpdate:(UIButton*)button;
+ (UIButton *)muv_smallButtonUpdate:(UIButton*)button;
+ (UIButton *)muv_bigButton:(UIButton*)button;
+ (UIButton *)muv_buttonCircle:(UIButton*)button;
+ (UIButton *)muv_buttonrNormalState:(UIButton *)button SetImage:(NSString *)imageName AndTitleColor:(UIColor *)color;

@end
