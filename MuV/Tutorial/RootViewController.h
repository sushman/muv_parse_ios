//
//  RootViewController.h
//  MuV
//
//  Created by Hing Huynh on 6/11/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RootViewController : UIViewController <UIPageViewControllerDataSource>

#pragma mark - Public Methods
- (void)goToPreviousContentViewController;
- (void)goToNextContentViewController;

@end
