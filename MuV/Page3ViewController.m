//
//  Page3ViewController.m
//  MuV
//
//  Created by Hing Huynh on 6/11/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "Page3ViewController.h"

@interface Page3ViewController ()

@end

@implementation Page3ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)actionSkip:(id)sender {
    [[NSUserDefaults standardUserDefaults]setBool:YES forKey :@"doneTutorial"];
    [self.rootViewController performSegueWithIdentifier:@"loginSegue" sender:self];}

@end
