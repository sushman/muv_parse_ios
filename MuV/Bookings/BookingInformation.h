//
//  BookingInformation.h
//  MuV
//
//  Created by Hing Huynh on 2/4/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Ride.h"

@interface BookingInformation : NSObject

@property (nonatomic, assign) BOOL isArrival;
@property (nonatomic, assign) BOOL isDeparture;
@property (nonatomic, assign) BOOL arrivalBooked;

@property (nonatomic, assign) Ride *arrivalRide;
@property (nonatomic, assign) Ride *departureRide;

+ (BookingInformation *) sharedMySingleton;

@end
