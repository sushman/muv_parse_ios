//
//  BookingMuVController.m
//  MuV
//
//  Created by Hing Huynh on 1/22/15.
//  Copyright (c) 2015 moback. All rights reserved.
//


#import "MuVBookingsViewController.h"
#import "MuVBestMatchesViewController.h"
#import "Helper.h"
#import "EventKitManager.h"
#import "LocationDetail.h"
#import "GoogleMapView.h"
#import "LocationManager.h"
#import "RideManager.h"
#import "TableViewCellOfLocations.h"
#import <MapKit/MapKit.h>
#import "UITextField+MuVTextFieldAdditions.h"
#import "UIView+UIViewAdditions.h"
#import "UIButton+MuVUIButtonAdditions.h"
#import "UserProfileInformation.h"
#import "MuVArrivalViewController.h"
#import "MuVDepartureViewController.h"
#import "BookingInformation.h"
#import "UIViewController+JASidePanel.h"
#import "MuVSidePanelController.h"
#import "SearchManager.h"
#import "TripManager.h"
#import "CouponManager.h"
#import "Coupon.h"
#import "PaymentManager.h"
#import "UIImage+WebP.h"
#import "MailManager.h"
#import "AdminManager.h"
#import "ParkAndRide.h"


#define DATE_FORMAT @"EEEE     MM / dd / yy"
#define TIME_FORMAT @" hh:mm a"

#define DATE_PLUS_TIME_FORMAT @"EEEE     MM / dd / yy hh:mm a"

@interface MuVBookingsViewController ()<CLLocationManagerDelegate,GMSMapViewDelegate>

@property (strong, nonatomic) JTCalendar *calendar;

@property (weak, nonatomic) IBOutlet UITextField *pickupLocationTextField;
@property (weak, nonatomic) IBOutlet UITextField *dropOffLocationTextField;

@property (weak, nonatomic) IBOutlet JTCalendarMenuView *calendarMenuView;
@property (weak, nonatomic) IBOutlet JTCalendarContentView *calendarContentView;
@property (weak, nonatomic) IBOutlet UIImageView *calendarMenuImage;
@property (weak, nonatomic) IBOutlet UIImageView *calendarContentImage;

@property (weak, nonatomic) IBOutlet UILabel *dateSelected;
@property (weak, nonatomic) IBOutlet UILabel *timePickerLabel;


@property (weak, nonatomic) IBOutlet UIView *timePickerView;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;

@property (weak, nonatomic) IBOutlet UIView *fareView;
@property (weak, nonatomic) IBOutlet UILabel *totalFare;

@property (weak, nonatomic) IBOutlet UIButton *oneWayTripButton;
@property (weak, nonatomic) IBOutlet UIButton *roundTripButton;

@property (weak, nonatomic) IBOutlet UIView *departTimeView;
@property (weak, nonatomic) IBOutlet UIView *arrivalTimeView;
@property (weak, nonatomic) IBOutlet UILabel *arriveTime;
@property (weak, nonatomic) IBOutlet UILabel *departTime;
@property (weak, nonatomic) IBOutlet UIButton *findMuV;

@property (weak, nonatomic) IBOutlet UIView *showCalendarView;

@property (weak, nonatomic) IBOutlet UIButton *setTimeButton;

@property (nonatomic, retain) GMSMarker *marker;
@property (nonatomic, retain) GMSMarker *markerPositionA;
@property (nonatomic, retain) GMSMarker *markerPositionB;

@property (weak, nonatomic) IBOutlet GMSMapView *mapView;

@property (weak, nonatomic) IBOutlet UIImageView *bookingBackgroundView;

@property (nonatomic, retain) CLLocationManager *locationManager;

@property (nonatomic, retain) NSMutableDictionary* coordinateDictionary;
@property (nonatomic) BOOL draggingPing;
@property(nonatomic)  float zoomLevel;

@property (nonatomic, assign) BOOL *isFiltered;
@property (nonatomic, strong) NSMutableArray *searchResults;
@property (nonatomic, strong) NSMutableArray *locationList;

@property (weak, nonatomic) IBOutlet UIView *arrivalDriverView;
@property (weak, nonatomic) IBOutlet UILabel *arrivalDriverName;
@property (weak, nonatomic) IBOutlet UIView *departureDriverView;
@property (weak, nonatomic) IBOutlet UILabel *departureDriverName;

@property (weak, nonatomic) IBOutlet UIView *paymentMethodView;
@property (weak, nonatomic) IBOutlet UIImageView *creditCompanyImage;
@property (weak, nonatomic) IBOutlet UIButton *creditCardLabelButton;
@property (weak, nonatomic) IBOutlet UIButton *paymentEditButton;

@property (weak, nonatomic) IBOutlet UITextField *couponTextField;

@property (weak, nonatomic) IBOutlet UIView *totalFareView;
@property (weak, nonatomic) IBOutlet UILabel *totalFareLabel;

@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *confirmButton;


@property (strong, nonatomic) UserProfileInformation *userInfo;
@property (strong, nonatomic) BookingInformation *bookingInfo;

@property (strong, nonatomic) IBOutlet UIView *transparentBackgroundView;
@property (strong, nonatomic) IBOutlet UIView *viewForPicker;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@property (strong, nonatomic) NSArray *couponArray;
@property (strong, nonatomic) NSMutableArray *couponNames;
@property (weak, nonatomic) IBOutlet UIButton *applyCouponBtn;


@property (weak, nonatomic) IBOutlet UIView *couponMessagingView;
@property (weak, nonatomic) IBOutlet UILabel *couponMessage;
@property (weak, nonatomic) IBOutlet UIButton *closeScreen;
@property (weak, nonatomic) IBOutlet UIButton *couponViewButton;
@property (weak, nonatomic) IBOutlet UIButton *couponApplyButton;
@property (weak, nonatomic) IBOutlet UIButton *selectCouponBtn;

@property (weak, nonatomic) IBOutlet UIButton *showCalendarButton;


@end

@implementation MuVBookingsViewController

BOOL isRoundTrip;
BOOL isArrival;
BOOL tripTypeSelected;
BOOL haveCurrentLocation;
BOOL paymentAdded;
BOOL isArrivalOnly;
BOOL isDepartureOnly;

BOOL isPickUpLocation;
BOOL isPickUpEdited;
BOOL isDropOffEdited;
BOOL isPickUpLocationTextField;
BOOL isDropOffLocationTextField;

BOOL isDriver;
BOOL couponApplied;



GMSCameraPosition *camera;
CLPlacemark *pickUpAddress;
CLPlacemark *dropOffAddress;
CLLocation *pickUpAddr;
CLLocation *dropOffAddr;
CLLocationDistance distanceBetweenLocation;

NSString *totalFare;
NSString *segueType;
NSString *location;

NSInteger row;
NSInteger keyBoardOffset;
NSUInteger originalAmount;

NSMutableArray *locationInputArray;
NSDateFormatter *timeFormatter;


double discountAmount;
double oneWayFare;
double originalFareCost;
float fareCost;
double mileage;

Ride *ride;
RideManager *rideManager;
TripManager *tripManager;
PaymentManager *paymentManager;
CouponManager *couponManager;
MailManager *mailManager;
EventKitManager *eventKitManager;
SearchManager *searchManager;
LocationDetail *locationDetail;
LocationDetail *currentLocation;
AdminManager *adminManager;

PFObject *couponObj;
PFObject *paymentDetail;
PFObject *driverDetails;
PFObject *usedCoupon;

UITapGestureRecognizer *tapForArrival;
UITapGestureRecognizer *tapForDeparture;
UITapGestureRecognizer *tapForCalendar;

enum routeType {arrival = 1, departure = 2, both = 3, none = 4};

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setUpScheduleFindButton];
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:37.7833
                                                            longitude:-122.4167
                                                                 zoom:5];
    _mapView.camera = camera;
    
    _currentDate = [Helper getFirstSecondOfTheMonth:[NSDate date]];
    originalAmount = 10;
    
    //get the admin settings
    AdminManager *adminManager = [AdminManager sharedInstance];
    
    if (!adminManager.adminSettings.costPerMile) {
        [adminManager getAdminSettingsWithCompletionBlock:^(AdminSettings *adminSetting, NSError *error) {
            //
        }];
    }
    
    rideManager = [RideManager new];
    mailManager = [MailManager new];
    couponManager = [CouponManager new];
    locationDetail = [LocationDetail new];
    searchManager = [SearchManager new];
    paymentManager = [PaymentManager new];
    eventKitManager = [EventKitManager new];
    
    [self loadCalendarEvents:YES];

    
    _userInfo = [UserProfileInformation sharedMySingleton];
    _bookingInfo = [BookingInformation sharedMySingleton];
     tripManager = [TripManager sharedManager];
    
    isPickUpLocation = YES;
    
    isDriver = ![[NSUserDefaults standardUserDefaults] boolForKey:@"riderMode"];
    
    if (isDriver) {
        _totalFare.hidden = YES;
    }
    
    _pickupLocationTextField.delegate = self;
    _dropOffLocationTextField.delegate = self;
    _couponTextField.delegate = self;
    
    [_pickupLocationTextField addChangeListenerTarget:self action:@selector(pickUpValueChanged)];
    [_dropOffLocationTextField addChangeListenerTarget:self action:@selector(dropOffValueChanged)];
    
    _pickupLocationTextField.placeholder = @"";
    _dropOffLocationTextField.placeholder = @"";
    _arriveTime.text = @"";
    _departTime.text = @"";
    
    [self showCustomisedView];
   
    [self initTableViewAddress];
    
    [self hideControls:YES];
    [self hideConfirmButton:YES];
    
    _activityIndicatorView.hidden = YES;

    [self addGesturesForTimePicker];
     
    [self setUpEditableTrip];
    
    if (_bookingInfo.arrivalRide || _bookingInfo.departureRide){
        _selectedRides = YES;
        [self setUpPageForSelectedRides];
    }
    
    [self setUpParkAndRide];
    
    [self initCalendarControl];
   
    [self loadCouponsAvailableForUser];
    
    [self keyBoardListeners];
    
   }

- (void)viewDidAppear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(eventHandler:) name:NSUSERDEFAULT_FOR_RIDERMODE object:nil];
    
    [self.calendar reloadData];
    [self.calendar reloadAppearance];
}

- (void)viewDidDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (NSDateFormatter *)getTimeFormatter:(BOOL)isSet {
    if (!timeFormatter) {
        timeFormatter = [NSDateFormatter new];
        if (isSet) {
             [timeFormatter setDateFormat:MUV_TIME_FORMAT];
        }
        else {
            [timeFormatter setDateFormat:TIME_FORMAT];
        }
    }
    return timeFormatter;
}

-(void)eventHandler: (NSNotification *) notification {
    
    [self checkDriverMode];
   
}

- (void)setUpScheduleFindButton {
    isDriver = ![[NSUserDefaults standardUserDefaults] boolForKey:@"riderMode"];
    
    if (isDriver) {
        [_findMuV setTitle:@"Schedule a MüV" forState:UIControlStateNormal];
        _totalFare.hidden = YES;
    }
    else {
        [_findMuV setTitle:@"Find a MüV" forState:UIControlStateNormal];
        _totalFare.hidden = NO;
    }

}

- (void)checkDriverMode {
    
    [self setUpScheduleFindButton];
    [self loadCalendarEvents:YES];
    
    [[TripManager sharedManager] setEditedRide:nil];
    
}


- (void)dealloc {
    // unregister for keyboard notification while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)keyBoardListeners {
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
}

- (void)setUpParkAndRide {
    
    if (tripManager.parkAndRide) {
        
        _pickupLocationTextField.text = @"park and ride";
        [self callGooglePlacesApi: @"park&ride"];
        _dropOffLocationTextField.text = tripManager.parkAndRide.dropOffAddress;
        _dateSelected.text = tripManager.parkAndRide.date;
        _arriveTime.text = tripManager.parkAndRide.arrivalTime;
        _departTime.text = tripManager.parkAndRide.departureTime;
        dropOffAddr = tripManager.parkAndRide.dropOffLocation;
        _totalFare.text = @"$0.00";
        
        if (tripManager.parkAndRide.isOneWay) {
            //[self setUpForOneWayTrip];
            [self oneWayTripButtonTapped:nil];
        }
        else {
           // [self setUpForRoundTrip];
            [self roundTripButtonTapped:nil];
        }
         _bookingBackgroundView.hidden = YES;
        _findMuV.hidden = YES;
        
        [[TripManager sharedManager] setParkAndRide:nil];
        
    }
}

- (void)setUpPageForSelectedRides {
    
    if (_userInfo.user[@"defaultToken"]){
        paymentDetail = _userInfo.user[@"defaultToken"];
        
        
        NSString *imageName = [NSString stringWithFormat:@"%@.png", paymentDetail[@"cardType"]];
        [_creditCompanyImage setImage:[UIImage imageNamed:imageName]];
        
        [_creditCardLabelButton setTitle:[NSString stringWithFormat:@"Credit Card xxxx%@", paymentDetail[@"lastFourDigits"]] forState:UIControlStateNormal];
        _creditCardLabelButton.enabled = NO;
        
        paymentAdded = YES;
        [self hideConfirmButton:NO];
        
      
        
        
        _pickupLocationTextField.enabled = NO;
        _dropOffLocationTextField.enabled = NO;
        _oneWayTripButton.enabled = NO;
        _roundTripButton.enabled = NO;
        
        
        [_arrivalTimeView removeGestureRecognizer:tapForArrival];
        [_departTimeView removeGestureRecognizer:tapForDeparture];
        [_showCalendarView removeGestureRecognizer:tapForCalendar];
        
        _showCalendarButton.enabled = NO;
        _showCalendarView.hidden = YES;
        
    }
    
   
    
    if (_bookingInfo.arrivalRide && _bookingInfo.departureRide) {
        
        [self setUpForRoundTrip];
        
    } else {
        
         [self setUpForOneWayTrip];
    }
    
    [self updateFare];
    
    _dateSelected.textColor = [UIColor colorWithRed:0.557f green:0.694f blue:0.275f alpha:1.00f];
    
    if (_bookingInfo.isArrival && _bookingInfo.isDeparture){
        _pickupLocationTextField.text = [NSString stringWithFormat:@"%@, %@, %@, %@",_bookingInfo.arrivalRide.startAddress, _bookingInfo.arrivalRide.startCity, _bookingInfo.arrivalRide.startState, _bookingInfo.arrivalRide.startZip];
        _dropOffLocationTextField.text = [NSString stringWithFormat:@"%@, %@, %@, %@",_bookingInfo.arrivalRide.endAddress, _bookingInfo.arrivalRide.endCity, _bookingInfo.arrivalRide.endState, _bookingInfo.arrivalRide.endZip];
        _dateSelected.text = _bookingInfo.arrivalRide.dateSelected;
        _arriveTime.text = _bookingInfo.arrivalRide.arriveTime;
        _departTime.text = _bookingInfo.departureRide.departTime;
        _arrivalDriverName.text = _bookingInfo.arrivalRide.driverName;
        _departureDriverName.text = _bookingInfo.departureRide.driverName;
        
    } else if (!_bookingInfo.isArrival && _bookingInfo.isDeparture){
        _pickupLocationTextField.text = [NSString stringWithFormat:@"%@, %@, %@, %@",_bookingInfo.departureRide.startAddress, _bookingInfo.departureRide.startCity, _bookingInfo.departureRide.startState, _bookingInfo.departureRide.startZip];
        _dropOffLocationTextField.text = [NSString stringWithFormat:@"%@, %@, %@, %@",_bookingInfo.departureRide.endAddress, _bookingInfo.departureRide.endCity, _bookingInfo.departureRide.endState, _bookingInfo.departureRide.endZip];;
        _dateSelected.text = _bookingInfo.departureRide.dateSelected;
        _departTime.text = _bookingInfo.departureRide.departTime;
        _departureDriverName.text = _bookingInfo.departureRide.driverName;
        
    } else {
        _pickupLocationTextField.text = [NSString stringWithFormat:@"%@, %@, %@, %@",_bookingInfo.arrivalRide.startAddress, _bookingInfo.arrivalRide.startCity, _bookingInfo.arrivalRide.startState, _bookingInfo.arrivalRide.startZip];
        _dropOffLocationTextField.text = [NSString stringWithFormat:@"%@, %@, %@, %@",_bookingInfo.arrivalRide.endAddress, _bookingInfo.arrivalRide.endCity, _bookingInfo.arrivalRide.endState, _bookingInfo.arrivalRide.endZip];
        _dateSelected.text = _bookingInfo.arrivalRide.dateSelected;
        _arriveTime.text = _bookingInfo.arrivalRide.arriveTime;
        _arrivalDriverName.text = _bookingInfo.arrivalRide.driverName;
    }
    
    [self hideControlsBelowSearchBar:NO];
    [self hideControlsforPaymentView:NO];
    
    _tableViewOfLocations.hidden = YES;
    _timePickerView.hidden = YES;
    
    _findMuV.hidden = YES;
}

- (void)setUpForRoundTrip {
    
    _oneWayTripButton = [UIButton muv_buttonrNormalState:_oneWayTripButton SetImage:MUV_ONE_WAY_IMAGE AndTitleColor:[UIColor lightGrayColor]];
    _roundTripButton = [UIButton muv_buttonrNormalState:_roundTripButton SetImage:MUV_ROUND_TRIP_HIGHLIGHTED_IMAGE AndTitleColor:MUV_GREEN_UICOLOR];
    isRoundTrip = YES;
    
}

- (void)oneWayButton {
    _oneWayTripButton = [UIButton muv_buttonrNormalState:_oneWayTripButton SetImage:MUV_ONE_WAY_HIGHLIGHTED_IMAGE AndTitleColor:MUV_GREEN_UICOLOR];
    _roundTripButton = [UIButton muv_buttonrNormalState:_roundTripButton SetImage:MUV_ROUND_TRIP_IMAGE  AndTitleColor:[UIColor lightGrayColor]];
    isRoundTrip = NO;
}

- (void)setUpForOneWayTrip {
    
    [self oneWayButton];
    
    if (_bookingInfo.arrivalRide) {
        _departureDriverName.text = @"";
        _departureDriverView.hidden = YES;
        
    }
    if (_bookingInfo.departureRide) {
        _arrivalDriverName.text = @"";
        _arrivalDriverView.hidden = YES;
    }
}

- (void)setUpEditableTrip {
    
    if (tripManager.editedRide && !tripManager.parkAndRide) {
        [self setUpForOneWayTrip];
        if (tripManager.editedRide) {
            
            _findMuV.hidden = NO;
            
            if (isDriver) {
                _pickupLocationTextField.text = tripManager.editedRide.trip.startAddress;
                [self placemarkerOnAddressIfValid: _pickupLocationTextField.text withPickUp:YES];
                
                _dropOffLocationTextField.text = tripManager.editedRide.trip.endAddress;
                [self placemarkerOnAddressIfValid:_dropOffLocationTextField.text withPickUp:NO];
                
                _totalFare.hidden = YES;
            }
            else {
                
                _totalFare.hidden = NO;
                PFObject *ride =  tripManager.editedRide.rideNo;
                _pickupLocationTextField.text =  [NSString stringWithFormat:@"%@ %@ %@ %@",ride[@"startAddress"], ride[@"startCity"], ride[@"startState"], ride[@"startZip"]];
                [self placemarkerOnAddressIfValid: _pickupLocationTextField.text withPickUp:YES];
                _dropOffLocationTextField.text = [NSString stringWithFormat:@"%@ %@ %@ %@",ride[@"endAddress"], ride[@"endCity"], ride[@"endState"], ride[@"endZip"]];
                [self placemarkerOnAddressIfValid:_dropOffLocationTextField.text withPickUp:NO];
                
                NSNumber *fare = ride[@"fare"];
                
                oneWayFare = [fare floatValue];
                
                totalFare = [@"$"stringByAppendingString:[NSString stringWithFormat:@"%.2lf", [fare floatValue]]];
                originalFareCost = [fare floatValue];
                
                _totalFare.text = [@"$"stringByAppendingString:[NSString stringWithFormat:@"%.2lf", originalFareCost]];
                fareCost = originalFareCost;
                
            }
            
            if ([tripManager.editedRide.routeType isEqualToString:ARRIVAL]) {
                _arriveTime.text =[[self getTimeFormatter:NO] stringFromDate:tripManager.editedRide.rideDate];
            }
            else {
                _departTime.text = [[self getTimeFormatter:NO] stringFromDate:tripManager.editedRide.rideDate];
            }
        }

        _showCalendarView.hidden = YES;
        _dateSelected.text = [self getFormattedDateInString:tripManager.editedRide.rideDate];
        _departTimeView.hidden = YES;
        _arrivalTimeView.hidden = YES;
        _roundTripButton.enabled = NO;
        [self oneWayTripButtonTapped:nil];
    }
}

- (void)loadCalendarEvents:(BOOL)isReload {
    
    if (!rideManager) {
         rideManager = [RideManager new];
    }
   
    if (isDriver) {
        [_findMuV setTitle:@"Schedule a MüV" forState:UIControlStateNormal];
        [rideManager getTripsForDriver:Calendar forDate:[NSDate date] withCompletionBlock:^(NSDictionary *rides, NSArray *Trips, NSError *error) {
            if (error == nil) {
                tripManager.rides = rides;
                if (isReload) {
                    [self.calendar setMenuMonthsView:_calendarMenuView];
                    [self.calendar setContentView:_calendarContentView];
                    [self.calendar setDataSource:self];

                    [self.calendar reloadData];
                    [self.calendar reloadAppearance];
                    
                }
            }
        }];
    }
    else {
        [_findMuV setTitle:@"Find a MüV" forState:UIControlStateNormal];
        [rideManager getRidesForRiderForTripView:Calendar forDate:[NSDate date] withCompletionBlock:^(NSDictionary *trips, NSArray *tripsDates, NSError *error) {
            if (error == nil) {
                tripManager.rides = trips;
                if (isReload) {
                    [self.calendar setMenuMonthsView:_calendarMenuView];
                    [self.calendar setContentView:_calendarContentView];
                    [self.calendar setDataSource:self];

                    [self.calendar reloadData];
                    [self.calendar reloadAppearance];
                }
            }
        }];
    }
}

- (void)showCustomisedView {
    
    _pickupLocationTextField = [UITextField muv_searchBarUpdate:_pickupLocationTextField];
    _dropOffLocationTextField = [UITextField muv_searchBarUpdate:_dropOffLocationTextField];
    
    _fareView = [UIView muv_viewUpdate:_fareView];
    _showCalendarView = [UIView muv_viewUpdate:_showCalendarView];
    _arrivalTimeView = [UIView muv_viewUpdate:_arrivalTimeView];
    _timePickerView = [UIView muv_viewUpdate:_timePickerView];
    _departTimeView = [UIView muv_viewUpdate:_departTimeView];
    
    _findMuV = [UIButton muv_buttonUpdate:_findMuV];
    _setTimeButton = [UIButton muv_buttonUpdate:_setTimeButton];
    
    _arrivalDriverView = [UIView muv_viewUpdate:_arrivalDriverView];
    _departureDriverView = [UIView muv_viewUpdate:_departureDriverView];
    _paymentMethodView = [UIView muv_viewUpdate:_paymentMethodView];
    _totalFareView = [UIView muv_viewUpdate:_totalFareView];
    _cancelButton = [UIButton muv_buttonUpdate:_cancelButton];
    _confirmButton = [UIButton muv_buttonUpdate:_confirmButton];
    
    _couponTextField = [UITextField muv_textFieldUpdate:_couponTextField];
    _couponApplyButton = [UIButton muv_buttonUpdate:_couponApplyButton];
    _couponViewButton = [UIButton muv_buttonUpdate:_couponViewButton];
    _selectCouponBtn = [UIButton muv_buttonUpdate:_selectCouponBtn];
    
    if ([_pickupLocationTextField respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        _pickupLocationTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Pick Up Location" attributes:@{NSForegroundColorAttributeName: MUV_GREEN_UICOLOR}];
    }
    
    if ([_dropOffLocationTextField respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        _dropOffLocationTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Drop Off Location" attributes:@{NSForegroundColorAttributeName: MUV_RED_UICOLOR}];
    }
    
}




- (void)loadCouponsAvailableForUser {
    
    _pickerView.dataSource = self;
    _pickerView.delegate = self;
    
    
    CouponManager *couponManager = [CouponManager new];
    _couponNames = [NSMutableArray new];
    [couponManager findMyEarnedPromoAndAdminCoupons:YES WithCompletionBlock:^(NSArray *coupons, NSError *error) {
        _couponArray = coupons;
        for (Coupon *coupon in _couponArray) {
            [_couponNames addObject:coupon.couponName];
        }
        [_pickerView reloadAllComponents];
    }];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    [tap setCancelsTouchesInView:NO];
}

-(void)dismissKeyboard {
    [_couponTextField resignFirstResponder];
}

#pragma IBAction

- (IBAction)viewCoupons:(id)sender {
    
    if (_couponNames.count <=0) {
        _couponMessage.text = @"You have no coupons at this time.";
        _pickerView.hidden = YES;
        _totalFareLabel.text = [NSString stringWithFormat:@"$%0.2f", originalFareCost];
        _couponTextField.text = @"";
        couponObj = nil;
        fareCost = originalFareCost;
        
        [self.view addSubview:_transparentBackgroundView];
        [self.view addSubview:_viewForPicker];
        return;
    }
    
    [_pickerView reloadAllComponents];
    _pickerView.hidden = NO;
    _selectCouponBtn.hidden = NO;
    [self.view addSubview:_transparentBackgroundView];
    [self.view addSubview:_viewForPicker];
}


- (IBAction)applyCoupons:(id)sender {
    NSMutableString *message = [NSMutableString new];
    if (_couponTextField.text.length <= 0) {
        _totalFareLabel.text = [NSString stringWithFormat:@"$%0.2f", originalFareCost];
        fareCost = originalFareCost;
        return;
    }
    __block double amount = originalFareCost;
    
    CouponManager *couponManager = [CouponManager new];
    [couponManager validateAndGetCoupon:_couponTextField.text withCompletionBlock:^(Coupon *coupon, NSError *error) {
        if (coupon) {
            // double amountToBeDeducted;
            [message appendString:@"  "];
            [message appendString:_couponTextField.text];
            [message appendString:@"\n  Coupon Amount"];
            
            //if coupon type is amount substract amount from total amount
            if ([coupon.couponType isEqualToString:@"amount"]) {
                amount = amount - [coupon.couponAmount intValue];
                if (amount < 0) {
                    amount = 0;
                    discountAmount = [coupon.couponAmount doubleValue] - amount;
                }
                else {
                    discountAmount = [coupon.couponAmount doubleValue];
                }
            }
            //take a percentage off
            else {
                double percentOff = (amount * [coupon.couponAmount intValue])/100;
                amount = amount - percentOff;
                if (amount < 0) {
                    amount = 0;
                    discountAmount = percentOff - amount;
                }
                else {
                    discountAmount = percentOff;
                }
            }
            [message appendString: [NSString stringWithFormat:@"$%0.2f", discountAmount]];
            _totalFareLabel.text = [NSString stringWithFormat:@"%0.2f", amount];
            fareCost = amount;
            [message appendString: [NSString stringWithFormat:@"\n  Total Cost will be $%@",  _totalFareLabel.text]];
           // _applyCouponBtn.hidden = YES;
            couponObj = coupon.coupon;
        }
        else {
            [message appendString: @"  Invalid Coupon."];
            couponObj = nil;
        }
        _couponMessage.text = message;
        [self.view addSubview:_transparentBackgroundView];
        [self.view addSubview:_viewForPicker];
        _pickerView.hidden = YES;
        _selectCouponBtn.hidden = YES;
    }];
}

- (IBAction)closePickerView:(id)sender {
    [_transparentBackgroundView removeFromSuperview];
    [_viewForPicker removeFromSuperview];
}

- (IBAction)couponSelected:(id)sender {
    row = [self.pickerView selectedRowInComponent:0];
    [_transparentBackgroundView removeFromSuperview];
    [_viewForPicker removeFromSuperview];
    
    Coupon *coupon = [_couponArray objectAtIndex:row];
    _couponTextField.text = coupon.couponName;
  //  _applyCouponBtn.hidden = NO;
    [_totalFareLabel setText:[NSString stringWithFormat:@"%0.2f", originalFareCost]];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return _couponNames.count;
}

- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return _couponNames[row];
}

- (IBAction)paymentEditButtonTapped:(id)sender {
    [self performSegueWithIdentifier:@"editPaymentSegue" sender:sender];
}

- (IBAction)addPaymentButtonTapped:(id)sender {
    [self performSegueWithIdentifier:@"addPaymentSegue" sender:sender];
}

- (void)updateBookingsWithPaymentDetails:(PFObject *)paymentDetails andSender:(id)sender {
    if (paymentDetails){
        paymentDetail = paymentDetails;
        [_creditCardLabelButton setTitle:[NSString stringWithFormat:@"Credit Card xxxx%@", paymentDetail[@"lastFourDigits"]] forState:UIControlStateNormal];
        
        NSString *imageName = [NSString stringWithFormat:@"%@.png", paymentDetail[@"cardType"]];
        [_creditCompanyImage setImage:[UIImage imageNamed:imageName]];
        
        _creditCardLabelButton.enabled = NO;
        paymentAdded = YES;
        [self hideConfirmButton:NO];
    } else {
        
        [_creditCardLabelButton setTitle:@"Add Payment" forState:UIControlStateNormal];
        [_creditCompanyImage setImage:nil];
        
        _creditCardLabelButton.enabled = YES;
        
        paymentAdded = NO;
        [self hideConfirmButton:YES];
    }
    
}


- (void)updatePaymentMethodsWithCardandSender:(id)sender {
    paymentDetail = _userInfo.user[@"defaultToken"];
    [_creditCardLabelButton setTitle:[NSString stringWithFormat:@"Credit Card xxxx%@", paymentDetail[@"lastFourDigits"]] forState:UIControlStateNormal];
    
    NSString *imageName = [NSString stringWithFormat:@"%@.png", paymentDetail[@"cardType"]];
    [_creditCompanyImage setImage:[UIImage imageNamed:imageName]];
    
    _creditCardLabelButton.enabled = NO;
    
    paymentAdded = YES;
    [self hideConfirmButton:NO];
}

- (IBAction)cancelButtonTapped:(id)sender {
    _bookingInfo.isArrival = NO;
    _bookingInfo.isDeparture = NO;
    _bookingInfo.arrivalBooked  = NO;
    _bookingInfo.departureRide = nil;
    _bookingInfo.arrivalRide = nil;
    
    tripTypeSelected = NO;
    
    dispatch_async(dispatch_get_main_queue(),^{
        MuVSidePanelController *sidePanelViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MuVSidePanelViewController"];
        sidePanelViewController.centerPanel = [self.storyboard instantiateViewControllerWithIdentifier:@"BookingNav"];
        [self presentViewController:sidePanelViewController animated:YES completion:nil];
    });
    
}

- (IBAction)confirmButtonTapped:(id)sender {
    
    if (!rideManager) {
        rideManager = [RideManager new];
    }
    
    _confirmButton.enabled = NO;
        
    __block float fare =  fareCost;
    
    // SPLITTING COST IN HALF IF ROUND TRIP
    if (isRoundTrip){
        fareCost = fareCost / 2;
        originalFareCost = originalFareCost / 2;
    }
    
    NSString *stringFareCost = [NSString stringWithFormat:@"%0.2f", fareCost];
    fareCost = [stringFareCost floatValue];
    
    NSString *stringOriginalCost = [NSString stringWithFormat:@"%0.2f", originalFareCost];
    originalFareCost = [stringOriginalCost floatValue];
    
    NSMutableArray *arrayOfRides = [NSMutableArray new];
    
    if (isRoundTrip){
        fare = fare / 2;
        
        _bookingInfo.departureRide.totalFareCost = [[NSDecimalNumber alloc] initWithFloat:fare];
        _bookingInfo.arrivalRide.totalFareCost = [[NSDecimalNumber alloc] initWithFloat:fare];
        
        PFObject *paymentArrival = [paymentManager createPaymentObject:paymentDetail andAmountCharged:[NSNumber numberWithFloat:fareCost] andCostWithNoCoupon:[NSNumber numberWithFloat:originalFareCost] andDriverDetail:_bookingInfo.arrivalRide.driverDetail andCouponId:couponObj andCouponAmount:[NSNumber numberWithFloat:(discountAmount/2)]];
        
        PFObject *paymentDeparture = [paymentManager createPaymentObject:paymentDetail andAmountCharged:[NSNumber numberWithFloat:fareCost] andCostWithNoCoupon:[NSNumber numberWithFloat:originalFareCost] andDriverDetail:_bookingInfo.arrivalRide.driverDetail andCouponId:couponObj andCouponAmount:[NSNumber numberWithFloat:(discountAmount/2)]];
        
        NSArray *arrayOfPayments = [[NSArray alloc]initWithObjects:paymentArrival, paymentDeparture, nil];
        
        [paymentManager savePaymentObject:arrayOfPayments withCompletionBlock:^(BOOL succeeded, NSError *error) {
            
            if (succeeded) {
                _bookingInfo.arrivalRide.payment = paymentArrival;
                _bookingInfo.departureRide.payment = paymentDeparture;
                
                [arrayOfRides addObject:_bookingInfo.arrivalRide];
                [arrayOfRides addObject:_bookingInfo.departureRide];
                
                [self bookAndApplyCoupon:arrayOfRides];
            }
            else {
                NSLog(@"error in saving payment information for arrival & departure");
                _confirmButton.enabled = YES;
            }
        }];
        
    } else {
        
        if (_bookingInfo.isDeparture) {
            
            PFObject *paymentDeparture = [paymentManager createPaymentObject:paymentDetail andAmountCharged:[NSNumber numberWithFloat:fareCost] andCostWithNoCoupon:[NSNumber numberWithFloat:originalFareCost] andDriverDetail:_bookingInfo.departureRide.driverDetail andCouponId:couponObj andCouponAmount:[NSNumber numberWithFloat:(discountAmount)]];
            
            NSArray *arrayOfPayments = [[NSArray alloc]initWithObjects:paymentDeparture, nil];
            
            [paymentManager savePaymentObject:arrayOfPayments withCompletionBlock:^(BOOL succeeded, NSError *error) {
                
                if (succeeded) {
                    _bookingInfo.departureRide.payment = paymentDeparture;
                    [arrayOfRides addObject:_bookingInfo.departureRide];
                    [self bookAndApplyCoupon:arrayOfRides];
                }
                else {
                    NSLog(@"error in saving payment information for arrival & departure");
                    _confirmButton.enabled = YES;
                }
            }];
            
        }
        if (_bookingInfo.isArrival) {
            
            PFObject *paymentArrival = [paymentManager createPaymentObject:paymentDetail andAmountCharged:[NSNumber numberWithFloat:fareCost] andCostWithNoCoupon:[NSNumber numberWithFloat:originalFareCost] andDriverDetail:_bookingInfo.arrivalRide.driverDetail andCouponId:couponObj andCouponAmount:[NSNumber numberWithFloat:(discountAmount)]];
            
            NSArray *arrayOfPayments = [[NSArray alloc]initWithObjects:paymentArrival, nil];
            
            [paymentManager savePaymentObject:arrayOfPayments withCompletionBlock:^(BOOL succeeded, NSError *error) {
                
                if (succeeded) {
                    _bookingInfo.arrivalRide.payment = paymentArrival;
                    [arrayOfRides addObject:_bookingInfo.arrivalRide];
                    [self bookAndApplyCoupon:arrayOfRides];
                }
                else {
                    NSLog(@"error in saving payment information for arrival & departure");
                    _confirmButton.enabled = YES;
                }
            }];
        }
    }
}

- (void)bookAndApplyCoupon:(NSArray *)arrayOfRides {
    
    if (!rideManager) {
        rideManager = [RideManager new];
    }

    [rideManager bookMuV:arrayOfRides withCompletionBlock:^(NSArray *arrRideObjs, NSError *error) {
        
        if (arrRideObjs) {
            
            for (PFObject *rideObj in arrRideObjs) {
                [rideObj[@"payment"] addUniqueObject:rideObj forKey:@"rides"];
                [rideObj[@"payment"] saveInBackground];
                [self createCalendarEvent:rideObj];
            }
            
            //if edited trip - why here?
            if (tripManager.editedRide.rideNo) {
                [tripManager setEditedRide:nil];
                NSString *calendarEventId = tripManager.editedRide.rideNo[@"calendarEventId"];
                [eventKitManager deleteEventWithIdentifier:calendarEventId];
            }

            if (couponObj) {
                
                [couponManager applyCoupon:couponObj andCouponAmount:[NSNumber numberWithFloat:discountAmount] withCompletionBlock:^(PFObject *coupon, NSError *error) {
                    couponObj = nil;
                }];
                
            }
            
            //SMN test
            if (arrayOfRides.count == 2) {
                // SEND EMAIL for RIDE CONFIRMATION
                [mailManager sendRideConfirmationForRiderWithRide:[arrRideObjs objectAtIndex:0]  isArrive:YES isDepart:YES withCompletionBlock:^(BOOL succeeded, NSError *error) {
                    [mailManager sendDriverNotificationForRiderCancelationWithRides:[arrRideObjs objectAtIndex:0] DepartureRide:[arrRideObjs objectAtIndex:1] isArrival:YES isDeparture:YES];
                }];
            }
            else {
                if (_bookingInfo.arrivalRide) {
                    [mailManager sendRideConfirmationForRiderWithRide:[arrRideObjs objectAtIndex:0]  isArrive:YES isDepart:NO withCompletionBlock:^(BOOL succeeded, NSError *error) {
                        [mailManager sendDriverNotificationForRiderCancelationWithRides:[arrRideObjs objectAtIndex:0] DepartureRide:nil isArrival:YES isDeparture:NO];
                    }];
                }
                else {
                    [mailManager sendRideConfirmationForRiderWithRide:[arrRideObjs objectAtIndex:0]  isArrive:NO isDepart:YES withCompletionBlock:^(BOOL succeeded, NSError *error) {
                        [mailManager sendDriverNotificationForRiderCancelationWithRides:nil DepartureRide:[arrRideObjs objectAtIndex:0] isArrival:NO isDeparture:YES];
                    }];
                }
            }
            
            
            
            _bookingInfo.arrivalRide = nil;
            _bookingInfo.departureRide = nil;
            
            [[TripManager sharedManager] setDateLoaded:nil];
            
            _confirmButton.enabled = YES;
            
            dispatch_async(dispatch_get_main_queue(),^{
                MuVSidePanelController *sidePanelViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MuVSidePanelViewController"];
                self.sidePanelController.centerPanel = [self.storyboard instantiateViewControllerWithIdentifier:@"ScheduleAnotherNav"];
                
                [self presentViewController:sidePanelViewController animated:YES completion:nil];
            });

        }
        
        if (error) {
            _confirmButton.enabled = YES;
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning"
                                                            message:@"There was a problem creating the Booking"
                                                           delegate:nil
                                                  cancelButtonTitle:nil
                                                  otherButtonTitles:@"Dismiss", nil];
            
            [alert show];
            
        }
    }];
}


- (void)createCalendarEvent:(PFObject *)ride {
    
    
    NSString *time;
    BOOL isArrival = NO;
    BOOL isDeparture = NO;
    if (ride[@"startTime"]) {
        time = _departTime.text;
        isDeparture = YES;
    }
    else {
        time = _arriveTime.text;
        isArrival = YES;
    }
    [eventKitManager storeEventInLocalCalendarWithEventName:@"MuV Ride" forDate:_dateSelected.text forTime:time forArrival:isArrival orDeparture:isDeparture withCompletionBlock:^(NSString *eventId, NSError *error) {
        if (eventId) {
            if (!rideManager) {
                rideManager = [RideManager new];
            }

            ride[@"calendarEventId"] = eventId;
            [rideManager updateRideObject:ride];
        }
    }];
}




- (IBAction)showCalendar:(id)sender {
    
    if (!tripManager.editedRide) {
        
        [self hideShowCalendarControls:NO];
        _timePickerView.hidden = YES;
        
    }
}

- (void)hideShowCalendarControls:(BOOL)hide {
    
    _calendarMenuView.hidden = hide;
    _calendarContentView.hidden = hide;
    _calendarContentImage.hidden = hide;
    _calendarMenuImage.hidden = hide;
    _loadNextMonth.hidden = hide;
    _loadPrevMonth.hidden = hide;
    _currentMonth.hidden = hide;
    
}

- (IBAction)setTime:(id)sender {
    
    keyBoardOffset = 0;
    [self keyboardWillShow];
    
    if (isArrival) {
        _arriveTime.text =[[self getTimeFormatter:NO] stringFromDate:[_datePicker date]];
    }
    else {
        _departTime.text = [[self getTimeFormatter:NO] stringFromDate:[_datePicker date]];
    }
    
    
    
    if (!isRoundTrip && isArrival) {
        _departTime.text = @"";
    }
    if (!isRoundTrip && !isArrival) {
        _arriveTime.text = @"";
    }
    _timePickerView.hidden = YES;
    
    if (![self validateArrivalSmallerThanDeparture]) {
        _findMuV.hidden = YES;
        return;
    }
    
    if(![self checkIfPastDate:NO] && [self areAllFieldsFilled]) {
        _findMuV.hidden = NO;
    }
}

- (BOOL)validateArrivalSmallerThanDeparture {
    BOOL isValid = YES;
    
    if (_dateSelected.text.length > 0 && ![_dateSelected.text isEqualToString:@"Select Date"]) {
        if (_arriveTime.text.length > 0 && _departTime.text.length > 0) {
            NSString *arrival = [NSString stringWithFormat:@"%@%@", _dateSelected.text,_arriveTime.text];
            NSDate *arrivalDate = [Helper getDateFromString:arrival];
            NSString *departure = [NSString stringWithFormat:@"%@%@", _dateSelected.text,_departTime.text];
            NSDate *departureDate = [Helper getDateFromString:departure];
            
            if ([Helper compareDates:arrivalDate andDateB:departureDate]) {
                [self showAlert:@"Warning" withMessage:@"Selected arrival time is greater than departure time"];
                isValid = NO;
                if (isArrival) {
                    _arriveTime.text = @"";
                }
                else {
                    _departTime.text = @"";
                }
            }
        }
    }
    return isValid;
}

- (BOOL)checkIfPastDate:(BOOL) dateChosenLater {
    BOOL isPast = NO;

    if (_dateSelected.text.length > 0 && ![_dateSelected.text isEqualToString:@"Select Date"]) {
        if (_arriveTime.text.length > 0) {
            if (![self validateTime:_arriveTime.text]) {
                if (dateChosenLater) {
                    _dateSelected.text = @"";
                    return YES;
                }
                isPast = YES;
                _arriveTime.text = @"";
                if (isRoundTrip) {
                    _findMuV.hidden = YES;
                }
            }
        }
        if (_departTime.text.length > 0) {
            if (![self validateTime:_departTime.text]) {
                _departTime.text = @"";
                isPast = YES;
                if (isRoundTrip) {
                    _findMuV.hidden = YES;
                }
            }
        }
    }
    return isPast;
}

- (BOOL)validateTime:(NSString *)time {
    BOOL isValid = YES;
    NSString *chosenDateTime = [NSString stringWithFormat:@"%@%@", _dateSelected.text,time];
    NSDate *date = [Helper getDateFromString:chosenDateTime];
    if ([Helper isPastDate:date]) {
        [self showAlert:@"Warning" withMessage:@"Please choose a future date and time"];
        isValid = NO;
    }
    return isValid;
}

- (void)showAlert:(NSString *)title withMessage:(NSString *)message {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}

- (IBAction)oneWayTripButtonTapped:(id)sender {
    
    if (isPickUpLocation) {
        [self textFieldShouldReturn:_pickupLocationTextField];
    }
    else {
        [self textFieldShouldReturn:_dropOffLocationTextField];
    }
    
    [self setUpForOneWayTrip];
    tripTypeSelected = YES;
    
    if (_arriveTime.text.length > 0 && _departTime.text.length > 0) {
        _arriveTime.text = @"";
        _departTime.text = @"";
        _findMuV.hidden = YES;
    }
   
    if ([self validAddress]) {
        [self updateFare];
        [self hideControlsBelowSearchBar:NO];
        _findMuV.hidden = NO;
    }
    else {
        [self hideControlsBelowSearchBar:YES];
         _findMuV.hidden = YES;
    }
}

- (void) updateFare {
    [Helper getDistanceBetweenLocationA:pickUpAddr andLocationB:dropOffAddr withCompletionBlock:^(CLLocationDirection distance, NSError *error){
        
        float costPerMile = 1.0;
        if (adminManager.adminSettings.costPerMile) {
            costPerMile = [adminManager.adminSettings.costPerMile floatValue];
        }
        
        double fare = distance * costPerMile;
        double doubleFare = fare * 2;
        
        mileage = distance;
        oneWayFare = fare;
        
        totalFare = [@"$"stringByAppendingString:[NSString stringWithFormat:@"%.2lf", fare]];
        
        if (isRoundTrip) {
            _totalFare.text = [@"$"stringByAppendingString:[NSString stringWithFormat:@"%.2lf", doubleFare]];
            originalFareCost = doubleFare;
        } else {
            _totalFare.text = [@"$"stringByAppendingString:[NSString stringWithFormat:@"%.2lf", fare]];
            originalFareCost = fare;
        }
        
        _totalFareLabel.text = [@"$"stringByAppendingString:[NSString stringWithFormat:@"%.2lf", originalFareCost]];
        fareCost = originalFareCost;
        
    }];
}

- (IBAction)roundTripButtonTapped:(id)sender {
    
    if (isPickUpLocation) {
        [self textFieldShouldReturn:_pickupLocationTextField];
    }
    else {
        [self textFieldShouldReturn:_dropOffLocationTextField];
    }
    
    [self setUpForRoundTrip];
    tripTypeSelected = YES;
    
    if ([self validAddress]){
        [self updateFare];
        [self hideControlsBelowSearchBar:NO];
    }
    else {
        [self hideControlsBelowSearchBar:YES];
        _findMuV.hidden = YES;
    }

    if (!(_arriveTime.text.length > 0 && _departTime.text.length > 0))  {
        _findMuV.hidden = YES;
    }
}

- (BOOL)validAddress {
    if ([_pickupLocationTextField.text isEqualToString:@"park and ride"]) {
        return NO;
    }
    if ([_dropOffLocationTextField.text isEqualToString:@"park and ride"]) {
        return NO;
    }
    if (_pickupLocationTextField.text.length <=0) {
        return NO;
    }
    if (_dropOffLocationTextField.text.length <=0) {
        return NO;
    }
    return YES;
}

//schedule or find a MuV
- (IBAction)findMuV:(id)sender {
    if (isDriver){
        [self scheduleMuV:sender];
       
    }
    else {
        //find a MuV
        [tripManager setParkAndRide:nil];
        _activityIndicatorView.hidden = NO;
        [_activityIndicator startAnimating];
        [self setUpRide:sender];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    [_activityIndicator stopAnimating];
    _activityIndicatorView.hidden = YES;

    if ([[segue identifier] isEqualToString: @"bestMatches"]) {
        MuVBestMatchesViewController *bestMatches = [segue destinationViewController];
        
        bestMatches.arrivalRide = _arrivalRide;
        bestMatches.departureRide = _departureRide;
        if (_arriveTime.text.length > 0) bestMatches.isArrival = YES;
        if (_departTime.text.length > 0) bestMatches.isDeparture = YES;
        return;
    }
    if ([[segue identifier]  isEqualToString: @"scheduleAnotherMuV"]) {
        return;
    }
    if ([[segue identifier]  isEqualToString: @"arrivalSegue"]) {
        MuVArrivalViewController *arrivalView = [segue destinationViewController];
        arrivalView.requestedRide = _arrivalRide;
        return;
    }
    if ([[segue identifier]  isEqualToString: @"departureSegue"]) {
        MuVDepartureViewController *departureView = [segue destinationViewController];
        departureView.requestedRide = _departureRide;
        return;
    }
    if ([[segue identifier]  isEqualToString: @"noMatchesSegue"]) {
        return;
    }
    if ([[segue identifier]  isEqualToString: @"notServicingSegue"]) {
        return;
    }
    if ([[segue identifier] isEqualToString: @"addPaymentSegue"]) {
        MuVPaymentsSetupViewController *paymentSetupViewController = [segue destinationViewController];
        paymentSetupViewController.paymentAlertDelegate = self;
    }
    if ([[segue identifier] isEqualToString: @"editPaymentSegue"]) {
        MuVPaymentMethodsViewController *paymentMethodsViewController = [segue destinationViewController];
        paymentMethodsViewController.bookingsAlertDelegate = self;
    }
}

- (void)setUpRide:(id)sender {
    
    if (!pickUpAddress) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning"
                                                        message:@"Something went wrong. Please re-enter your pick up location."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        _pickupLocationTextField.text = @"";
        _activityIndicatorView.hidden = YES;
        [_activityIndicator stopAnimating];
        return;
    }
    
    if (!dropOffAddress) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning"
                                                        message:@"Something went wrong. Please re-enter your drop off location."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        _activityIndicatorView.hidden = YES;
        [_activityIndicator stopAnimating];
        _dropOffLocationTextField.text = @"";
        return;
    }

    
    [locationDetail addPastAddressIfNotDuplicate:[NSString stringWithFormat:@"%@ %@, %@, %@, %@", pickUpAddress.subThoroughfare, pickUpAddress.thoroughfare, pickUpAddress.locality,pickUpAddress.administrativeArea, pickUpAddress.postalCode]];
    
    [locationDetail addPastAddressIfNotDuplicate:[NSString stringWithFormat:@"%@ %@, %@, %@, %@", dropOffAddress.subThoroughfare, dropOffAddress.thoroughfare, dropOffAddress.locality,dropOffAddress.administrativeArea, dropOffAddress.postalCode]];
    
    if (isRoundTrip){
        
        [self setupArrivalAndDeparture];
        
    } else if (_arriveTime.text.length) {
        
        [self setupArrival];
        
    } else {
        
        [self setupDeparture];
        
    }
}


- (void)setupArrivalAndDeparture {
    
    if (!rideManager) {
        rideManager = [RideManager new];
    }
    
    __block NSDate *time =  [Helper getDateFromString:[NSString stringWithFormat:@"%@%@",_dateSelected.text,_arriveTime.text]];
    __block NSString *startLocation = [NSString stringWithFormat:@"%0.8f,%0.8f", pickUpAddress.location.coordinate.latitude, pickUpAddress.location.coordinate.longitude];
    __block NSString *endLocation = [NSString stringWithFormat:@"%0.8f,%0.8f", dropOffAddress.location.coordinate.latitude, dropOffAddress.location.coordinate.longitude];
    
    [self initializeArrival];
    [self setupRide:_arrivalRide withStartLocation:pickUpAddress andEndLocation:dropOffAddress];
    
    [self initializeDeparture];
    [self setupRide:_departureRide withStartLocation:dropOffAddress andEndLocation:pickUpAddress];
    
    
    [searchManager searchForDate:time WithStartAddress:startLocation AndEndAddress:endLocation AndArrival:@"true" withCompletionBlock:^(NSArray *results, NSError *error) {
        
        
        //if (!error && results.count >= 0) {
        
        [rideManager getBestMatchedRoutesExcludingCurrentUserForRides:results withCompletionBlock:^(NSArray *filteredResults, NSError *error) {
            _arrivalRide.searchResults = filteredResults;
            
            time = [Helper getDateFromString:[NSString stringWithFormat:@"%@%@",_dateSelected.text,_departTime.text]];
            startLocation = [NSString stringWithFormat:@"%0.8f,%0.8f", dropOffAddress.location.coordinate.latitude, dropOffAddress.location.coordinate.longitude];
            endLocation = [NSString stringWithFormat:@"%0.8f,%0.8f", pickUpAddress.location.coordinate.latitude, pickUpAddress.location.coordinate.longitude];
            
            [searchManager searchForDate:time WithStartAddress:startLocation AndEndAddress:endLocation AndArrival:@"false" withCompletionBlock:^(NSArray *results, NSError *error) {
                
                //   if (!error && results.count >= 0) {
                
                [rideManager getBestMatchedRoutesExcludingCurrentUserForRides:results withCompletionBlock:^(NSArray *filteredResults, NSError *error) {
                    _departureRide.searchResults = filteredResults;
                    
                    if (!isRoundTrip || (isRoundTrip && _departureRide.searchResults.count) || (isRoundTrip && !_departureRide.searchResults.count)) {
                        
                        if (isRoundTrip && !_arrivalRide.searchResults.count && !_departureRide.searchResults.count){
                            segueType = @"noMatchesSegue";
                            
                        }
                        else if (isRoundTrip && !_arrivalRide.searchResults.count && _departureRide.searchResults.count){
                            segueType = @"departureSegue";
                            
                        }
                        else if (isRoundTrip && _arrivalRide.searchResults.count && !_departureRide.searchResults.count){
                            segueType = @"arrivalSegue";
                            
                        }
                        else if (!_arrivalRide.searchResults.count && !isRoundTrip) {
                            segueType = @"noMatchesSegue";
                            
                        }
                        else {
                            segueType = @"bestMatches";
                        }
                        
                        [self segueToNextView];
                    }
                    
                }];
                // }
            }];
        }];
        // }
        if (error) {
            
            if (error.code == -1005) {
                _activityIndicatorView.hidden = YES;
                [_activityIndicator stopAnimating];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning"
                                                                message:@"Please check your network connection."
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
                
                
            }
            if (error.code == -1009) {
                _activityIndicatorView.hidden = YES;
                [_activityIndicator stopAnimating];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning"
                                                                message:@"Internet connection seems to be offline."
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
                
            }
            
        }
        
    }];
}

- (void)initializeArrival {
    _arrivalRide = [Ride new];
    _arrivalRide.routeType = @"arrival";
    _arrivalRide.mileage = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%0.2f",mileage]];
    _arrivalRide.dateSelected = _dateSelected.text;
    _arrivalRide.arriveTime = _arriveTime.text;
    _arrivalRide.endTime = [Helper getDateFromString:[NSString stringWithFormat:@"%@%@",_dateSelected.text,_arriveTime.text]];
    _arrivalRide.fare = totalFare;
}

- (void)initializeDeparture {
    _departureRide = [Ride new];
    _departureRide.routeType = @"departure";
    _departureRide.mileage = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%0.2f",mileage]];
    _departureRide.dateSelected = _dateSelected.text;
    _departureRide.departTime = _departTime.text;
    _departureRide.startTime = [Helper getDateFromString:[NSString stringWithFormat:@"%@%@",_dateSelected.text,_departTime.text]];
    _departureRide.fare = totalFare;
}



- (void)setupArrival {
    
    if (!rideManager) {
        rideManager = [RideManager new];
    }
    
    [self initializeArrival];
    [self setupRide:_arrivalRide withStartLocation:pickUpAddress andEndLocation:dropOffAddress];
    
    NSDate *time =  [Helper getDateFromString:[NSString stringWithFormat:@"%@%@",_dateSelected.text,_arriveTime.text]];
    NSString *startLocation = [NSString stringWithFormat:@"%0.8f,%0.8f", pickUpAddress.location.coordinate.latitude, pickUpAddress.location.coordinate.longitude];
    NSString *endLocation = [NSString stringWithFormat:@"%0.8f,%0.8f", dropOffAddress.location.coordinate.latitude, dropOffAddress.location.coordinate.longitude];
    
    [searchManager searchForDate:time WithStartAddress:startLocation AndEndAddress:endLocation AndArrival:@"true" withCompletionBlock:^(NSArray *results, NSError *error)  {
        
        if (!error) {
        
            [rideManager getBestMatchedRoutesExcludingCurrentUserForRides:results withCompletionBlock:^(NSArray *filteredResults, NSError *error) {
                _arrivalRide.searchResults = filteredResults;
                
                if (!isRoundTrip || (isRoundTrip && _departureRide.searchResults.count) || (isRoundTrip && !_departureRide.searchResults.count)) {
                    
                    if (isRoundTrip && !_arrivalRide.searchResults.count && !_departureRide.searchResults.count){
                        segueType = @"noMatchesSegue";
                        
                    } else if (isRoundTrip && !_arrivalRide.searchResults.count && _departureRide.searchResults.count){
                        segueType = @"departureSegue";
                        
                    } else if (!_arrivalRide.searchResults.count && !isRoundTrip) {
                        segueType = @"noMatchesSegue";
                        
                    } else {
                        segueType = @"bestMatches";
                    }
                    
                    [self segueToNextView];
                }

            }];
        }
    }];
}

- (void)setupDeparture {
    
    if (!rideManager) {
        rideManager = [RideManager new];
    }
    
    [self initializeDeparture];
    [self setupRide:_departureRide withStartLocation:pickUpAddress andEndLocation:dropOffAddress];
    
    NSDate *time =  [Helper getDateFromString:[NSString stringWithFormat:@"%@%@",_dateSelected.text,_departTime.text]];
    NSString *startLocation = [NSString stringWithFormat:@"%0.8f,%0.8f", pickUpAddress.location.coordinate.latitude, pickUpAddress.location.coordinate.longitude];
    NSString *endLocation = [NSString stringWithFormat:@"%0.8f,%0.8f", dropOffAddress.location.coordinate.latitude, dropOffAddress.location.coordinate.longitude];


    [searchManager searchForDate:time WithStartAddress:startLocation AndEndAddress:endLocation AndArrival:@"false" withCompletionBlock:^(NSArray *results, NSError *error) {
        
        if (!error) {
            
            [rideManager getBestMatchedRoutesExcludingCurrentUserForRides:results withCompletionBlock:^(NSArray *filteredResults, NSError *error) {
                _departureRide.searchResults = filteredResults;
                
                if (!isRoundTrip || (isRoundTrip && _arrivalRide.searchResults.count) || (isRoundTrip && !_arrivalRide.searchResults.count)) {
                    
                    if (isRoundTrip && !_arrivalRide.searchResults.count && !_departureRide.searchResults.count ){
                        
                        segueType = @"noMatchesSegue";
                        
                    } else if (isRoundTrip && _arrivalRide.searchResults.count && !_departureRide.searchResults.count){
                        
                        segueType = @"arrivalSegue";
                        
                    } else if (!_departureRide.searchResults.count && !isRoundTrip) {
                        
                        segueType = @"noMatchesSegue";
                        
                    } else {
                        
                        segueType = @"bestMatches";
                        
                    }
                    
                    [self segueToNextView];
                }
                
            }];
        }
    }];

}

-(void) segueToNextView {
    
    if (!rideManager) {
        rideManager = [RideManager new];
    }
    
    float fare =  fareCost;
    if (isRoundTrip){
        fare = fare / 2;
    }
    
    _arrivalRide.totalFareCost = [[NSDecimalNumber alloc] initWithFloat:fare];
    _departureRide.totalFareCost = [[NSDecimalNumber alloc] initWithFloat:fare];
    
      NSMutableArray *rideArray =  [NSMutableArray new];
    
    if ([segueType isEqualToString:@"bestMatches"]) { //found what they were looking for
        [self performSegueWithIdentifier:@"bestMatches" sender:nil];
    } else if ([segueType isEqualToString:@"arrivalSegue"]) { //only arrival found
        
        //since only arrival driver found, create a record for the departure trip as requested ride
        
        [rideArray addObject:_departureRide];
        
        [self bookRideWithoutMatches:rideArray];
        [self performSegueWithIdentifier:@"arrivalSegue" sender:nil];
        
    } else if ([segueType isEqualToString:@"departureSegue"]) { //only departure found
        
        //since only departure driver found, create a record for the arrival trip as requested ride
        [rideArray addObject:_arrivalRide];
        
        [self bookRideWithoutMatches:rideArray];
        
        [self performSegueWithIdentifier:@"departureSegue" sender:nil];
        
    } else if ([segueType isEqualToString:@"noMatchesSegue"]) { // no matches
        ParkAndRide *parkAndRide = [ParkAndRide new];
        parkAndRide.dropOffAddress = _dropOffLocationTextField.text;
        parkAndRide.dropOffLocation = dropOffAddr;
        parkAndRide.pickUpLocation = pickUpAddr;
        
        if (isRoundTrip) {
            
            parkAndRide.isOneWay = false;
            parkAndRide.arrivalTime = _arriveTime.text;
            parkAndRide.departureTime = _departTime.text;
            parkAndRide.date = _dateSelected.text;
            [rideArray addObject:_arrivalRide];
            [rideArray addObject:_departureRide];

            [self bookRideWithoutMatches:rideArray];
        }
        else {
            parkAndRide.isOneWay = true;
            parkAndRide.date = _dateSelected.text;
            
            if (_departTime.text.length > 0) {
                parkAndRide.departureTime = _departTime.text;
                [rideArray addObject:_departureRide];
                [self bookRideWithoutMatches:rideArray];
            }
            if (_arriveTime.text.length > 0) {
                parkAndRide.arrivalTime = _arriveTime.text;
                [rideArray addObject:_arrivalRide];
                [self bookRideWithoutMatches:rideArray];
            }
        }
      //  NSLog(@"SMN Pask and ride is %@", parkAndRide.dropOffAddress);
        [[TripManager sharedManager] setParkAndRide:parkAndRide];
        [self performSegueWithIdentifier:@"noMatchesSegue" sender:nil];
    } else { // not servicing area yet
        [self performSegueWithIdentifier:@"notServicingSegue" sender:nil];
    }
}

- (void)bookRideWithoutMatches:(NSArray *)rides {
    
    if (!rideManager) {
        rideManager = [RideManager new];
    }

    
    [rideManager bookMuV:rides withCompletionBlock:^(NSArray *arrRouteObjs, NSError *error) {
        if (error) {
            NSLog(@"Error in saving the requested rides %@", error);
        }
        else {
              [[TripManager sharedManager] setDateLoaded:nil];
        }
        
    }];
}


- (void)scheduleMuV:(id)sender {
    
    if (!rideManager) {
        rideManager = [RideManager new];
    }

    _findMuV.enabled = NO;
    Ride *ride = [Ride new];
    
    ride.driverDetail = _userInfo.driverDetails;
    
    if (!ride.driverDetail[@"merchantAccount"]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message"
                                                        message:@"Please add your bank account to schedule a ride."
                                                       delegate:nil
                                              cancelButtonTitle:nil
                                              otherButtonTitles:@"Dismiss", nil];
        [alert show];
        return;
    }
    
    
    if (ride.driverDetail == nil)
        return;

    NSInteger numSchedules = 1;
    
    if (_arriveTime.text.length > 0 && _departTime.text.length > 0) {
        isRoundTrip = YES;
    }
    else {
        isRoundTrip = NO;
    }
    
    //if round trip schedule two trips
    if (isRoundTrip) {
        numSchedules = 2;
    }
    
    NSMutableArray *arrayOfRides = [NSMutableArray new];
    for (int i = 0; i < numSchedules; i++) {
        ride = [Ride new];
        if (numSchedules == 2) {
            ride.isRoundTrip = YES;
        }
        else {
            ride.isRoundTrip = NO;
        }
        ride.driverDetail = _userInfo.driverDetails;
        ride.mileage = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%0.2f",mileage]];
        
        //if one way trip or the first leg of the trip
        if (numSchedules == 1 || (numSchedules == 2 && i == 0)) {
            ride.routeType = [self setRouteType];
            if ([ride.routeType isEqualToString:@"arrival"]) {
                ride.rideDate = [Helper getDateFromString:[NSString stringWithFormat:@"%@%@",_dateSelected.text,_arriveTime.text]];
            }
            else {
                ride.rideDate = [Helper getDateFromString:[NSString stringWithFormat:@"%@%@",_dateSelected.text,_departTime.text]];
            }
            
            [arrayOfRides addObject:[self setupRide:ride withStartLocation:pickUpAddress andEndLocation:dropOffAddress]];
        }
        //schedule the second leg of the trip
        else {
            ride.routeType = @"departure";
            ride.rideDate = [Helper getDateFromString:[NSString stringWithFormat:@"%@%@",_dateSelected.text,_departTime.text]];
            [arrayOfRides addObject:[self setupRide:ride withStartLocation:dropOffAddress andEndLocation:pickUpAddress]];
        }
    }
    
    [locationDetail addPastAddressIfNotDuplicate:[NSString stringWithFormat:@"%@ %@, %@, %@, %@", pickUpAddress.subThoroughfare, pickUpAddress.thoroughfare, pickUpAddress.locality,pickUpAddress.administrativeArea, pickUpAddress.postalCode]];
    
    [locationDetail addPastAddressIfNotDuplicate:[NSString stringWithFormat:@"%@ %@, %@, %@, %@", dropOffAddress.subThoroughfare, dropOffAddress.thoroughfare, dropOffAddress.locality,dropOffAddress.administrativeArea, dropOffAddress.postalCode]];
    
    [rideManager scheduleDriversMuV:arrayOfRides withCompletionBlock:^(NSArray *arrRouteObjs, NSError *error) {
        if (arrRouteObjs) {
            
          //  [self performSegueWithIdentifier:@"scheduleAnotherMuV" sender:sender];
            
            dispatch_async(dispatch_get_main_queue(),^{
                MuVSidePanelController *sidePanelViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MuVSidePanelViewController"];
                self.sidePanelController.centerPanel = [self.storyboard instantiateViewControllerWithIdentifier:@"ScheduleAnotherNav"];
                
                [self presentViewController:sidePanelViewController animated:YES completion:nil];
            });
            
            _findMuV.enabled = YES;
            [[TripManager sharedManager] setDateLoaded:nil];
            // SEND DRIVER CONFIRMATION EMAIL FOR BOOKING
            if (isRoundTrip){
                [mailManager sendDriverSchedulesMUVConfirmationWithRoute:[arrRouteObjs objectAtIndex:0] isArrive:YES isDepart:YES];
            } else if ([ride.routeType isEqualToString:@"arrival"]) {
                [mailManager sendDriverSchedulesMUVConfirmationWithRoute:[arrRouteObjs objectAtIndex:0] isArrive:YES isDepart:NO];
            } else {
                [mailManager sendDriverSchedulesMUVConfirmationWithRoute:[arrRouteObjs objectAtIndex:0] isArrive:NO isDepart:YES];
            }
            
            [self addNativeCalendarEventForScheduleMuv:arrRouteObjs];
            //if edited trip then
            if (tripManager.editedRide.trip.route) {
                tripManager.editedRide.trip.route[@"isEdited"] = [NSNumber numberWithBool:YES];
                NSString *calendarEventId = tripManager.editedRide.trip.route[@"calendarEventId"];
                [rideManager updatePFObject:tripManager.editedRide.trip.route withCompletionBlock:^(BOOL succeeded, NSError *error) {
                    if (succeeded) {
                        //set it back to nil
                        [tripManager setEditedRide:nil];
                    }
                }];
                [eventKitManager deleteEventWithIdentifier:calendarEventId];
            }
        }
    }];
}

- (void)addNativeCalendarEventForScheduleMuv:(NSArray *)arrRouteObjects {
    
    if (!rideManager) {
        rideManager = [RideManager new];
    }

    
    for (PFObject *routeObj in arrRouteObjects) {
        if ([routeObj[@"routeType"] isEqualToString:ARRIVAL]) {
            [eventKitManager storeEventInLocalCalendarWithEventName:@"MuV Driver Arrival Schedule " forDate:_dateSelected.text forTime:_arriveTime.text forArrival:YES orDeparture:NO withCompletionBlock:^(NSString *eventId, NSError *error) {
                routeObj[@"calendarEventId"] = eventId;
                [rideManager updatePFObject:routeObj];
                
                
            }];
        }
        else {
            [eventKitManager storeEventInLocalCalendarWithEventName:@"MuV Driver Departure Schedule" forDate:_dateSelected.text forTime:_departTime.text forArrival:NO orDeparture:YES withCompletionBlock:^(NSString *eventId, NSError *error) {
                routeObj[@"calendarEventId"] = eventId;
                [rideManager updatePFObject:routeObj];
            }];
        }
    }
}

- (Ride *)setupRide:(Ride *)ride withStartLocation:(CLPlacemark *)startPlacemark andEndLocation:(CLPlacemark *)endPlacemark {
    
    ride.startAddress = [NSString stringWithFormat:@"%@ %@",startPlacemark.subThoroughfare, startPlacemark.thoroughfare];
    ride.startCity = startPlacemark.locality;
    ride.startState = startPlacemark.administrativeArea;
    ride.startZip = startPlacemark.postalCode;
    ride.startLocation = [NSString stringWithFormat:@"%0.8f,%0.8f", startPlacemark.location.coordinate.latitude, startPlacemark.location.coordinate.longitude];
    ride.endAddress = [NSString stringWithFormat:@"%@ %@",endPlacemark.subThoroughfare, endPlacemark.thoroughfare];
    ride.endCity = endPlacemark.locality;
    ride.endState = endPlacemark.administrativeArea;
    ride.endZip = endPlacemark.postalCode;
    ride.endLocation = [NSString stringWithFormat:@"%0.8f,%0.8f", endPlacemark.location.coordinate.latitude, endPlacemark.location.coordinate.longitude];
    
    return ride;
}

- (NSString *)setRouteType {
    if (_arriveTime.text.length > 0) return @"arrival";
    else return @"departure";
}

#pragma mark - UISearchDisplayController Delegate Methods

- (NSMutableArray *)filterContentForSearchText:(NSString*)searchText {
    // Filter the array using NSPredicate
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.address contains[c] %@",searchText];
    NSArray *tempArray = [locationInputArray filteredArrayUsingPredicate:predicate];
    
    if (tempArray.count > 0) {
         _tableViewOfLocations.hidden = NO;
    }
    return [tempArray mutableCopy];
}

- (void)pickUpValueChanged {

    NSString *searchText = _pickupLocationTextField.text;
    [self hideControlsBelowSearchBar:YES];
    _tableViewOfLocations.allowsSelection = YES;
    _totalFare.text = @"";
    NSMutableArray *results = [self filterContentForSearchText:searchText];
    
    [locationDetail getPossibilitiesFromAddress:searchText forRegion:currentLocation.placemark.location.coordinate withCompletionBLock:^(NSMutableArray *validAddresses, NSError *error) {
        if (!error){
            [results addObjectsFromArray:validAddresses];
            [self loadAddresses:results];
            [_tableViewOfLocations reloadData];
            [self adjustHeightOfTableviewWith:results];
        }
    }];
    
}

- (void)dropOffValueChanged {
    
    NSString *searchText = _dropOffLocationTextField.text;
    
    [self hideControlsBelowSearchBar:YES];
    _tableViewOfLocations.allowsSelection = YES;
    _totalFare.text = @"";
  
    NSMutableArray *results = [self filterContentForSearchText:searchText];
   
    [locationDetail getPossibilitiesFromAddress:searchText forRegion:currentLocation.placemark.location.coordinate withCompletionBLock:^(NSMutableArray *validAddresses, NSError *error) {
        if (!error){
            [results addObjectsFromArray:validAddresses];
            [self loadAddresses:results];
            [_tableViewOfLocations reloadData];
            [self adjustHeightOfTableviewWith:results];
        }
    }];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField {
    
    if (textField == _couponTextField) {
     //   _applyCouponBtn.hidden = YES;
        keyBoardOffset = 100;
    }
    else {
        keyBoardOffset = 0;
        if (textField == _dropOffLocationTextField){
            isPickUpLocation = NO;
        } else {
            isPickUpLocation = YES;
        }
        
        _totalFare.text = @"";
        _tableViewOfLocations.allowsSelection = YES;
        NSString *searchText = @" ";
        NSArray *results = [self filterContentForSearchText:searchText];
        
        [self loadAddresses:results];
        [_tableViewOfLocations reloadData];
        [self adjustHeightOfTableviewWith:results];
    }
}

- (BOOL)isSameLocation {
    
    BOOL same = NO;
    if ([_pickupLocationTextField.text isEqualToString:_dropOffLocationTextField.text]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning"
                                                        message:@"You have entered the same address as your pick up and drop off location."
                                                       delegate:nil
                                              cancelButtonTitle:nil
                                              otherButtonTitles:@"Dismiss", nil];
        [alert show];
        same = YES;
    }
    return same;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    if (textField != _couponTextField) {
        
        
        if (textField == _pickupLocationTextField) {
            if (_pickupLocationTextField.text.length <= 0) {
                return YES;
            }
            if ([self isSameLocation]) {
                _pickupLocationTextField.text = @"";
                return YES;
            }
           
            [self placemarkerOnAddressIfValid:_pickupLocationTextField.text withPickUp:YES];
        }
        
        if (textField == _dropOffLocationTextField) {
            if (_dropOffLocationTextField.text.length <= 0) {
                return YES;
            }
            if ([self isSameLocation]) {
                _dropOffLocationTextField.text = @"";

                return YES;
            }

            [self placemarkerOnAddressIfValid:_dropOffLocationTextField.text withPickUp:NO];
        }
    }
    else {
        if (textField.text.length == 0) {
            _totalFareLabel.text = [NSString stringWithFormat:@"$%0.2f", originalFareCost];
            fareCost = originalFareCost;
        }
    }
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    
    [textField resignFirstResponder];
    
    if (textField == _couponTextField) {
      //  _applyCouponBtn.hidden = NO;
      //  if (textField.text.length == 0) {
            _totalFareLabel.text = [NSString stringWithFormat:@"$%0.2f", originalFareCost];
            fareCost = originalFareCost;
      //  }
    }
    else {
        
        if (textField == _pickupLocationTextField) {
            if (_pickupLocationTextField.text.length <= 0) {
                return;
            }
            if ([self isSameLocation]) {
                _pickupLocationTextField.text = @"";
                return;
            }


            [self placemarkerOnAddressIfValid:_pickupLocationTextField.text withPickUp:YES];
        }
        
        if (textField == _dropOffLocationTextField) {
            if (_dropOffLocationTextField.text.length <= 0) {
                return;
            }
            if ([self isSameLocation]) {
                _dropOffLocationTextField.text = @"";

                return;
            }

            [self placemarkerOnAddressIfValid:_dropOffLocationTextField.text withPickUp:NO];
        }
        
        NSArray *results = [self filterContentForSearchText:textField.text];
        
        [self loadAddresses:results];
    }
}

- (void)updateFareAfterAddressValidated {
    
    if ([self validAddress] && tripTypeSelected) {
        [self hideControlsBelowSearchBar:NO];
        [self updateFare];
    }
}

-(void) adjustHeightOfTableviewWith: (NSArray *)locations{
    
    CGRect tableFrame = [_tableViewOfLocations frame];
    
    CGFloat currentHeight = tableFrame.size.height;
    
    if (locations.count == 2){
        tableFrame.size.height = 100;
        
    } else if (locations.count == 1){
        tableFrame.size.height = 50;
        
    } else if (locations.count == 0){
        tableFrame.size.height = 0;
        
    }
    else {
        tableFrame.size.height = 150;
    }
    if (currentHeight != tableFrame.size.height){
        [UIView animateWithDuration:0.2 animations:^{
            _tableViewOfLocations.frame = CGRectMake(_tableViewOfLocations.frame.origin.x, _tableViewOfLocations.frame.origin.y, _tableViewOfLocations.frame.size.width, tableFrame.size.height);
        }];
    }
}

#pragma Address Fields
- (void) initTableViewAddress {
    // get current address
    _locationManager = [CLLocationManager new];
    _locationManager.delegate = self;
    
    // Check for iOS 8. Without this guard the code will crash with "unknown selector" on iOS 7.
    if ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [self.locationManager requestAlwaysAuthorization];
    }
    [self.locationManager startUpdatingLocation];
    [_locationManager startUpdatingLocation];
    
    [locationDetail getUserLocationsWithCompletionBlock:^(NSMutableArray *locationArray, NSError *error){
        if (!error){
            locationInputArray = locationArray;
            [self adjustHeightOfTableviewWith:locationInputArray];
        }
    }];
    
    _tableViewOfLocations.delegate = self;
    _tableViewOfLocations.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

// Delegate method from the CLLocationManagerDelegate protocol.
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    // If it's a relatively recent event, turn off updates to save power.
    CLLocation* location = [locations lastObject];
    NSDate* eventDate = location.timestamp;
    NSTimeInterval howRecent = [eventDate timeIntervalSinceNow];
    
    if (abs(howRecent) < 15.0) {
       
        
        [manager stopUpdatingLocation];
        
        camera = [GMSCameraPosition cameraWithLatitude:location.coordinate.latitude
                                             longitude:location.coordinate.longitude zoom:8];
        
        _mapView.camera = camera;
        
        if (!haveCurrentLocation){
            haveCurrentLocation = YES;
            CLGeocoder *geocoder = [[CLGeocoder alloc] init];
            [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
                if (placemarks) {
                    if (currentLocation) {
                        
                        currentLocation = [LocationDetail new];
                        currentLocation.pictureName = @"MuV_LocatorSearchIcon.png";
                        currentLocation.addressType = @"Current";
                        currentLocation.placemark = [placemarks lastObject];
                        
                        if (currentLocation.placemark.subThoroughfare) {
                            currentLocation.address = [NSString stringWithFormat:@"%@ %@, %@, %@, %@", currentLocation.placemark.subThoroughfare, currentLocation.placemark.thoroughfare, currentLocation.placemark.locality, currentLocation.placemark.administrativeArea, currentLocation.placemark.postalCode];
                        }
                        else {
                            currentLocation.address = [NSString stringWithFormat:@"%@, %@, %@, %@", currentLocation.placemark.thoroughfare, currentLocation.placemark.locality, currentLocation.placemark.administrativeArea, currentLocation.placemark.postalCode];
                        }
                        
                        if (locationInputArray.count >= 2){
                            [locationInputArray insertObject:currentLocation atIndex:2];
                        } else {
                            [locationInputArray addObject:currentLocation];
                        }
                        [_tableViewOfLocations reloadData];
                        
                    }
                }
            }];
        }
    }
}

//tableView Delegate
- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row >= _tableViewOfLocations.inputArray.count) {
        [_pickupLocationTextField resignFirstResponder];
        [_dropOffLocationTextField resignFirstResponder];
        _tableViewOfLocations.hidden = YES;
        return;
    }
    
    LocationDetail *locDetail = (LocationDetail *)  _tableViewOfLocations.inputArray[indexPath.row];
    location = locDetail.address;
    
    _tableViewOfLocations.hidden = YES;
    
    MKCoordinateRegion region;
    region.center.latitude = locDetail.placemark.location.coordinate.latitude;
    region.center.longitude = locDetail.placemark.location.coordinate.longitude;
    
    if (isPickUpLocation) {
        isPickUpEdited = YES;
        _pickupLocationTextField.text = location;
        pickUpAddress = locDetail.placemark;
        pickUpAddr = locDetail.placemark.location;
        [_pickupLocationTextField resignFirstResponder];
        _tableViewOfLocations.hidden = YES;
        
        if (_dropOffLocationTextField.text.length > 0) {
            [self placemarkerOnAddressIfValid:_dropOffLocationTextField.text withPickUp:NO];
        }
        
    }
    else {
        isDropOffEdited = YES;
        _dropOffLocationTextField.text = location;
        dropOffAddress = locDetail.placemark;
        dropOffAddr = locDetail.placemark.location;
        [_dropOffLocationTextField resignFirstResponder];
        _tableViewOfLocations.hidden = YES;
        
        if (_pickupLocationTextField.text.length > 0) {
            [self placemarkerOnAddressIfValid:_pickupLocationTextField.text withPickUp:YES];
        }
    }
    
    _fareView.hidden = NO;
    
    if ([self areAddressesAndFareFilled]) {
        
        if (tripManager.editedRide) {
            _roundTripButton.enabled = NO;
            
            if ([tripManager.editedRide.routeType isEqualToString: ARRIVAL]) {
                _arrivalTimeView.hidden = NO;
            }
            else {
                _departTimeView.hidden = NO;
            }
        }
        else {
            _showCalendarView.hidden = NO;
        }
    }
}


-(void)placemarkerOnAddressIfValid:(NSString *) address withPickUp:(BOOL)isPickUp {
    
    if (([address rangeOfString:@"park and ride" options:NSCaseInsensitiveSearch].location != NSNotFound) || ([address rangeOfString:@"park & ride" options:NSCaseInsensitiveSearch].location != NSNotFound)) {
        
        if (!_locationManager.location) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning"
                                                            message:@"Please check your GPS and/or internet connection."
                                                           delegate:nil
                                                  cancelButtonTitle:nil
                                                  otherButtonTitles:@"Dismiss", nil];
            [alert show];
            return;
        }

        NSString *places = @"parking|bus_station|train_station|transit_station";
        GoogleMapView *googleMapView = [GoogleMapView new];
        CLLocationCoordinate2D coordLocation = _locationManager.location.coordinate;
        
        if (isPickUp && pickUpAddr) {
            coordLocation = pickUpAddr.coordinate;
        }
        if (!isPickUp && dropOffAddr) {
            coordLocation = dropOffAddr.coordinate;
        }
        
        [googleMapView fetchPlacesNearCoordinate:coordLocation withSearchString:places withCompletionBlock:^(NSArray *matchesFound, NSError *error) {
            
            _tableViewOfLocations.hidden = YES;
            
            if (matchesFound) {
                [self plotPositions:matchesFound withLocation:coordLocation];
            }
            else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message"
                                                                message:@"Could not find location."
                                                               delegate:nil
                                                      cancelButtonTitle:nil
                                                      otherButtonTitles:@"Dismiss", nil];
                [alert show];
            }
            return;
        }];
    }
    else {
        CLGeocoder* geoCoder = [[CLGeocoder alloc]init];
        [geoCoder geocodeAddressString:address completionHandler:^(NSArray *placemarks, NSError *error) {
            
            if (placemarks) {
                
               
                CLPlacemark *addressPlacemark = [placemarks firstObject];
                
                NSLog(@"%@ %@",addressPlacemark.postalCode, addressPlacemark.thoroughfare);
                if (addressPlacemark.postalCode.length <=0 || addressPlacemark.thoroughfare.length <= 0 || addressPlacemark.subThoroughfare.length <= 0) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message"
                                                                    message:@"Please enter the complete address."
                                                                   delegate:nil
                                                          cancelButtonTitle:nil
                                                          otherButtonTitles:@"Dismiss", nil];
                    
                    
                    [alert show];
                    if (isPickUp) {
                        _pickupLocationTextField.text = @"";
                    }
                    else {
                        _dropOffLocationTextField.text = @"";
                    }
                    return;
                }
                
                [self setPickUpOrDropOffLocation:address andCLPlaceMark:addressPlacemark andIsPickUp:isPickUp];
                
                [self updateFareAfterAddressValidated];
            }
        }];
    }
    
}

- (void)callGooglePlacesApi:(NSString *) address {
    NSString *places;
    if (([address rangeOfString:@"park and ride" options:NSCaseInsensitiveSearch].location != NSNotFound) || ([address rangeOfString:@"park & ride" options:NSCaseInsensitiveSearch].location != NSNotFound)) {
        places = @"park&ride";
    }
    GoogleMapView *googleMapView = [GoogleMapView new];
    [googleMapView fetchPlacesNearCoordinate:_locationManager.location.coordinate withSearchString:places withCompletionBlock:^(NSArray *matchesFound, NSError *error) {
        _tableViewOfLocations.hidden = YES;
        
        if (matchesFound.count > 0) {
            
            [self plotPositions:matchesFound withLocation:_locationManager.location.coordinate];
            return;
        }
    }];
}


- (void)setPickUpOrDropOffLocation:(NSString *)address andCLPlaceMark:(CLPlacemark *)addressPlacemark andIsPickUp:(BOOL)isPickUp {
    if (isPickUp) {
        
        if (![_pickupLocationTextField.text isEqualToString:address]) {
            isPickUpEdited = YES;
        }
        else {
            isPickUpEdited = NO;
        }
        _pickupLocationTextField.text = address;
        pickUpAddr = [addressPlacemark location];
        pickUpAddress = addressPlacemark;
        
        
        [self setPickupLocation:pickUpAddr AndDropOffLocation:dropOffAddr];
        [_pickupLocationTextField resignFirstResponder];
        _tableViewOfLocations.hidden = YES;
 
    }
    else {
        if (![_dropOffLocationTextField.text isEqualToString:address]) {
            isDropOffEdited = YES;
        }
        else {
            isDropOffEdited = NO;

        }
        _dropOffLocationTextField.text = address;
        dropOffAddr = [addressPlacemark location];
        dropOffAddress = addressPlacemark;
        [self setPickupLocation:pickUpAddr AndDropOffLocation:dropOffAddr];
        [_dropOffLocationTextField resignFirstResponder];
        _tableViewOfLocations.hidden = YES;
       
    }
}



- (void)plotPositions:(NSArray *)data withLocation:(CLLocationCoordinate2D)coord {
    
    float zoomLevel = 11;
    camera = [GMSCameraPosition cameraWithLatitude:coord.latitude
                                         longitude:coord.longitude zoom:zoomLevel];
    
    _mapView.delegate = self;
    _mapView.camera = camera;
    
    
    
    //Loop through the array of places returned from the Google API.
    for (int i=0; i<[data count]; i++) {
        //Retrieve the NSDictionary object in each index of the array.
        NSDictionary* place = [data objectAtIndex:i];
        
        //There is a specific NSDictionary object that gives us location info.
        NSDictionary *geo = [place objectForKey:@"geometry"];
        
        
        //Get our name and address info for adding to a pin.
        NSString *name=[place objectForKey:@"name"];
        //Get the lat and long for the location.
        NSDictionary *loc = [geo objectForKey:@"location"];
        
        //Create a special variable to hold this coordinate info.
        CLLocationCoordinate2D placeCoord;
        
        //Set the lat and long.
        placeCoord.latitude=[[loc objectForKey:@"lat"] doubleValue];
        placeCoord.longitude=[[loc objectForKey:@"lng"] doubleValue];
        
        _marker = [GMSMarker markerWithPosition:placeCoord];
        _marker.title = name;
        // _marker.icon = [UIImage s];
        _marker.map = _mapView;
    }
}



- (void)loadAddresses:(NSArray *) results {
    _tableViewOfLocations.allowsSelection = YES;
    _tableViewOfLocations.inputArray = results;
    
    if (_tableViewOfLocations.inputArray.count > 0) {
         _tableViewOfLocations.hidden = NO;
    }
   
}

#pragma TimePicker
- (void)tapArrival:(id)sender {
    _timePickerLabel.text = @"What is your desired time to arrive at work?";
    _timePickerView.hidden = NO;
    
    if (_arriveTime.text.length > 0) {
        NSDate *date = [[self getTimeFormatter:YES] dateFromString:_arriveTime.text];
        [_datePicker setDate:date];
    }
   
    isArrival = YES;
    [self hideShowCalendarControls:YES];
    
    if ([[UIScreen mainScreen] bounds].size.height < 568) {
        keyBoardOffset = 100;
        [self keyboardWillShow];
    }
}


- (void)tapDeparture:(id)sender {
    _timePickerLabel.text = @"What is your desired time to depart from work?";
    _timePickerView.hidden = NO;
      [self hideShowCalendarControls:YES];
    isArrival = NO;
    if (_departTime.text.length > 0) {
        
        NSDate *date = [[self getTimeFormatter:YES] dateFromString:_departTime.text];
        [_datePicker setDate:date];
        
    }
    if ([[UIScreen mainScreen] bounds].size.height < 568) {
         keyBoardOffset = 100;
         [self keyboardWillShow];
        
    }
}

-(void)tapCalendar:(id)sender {
    [self showCalendar:sender];
}

- (void) addGesturesForTimePicker {
    tapForArrival =[[UITapGestureRecognizer alloc] initWithTarget:self    action:@selector(tapArrival:)];
    [tapForArrival setNumberOfTapsRequired:1];
    [_arrivalTimeView addGestureRecognizer:tapForArrival];
    
    tapForDeparture =[[UITapGestureRecognizer alloc] initWithTarget:self    action:@selector(tapDeparture:)];
    [tapForDeparture setNumberOfTapsRequired:1];
    [_departTimeView addGestureRecognizer:tapForDeparture];
    
    tapForCalendar =[[UITapGestureRecognizer alloc] initWithTarget:self    action:@selector(tapCalendar:)];
    [tapForCalendar setNumberOfTapsRequired:1];
    [_showCalendarView addGestureRecognizer:tapForCalendar];
    
}

#pragma Helper Methods
- (void) hideControls:(BOOL) hide {
    _tableViewOfLocations.hidden = hide;
    _timePickerView.hidden = hide;
    _calendarContentView.hidden = hide;
    _calendarMenuView.hidden = hide;
    _calendarContentImage.hidden = hide;
    _calendarMenuImage.hidden = hide;
    _findMuV.hidden = hide;
    
    [self hideControlsBelowSearchBar:hide];
    [self hideControlsforPaymentView:hide];
}

-(void)hideControlsBelowSearchBar:(BOOL) hide {
    _bookingBackgroundView.hidden = hide;
    
    if (tripManager.editedRide) {
        if (!hide) {
            _showCalendarView.hidden = hide;
            if ([tripManager.editedRide.routeType isEqualToString: ARRIVAL] || isDepartureOnly) {
                _arrivalTimeView.hidden = hide;
            }
            else if (![tripManager.editedRide.routeType isEqualToString: ARRIVAL] || isArrivalOnly)    {
                _departTimeView.hidden = hide;
            }
        }
    }
    else {
        _arrivalTimeView.hidden = hide;
        _departTimeView.hidden = hide;
        _showCalendarView.hidden = hide;
    }
}

-(void)hideControlsforPaymentView:(BOOL) hide {
    _arrivalDriverView.hidden = hide;
    _departureDriverView.hidden = hide;
    _paymentMethodView.hidden = hide;
    //_couponTextField.hidden = hide; //uncomment this after functionality done SMN
    _totalFareView.hidden = hide;
    
    _cancelButton.hidden = hide;
    
    //coupon stuff
    _couponTextField.hidden = hide;
    _couponViewButton.hidden = hide;
    _couponApplyButton.hidden = hide;
}

-(void)hideConfirmButton:(BOOL) hide {
    _confirmButton.hidden = hide;
    _paymentEditButton.hidden = hide;
    
}

//check if all the fields on this form are filled to enable the MuV button
- (BOOL) areAllFieldsFilled {
    
    bool isFilled = NO;
    
    if (![_dateSelected.text isEqualToString:@"Select Date"] ) {
        if (isRoundTrip){
            if (_arriveTime.text.length && _departTime.text.length) {
                isFilled = YES;
            }
        } else {
            if (_arriveTime.text.length ||_departTime.text.length > 0) {
                isFilled = YES;
            }
        }
    }
    
    return isFilled;
}

- (BOOL) areAddressesFilled {
    bool isFilled = YES;
    
    if ((!_pickupLocationTextField.text.length > 0) || ([_pickupLocationTextField.text  isEqual: @"park and ride"])) {
        isFilled = NO;
    }
    if (!_dropOffLocationTextField.text.length > 0 || ([_dropOffLocationTextField.text  isEqual: @"park and ride"])) {
        isFilled = NO;
    }
    return isFilled;
}

- (BOOL) areAddressesAndFareFilled {
    bool isFilled = YES;
    
    if (![self areAddressesFilled]) {
        isFilled = NO;
    }
    if (!_totalFare.text.length > 0) {
        isFilled = NO;
    }
    return isFilled;
}

#pragma mark - GMSMapViewDelegate

- (void)mapView:(GMSMapView *)mapView didEndDraggingMarker:(GMSMarker *)marker{
    
    marker.title = [NSString stringWithFormat:@"%.2f %.2f", marker.position.latitude,marker.position.longitude];
    [self.mapView.settings setScrollGestures:YES];
    [self.marker setIcon:[UIImage imageNamed:@"map_pin_blue_lg"]];
    
}

- (void)mapView:(GMSMapView *)mapView didTapOverlay:(GMSOverlay *)overlay{
    [self.mapView.settings setScrollGestures:YES];
}

- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker {

    GoogleMapView *map = [GoogleMapView new];
    [map reverseGeocodeCoordinate:marker.position withCompletionBlock:^(BOOL completed, NSString *address1, NSString *city, NSString *state, NSString *zip) {
        
        if (completed) {
            NSString *address = [NSString stringWithFormat:@"%@, %@, %@, %@",address1, city, state, zip];
            CLLocation *location = [[CLLocation alloc] initWithLatitude:marker.position.latitude longitude:marker.position.longitude];
            if (isPickUpLocation) {
                _pickupLocationTextField.text = address;
                pickUpAddr = location;
                
            }
            else {
                _dropOffLocationTextField.text = address;
                dropOffAddr = location;
            }
            [self updateFare];
            if (_dateSelected.text.length > 0 && ![_dateSelected.text isEqualToString:@"Select Date"]) {
                if ((isRoundTrip && _arriveTime.text.length > 0 && _departTime.text.length > 0) || (!isRoundTrip && _arriveTime.text.length > 0) || (!isRoundTrip && _departTime.text.length > 0)) {
                    [self hideControlsBelowSearchBar:NO];
                    _findMuV.hidden = NO;
                }
            }
        }
    }];
    
    return NO;
}


#pragma mark - CCLocationManagerDelegate

-(void) setPickupLocation:(CLLocation *)pickUpLocation AndDropOffLocation:(CLLocation *)dropOffLocation {
    
    [self.mapView clear];
    
    float zoom=10;
    
    if(self.zoomLevel>0) zoom=self.zoomLevel;
    
    if (pickUpLocation) {
        camera = [GMSCameraPosition cameraWithLatitude:pickUpAddress.location.coordinate.latitude
                                             longitude:pickUpAddress.location.coordinate.longitude
                                                  zoom:zoom];
    } else {
        camera = [GMSCameraPosition cameraWithLatitude:dropOffAddress.location.coordinate.latitude
                                             longitude:dropOffAddress.location.coordinate.longitude
                                                  zoom:zoom];
    }
    
    _mapView.delegate = self;
    _mapView.camera = camera;
    
    if (pickUpLocation) {
        
        CLLocationCoordinate2D pickUpPosition = CLLocationCoordinate2DMake(pickUpAddress.location.coordinate.latitude,pickUpAddress.location.coordinate.longitude);

        self.markerPositionA = [GMSMarker markerWithPosition:pickUpPosition];
        
        if (pickUpAddress.subThoroughfare) {
             self.markerPositionA.snippet = [NSString stringWithFormat:@"%@ %@, %@, %@, %@", pickUpAddress.subThoroughfare, pickUpAddress.thoroughfare, pickUpAddress.locality,pickUpAddress.administrativeArea, pickUpAddress.postalCode];
        }
        else {
             self.markerPositionA.snippet = [NSString stringWithFormat:@"%@, %@, %@, %@", pickUpAddress.thoroughfare, pickUpAddress.locality,pickUpAddress.administrativeArea, pickUpAddress.postalCode];
        }
       
        self.markerPositionA.title=@"Pick Up Location";
    }
    if (dropOffLocation) {
        
        CLLocationCoordinate2D dropOffPosition = CLLocationCoordinate2DMake(dropOffAddress.location.coordinate.latitude,dropOffAddress.location.coordinate.longitude);
        
        self.markerPositionB = [GMSMarker markerWithPosition:dropOffPosition];
        if (dropOffAddress.subThoroughfare) {
             self.markerPositionB.snippet = [NSString stringWithFormat:@"%@ %@, %@, %@, %@", dropOffAddress.subThoroughfare, dropOffAddress.thoroughfare, dropOffAddress.locality,dropOffAddress.administrativeArea, dropOffAddress.postalCode];
        }
        else {
            self.markerPositionB.snippet = [NSString stringWithFormat:@"%@, %@, %@, %@", dropOffAddress.thoroughfare, dropOffAddress.locality,dropOffAddress.administrativeArea, dropOffAddress.postalCode];
        }
       
        self.markerPositionB.title= @"Drop Off Location";
    }
    
    self.markerPositionA.map = self.mapView;
    self.markerPositionB.map = self.mapView;
}

#pragma mark - JTCalendarDataSource

- (void) initCalendarControl {
    
    [self setCurrentDate:_currentDate];
    
    [self hideShowCalendarControls:YES];
    
    // _eventsTable.delegate = self;
    // Do any additional setup after loading the view, typically from a nib.
    self.calendar = [JTCalendar new];
    
    // All modifications on calendarAppearance have to be done before setMenuMonthsView and setContentView
    // Or you will have to call reloadAppearance
    {
        self.calendar.calendarAppearance.calendar.firstWeekday = 1; // Sunday == 1, Saturday == 7
        self.calendar.calendarAppearance.dayCircleRatio = 9. / 10.;
        self.calendar.calendarAppearance.ratioContentMenu = 1.;
        self.calendar.calendarAppearance.showEvents = YES;
        
    }
    
    [self.calendar setMenuMonthsView:_calendarMenuView];
    [self.calendar setContentView:_calendarContentView];
    [self.calendar setDataSource:self];
}

- (NSInteger)calendarHaveEvent:(JTCalendar *)calendar date:(NSDate *)date {
    bool isArrivalScheduled = NO;
    bool isDepartureScheduled = NO;
    bool isArrivalRequested = NO;
    bool isDepartureRequested = NO;
    
    if (![tripManager.rides count] > 0) {
        return 0;
    }
    else {
        
        NSArray *arrayOfTrips = [tripManager.rides objectForKey:[Helper getStringFromDate:date]];
        
        if (arrayOfTrips.count > 0) {
            for (Ride *ride in arrayOfTrips) {
                if ([ride.routeType isEqualToString:ARRIVAL]) {
                    if (ride.isRequested) {
                        isArrivalRequested = YES;
                    }
                    else {
                        isArrivalScheduled = YES;
                    }
                }
                if ([ride.routeType isEqualToString:DEPARTURE]) {
                    if (ride.isRequested) {
                        isDepartureRequested = YES;
                    }
                    else {
                        isDepartureScheduled = YES;
                    }
                }
            }
            if (isArrivalScheduled && isDepartureScheduled) return Arrival_Departure_Scheduled;
            if (isArrivalScheduled && isDepartureRequested) return ArrivalScheduled_DepartureRequested;
            if (isArrivalRequested && isDepartureScheduled) return ArrivalRequested_DepartureScheduled;
            if (isArrivalRequested && isDepartureRequested) return Arrival_Departure_Requested;
            
            if (isArrivalScheduled) return ArrivalScheduled;
            if (isDepartureScheduled) return DepartureScheduled;
            
            if (isArrivalRequested) return ArrivalRequested;
            if (isDepartureRequested) return DepartureRequested;
        }
        return NoEvent;
    }
    
}

- (void)setCurrentDate:(NSDate *)currentDate {
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    calendar.timeZone =[NSTimeZone localTimeZone];
    NSDateComponents *comps = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth fromDate:currentDate];
    
    NSInteger monthIndex = comps.month;
    NSInteger year = comps.year;
    
    while(monthIndex <= 0){
        monthIndex += 12;
    }
    
    static NSDateFormatter *dateFormatter;
    if(!dateFormatter){
        dateFormatter = [NSDateFormatter new];
        dateFormatter.timeZone = [NSTimeZone localTimeZone];
    }
    
    _currentMonth.text = [NSString stringWithFormat:@"%@ %ld",[[dateFormatter standaloneMonthSymbols][monthIndex - 1] capitalizedString], (long)year];
 }

- (void)loadNextPrevMonth:(BOOL)isNextMonth {
    if (!rideManager) {
        rideManager = [RideManager new];
    }
    if (isDriver) {
        [rideManager getTripsForDriver:Calendar forDate:_currentDate withCompletionBlock:^(NSDictionary *rides, NSArray *tripDates, NSError *error) {
            if (error == nil) {
               // NSLog(@"SMN Month is %@ and num routes are %lu", _currentDate, (unsigned long)rides.count);
                tripManager.rides = rides;
                tripManager.tripDates = tripDates;
                if (isNextMonth) {
                    [self.calendar loadNextMonth];
                }
                else {
                    [self.calendar loadPreviousMonth];
                }
                [self setCurrentDate:_currentDate];
            }
        }];
    }
    else {
        [rideManager getRidesForRiderForTripView:Calendar forDate:_currentDate withCompletionBlock:^(NSDictionary *rides, NSArray *tripDates, NSError *error) {
            if (error == nil) {
             //   NSLog(@"Month is %@ and num routes are %lu", _currentDate, (unsigned long)rides.count);
                tripManager.rides = rides;
                tripManager.tripDates = tripDates;
                if (isNextMonth) {
                    [self.calendar loadNextMonth];
                }
                else {
                    [self.calendar loadPreviousMonth];
                }
                [self setCurrentDate:_currentDate];
            }
            else {
                NSLog(@"Error in calendar loading");
            }
        }];
    }
}


- (IBAction)prevMonth:(id)sender {
    
    NSCalendar *cal         = [NSCalendar currentCalendar];
    NSDateComponents *comps = [cal components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit
                                     fromDate:_currentDate];
    comps.month             -= 1;
    _currentDate          = [cal dateFromComponents:comps];
    
    [self loadNextPrevMonth:NO];
}


- (IBAction)nextMonth:(id)sender {
    NSCalendar *cal         = [NSCalendar currentCalendar];
    NSDateComponents *comps = [cal components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit
                                     fromDate:_currentDate];
    comps.month             += 1;
    _currentDate          = [cal dateFromComponents:comps];
    
    [self setCurrentDate:_currentDate];
    
    [self loadNextPrevMonth:YES];

}

- (void)getCoordinatesFromAddress:(NSString *)address {
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder geocodeAddressString:_pickupLocationTextField.text completionHandler:^(NSArray *placemarks, NSError *error) {
        if (error) {
            NSLog(@"%@", error);
        } else {
            CLPlacemark *placemark = [placemarks lastObject];
            MKCoordinateRegion region;
            region.center.latitude = placemark.location.coordinate.latitude;
            region.center.longitude = placemark.location.coordinate.longitude;
            if (isPickUpLocation) {
                pickUpAddr = placemark.location;
            }
            else {
                dropOffAddr = placemark.location;
            }
        }
    }];
}



- (void)calendarDidDateSelected:(JTCalendar *)calendar date:(NSDate *)date {
    NSString *dateInString = [Helper getStringFromDate:date];
    NSArray *arrayOfTrips = [tripManager.rides objectForKey:dateInString];
    
    if ([Helper isPastDay:date])
        return;
    
    if (isDriver) {
        //check if date selected is within 30 days
        if(![Helper isDate:date WithinRange:30])
            return;
    }
    else {
        //check if date selected is within 15 days
        if(![Helper isDate:date WithinRange:15])
            return;
    }
    if (arrayOfTrips.count == 2) {
        
        if (isDriver) {
            return;
        }
        
        //msg saying two dates
        NSInteger tripStatus = [self checkIfBookingAllowed:arrayOfTrips];
        if (tripStatus == Arrival_Departure_Scheduled) {
            return;
        }
        else if (tripStatus == ArrivalRequested_DepartureScheduled) {
            
            [self viewForArriveOnly];
            [self updateFare];
        }
        else if (tripStatus == ArrivalScheduled_DepartureRequested) {
            
            [self viewForDepartOnly];
            [self updateFare];
        }
    }
    else if (arrayOfTrips.count == 1) {
        //check if arrival or departure
        //give appropriate msg and disable the text field
         Ride *ride = arrayOfTrips[0];
        
        
        if (!isDriver) {
            BOOL canBookBothLegs = NO;
            
            if (ride.isRequested) {
                canBookBothLegs = YES;
            }
            else {
                canBookBothLegs = NO;
            }
            
            if (!canBookBothLegs && [ride.routeType isEqualToString:ARRIVAL]) {
                [self viewForDepartOnly];
                [self updateFare];
            }
            if (!canBookBothLegs && ![ride.routeType isEqualToString:ARRIVAL]) {
                [self updateFare];
                [self viewForArriveOnly];
            }
        }
        else {
            if ([ride.routeType isEqualToString:ARRIVAL]) {
                [self viewForDepartOnly];
            }
            else {
                [self viewForArriveOnly];
            }
        }
    }
    else {
        [self viewForRoundTrip];
        
    }
    [self setCalendarField:date];

}

- (void)viewForDepartOnly {
    [self oneWayButton];
    isDepartureOnly = YES;
    _arrivalTimeView.hidden = YES;
    _arriveTime.text = @"";
    _departTimeView.hidden = NO;
    _roundTripButton.enabled = NO;
    _oneWayTripButton.enabled = NO;
}

- (void)viewForArriveOnly {
     [self oneWayButton];
    isArrivalOnly = YES;
    _departTimeView.hidden = YES;
    _departTime.text = @"";
    _arrivalTimeView.hidden = NO;
    _roundTripButton.enabled = NO;
    _oneWayTripButton.enabled = NO;
    
}

- (void)viewForRoundTrip {
    _departTimeView.hidden = NO;
    _arrivalTimeView.hidden = NO;
    isDepartureOnly = NO;
    isArrivalOnly = NO;
    _roundTripButton.enabled = YES;
    _oneWayTripButton.enabled = YES;
}

- (NSInteger)checkIfBookingAllowed:(NSArray *)arrayOfTrips {
    BOOL isArrivalRequested = NO;
    BOOL isDepartureRequested = NO;
    NSInteger retValue = Arrival_Departure_Scheduled;
    for (Ride *ride in arrayOfTrips) {
        if (ride.isRequested) {
            if ([ride.routeType isEqualToString:ARRIVAL]) {
                isArrivalRequested = YES;
            }
            else {
                isDepartureRequested = YES;
            }
        }
        else {
            if ([ride.routeType isEqualToString:DEPARTURE]) {
                isDepartureRequested = NO;
            }
            else {
                isArrivalRequested = NO;
            }
        }
    }
    if ((isArrivalRequested) && (isDepartureRequested)) {
        retValue = Arrival_Departure_Requested;
    }
    else if ((isArrivalRequested) && (!isDepartureRequested)) {
        retValue = ArrivalRequested_DepartureScheduled;
    }
    if ((!isArrivalRequested) && (isDepartureRequested)) {
        retValue = ArrivalScheduled_DepartureRequested;
    }
    if ((!isArrivalRequested) && (!isDepartureRequested)) {
        retValue = Arrival_Departure_Scheduled ;
    }
    return retValue;
}

- (void)setCalendarField:(NSDate *)date {
    
    _dateSelected.text = [self getFormattedDateInString:date];
    _dateSelected.textColor = [UIColor colorWithRed:0.557f green:0.694f blue:0.275f alpha:1.00f];
    
    if (![self checkIfPastDate:YES]) {
        [self hideShowCalendarControls:YES];
        
        if([self areAllFieldsFilled]) {
            _findMuV.hidden = NO;
        }

    }
    
}

- (NSString *) getFormattedDateInString:(NSDate *)date {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:DATE_FORMAT];
    
    //Optionally for time zone conversions
    [formatter setTimeZone:[NSTimeZone systemTimeZone]];
    return [formatter stringFromDate:date];
}

#pragma Move the screen up or down



- (void)keyboardWillShow {
    // Animate the current view out of the way
    if (self.view.frame.origin.y >= 0) {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0) {
        [self setViewMovedUp:NO];
    }
}

- (void)keyboardWillHide {
    if (self.view.frame.origin.y >= 0) {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0) {
        [self setViewMovedUp:NO];
    }
}

//method to move the view up/down whenever the keyboard is shown/dismissed
- (void)setViewMovedUp:(BOOL)movedUp {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    if (movedUp) {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        rect.origin.y -= keyBoardOffset;
        rect.size.height += keyBoardOffset;
    }
    else {
        // revert back to the normal state.
        rect.origin.y += keyBoardOffset;
        rect.size.height -= keyBoardOffset;
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    for (UIView * txt in self.view.subviews){
        if ([txt isKindOfClass:[UITextField class]] && [txt isFirstResponder]) {
            [txt resignFirstResponder];
        }
    }
}

@end
