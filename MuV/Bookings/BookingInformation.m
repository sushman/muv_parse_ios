//
//  BookingInformation.m
//  MuV
//
//  Created by Hing Huynh on 2/4/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "BookingInformation.h"

@implementation BookingInformation

static BookingInformation* bookingInfo = nil;

+(BookingInformation*)sharedMySingleton
{
    @synchronized([BookingInformation class])
    {
        if (!bookingInfo)
            bookingInfo = [[self alloc] init];
        
        return bookingInfo;
    }
    
    return nil;
}

- (id)init {
    if (self = [super init]) {
    }
    return self;
}

@end
