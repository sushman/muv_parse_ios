//
//  DriverDetailsCell.m
//  MuV
//
//  Created by Hing Huynh on 1/28/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "DriverDetailsCell.h"
#import "UIImageView+MuVUIImageViewAdditions.h"

@implementation DriverDetailsCell

-(void)layoutSubviews
{
    _profileImageView = [UIImageView muv_imageCircle:_profileImageView];
    
}


@end
