//
//  CarDetailsCell.m
//  MuV
//
//  Created by Hing Huynh on 1/28/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "CarDetailsCell.h"
#import "UIButton+MuVUIButtonAdditions.h"
#import "UIImageView+MuVUIImageViewAdditions.h"

@implementation CarDetailsCell

-(void)layoutSubviews
{
    _carImageView = [UIImageView muv_imageCircle:_carImageView];
    
    _riderOneButton = [UIButton muv_buttonCircle:_riderOneButton];
    _riderTwoButton = [UIButton muv_buttonCircle:_riderTwoButton];
    _riderThreeButton = [UIButton muv_buttonCircle:_riderThreeButton];
    _riderFourButton = [UIButton muv_buttonCircle:_riderFourButton];
    _riderFiveButton = [UIButton muv_buttonCircle:_riderFiveButton];
    
    _bookMuVButon = [UIButton muv_buttonUpdate:_bookMuVButon];
    
}


@end
