//
//  MuVDriveProfileTableView.m
//  MuV
//
//  Created by Hing Huynh on 1/28/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "MuVDriveProfileTableView.h"
#import "DriverAboutMeCell.h"
#import "DriverDetailsCell.h"
#import "CarDetailsCell.h"
#import "UIImageView+WebCache.h"
#import "UIButton+WebCache.h"
#import "MuVRiderProfileTableView.h"
#import "MuVBestMatchesViewController.h"
#import "UIViewController+JASidePanel.h"
#import "MuVSidePanelController.h"
#import "ControlVariables.h"

@implementation MuVDriveProfileTableView- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _bookingInfo = [BookingInformation sharedMySingleton];
    _userInfo = [UserProfileInformation sharedMySingleton];
    self.tableView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"MuV_MainWhiteBG"]];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0 || indexPath.row == 1){
        return 170;
    } else {
        return 164;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0){
        DriverDetailsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DriverDetailsCell"];
        cell.nameLabel.text = _ride.driverName;
        
        NSString *gender = @"     ";
        NSString *age = @"     ";
        NSString *relationship = @"     ";
        
        if (_ride.userDetails[@"age"]){
            age = _ride.userDetails[@"age"];
        }
        if (_ride.userDetails[@"gender"]){
            gender = [[_ride.userDetails[@"gender"]substringToIndex:1]capitalizedString];
            
        }
        if (_ride.userDetails[@"relationship"]){
            relationship = [_ride.userDetails[@"relationship"]capitalizedString];
        }
        
        cell.extraLabel.text = [NSString stringWithFormat:@"%@ / %@ / %@",age,gender,relationship];
        
        cell.occupationLabel.text = _ride.userDetails[@"occupation"];
        cell.companyLabel.text = [_ride.userDetails[@"companyName"]capitalizedString];
        [cell.profileImageView sd_setImageWithURL:[NSURL URLWithString:_ride.driverPicture] placeholderImage:[UIImage imageNamed:NOIMAGE_AVATAR]];
        return cell;
        
    } else if (indexPath.row == 1) {
        DriverAboutMeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DriverAboutMeCell"];
        cell.profileTextView.text = _ride.userDetails[@"profile"];
        return cell;
        
    } else {
        CarDetailsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CarDetailsCell"];
        
        PFFile *image = _ride.driverDetails[@"carImage"];
        [cell.carImageView sd_setImageWithURL:[NSURL URLWithString:image.url] placeholderImage:[UIImage imageNamed:MUV_NO_CAR_IMAGE]];
        
        [cell.carLabel setText:[NSString stringWithFormat:@"%@ %@ %@", _ride.driverDetails[@"carYear"], _ride.driverDetails[@"carModel"], _ride.driverDetails[@"carMake"]]];
        
        
        
        // UPDATING UI TO REFLECT DRIVER RATING
        if ([_ride.driverRating floatValue] > 0){
            [cell.starOne setImage:[UIImage imageNamed:@"MuV_RateStar_HalfStar"]];
        }
        if ([_ride.driverRating floatValue] > 0.5){
            [cell.starOne setImage:[UIImage imageNamed:@"MuV_RateStar_Blu"]];
        }
        if ([_ride.driverRating floatValue] > 1){
            [cell.starTwo setImage:[UIImage imageNamed:@"MuV_RateStar_HalfStar"]];
        }
        if ([_ride.driverRating floatValue] > 1.5){
            [cell.starTwo setImage:[UIImage imageNamed:@"MuV_RateStar_Blu"]];
        }
        if ([_ride.driverRating floatValue] > 2){
            [cell.starThree setImage:[UIImage imageNamed:@"MuV_RateStar_HalfStar"]];
        }
        if ([_ride.driverRating floatValue] > 2.5){
            [cell.starThree setImage:[UIImage imageNamed:@"MuV_RateStar_Blu"]];
        }
        if ([_ride.driverRating floatValue] > 3){
            [cell.starFour setImage:[UIImage imageNamed:@"MuV_RateStar_HalfStar"]];
        }
        if ([_ride.driverRating floatValue] > 3.5){
            [cell.starFour setImage:[UIImage imageNamed:@"MuV_RateStar_Blu"]];
        }
        if ([_ride.driverRating floatValue] > 4){
            [cell.starFive setImage:[UIImage imageNamed:@"MuV_RateStar_HalfStar"]];
        }
        if ([_ride.driverRating floatValue] > 4.5){
            [cell.starFive setImage:[UIImage imageNamed:@"MuV_RateStar_Blu"]];
        }
        
        if ([_ride.totalSeats isEqual: @4]){
            cell.riderFiveButton.hidden = YES;
        } else if ([_ride.totalSeats isEqual: @3]){
            cell.riderFiveButton.hidden = YES;
            cell.riderFourButton.hidden = YES;
        } else if ([_ride.totalSeats isEqual: @2]) {
            cell.riderFiveButton.hidden = YES;
            cell.riderFourButton.hidden = YES;
            cell.riderThreeButton.hidden = YES;
        } else if ([_ride.totalSeats isEqual: @1]) {
            cell.riderFiveButton.hidden = YES;
            cell.riderFourButton.hidden = YES;
            cell.riderThreeButton.hidden = YES;
            cell.riderTwoButton.hidden = YES;
        } else {
            
        }
        
        // UPDATE PASSENGER IMAGES
        if (_ride.otherPassengers.count > 0){
            int passengerCount = 1;
            for (PFObject *passengerRide in _ride.otherPassengers){
                PFObject *rider = passengerRide[@"rider"];
                PFFile *passengerImage =  rider[@"profileImage"];
                if (passengerCount == 1 ){
                    [cell.riderOneButton sd_setImageWithURL:[NSURL URLWithString:passengerImage.url] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"MuV_NOimageAvatar"]];
                    cell.riderOneButton.enabled = YES;
                }
                if (passengerCount == 2){
                    [cell.riderTwoButton sd_setImageWithURL:[NSURL URLWithString:passengerImage.url] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"MuV_NOimageAvatar"]];
                    cell.riderTwoButton.enabled = YES;
                    
                }
                if (passengerCount == 3){
                    [cell.riderThreeButton sd_setImageWithURL:[NSURL URLWithString:passengerImage.url] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"MuV_NOimageAvatar"]];
                    cell.riderThreeButton.enabled = YES;
                    
                }
                if (passengerCount == 4){
                    [cell.riderFourButton sd_setImageWithURL:[NSURL URLWithString:passengerImage.url] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"MuV_NOimageAvatar"]];
                    cell.riderFourButton.enabled = YES;
                    
                }
                if (passengerCount == 5){
                    [cell.riderFiveButton sd_setImageWithURL:[NSURL URLWithString:passengerImage.url] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"MuV_NOimageAvatar"]];
                    cell.riderFiveButton.enabled = YES;
                    
                }
                passengerCount++;
            }
        }
        
        return cell;
    }
    
}

- (IBAction)ButtonPressed:(id)sender {
    switch ([sender tag]) {
        case Phone:
            [self callDriver];
            break;
        case Email:
            [self emailDriver];
            break;
        case Text:
            [self textDriver];
            break;
        case PassengerOne:
            _riderNumber = 0;
            [self performSegueWithIdentifier:@"riderProfileSegue" sender:sender];
            break;
        case PassengerTwo:
            _riderNumber = 1;
            [self performSegueWithIdentifier:@"riderProfileSegue" sender:sender];
            break;
        case PassengerThree:
            _riderNumber = 2;
            [self performSegueWithIdentifier:@"riderProfileSegue" sender:sender];
            break;
        case PassengerFour:
            _riderNumber = 3;
            [self performSegueWithIdentifier:@"riderProfileSegue" sender:sender];
            break;
        case PassengerFive:
            _riderNumber = 4;
            [self performSegueWithIdentifier:@"riderProfileSegue" sender:sender];
            break;
        case Book:
            if ((_arrivalBooked && _isArrival && _isDeparture)||(_arrivalRide && !_isDeparture)){
                [self bookMUV:(BestMatchedRides *) _ride];
                [self saveBookingToSingleton];
                [self loadBookingView];
            } else {
                [self bookMUV:(BestMatchedRides *) _ride];
                [self performSegueWithIdentifier:@"backToBestMatchesSegue" sender:sender];
            }
        default:
            break;
    }
}

- (void) callDriver {
    NSString *phoneNumber = [[_ride.userDetails[@"phone"] componentsSeparatedByCharactersInSet:
                              [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                             componentsJoinedByString:@""];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[@"tel:" stringByAppendingString:phoneNumber]]];
}

- (void) emailDriver {
    if([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailComposer = [[MFMailComposeViewController alloc]init];
        [mailComposer setToRecipients:@[_ride.userDetails[@"email"]]];
        mailComposer.mailComposeDelegate = self;
        [mailComposer setSubject:@"Message from MüV User"];
        [self presentViewController:mailComposer animated:YES completion:NULL];
    }else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Your device's email client is not setup." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
}

-(void) textDriver {
    if(![MFMessageComposeViewController canSendText]) {
        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Your device doesn't support SMS." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [warningAlert show];
        return;
    }
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    NSString *phoneNumber = [[_ride.userDetails[@"phone"] componentsSeparatedByCharactersInSet:
                              [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                             componentsJoinedByString:@""];
    [messageController setRecipients:@[phoneNumber]];
    [self presentViewController:messageController animated:YES completion:nil];
}


- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed to send SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            break;
        }
        case MessageComposeResultSent:
            break;
        default:
            break;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)mailComposeController:(MFMailComposeViewController *)controller
         didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    if (result) {
        NSLog(@"Result : %d",result);
    }
    if (error) {
        NSLog(@"Error : %@",error);
    }
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString: @"riderProfileSegue"]) {
        MuVRiderProfileTableView *profileViewController = (MuVRiderProfileTableView *) [segue destinationViewController];
        profileViewController.ride = _ride;
        NSLog(@"%d", _riderNumber);
        profileViewController.passenger = [_ride.otherPassengers objectAtIndex:_riderNumber][@"rider"];
        profileViewController.isArrival = _isArrival;
        profileViewController.isDeparture = _isDeparture;
        profileViewController.arrivalBooked = _arrivalBooked;
        profileViewController.arrivalRide = _arrivalRide;
        profileViewController.departureRide = _departureRide;
    }
    
    if ([segue.identifier isEqualToString: @"backToBestMatchesSegue"]) {
        MuVBestMatchesViewController *bestMatchesViewController = (MuVBestMatchesViewController *) [segue destinationViewController];
        bestMatchesViewController.isArrival = YES;
        bestMatchesViewController.isDeparture = YES;
        bestMatchesViewController.arrivalBooked = YES;
        bestMatchesViewController.arrivalRide = _arrivalRide;
        bestMatchesViewController.departureRide = _departureRide;
    }
}

- (void)bookMUV:(BestMatchedRides *)rides {
    
    //record the data
    if (_isArrival && _isDeparture &&_arrivalBooked) {
        _departureRide.rideNo = rides.route;
        _departureRide.driverDetail = rides.driverDetails;
        _departureRide.userDetail = _userInfo.user;
        _departureRide.driverName = rides.driverName;
        _departureRide.passengers = [rides.otherPassengers mutableCopy];
    } else if ((_isArrival && _isDeparture)||(_isArrival && !_isDeparture)) {
        _arrivalRide.rideNo = rides.route;
        _arrivalRide.driverDetail = rides.driverDetails;
        _arrivalRide.userDetail = _userInfo.user;
        _arrivalRide.driverName = rides.driverName;
        _departureRide.passengers = [rides.otherPassengers mutableCopy];
        
    } else {
        _departureRide.rideNo = rides.route;
        _departureRide.driverDetail = rides.driverDetails;
        _departureRide.userDetail = _userInfo.user;
        _departureRide.driverName = [rides.driverName mutableCopy];
    }
    
}

- (void) loadBookingView {
    dispatch_async(dispatch_get_main_queue(),^{
        MuVSidePanelController *sidePanelViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MuVSidePanelViewController"];
        sidePanelViewController.centerPanel = [self.storyboard instantiateViewControllerWithIdentifier:@"BookingNav"];
        [self presentViewController:sidePanelViewController animated:YES completion:nil];
    });
}

- (void) saveBookingToSingleton {
    _bookingInfo.isArrival = _isArrival;
    _bookingInfo.isDeparture = _isDeparture;
    _bookingInfo.arrivalBooked = _arrivalBooked;
    _bookingInfo.arrivalRide = _arrivalRide;
    _bookingInfo.departureRide = _departureRide;
}

@end
