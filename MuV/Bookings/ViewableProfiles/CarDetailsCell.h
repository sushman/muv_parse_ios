//
//  CarDetailsCell.h
//  MuV
//
//  Created by Hing Huynh on 1/28/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CarDetailsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *carImageView;

@property (weak, nonatomic) IBOutlet UIButton *riderOneButton;
@property (weak, nonatomic) IBOutlet UIButton *riderTwoButton;
@property (weak, nonatomic) IBOutlet UIButton *riderThreeButton;
@property (weak, nonatomic) IBOutlet UIButton *riderFourButton;
@property (weak, nonatomic) IBOutlet UIButton *riderFiveButton;

@property (weak, nonatomic) IBOutlet UIImageView *starOne;
@property (weak, nonatomic) IBOutlet UIImageView *starTwo;
@property (weak, nonatomic) IBOutlet UIImageView *starThree;
@property (weak, nonatomic) IBOutlet UIImageView *starFour;
@property (weak, nonatomic) IBOutlet UIImageView *starFive;

@property (weak, nonatomic) IBOutlet UILabel *carLabel;

@property (weak, nonatomic) IBOutlet UIButton *bookMuVButon;

@end
