//
//  DriverAboutMeCell.h
//  MuV
//
//  Created by Hing Huynh on 1/28/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DriverAboutMeCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UITextView *profileTextView;

@end
