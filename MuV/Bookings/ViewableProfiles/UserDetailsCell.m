//
//  UserDetailsCell.m
//  MuV
//
//  Created by Hing Huynh on 1/28/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "UserDetailsCell.h"
#import "UIImageView+MuVUIImageViewAdditions.h"

@implementation UserDetailsCell

-(void)layoutSubviews
{
    _profileImageView = [UIImageView muv_imageCircle:_profileImageView];
    
}

@end
