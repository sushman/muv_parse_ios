//
//  MuVRiderProfileTableView.h
//  MuV
//
//  Created by Hing Huynh on 1/28/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BestMatchedRides.h"
#import <Parse/Parse.h>
#import <MessageUI/MessageUI.h>
#import "Ride.h"
#import "UserProfileInformation.h"
#import "BookingInformation.h"

typedef enum{
    
    PhonePassenger = 0,
    EmailPassenger = 1,
    TextPassenger = 2,
    BookPassenger = 3
    
}passengerProfileButtons;

@interface MuVRiderProfileTableView : UITableViewController<UIDocumentInteractionControllerDelegate,MFMessageComposeViewControllerDelegate,MFMailComposeViewControllerDelegate>

@property (nonatomic, strong) BestMatchedRides *ride;
@property (nonatomic, strong) PFObject *passenger;
@property (assign, nonatomic) int riderNumber;
@property (nonatomic, strong) UserProfileInformation *userInfo;
@property (nonatomic, strong) BookingInformation *bookingInfo;

@property (nonatomic, assign) BOOL isArrival;
@property (nonatomic, assign) BOOL isDeparture;
@property (nonatomic, assign) BOOL arrivalBooked;

@property (nonatomic, assign) Ride *arrivalRide;
@property (nonatomic, assign) Ride *departureRide;

@end
