//
//  MuVDriveProfileTableView.h
//  MuV
//
//  Created by Hing Huynh on 1/28/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BestMatchedRides.h"
#import <MessageUI/MessageUI.h>
#import "Ride.h"
#import "UserProfileInformation.h"
#import "BookingInformation.h"

typedef enum{
    
    Phone = 0,
    Email = 1,
    Text = 2,
    PassengerOne = 3,
    PassengerTwo = 4,
    PassengerThree = 5,
    PassengerFour = 6,
    PassengerFive = 7,
    Book = 8
    
}driverProfileButtons;

@interface MuVDriveProfileTableView : UITableViewController<UIDocumentInteractionControllerDelegate,MFMessageComposeViewControllerDelegate,MFMailComposeViewControllerDelegate>

@property (nonatomic, strong) BestMatchedRides *ride;
@property (assign, nonatomic) int riderNumber;
@property (nonatomic, strong) UserProfileInformation *userInfo;
@property (nonatomic, strong) BookingInformation *bookingInfo;

@property (nonatomic, assign) BOOL isArrival;
@property (nonatomic, assign) BOOL isDeparture;
@property (nonatomic, assign) BOOL arrivalBooked;

@property (nonatomic, assign) Ride *arrivalRide;
@property (nonatomic, assign) Ride *departureRide;
@end
