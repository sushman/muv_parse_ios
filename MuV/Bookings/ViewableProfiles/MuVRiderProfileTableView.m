//
//  MuVRiderProfileTableView.m
//  MuV
//
//  Created by Hing Huynh on 1/28/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "MuVRiderProfileTableView.h"
#import "AboutMeCell.h"
#import "UserDetailsCell.h"
#import "BookMuVCell.h"
#import "UIImageView+WebCache.h"
#import "MuVBestMatchesViewController.h"
#import "UIViewController+JASidePanel.h"
#import "MuVSidePanelController.h"

@implementation MuVRiderProfileTableView- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _bookingInfo = [BookingInformation sharedMySingleton];
    _userInfo = [UserProfileInformation sharedMySingleton];
    self.tableView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"MuV_MainWhiteBG"]];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0 || indexPath.row == 1){
        return 170;
    } else {
        return 164;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0){
        UserDetailsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UserDetailsCell"];
        NSString *lastInitial = [[_passenger[@"lastName"] substringToIndex:1]capitalizedString];
        cell.nameLabel.text = [NSString stringWithFormat:@"%@ %@.",[_passenger[@"firstName"]capitalizedString],lastInitial];
        
        NSString *gender = @"     ";
        NSString *age = @"     ";
        NSString *relationship = @"     ";
        
        if (_passenger[@"age"]){
            age = _passenger[@"age"];
        }
        if (_passenger[@"gender"]){
            gender = [[_passenger[@"gender"]substringToIndex:1]capitalizedString];
            
        }
        if (_passenger[@"relationship"]){
            relationship = [_passenger[@"relationship"]capitalizedString];
        }
        
        cell.extraLabel.text = [NSString stringWithFormat:@"%@ / %@ / %@",age,gender,relationship];
        
        cell.occupationLabel.text = [_passenger[@"occupation"]capitalizedString];
        cell.companyLabel.text = [_passenger[@"companyName"]capitalizedString];
        PFFile *image = _passenger[@"profileImage"];
        [cell.profileImageView sd_setImageWithURL:[NSURL URLWithString:image.url] placeholderImage:[UIImage imageNamed:@"MuV_NOimageAvatar"]];
        
        if ([_passenger[@"isContactDetailPublic"]boolValue]) {
            cell.phoneButton.enabled = YES;
            cell.textButton.enabled = YES;
            [cell.phoneButton setImage:[UIImage imageNamed:@"MuV_Phone_Grn"] forState:UIControlStateNormal];
            [cell.textButton setImage:[UIImage imageNamed:@"MuV_ChatBbl_Grn"] forState:UIControlStateNormal];
        }
        
        return cell;
        
    } else if (indexPath.row == 1) {
        AboutMeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AboutMeCell"];
        cell.profileTextView.text = _passenger[@"profile"];
        return cell;
        
    } else {
        BookMuVCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BookMuVCell"];
        return cell;
    }
    
}

- (IBAction)ButtonPressed:(id)sender {
    switch ([sender tag]) {
        case PhonePassenger:
            [self callPassenger];
            break;
        case EmailPassenger:
            [self emailPassenger];
            break;
        case TextPassenger:
            [self textPassenger];
            break;
        case BookPassenger:
            if ((_arrivalBooked && _isArrival && _isDeparture)||(_arrivalRide && !_isDeparture)){
                [self bookMUV:(BestMatchedRides *) _ride];
                [self saveBookingToSingleton];
                [self loadBookingView];
            } else {
                [self bookMUV:(BestMatchedRides *) _ride];
                [self performSegueWithIdentifier:@"backToBestMatchesSegue" sender:sender];
            }
            break;
        default:
            break;
    }
}

- (void) callPassenger {
    NSString *phoneNumber = [[_passenger[@"phone"] componentsSeparatedByCharactersInSet:
                              [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                             componentsJoinedByString:@""];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[@"tel:" stringByAppendingString:phoneNumber]]];
}

- (void) emailPassenger {
    if([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailComposer = [[MFMailComposeViewController alloc]init];
        [mailComposer setToRecipients:@[_passenger[@"email"]]];
        mailComposer.mailComposeDelegate = self;
        [mailComposer setSubject:@"Message from MüV User"];
        [self presentViewController:mailComposer animated:YES completion:NULL];
    }else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Your device's email client is not setup." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
}

-(void) textPassenger {
    if(![MFMessageComposeViewController canSendText]) {
        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Your device doesn't support SMS." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [warningAlert show];
        return;
    }
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    NSString *phoneNumber = [[_passenger[@"phone"] componentsSeparatedByCharactersInSet:
                              [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                             componentsJoinedByString:@""];
    [messageController setRecipients:@[phoneNumber]];
    // Present message view controller on screen
    [self presentViewController:messageController animated:YES completion:nil];
}


- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed to send SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            break;
        }
        case MessageComposeResultSent:
            break;
        default:
            break;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)mailComposeController:(MFMailComposeViewController *)controller
         didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    if (result) {
        NSLog(@"Result : %d",result);
    }
    if (error) {
        NSLog(@"Error : %@",error);
    }
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString: @"backToBestMatchesSegue"]) {
        MuVBestMatchesViewController *bestMatchesViewController = (MuVBestMatchesViewController *) [segue destinationViewController];
        bestMatchesViewController.isArrival = YES;
        bestMatchesViewController.isDeparture = YES;
        bestMatchesViewController.arrivalBooked = YES;
        bestMatchesViewController.arrivalRide = _arrivalRide;
        bestMatchesViewController.departureRide = _departureRide;
    }
}

- (void)bookMUV:(BestMatchedRides *)rides {
    
    //record the data
    if (_isArrival && _isDeparture &&_arrivalBooked) {
        _departureRide.rideNo = rides.route;
        _departureRide.driverDetail = rides.driverDetails;
        _departureRide.userDetail = _userInfo.user;
        _departureRide.driverName = rides.driverName;
        _departureRide.passengers = [rides.otherPassengers mutableCopy];
    } else if ((_isArrival && _isDeparture)||(_isArrival && !_isDeparture)) {
        _arrivalRide.rideNo = rides.route;
        _arrivalRide.driverDetail = rides.driverDetails;
        _arrivalRide.userDetail = _userInfo.user;
        _arrivalRide.driverName = rides.driverName;
        _departureRide.passengers = [rides.otherPassengers mutableCopy];
        
    } else {
        _departureRide.rideNo = rides.route;
        _departureRide.driverDetail = rides.driverDetails;
        _departureRide.userDetail = _userInfo.user;
        _departureRide.driverName = [rides.driverName mutableCopy];
    }
    
}

- (void) loadBookingView {
    dispatch_async(dispatch_get_main_queue(),^{
        MuVSidePanelController *sidePanelViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MuVSidePanelViewController"];
        sidePanelViewController.centerPanel = [self.storyboard instantiateViewControllerWithIdentifier:@"BookingNav"];
        [self presentViewController:sidePanelViewController animated:YES completion:nil];
    });
}

- (void) saveBookingToSingleton {
    _bookingInfo.isArrival = _isArrival;
    _bookingInfo.isDeparture = _isDeparture;
    _bookingInfo.arrivalBooked = _arrivalBooked;
    _bookingInfo.arrivalRide = _arrivalRide;
    _bookingInfo.departureRide = _departureRide;
}

@end
