//
//  MuVBookingViewController.h
//  MuV
//
//  Created by Hing Huynh on 1/22/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JTCalendar.h"
#import "TableViewOfLocations.h"
#import "ControlVariables.h"
#import "Ride.h"
#import "MuVPaymentMethodsViewController.h"
#import "MuVPaymentsSetupViewController.h"

@interface MuVBookingsViewController : UIViewController <JTCalendarDataSource, UITextViewDelegate, TableViewOfLocationProtocol, UITextFieldDelegate, UpdateBookingsProtocol, PaymentMethodsProtocol>

@property (weak, nonatomic) IBOutlet TableViewOfLocations *tableViewOfLocations;
@property (nonatomic, assign) BOOL selectedRides;
@property (strong, nonatomic) Ride *arrivalRide;
@property (strong, nonatomic) Ride *departureRide;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIView *activityIndicatorView;

@property (weak, nonatomic) IBOutlet UIButton *loadPrevMonth;
@property (weak, nonatomic) IBOutlet UILabel *currentMonth;

@property (weak, nonatomic) IBOutlet UIButton *loadNextMonth;

@property (strong, nonatomic) NSDate *currentDate;

@end
