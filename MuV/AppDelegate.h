//
//  AppDelegate.h
//  MuV
//
//  Created by Hing Huynh on 1/7/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MuVTripViewController.h"
#import "MuVTripCalendarViewController.h"
#import "MuVSidePanelController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) MuVTripViewController *tripViewController;
@property (strong, nonatomic) MuVTripCalendarViewController *calendarViewController;
@property (strong, nonatomic) MuVSidePanelController *sidePanelViewController;

@end

