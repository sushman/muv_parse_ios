//
//  GoogleMapView.h
//  MuV
//
//  Created by SushmaN on 1/9/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GoogleMaps/GoogleMaps.h>

@interface GoogleMapView : GMSMapView <CLLocationManagerDelegate>

@property(nonatomic, retain) CLLocationManager *locationManager;
//- (NSString *) reverseGeocodeCoordinate:(CLLocationCoordinate2D)coordinate;
-(void)reverseGeocodeCoordinate:(CLLocationCoordinate2D)coordinate withCompletionBlock:
        (void (^)(BOOL completed, NSString *address1, NSString *city, NSString *state, NSString *zip))completionBlock;
- (void)fetchPlacesNearCoordinate:(CLLocationCoordinate2D )coordinate withSearchString:(NSString *)parkAndRide withCompletionBlock:(void (^)(NSArray *places, NSError *error))completionBlock;

- (void)getGoogleDirectionForQuery:(NSString *)queryString withCompletionBlock:(void (^)(NSArray *places, NSError *error))completionBlock;

@end
