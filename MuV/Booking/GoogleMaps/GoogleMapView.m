//
//  GoogleMapView.m
//  MuV
//
//  Created by SushmaN on 1/9/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "GoogleMapView.h"
#import <AFNetworking.h>
#import "ControlVariables.h"

#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)


@implementation GoogleMapView


-(void)reverseGeocodeCoordinate:(CLLocationCoordinate2D)coordinate withCompletionBlock:
       (void (^)(BOOL completed, NSString *address1, NSString *city, NSString *state, NSString *zip))completionBlock {
    
    [[GMSGeocoder geocoder] reverseGeocodeCoordinate:coordinate completionHandler:^(GMSReverseGeocodeResponse* response, NSError* error) {
        
        if (!error) {
            for(GMSAddress* addressObj in [response results]) {
                
                if (addressObj.thoroughfare.length <= 0 || addressObj.postalCode.length <=0) {
                }
                else {
                    completionBlock(YES, addressObj.thoroughfare, addressObj.locality, addressObj.administrativeArea,addressObj.postalCode);
                    break;
                }
            }
        }
        else {
            completionBlock(NO, NULL, NULL, NULL,NULL);
        }
        if (error) {
            completionBlock(NO, NULL, NULL, NULL, NULL);
        }
    }];
}



- (void)reverseGeocodeLocation:(CLLocation *)location {
    CLGeocoder* reverseGeocoder = [[CLGeocoder alloc] init];
    if (reverseGeocoder) {
        [reverseGeocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
            CLPlacemark* placemark = [placemarks firstObject];
            if (placemark) {
                //Using blocks, get zip code
              //  _addressLabel.text = [NSString stringWithFormat:@"%@ , %@ , %@, %@", [placemark thoroughfare], [placemark locality], [placemark administrativeArea], [placemark country]];
            }
        }];
    }else{
        // MKReverseGeocoder* rev = [[MKReverseGeocoder alloc] initWithCoordinate:location.coordinate];
        // rev.delegate = self;//using delegate
        //[rev start];
        //[rev release]; release when appropriate
    }
    //[reverseGeocoder release];release when appropriate
}

- (void)fetchPlacesNearCoordinate:(CLLocationCoordinate2D )coordinate withSearchString:(NSString *)places withCompletionBlock:(void (^)(NSArray *places, NSError *error))completionBlock  {
    NSString *apiKey = @"AIzaSyDPsmLnIRS5NxBuIEsOc1AppWwFssWCUEM";
    NSString *queryString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/nearbysearch/json?key=%@&location=%f,%f&radius=10000&sensor=true&rankby=prominence&types=%@",apiKey, coordinate.latitude, coordinate.longitude, places];
  //  NSLog(@"QueryString %@", queryString);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    AFHTTPResponseSerializer *serializer = [AFHTTPResponseSerializer serializer];
    
    manager.responseSerializer = serializer;
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    NSString *encoded = [queryString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [manager POST:encoded parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError* error = nil;
        NSArray* json = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
        NSDictionary *responseDictionary = (NSDictionary*)json;
        
        NSArray *placesArray = [responseDictionary valueForKey:@"results"];
        completionBlock(placesArray, nil);
    }
    failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        completionBlock(nil, error);
    }];
}

- (void)getGoogleDirectionForQuery:(NSString *)queryString withCompletionBlock:(void (^)(NSArray *places, NSError *error))completionBlock  {
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    AFHTTPResponseSerializer *serializer = [AFHTTPResponseSerializer serializer];
    
    manager.responseSerializer = serializer;
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager POST:queryString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError* error = nil;
        NSArray* json = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
        NSDictionary *responseDictionary = (NSDictionary*)json;
        
        NSArray *placesArray = [responseDictionary valueForKey:@"routes"];
        completionBlock(placesArray, nil);
    }
    failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              completionBlock(nil, error);
    }];
}


@end
