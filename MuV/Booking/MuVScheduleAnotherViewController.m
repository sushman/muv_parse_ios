//
//  ScheduleAnotherViewController.m
//  MuV
//
//  Created by SushmaN on 1/20/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "MuVScheduleAnotherViewController.h"
#import "UIButton+MuVUIButtonAdditions.h"
#import "JASidePanelController.h"
#import "UIViewController+JASidePanel.h"
#import "MuVSidePanelController.h"
#import <MessageUI/MessageUI.h>
#import "BookingInformation.h"
#import "CouponManager.h"
#import "ActivityProvider.h"

@interface MuVScheduleAnotherViewController () <MFMailComposeViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIButton *yesButton;
@property (weak, nonatomic) IBOutlet UIButton *noButton;
@property (weak, nonatomic) IBOutlet UIButton *inviteFriendsButton;
@property (weak, nonatomic) IBOutlet UIButton *shareButton;
@property (strong, nonatomic) BookingInformation *bookingInfo;

@end

@implementation MuVScheduleAnotherViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _yesButton = [UIButton muv_buttonUpdate:_yesButton];
    _noButton = [UIButton muv_buttonUpdate:_noButton];
    _inviteFriendsButton = [UIButton muv_buttonUpdate:_inviteFriendsButton];
    _shareButton = [UIButton muv_buttonUpdate:_shareButton];
}

- (IBAction)yesButtonTapped:(id)sender {
    MuVSidePanelController *sidePanelViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MuVSidePanelViewController"];
    self.sidePanelController.centerPanel = [self.storyboard instantiateViewControllerWithIdentifier:@"BookingNav"];
    [self presentViewController:sidePanelViewController animated:YES completion:nil];
    
}

- (IBAction)noButtonTapped:(id)sender {
    dispatch_async(dispatch_get_main_queue(),^{
        MuVSidePanelController *sidePanelViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MuVSidePanelViewController"];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main2" bundle:nil];
        self.sidePanelController.centerPanel = [storyboard instantiateViewControllerWithIdentifier:@"Tripsboard"];
        
        [self presentViewController:sidePanelViewController animated:YES completion:nil];
    });}

- (IBAction)inviteFriendsButtonTapped:(id)sender {
    if([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailComposer = [[MFMailComposeViewController alloc]init];
        mailComposer.mailComposeDelegate = self;
        [mailComposer setSubject:@"Invitation to MüV"];
        NSString *bodyString = [NSString stringWithFormat:@"Improve Your Commute with MüV\nI thought you’d like to know about MüV – a safe, reliable and affordable\nride share service that offers a great alternative for your daily commute.\nHere is a coupon code to use when you MüV:\n%@\nDownload the free app\n http://www.muvtogether.com",[CouponManager sharedInstance].couponName];
        [mailComposer setMessageBody:bodyString isHTML:NO];
        [self presentViewController:mailComposer animated:YES completion:NULL];
    }else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Your device's email client is not setup." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
}


-(void)mailComposeController:(MFMailComposeViewController *)controller
         didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    if (result) {
        NSLog(@"Result : %d",result);
    }
    if (error) {
        NSLog(@"Error : %@",error);
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)shareButtonTapped:(id)sender {
    ActivityProvider *activityProvider = [[ActivityProvider alloc] init];
    
    NSArray *items = @[activityProvider];
    
    UIActivityViewController *ActivityView = [[UIActivityViewController alloc]
                                              initWithActivityItems:items
                                              applicationActivities:nil];
    [ActivityView setValue:@"Get The MüV App" forKey:@"subject"];
    
    
    
    [ActivityView setExcludedActivityTypes:
     @[UIActivityTypeAssignToContact,
       UIActivityTypeCopyToPasteboard,
       UIActivityTypePrint,
       UIActivityTypeSaveToCameraRoll,
       UIActivityTypePostToWeibo]];
    
    [self presentViewController:ActivityView animated:YES completion:nil];
    [ActivityView setCompletionHandler:^(NSString *act, BOOL done)
     {
         NSString *ServiceMsg = nil;
         if ( [act isEqualToString:UIActivityTypeMail] )           ServiceMsg = @"Mail sent!";
         if ( [act isEqualToString:UIActivityTypePostToTwitter] )  ServiceMsg = @"Post on twitter, ok!";
         if ( [act isEqualToString:UIActivityTypePostToFacebook] ) ServiceMsg = @"Post on facebook, ok!";
         if ( [act isEqualToString:UIActivityTypeMessage] )        ServiceMsg = @"SMS sended!";
         if ( done )
         {
             UIAlertView *Alert = [[UIAlertView alloc] initWithTitle:ServiceMsg message:@"" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
             [Alert show];
         }
     }];

}


@end
