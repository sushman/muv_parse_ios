//
//  TableViewCellOfLocations.h
//  MuV
//
//  Created by SushmaN on 1/14/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewCellOfLocations : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *addressType;
@property (weak, nonatomic) IBOutlet UILabel *address;
@property (weak, nonatomic) IBOutlet UIImageView *addressTypeIcon;

@end
