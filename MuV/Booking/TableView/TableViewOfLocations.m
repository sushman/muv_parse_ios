 //
//  TableViewOfLocations.m
//  MuV
//
//  Created by SushmaN on 1/14/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "TableViewOfLocations.h"
#import "TableViewCellOfLocations.h"
#import "LocationDetail.h"

@implementation TableViewOfLocations


- (id)initWithFrame:(CGRect)frame
{
    if ((self = [super initWithFrame:frame]))
    {
    }
    
    return self;
}


- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.delegate = self;
        self.dataSource = self;
        self.inputArray = [[NSMutableArray alloc] init];
    }
    
    return self;
    
}




- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    TableViewCellOfLocations *newCell = (TableViewCellOfLocations *)[tableView dequeueReusableCellWithIdentifier: @"locationCell" forIndexPath:indexPath];
    if (indexPath.row >= _inputArray.count) {
        return newCell;
    }
    LocationDetail *locDet =[LocationDetail new];
    locDet = (LocationDetail *) _inputArray[indexPath.row];
    
    newCell.addressTypeIcon.image = [UIImage imageNamed:locDet.pictureName];
    newCell.addressType.text = locDet.addressType;
    newCell.address.text = locDet.address;
    return newCell;
}




- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_inputArray count];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_tableAlertDelegate tableView:self didSelectRowAtIndexPath:indexPath];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
    
}

- (NSString *)getDurationInMins: (NSNumber *)seconds {
    //check if number is less than 60 seconds
    if (seconds <  [NSNumber numberWithInt:60]) {
        return [seconds stringValue];;
    }
    else {
        //  sec = seconds % ([NSNumber numberWithInt:60]);
        //  seconds = seconds / [NSNumber numberWithInt:60];
    }
    
    return @"";
}

#pragma mark Content Filtering

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    // Update the filtered array based on the search text and scope.
    
    // Remove all objects from the filtered search array
    [_filteredAddressArray removeAllObjects];
    
    // Filter the array using NSPredicate
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.name contains[c] %@",searchText];
    NSArray *tempArray = [_inputArray filteredArrayUsingPredicate:predicate];
    
    if(![scope isEqualToString:@"All"]) {
        // Further filter the array with the scope
        NSPredicate *scopePredicate = [NSPredicate predicateWithFormat:@"SELF.category contains[c] %@",scope];
        tempArray = [tempArray filteredArrayUsingPredicate:scopePredicate];
    }
    
    _filteredAddressArray = [NSMutableArray arrayWithArray:tempArray];
}




@end



