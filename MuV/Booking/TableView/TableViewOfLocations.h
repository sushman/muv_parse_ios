//
//  TableViewOfLocations.h
//  MuV
//
//  Created by SushmaN on 1/14/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TableViewOfLocationProtocol;


@interface TableViewOfLocations : UITableView  <UITableViewDataSource, UITableViewDelegate>


@property(strong, nonatomic) NSArray *inputArray;
@property (weak, nonatomic) id <TableViewOfLocationProtocol> tableAlertDelegate;
@property (strong,nonatomic) NSMutableArray *filteredAddressArray;
//@property (assign, nonatomic) BOOL isFilteredArray;
@property IBOutlet UISearchBar *addressSearchBar;

@end

@protocol TableViewOfLocationProtocol

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;

@end
