//
//  LocationTracker.h
//  Location
//
//  Created by Rick
//  Copyright (c) 2014 Location. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "LocationShareModel.h"
#import "UserProfileInformation.h"

@protocol CurrentLocationProtocol;


@interface LocationTracker : NSObject <CLLocationManagerDelegate>

@property (nonatomic) CLLocationCoordinate2D myLastLocation;
@property (nonatomic) CLLocationAccuracy myLastLocationAccuracy;

@property (strong,nonatomic) LocationShareModel * shareModel;

@property (nonatomic) CLLocationCoordinate2D myLocation;
@property (nonatomic) CLLocationAccuracy myLocationAccuracy;
@property (strong,nonatomic) UserProfileInformation *userInfo;
@property (weak, nonatomic) id <CurrentLocationProtocol> locationAlertDelegate;

+ (LocationTracker *) shared;

+ (CLLocationManager *)sharedLocationManager;

- (void)startLocationTracking;
- (void)stopLocationTracking;
- (void)updateLocationToServer;



@end

@protocol CurrentLocationProtocol

- (void)getCurrentLocation:(CLLocationCoordinate2D) myLastLocation;

@end
