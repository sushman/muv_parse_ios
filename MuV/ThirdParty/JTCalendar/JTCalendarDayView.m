//
//  JTCalendarDayView.m
//  JTCalendar
//
//  Created by Jonathan Tribouharet
//




#import "JTCalendarDayView.h"

#import "JTCircleView.h"
#import "MuVRectView.h"
#import "MuVBridgeView.h"
#import "Helper.h"
#import "Ride.h"
#import "RideManager.h"
#import "TripManager.h"
#import "ControlVariables.h"


@interface JTCalendarDayView (){
    MuVRectView *circleView;
    MuVBridgeView *bridgeView;
    UIImageView *singleRight;
    UIImageView *singleLeft;
    UIImageView *imageview;
    UILabel *textLabel;
    JTCircleView *startTripDotView;
    JTCircleView *endTripDotView;
    BOOL isSelected;
    int cacheIsToday;
    BOOL isDriver;
    NSString *cacheCurrentDateText;
}
@end

static NSString *kJTCalendarDaySelected = @"kJTCalendarDaySelected";

@implementation JTCalendarDayView

TripManager *tripManager;

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(!self){
        return nil;
    }
    
    [self commonInit];
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(!self){
        return nil;
    }
    
    [self commonInit];
    
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter]  removeObserver:self];
}

- (void)commonInit
{
    isSelected = NO;
    self.isOtherMonth = NO;
    
    
    {
        circleView = [MuVRectView new];
        [self addSubview:circleView];
    }
    
    {
        textLabel = [UILabel new];
        [self addSubview:textLabel];
    }
    
   {
        startTripDotView = [JTCircleView new];
        [self addSubview:startTripDotView];
        startTripDotView.hidden = YES;
   }
    
    {
         endTripDotView = [JTCircleView new];
         [self addSubview:endTripDotView];
         endTripDotView.hidden = YES;
    }
    
    {
        bridgeView = [MuVBridgeView new];
        [self addSubview:bridgeView];
        bridgeView.hidden = YES;
    }
    
    
    {
        UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTouch)];
        self.userInteractionEnabled = YES;
        [self addGestureRecognizer:gesture];
    }
    
    {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didDaySelected:) name:kJTCalendarDaySelected object:nil];
       // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(modeChanged:) name:MODE_CHANGE object:nil];

    }
    
   // isDriver = ![[NSUserDefaults standardUserDefaults] boolForKey:@"riderMode"];
}

- (void)layoutSubviews
{
    [self configureConstraintsForSubviews];
    
    // No need to call [super layoutSubviews]
}

// Avoid to calcul constraints (very expensive)
- (void)configureConstraintsForSubviews
{
    textLabel.frame = CGRectMake(0, 0, self.frame.size.width/2, self.frame.size.height/2);
    
    CGFloat sizeCircle = MIN(self.frame.size.width, self.frame.size.height);
    CGFloat sizeDot = sizeCircle;
    
    sizeCircle = sizeCircle * self.calendarManager.calendarAppearance.dayCircleRatio;
    sizeDot = sizeDot * self.calendarManager.calendarAppearance.dayDotRatio;
    
    sizeCircle = roundf(sizeCircle);
    sizeDot = roundf(sizeDot);
    
    circleView.frame = CGRectMake(0.75, 0.75, self.frame.size.width - 1, self.frame.size.height - 1);
    
    if (self.calendarManager.calendarAppearance.showEvents) {
        
        startTripDotView.frame = CGRectMake(0, 0, sizeDot, sizeDot);
        startTripDotView.center = CGPointMake(self.frame.size.width/4.+2.5, (self.frame.size.height / 6. - 5) + sizeDot * 2.5);
        startTripDotView.layer.cornerRadius = sizeDot;
    }
    
    if (self.calendarManager.calendarAppearance.showEvents) {
        endTripDotView.frame = CGRectMake(0, 0, sizeDot, sizeDot);
        endTripDotView.center = CGPointMake(self.frame.size.width/8.+27.5, (self.frame.size.height / 6. - 5) + sizeDot * 2.5);
        endTripDotView.layer.cornerRadius = sizeDot;
    }
    
    bridgeView.frame = (CGRectMake(sizeDot*2-1, self.frame.size.height/2+7, self.frame.size.width/4-2, self.frame.size.height/12));
}


- (void)setDate:(NSDate *)date {
    isDriver = ![[NSUserDefaults standardUserDefaults] boolForKey:@"riderMode"];
    static NSDateFormatter *dateFormatter;
    if(!dateFormatter){
        dateFormatter = [NSDateFormatter new];
        dateFormatter.timeZone = self.calendarManager.calendarAppearance.calendar.timeZone;
        [dateFormatter setDateFormat:@"dd"];
    }
    
    
    if ([Helper isPastDate:date] && ![Helper isSameDay:date otherDay:[NSDate date]]) {
        _isPastDate=YES;
    }
    
    else {
         _isPastDate=NO;
        _isArrivalRequested = NO;
        _isArrivalScheduled = NO;
        _isDepartureRequested = NO;
        _isDepartureScheduled = NO;
        
        if ((isDriver && ![Helper isDate:date WithinRange:30]) || (!isDriver && ![Helper isDate:date WithinRange:15])) {
            _isTooMuchInFutureDate = YES;
        }
        else {
            _isTooMuchInFutureDate = NO;
            
            if (tripManager.rides.count > 0) {
                //  NSLog(@"String from date is %@",[Helper getStringFromDate:date] );
                NSArray *arrayOfTrips = (NSArray *)[tripManager.rides objectForKey:[Helper getStringFromDate:date]];
                if (arrayOfTrips.count > 0) {
                    for (Ride *ride in arrayOfTrips) {
                        if ([ride.routeType isEqualToString:ARRIVAL]) {
                            if (ride.isRequested) {
                                _isArrivalRequested = YES;
                            }
                            else {
                                _isArrivalScheduled=YES;
                            }
                        }
                        if ([ride.routeType isEqualToString:DEPARTURE]) {
                            if (ride.isRequested) {
                                _isDepartureRequested = YES;
                            }
                            else {
                                _isDepartureScheduled = YES;
                            }
                        }
                    }
                }
            }

        }
        
    }
    
    _date = date;
    
    textLabel.text = [dateFormatter stringFromDate:date];
    
    cacheIsToday = -1;
    cacheCurrentDateText = nil;
}

- (void)didTouch {
    
    if(self.isOtherMonth){
        return;
    }

    [self.calendarManager setCurrentDateSelected:self.date];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kJTCalendarDaySelected object:self.date];
    
    [self.calendarManager.dataSource calendarDidDateSelected:self.calendarManager date:self.date];
    
    NSInteger currentMonthIndex = [self monthIndexForDate:self.date];
    NSInteger calendarMonthIndex = [self monthIndexForDate:self.calendarManager.currentDate];
    
    currentMonthIndex = currentMonthIndex % 12;
    
    if(currentMonthIndex == (calendarMonthIndex + 1) % 12){
        // [self.calendarManager loadNextMonth];
    }
    else if(currentMonthIndex == (calendarMonthIndex + 12 - 1) % 12){
        // [self.calendarManager loadPreviousMonth];
    }
}

- (void)didDaySelected:(NSNotification *)notification
{
    NSDate *dateSelected = [notification object];
    
    if([self isSameDate:dateSelected]){
        if(!isSelected){
            [self setAsSelectedDate:YES animated:YES];
        }
    }
    else if(isSelected){
        [self setAsSelectedDate:NO animated:YES];
    }
}


- (void)setAsSelectedDate:(BOOL) selected animated:(BOOL)animated {
    if (isSelected == selected) {
        animated = NO;
    }
    isSelected = selected;
    circleView.transform = CGAffineTransformIdentity;
    CGAffineTransform tr = CGAffineTransformIdentity;
    CGFloat opacity = 1.;
    if(selected){
        if(!self.isOtherMonth){
            circleView.backgroundColor = [self.calendarManager.calendarAppearance dayCircleColorSelected];
            textLabel.textColor = [self.calendarManager.calendarAppearance dayTextColorSelected];
        }
        else{
            circleView.backgroundColor = [self.calendarManager.calendarAppearance dayCircleColorSelected];
            textLabel.textColor = [self.calendarManager.calendarAppearance dayTextColorSelectedOtherMonth];
        }
        
        circleView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.1, 0.1);
        tr = CGAffineTransformIdentity;
    }
    else {
        if(!self.isOtherMonth){
            textLabel.textColor = [self.calendarManager.calendarAppearance dayTextColor];
        }
        else {
            textLabel.textColor = [self.calendarManager.calendarAppearance dayCircleColorSelectedOtherMonth];
        }
        if (self.isPastDate) {
            circleView.backgroundColor = [UIColor colorWithRed:0.867f green:0.898f blue:0.949f alpha:1.00f];
        }
        else {
            
            if ([self isToday]){
                circleView.backgroundColor = [UIColor colorWithRed:0.867f green:0.898f blue:0.949f alpha:1.00f];
                return;
            }
            
            if (self.isTooMuchInFutureDate) {
                circleView.backgroundColor = [UIColor colorWithRed:0.867f green:0.898f blue:0.949f alpha:1.00f];
            }
            else {
                circleView.backgroundColor = [UIColor clearColor];
                if((self.isArrivalRequested && self.isDepartureScheduled) ||
                   (self.isArrivalScheduled && self.isDepartureScheduled) ||
                   (self.isArrivalRequested && self.isDepartureRequested) ||
                   (self.isArrivalScheduled && self.isDepartureRequested)) {
                    circleView.backgroundColor = [UIColor colorWithRed:0.867f green:0.898f blue:0.949f alpha:1.00f];
                }
            }
        }
    }
    
    
    
    if(animated){
        [UIView animateWithDuration:.3 animations:^{
            circleView.layer.opacity = opacity;
            circleView.transform = tr;
        }];
    }
    else{
        circleView.layer.opacity = opacity;
        circleView.transform = tr;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    if(isSelected == selected){
        animated = NO;
    }
    
    isSelected = selected;
    
    circleView.transform = CGAffineTransformIdentity;
    CGAffineTransform tr = CGAffineTransformIdentity;
    CGFloat opacity = 1.;
    
    if(selected){
        if(!self.isOtherMonth){
            circleView.color = [self.calendarManager.calendarAppearance dayCircleUnselectableDays];
            textLabel.textColor = [self.calendarManager.calendarAppearance dayTextColorSelected];
        }
        else{
            circleView.color = [self.calendarManager.calendarAppearance dayCircleUnselectableDays];
            textLabel.textColor = [self.calendarManager.calendarAppearance dayTextColorSelectedOtherMonth];
        }
        
        circleView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.1, 0.1);
        tr = CGAffineTransformIdentity;
    }
    else if([self isToday]){
        if(!self.isOtherMonth){
            circleView.color = [self.calendarManager.calendarAppearance dayCircleColorToday];
            textLabel.textColor = [self.calendarManager.calendarAppearance dayTextColorToday];
        }
        else{
            circleView.color = [self.calendarManager.calendarAppearance dayCircleColorTodayOtherMonth];
            textLabel.textColor = [self.calendarManager.calendarAppearance dayTextColorTodayOtherMonth];
        }
    }
    else{
        if(!self.isOtherMonth){
            textLabel.textColor = [self.calendarManager.calendarAppearance dayTextColor];
        }
        else{
            textLabel.textColor = [self.calendarManager.calendarAppearance dayTextColorOtherMonth];
        }
        
        opacity = 0.;
    }
    
    if (self.isPastDate) {
        startTripDotView.color = UICOLOR_PAST_TRIPS;
        endTripDotView.color = UICOLOR_PAST_TRIPS;
        circleView.backgroundColor = [UIColor colorWithRed:0.867f green:0.898f blue:0.949f alpha:1.00f];
    }
    else {
        
        if (self.isTooMuchInFutureDate) {
             circleView.backgroundColor = [UIColor colorWithRed:0.867f green:0.898f blue:0.949f alpha:1.00f];
        }
        else {
            circleView.backgroundColor = [UIColor clearColor];
            // No Events - Return 0
            // Start End Scheduled - Return 1
            // Start Scheduled End Requested - Return 2
            // Start Requested End Scheduled - Return 3
            // Start End Requested - Return 4
            
            if (self.isDepartureRequested) {
                endTripDotView.color = UICOLOR_PENDING_TRIP;
            }
            if (self.isDepartureScheduled) {
                endTripDotView.color = UICOLOR_SCHEDULED_TRIP;
            }
            
            if (self.isArrivalRequested) {
                startTripDotView.color = UICOLOR_PENDING_TRIP;
            }
            
            if (self.isArrivalScheduled) {
                startTripDotView.color = UICOLOR_SCHEDULED_TRIP;
            }
            
            if((self.isArrivalRequested && self.isDepartureScheduled) ||
               (self.isArrivalScheduled && self.isDepartureScheduled) ||
               (self.isArrivalRequested && self.isDepartureRequested) ||
               (self.isArrivalScheduled && self.isDepartureRequested)) {
                
                circleView.backgroundColor = [UIColor colorWithRed:0.867f green:0.898f blue:0.949f alpha:1.00f];
                bridgeView.color = UICOLOR_PAST_TRIPS;
                
            }
            else {
                circleView.backgroundColor = [UIColor clearColor];
            }

        }
    }
    
    if(animated){
        [UIView animateWithDuration:.3 animations:^{
            circleView.layer.opacity = opacity;
            circleView.transform = tr;
        }];
    }
//    else{
//        circleView.layer.opacity = opacity;
//        circleView.transform = tr;
//    }
}

- (void)setIsOtherMonth:(BOOL)isOtherMonth
{
    _isOtherMonth = isOtherMonth;
    [self setSelected:isSelected animated:NO];
}

- (void)setIsPastDate:(BOOL)isPastDate
{
    _isPastDate = isPastDate;
    // [self setSelected:isSelected rideType: 1 animated:NO];
}

- (void)setIsArrivalRequested:(BOOL)isArrivalRequested {
    _isArrivalRequested = isArrivalRequested;
    [self setSelected:isSelected animated:NO];
}

- (void)setIsDepartureRequested:(BOOL)isDepartureRequested {
    _isDepartureRequested = isDepartureRequested;
    [self setSelected:isSelected animated:NO];
}

- (void)setIsArrivalScheduled:(BOOL)isArrivalScheduled {
    _isArrivalScheduled = isArrivalScheduled;
    [self setSelected:isSelected animated:NO];
}

- (void)setIsDepartureScheduled:(BOOL)isDepartureScheduled {
    _isDepartureScheduled = isDepartureScheduled;
    [self setSelected:isSelected animated:NO];
}




- (void)reloadData
{
    NSInteger eventType = [self.calendarManager.dataSource calendarHaveEvent:self.calendarManager date:self.date];
    bridgeView.hidden = YES;
    startTripDotView.hidden = YES;
    endTripDotView.hidden = YES;
    //no events
    if (eventType == 0) {
      //  startTripDotView.hidden = YES;
      //  endTripDotView.hidden = YES;
    }
    //has one event
    else if (eventType == 5 || eventType == 7) {
        startTripDotView.hidden = NO;
      //  endTripDotView.hidden = YES;
    }
    //has one event
    else if (eventType == 6 || eventType == 8) {
      //  startTripDotView.hidden = YES;
        endTripDotView.hidden = NO;
    }
    //both has events
    else {
        startTripDotView.hidden = NO;
        endTripDotView.hidden = NO;
        bridgeView.hidden = NO;
    }
    
    BOOL selected = [self isSameDate:[self.calendarManager currentDateSelected]];
    [self setSelected:selected animated:NO];
}


- (BOOL)isToday
{
    if(cacheIsToday == 0){
        return NO;
    }
    else if(cacheIsToday == 1){
        return YES;
    }
    else{
        if([self isSameDate:[NSDate date]]){
            cacheIsToday = 1;
            return YES;
        }
        else{
            cacheIsToday = 0;
            return NO;
        }
    }
}

- (BOOL)isSameDate:(NSDate *)date
{
    static NSDateFormatter *dateFormatter;
    if(!dateFormatter){
        dateFormatter = [NSDateFormatter new];
        dateFormatter.timeZone = self.calendarManager.calendarAppearance.calendar.timeZone;
        [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    }
    
    if(!cacheCurrentDateText){
        cacheCurrentDateText = [dateFormatter stringFromDate:self.date];
    }
    
    NSString *dateText2 = [dateFormatter stringFromDate:date];
    
    if ([cacheCurrentDateText isEqualToString:dateText2]) {
        return YES;
    }
    
    return NO;
}

- (NSInteger)monthIndexForDate:(NSDate *)date
{
    NSCalendar *calendar = self.calendarManager.calendarAppearance.calendar;
    NSDateComponents *comps = [calendar components:NSCalendarUnitMonth fromDate:date];
    return comps.month;
}

- (void)reloadAppearance
{
    textLabel.textAlignment = NSTextAlignmentCenter;
    textLabel.font = self.calendarManager.calendarAppearance.dayTextFont;
    
    [self configureConstraintsForSubviews];
    //[self setSelected:isSelected animated:NO pastDate:NO];
}


@end


// Copyright belongs to original author
// http://code4app.net (en) http://code4app.com (cn)
// From the most professional code share website: Code4App.net
