//
//  JTRectView.m
//  MuV
//
//  Created by SushmaN on 1/29/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "MuVRectView.h"

@implementation MuVRectView

- (instancetype)init
{
    self = [super init];
    if(!self){
        return nil;
    }
    
   // self.layer.borderWidth = 0.5f;
  //  self.layer.borderColor = [UIColor orangeColor].CGColor;
    self.backgroundColor = [UIColor clearColor];
    self.color = [UIColor clearColor];
    self.opaque = NO;
    
    return self;
}


- (void)drawRect:(CGRect)rect
{
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(ctx, [self.backgroundColor CGColor]);
    CGContextFillRect(ctx, rect);
    
    rect = CGRectInset(rect, .5, .5);
    
    CGContextSetStrokeColorWithColor(ctx, [self.color CGColor]);
    CGContextSetFillColorWithColor(ctx, [self.color CGColor]);
    
    CGContextFillPath(ctx);
}

- (void)setColor:(UIColor *)color
{
    self->_color = color;
    
    [self setNeedsDisplay];
}

@end
