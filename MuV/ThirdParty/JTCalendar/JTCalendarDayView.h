//
//  JTCalendarDayView.h
//  JTCalendar
//
//  Created by Jonathan Tribouharet
//

#import <UIKit/UIKit.h>

#import "JTCalendar.h"

@interface JTCalendarDayView : UIView

@property (weak, nonatomic) JTCalendar *calendarManager;

@property (strong, nonatomic) NSDate *date;
@property (assign, nonatomic) BOOL isTooMuchInFutureDate;
@property (assign, nonatomic) BOOL isOtherMonth;
@property (assign, nonatomic) BOOL isUnselectableDate;
@property (assign, nonatomic) BOOL isPastDate;
@property (assign, nonatomic) BOOL isArrivalScheduled;
@property (assign, nonatomic) BOOL isArrivalRequested;
@property (assign, nonatomic) BOOL isDepartureScheduled;
@property (assign, nonatomic) BOOL isDepartureRequested;
@property (assign, nonatomic) BOOL isShowevent;

- (void)reloadData;
- (void)reloadAppearance;

@end

// Copyright belongs to original author
// http://code4app.net (en) http://code4app.com (cn)
// From the most professional code share website: Code4App.net
