//
//  JTRectView.h
//  MuV
//
//  Created by SushmaN on 1/29/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MuVRectView : UIView

@property (nonatomic, strong) UIColor *color;

@end
