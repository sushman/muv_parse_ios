//
//  JTRectView.m
//  MuV
//
//  Created by SushmaN on 1/29/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "MuVBridgeView.h"

@implementation MuVBridgeView

- (instancetype)init
{
    self = [super init];
    if(!self){
        return nil;
    }
    
    
    self.backgroundColor = [UIColor colorWithRed:0.767f green:0.798f blue:0.749f alpha:1.00f];
    self.color = [UIColor clearColor];
    
    return self;
}


- (void)drawRect:(CGRect)rect
{
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(ctx, [self.backgroundColor CGColor]);
    CGContextFillRect(ctx, rect);
    
    rect = CGRectInset(rect, .5, .5);
    
    CGContextSetStrokeColorWithColor(ctx, [self.color CGColor]);
    CGContextSetFillColorWithColor(ctx, [self.color CGColor]);
    
    CGContextFillPath(ctx);
}

- (void)setColor:(UIColor *)color
{
    self->_color = color;
    
    [self setNeedsDisplay];
}

@end
