//
//  JTCalendarAppearance.m
//  JTCalendar
//
//  Created by Jonathan Tribouharet
//

#import "JTCalendarAppearance.h"
#import "ControlVariables.h"

@implementation JTCalendarAppearance

- (instancetype)init
{
    self = [super init];
    if(!self){
        return nil;
    }
    
    [self setDefaultValues];
    
    return self;
}

- (void)setDefaultValues
{
    self.isWeekMode = NO;
    
    self.weekDayFormat = JTCalendarWeekDayFormatShort;
    
    self.ratioContentMenu = 2.;
    self.dayCircleRatio = 1.;
    self.dayDotRatio = 2.7 / 9.;
    
    self.menuMonthTextFont = MUV_BOLD_FONT(14);
    self.weekDayTextFont = [UIFont systemFontOfSize:11];
    self.dayTextFont = MUV_REGULAR_FONT(12);
    
    self.menuMonthTextColor = [UIColor whiteColor];
    self.weekDayTextColor = [UIColor colorWithRed:152./256. green:147./256. blue:157./256. alpha:1.];
    
    
    self.dayTextColorOtherMonth = [UIColor blackColor];
    self.dayDotColorPast = [UIColor orangeColor];
    self.dayCircleColorSelected = [UIColor colorWithRed:0.361f green:0.521f blue:0.899f alpha:1.00f];
    self.dayTextColorSelected = [UIColor blackColor];
    self.dayDotColorSelected = [UIColor whiteColor];
    self.dayTextColorOtherMonth = [UIColor clearColor];
    
    self.dayCircleColorSelectedOtherMonth = [UIColor clearColor];
    self.dayTextColorSelectedOtherMonth = self.dayTextColorOtherMonth;
    self.dayTextColorOtherMonth = self.dayTextColorOtherMonth;
    
    self.dayCircleColorToday = [UIColor colorWithRed:0x33/256. green:0xB3/256. blue:0xEC/256. alpha:.5];
    self.dayTextColorToday = self.weekDayTextColor;
    
    self.dayCircleColorTodayOtherMonth = self.dayCircleColorToday;
    self.dayTextColorTodayOtherMonth = self.dayTextColorToday;
    self.dayDotColorPast = self.dayTextColorToday;
    
    self.dayCircleUnselectableDays = [UIColor colorWithRed:0.867f green:0.898f blue:0.949f alpha:1.00f];
}

- (NSCalendar *)calendar
{
    static NSCalendar *calendar;
    static dispatch_once_t once;
    
    dispatch_once(&once, ^{
        calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        calendar.timeZone = [NSTimeZone localTimeZone];
    });
    
    return calendar;
}


- (void)setDayTextColorForAll:(UIColor *)textColor
{
    self.dayTextColor = textColor;
    self.dayTextColorSelected = textColor;
    self.dayTextColorOtherMonth = textColor;
    self.dayTextColorToday = textColor;
}

//- (void)setDayCircleUnselectableDay:(UIColor *)dotColor {
// self.dayDotColorPast = dotColor;
//}

@end

// Copyright belongs to original author
// http://code4app.net (en) http://code4app.com (cn)
// From the most professional code share website: Code4App.net
