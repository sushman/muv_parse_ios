//
//  MuVHomeSetupViewController.m
//  MuV
//
//  Created by Hing Huynh on 1/7/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "MuVHomeSetupViewController.h"
#import "TPKeyboardAvoidingScrollView.h"
#import <Parse/Parse.h>
#import "NSString+MuVStringAdditions.h"
#import "UIButton+MuVUIButtonAdditions.h"
#import "UITextField+MuVTextFieldAdditions.h"
#import "UserProfileInformation.h"


@interface MuVHomeSetupViewController ()

@property (weak, nonatomic) IBOutlet UIButton *submitChangesButton;

@property (weak, nonatomic) IBOutlet UITextField *addressTextField;
@property (weak, nonatomic) IBOutlet UITextField *cityTextField;
@property (weak, nonatomic) IBOutlet UITextField *stateTextField;
@property (weak, nonatomic) IBOutlet UITextField *zipCodeTextField;
@property (strong, nonatomic) UserProfileInformation *userInfo;
@property (strong, nonatomic) CLPlacemark *validAddress;

@end

@implementation MuVHomeSetupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.scrollView contentSizeToFit];
    
    _userInfo = [UserProfileInformation sharedMySingleton];
    
    _addressTextField.text = _userInfo.user[@"homeAddress"];
    _cityTextField.text = _userInfo.user[@"homeCity"];
    _stateTextField.text = _userInfo.user[@"homeState"];
    _zipCodeTextField.text = _userInfo.user[@"homeZip"];
    
    _submitChangesButton = [UIButton muv_buttonUpdate:_submitChangesButton];
    
    _addressTextField = [UITextField muv_textFieldUpdate:_addressTextField];
    _cityTextField = [UITextField muv_textFieldUpdate:_cityTextField];
    _stateTextField = [UITextField muv_textFieldUpdate:_stateTextField];
    _zipCodeTextField = [UITextField muv_textFieldUpdate:_zipCodeTextField];
}

- (void)viewDidUnload
{
    [self setScrollView:nil];
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (IBAction)gotoProfileView:(id)sender {
    [self compareAndSaveUserWithCompletionBlock:^(BOOL valid, NSError *error) {
        if (valid) {
            dispatch_async((dispatch_get_main_queue()), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
        }
    }];
}


- (BOOL)hasValuesChanged {
    BOOL hasChanged = YES;
    if ((!_addressTextField.text.muv_isEmpty) && ![_addressTextField.text isEqualToString:_userInfo.user[@"homeAddress"]]) {
        return hasChanged;
    }
    else if (!_cityTextField.text.muv_isEmpty && ![_cityTextField.text isEqualToString:_userInfo.user[@"homeCity"]]) {
        return hasChanged;
    }
    else if (!_stateTextField.text.muv_isEmpty && ![_stateTextField.text isEqualToString:_userInfo.user[@"homeState"]]) {
        return hasChanged;
    }
    else if (!_zipCodeTextField.text.muv_isEmpty && ![_zipCodeTextField.text isEqualToString:_userInfo.user[@"homeZip"]]) {
        return hasChanged;
    }
    else {
        hasChanged = NO;
    }
    return hasChanged;
}

- (void)compareAndSaveUserWithCompletionBlock:(void (^)(BOOL valid, NSError *error))completionBlock {
    
    
    if ((_addressTextField.text.length <= 0) && (_cityTextField.text.length <=0) && (_stateTextField.text.length <=0) && (_zipCodeTextField.text.length <= 0)) {
        completionBlock (YES, nil);
        return;
    }
    
    if (!self.hasValuesChanged) {
        completionBlock (YES, nil);
        return;
    }

    
    NSString *fullAddress = [NSString stringWithFormat:@"%@, %@, %@, %@", _addressTextField.text, _cityTextField.text, _stateTextField.text, _zipCodeTextField.text];
    [self isAddressValid:fullAddress withCompletionBlock:^(BOOL valid, NSError *error){
        if (valid){
            _userInfo.user[@"homeAddress"] = [NSString stringWithFormat:@"%@ %@",_validAddress.subThoroughfare, _validAddress.thoroughfare];
            _userInfo.user[@"homeCity"] = _validAddress.locality;
            _userInfo.user[@"homeState"] = _validAddress.administrativeArea;
            _userInfo.user[@"homeZip"] = _validAddress.postalCode;
            [_userInfo.user saveInBackground];
            
            _addressTextField.text = _userInfo.user[@"homeAddress"];
            _cityTextField.text = _userInfo.user[@"homeCity"];
            _stateTextField.text = _userInfo.user[@"homeState"];
            _zipCodeTextField.text = _userInfo.user[@"homeZip"];
            
             completionBlock (valid, nil);
        } else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message"
                                                            message:@"Could not find location, please try again."
                                                           delegate:nil
                                                  cancelButtonTitle:nil
                                                  otherButtonTitles:@"Dismiss", nil];
            [alert show];
            completionBlock (NO, nil);

        }
    }];
}


- (IBAction)forwardButtonTapped:(id)sender {
    
    [self compareAndSaveUserWithCompletionBlock:^(BOOL valid, NSError *error) {
        if (valid) {
            dispatch_async((dispatch_get_main_queue()), ^{
                [self performSegueWithIdentifier:@"workLocationSegue" sender:sender];
            });
        }
    }];
    
}

-(void)isAddressValid:(NSString *) address withCompletionBlock:(void (^)(BOOL valid, NSError *error))completionBlock{
    
    CLGeocoder* geoCoder = [[CLGeocoder alloc]init];
    [geoCoder geocodeAddressString:address completionHandler:^(NSArray *placemarks, NSError *error){
        if (!error){
            _validAddress = [placemarks firstObject];
            completionBlock(YES, nil);
        } else {
            completionBlock(NO, error);
        }
    }];
}

@end
