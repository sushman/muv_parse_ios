//
//  MuVUserLikesViewController.m
//  MuV
//
//  Created by Hing Huynh on 1/7/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "MuVUserLikesViewController.h"
#import "TPKeyboardAvoidingScrollView.h"
#import <Parse/Parse.h>
#import "MuVSidePanelController.h"
#import "UIButton+MuVUIButtonAdditions.h"
#import "UserProfileInformation.h"
#import "UITextView+PlaceHolder.h"

@interface MuVUserLikesViewController ()<UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *completeProfileButton;

@property (weak, nonatomic) IBOutlet UITextView *userLikesTextView;

@property (strong, nonatomic) UserProfileInformation *userInfo;

@property (assign, nonatomic) BOOL valueChanged;

@end

@implementation MuVUserLikesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.scrollView contentSizeToFit];
    
    _userInfo = [UserProfileInformation sharedMySingleton];
    
    if (_userInfo.user[@"profile"]){
        _userLikesTextView.text = _userInfo.user[@"profile"];
    }
    
    
    
    if (self.view.bounds.size.height == 480){
        NSLog(@"here");
        CGRect originalFrame = _userLikesTextView.frame;
        _userLikesTextView.frame = CGRectMake(CGRectGetMinX(originalFrame), CGRectGetMinY(originalFrame), originalFrame.size.width, originalFrame.size.height - 88.0f);
    }
    
    _userLikesTextView.layer.borderWidth = 2.0f;
    _userLikesTextView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
   
    _userLikesTextView.placeholder = @"This is a little bit about myself..";
    _userLikesTextView.placeholderColor = [UIColor lightGrayColor]; // optional
    
    _userLikesTextView.delegate = self;
    
    _completeProfileButton = [UIButton muv_buttonUpdate:_completeProfileButton];
    
    
}

- (IBAction)saveAndGoUserInfoView:(id)sender {
    
    [self compareAndSaveUser];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationController popViewControllerAnimated:YES];
    });
}


- (void)viewDidUnload {
    [self setScrollView:nil];
    [super viewDidUnload];
}


- (IBAction)completeProfileButtonTapped:(id)sender {
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self compareAndSaveUser];
        
        MuVSidePanelController *sidePanelViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MuVSidePanelViewController"];
        [self presentViewController:sidePanelViewController animated:YES completion:nil];
    });
}

- (void)compareAndSaveUser {
    if (![_userInfo.user[@"profile"] isEqualToString:_userLikesTextView.text]) {
        _userInfo.user[@"profile"] = _userLikesTextView.text;
        PFObject *object = _userInfo.user;
        [object saveInBackground];
    }
}

-(BOOL)textViewShouldEndEditing:(UITextView *)textView {
    
    [textView resignFirstResponder];
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    [textView resignFirstResponder];
}




@end
