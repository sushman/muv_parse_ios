//
//  MuVProfilePictureViewController.m
//  MuV
//
//  Created by Hing Huynh on 1/7/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "MuVProfilePictureViewController.h"
#import <Parse/Parse.h>
#import "UIButton+WebCache.h"
#import "UIImage+ResizeMagick.h"
#import "UIButton+MuVUIButtonAdditions.h"
#import "UserProfileInformation.h"


@interface MuVProfilePictureViewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>{
    UIImagePickerController *imagePicker;
    UIImage                 *selectedImage;
}

@property (weak, nonatomic) IBOutlet UIButton *submitChangesButton;
@property (weak, nonatomic) IBOutlet UIButton *imageButton;
@property (weak, nonatomic) IBOutlet UIButton *uploadButton;

@property (strong,nonatomic) UserProfileInformation *userInfo;

@end

@implementation MuVProfilePictureViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _userInfo = [UserProfileInformation sharedMySingleton];

    _imageButton.layer.masksToBounds = YES;
    _imageButton.layer.cornerRadius = _imageButton.frame.size.width/2;
    _imageButton.layer.borderColor = [UIColor blackColor].CGColor;
    _imageButton.layer.borderWidth = 2.0f;
    
    
    if (_userInfo.user[@"profileImage"]){
        PFFile *imageFile = _userInfo.user[@"profileImage"];
        NSString *stringURL = imageFile.url;
        [_imageButton sd_setBackgroundImageWithURL:[NSURL URLWithString:stringURL] forState:UIControlStateNormal];
        [_imageButton setTitle:@"" forState:UIControlStateNormal];
    }
    
    _submitChangesButton = [UIButton muv_buttonUpdate:_submitChangesButton];
    
}

- (IBAction)imageButtonTapped:(id)sender {
    [self takePicture];
}

- (IBAction)uploadButtonTapped:(id)sender {
    [self takePicture];
}

- (IBAction)forwardButtonTapped:(id)sender {
    
    [self compareAndSaveUser];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self performSegueWithIdentifier:@"userInfoSegue" sender:sender];
    });
;
}

- (IBAction)gotoSetupWorkLocation:(id)sender {
    
    [self compareAndSaveUser];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationController popViewControllerAnimated:YES];
    });
}


- (void)compareAndSaveUser {
    
    if (selectedImage){
        NSData *imageData = UIImageJPEGRepresentation(selectedImage, 0.7f);;
        PFFile *profileImage = [PFFile fileWithName:@"photo.jpg" data:imageData];
        
        _userInfo.user[@"profileImage"] = profileImage;
        [_userInfo.user saveInBackground];
    }
}



- (void)takePicture {
    if(!imagePicker){
        imagePicker = [[UIImagePickerController alloc]init];
        imagePicker.delegate = self;
        imagePicker.allowsEditing = YES;
    }
    
    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:imagePicker animated:YES completion:nil];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    selectedImage = [[info objectForKey:UIImagePickerControllerOriginalImage] resizedImageByMagick:@"200x200#"];
    [_imageButton setBackgroundImage:selectedImage forState:UIControlStateNormal];
    [_imageButton setTitle:@"" forState:UIControlStateNormal];
    [picker dismissViewControllerAnimated:NO completion:nil];
}

@end
