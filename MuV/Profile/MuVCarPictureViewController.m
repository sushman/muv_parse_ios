//
//  MuVCarPictureViewController.m
//  MuV
//
//  Created by Hing Huynh on 1/9/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "MuVCarPictureViewController.h"
#import "TPKeyboardAvoidingScrollView.h"
#import <Parse/Parse.h>
#import "UIButton+WebCache.h"
#import "UIImage+ResizeMagick.h"
#import "UIButton+MuVUIButtonAdditions.h"
#import "UITextField+MuVTextFieldAdditions.h"
#import "UserProfileInformation.h"
#import "MuVSidePanelController.h"
#import "RideManager.h"
#import "Helper.h"

@interface MuVCarPictureViewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>{
    UIImagePickerController *imagePicker;
    BOOL                    isPictureadded;
    UIImage                 *selectedImage;
}

@property (weak, nonatomic) IBOutlet UIButton *submitChangesButton;
@property (weak, nonatomic) IBOutlet UIButton *imageButton;

@property (weak, nonatomic) IBOutlet UITextField *yearTextField;
@property (weak, nonatomic) IBOutlet UITextField *modelTextField;
@property (weak, nonatomic) IBOutlet UITextField *seatTextField;
@property (weak, nonatomic) IBOutlet UITextField *makeTextField;
@property (weak, nonatomic) IBOutlet UITextField *mileageTextField;
@property (weak, nonatomic) IBOutlet UITextField *maxMilesTextField;
@property (weak, nonatomic) IBOutlet UITextField *licensePlateNumber;

@property (strong,nonatomic) UserProfileInformation *userInfo;

@end

@implementation MuVCarPictureViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.scrollView contentSizeToFit];
    
    _userInfo = [UserProfileInformation sharedMySingleton];
    
    if (!_userInfo.driverDetails) {
        return;
    }

    _imageButton.layer.masksToBounds = YES;
    _imageButton.layer.cornerRadius = _imageButton.frame.size.width/2;
    _imageButton.layer.borderColor = [UIColor blackColor].CGColor;
    _imageButton.layer.borderWidth = 2.0f;
    
    _yearTextField.text = [_userInfo.driverDetails[@"carYear"]stringValue];
    _modelTextField.text = _userInfo.driverDetails  [@"carModel"];
    _seatTextField.text = [_userInfo.driverDetails[@"carSeatCount"]stringValue];
    _makeTextField.text = _userInfo.driverDetails[@"carMake"];
    _mileageTextField.text = [_userInfo.driverDetails[@"carOdometer"]stringValue];
    _maxMilesTextField.text = [_userInfo.driverDetails[@"maxExtraDistance"]stringValue];
    _licensePlateNumber.text = _userInfo.driverDetails[@"licenseNo"];
    
    if (_userInfo.driverDetails[@"carImage"]){
        PFFile *imageFile = _userInfo.driverDetails[@"carImage"];
        NSString *stringURL = imageFile.url;
        [_imageButton sd_setBackgroundImageWithURL:[NSURL URLWithString:stringURL] forState:UIControlStateNormal];
        [_imageButton setTitle:@"" forState:UIControlStateNormal];
    }
    
    _submitChangesButton = [UIButton muv_buttonUpdate:_submitChangesButton];
    _yearTextField = [UITextField muv_textFieldUpdate:_yearTextField];
    _modelTextField = [UITextField muv_textFieldUpdate:_modelTextField];
    _seatTextField = [UITextField muv_textFieldUpdate:_seatTextField];
    _makeTextField = [UITextField muv_textFieldUpdate:_makeTextField];
    _mileageTextField = [UITextField muv_textFieldUpdate:_mileageTextField];
    _maxMilesTextField = [UITextField muv_textFieldUpdate:_maxMilesTextField];
    _licensePlateNumber = [UITextField muv_textFieldUpdate:_licensePlateNumber];
}

- (void)viewDidUnload
{
    [self setScrollView:nil];
    [super viewDidUnload];
}

- (IBAction)imageButtonTapped:(id)sender {
//    [self takePicture];
}

- (void)takePicture {
    if(!imagePicker){
        imagePicker = [[UIImagePickerController alloc]init];
        imagePicker.delegate = self;
        imagePicker.allowsEditing = YES;
    }
    
    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:imagePicker animated:YES completion:nil];
}

- (IBAction)submit:(id)sender {
    _submitChangesButton.enabled = NO;
    [self compareAndSaveUserWithCompletionBlock:^(BOOL valid, NSError *error) {
        if (valid) {
            dispatch_async((dispatch_get_main_queue()), ^{
                MuVSidePanelController *sidePanelViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MuVSidePanelViewController"];
                [self presentViewController:sidePanelViewController animated:YES completion:nil];
            });
        }
    }];

}

- (IBAction)forwardButtonTapped:(id)sender {
    
    [self compareAndSaveUserWithCompletionBlock:^(BOOL valid, NSError *error) {
        if (valid) {
            dispatch_async((dispatch_get_main_queue()), ^{
                [self performSegueWithIdentifier:@"homeLocationSegue" sender:sender];
            });
        }
    }];
    
   
}

- (IBAction)backButtonTapped:(id)sender {
    
    [self compareAndSaveUserWithCompletionBlock:^(BOOL valid, NSError *error) {
        if (valid) {
            dispatch_async((dispatch_get_main_queue()), ^{
                 [self.navigationController popViewControllerAnimated:YES];
            });
        }
    }];
}


-(void)compareAndSaveUserWithCompletionBlock:(void (^)(BOOL valid, NSError *error))completionBlock {
    
    if (!self.hasValuesChanged) {
        completionBlock (YES, nil);
        return;
    }
    
    if (![Helper isNumeric:_seatTextField.text] || ![Helper isNumeric:_maxMilesTextField.text]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning"
                                                        message:@"Please enter numeric values."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        return;
    }
    // need a filter on seatCount, year, and mileage
    if (_userInfo.driverDetails) {
      
        [self addCarAttributes];
        [_userInfo.driverDetails saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (succeeded) {
                RideManager *rideManager = [RideManager new];
                [rideManager updateTotalDriverSeatsAvailable:[NSNumber numberWithInteger:[_seatTextField.text integerValue]]];
                completionBlock (YES, nil);
            }
            else {
                completionBlock (NO, nil);
            }
        }];
        
    } else {
        _userInfo.driverDetails = [PFObject objectWithClassName:@"driverDetail"];
        
        [self addCarAttributes];
        
        [_userInfo.driverDetails saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
            if (succeeded){
                _userInfo.user[@"driverdetail"] = _userInfo.driverDetails;
                [_userInfo.user saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                    if (succeeded) {
                         completionBlock (YES, nil);
                    }
                    else {
                         completionBlock (NO, nil);
                    }
                }];
            }
            else {
                completionBlock (NO, nil);
            }
        }];
    }
}

- (BOOL)hasValuesChanged {
    BOOL hasChanged = YES;
    
    NSString *valueSeat = [_userInfo.driverDetails[@"carSeatCount"] stringValue];
    NSString *valueXtraDist = [_userInfo.driverDetails[@"maxExtraDistance"] stringValue];
    if (!_seatTextField.text.length <= 0 && ![_seatTextField.text isEqualToString:valueSeat]) {
        return hasChanged;
    }
    else if (!_maxMilesTextField.text.length <= 0 && ![_maxMilesTextField.text isEqualToString:valueXtraDist]) {
        return hasChanged;
    }
    else {
        hasChanged = NO;
    }
    return hasChanged;
}

-(void) addCarAttributes {
   // _userInfo.driverDetails[@"carYear"] = [NSNumber numberWithInteger:[_yearTextField.text integerValue]];
   // _userInfo.driverDetails[@"carModel"] =   _modelTextField.text;
  //  _userInfo.driverDetails[@"carMake"] = _makeTextField.text;
  //  _userInfo.driverDetails[@"carOdometer"] = [NSNumber numberWithInteger:[_mileageTextField.text integerValue]];
    
    _userInfo.driverDetails[@"carSeatCount"] = [NSNumber numberWithInteger:[_seatTextField.text integerValue]];
    _userInfo.driverDetails[@"maxExtraDistance"] = [NSNumber numberWithInteger:[_maxMilesTextField.text integerValue]];
    if (selectedImage){
        NSData *imageData = UIImageJPEGRepresentation(selectedImage, 0.7f);;
        PFFile *carImage =  [PFFile fileWithName:@"photo.jpg" data:imageData];

        _userInfo.driverDetails[@"carImage"] = carImage;
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{

    selectedImage = [[info objectForKey:UIImagePickerControllerOriginalImage] resizedImageByMagick:@"200x200#"];
    [_imageButton setBackgroundImage:selectedImage forState:UIControlStateNormal];
    [_imageButton setTitle:@"" forState:UIControlStateNormal];
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}

@end
