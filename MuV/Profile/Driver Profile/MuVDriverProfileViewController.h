//
//  MuVDriverProfileViewController.h
//  MuV
//
//  Created by Hing Huynh on 1/12/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TPKeyboardAvoidingScrollView;

@interface MuVDriverProfileViewController : UIViewController

@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@property (strong, nonatomic) NSString *segueIdentifier;
@end
