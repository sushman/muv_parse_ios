//
//  MuVDriverDetailsViewController.m
//  MuV
//
//  Created by Hing Huynh on 1/15/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "MuVDriverDetailsViewController.h"
#import "TPKeyboardAvoidingScrollView.h"
#import <Parse/Parse.h>
#import "UIButton+WebCache.h"
#import "UIImage+ResizeMagick.h"
#import "UIButton+MuVUIButtonAdditions.h"
#import "UITextField+MuVTextFieldAdditions.h"
#import "UserProfileInformation.h"
#import "MailManager.h"
#import "ControlVariables.h"

@interface MuVDriverDetailsViewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>{
    UIImagePickerController *imagePicker;
    BOOL                    isPictureadded;
    UIImage                 *selectedImage;
}

@property (weak, nonatomic) IBOutlet UIButton *submitChangesButton;
@property (weak, nonatomic) IBOutlet UIButton *imageButton;
@property (weak, nonatomic) IBOutlet UIButton *uploadButton;

@property (weak, nonatomic) IBOutlet UITextField *yearTextField;
@property (weak, nonatomic) IBOutlet UITextField *modelTextField;
@property (weak, nonatomic) IBOutlet UITextField *seatTextField;
@property (weak, nonatomic) IBOutlet UITextField *makeTextField;
@property (weak, nonatomic) IBOutlet UITextField *mileageTextField;
@property (weak, nonatomic) IBOutlet UITextField *maxMilesTextField;
@property (weak, nonatomic) IBOutlet UITextField *licencePlateNumber;

@property (strong,nonatomic) UserProfileInformation *userInfo;

@end

@implementation MuVDriverDetailsViewController

- (void)viewDidLoad {

    [super viewDidLoad];
    [_scrollView contentSizeToFit];
    
   
    
    _maxMilesTextField.hidden = YES;
    _userInfo = [UserProfileInformation sharedMySingleton];
    
    if (!_userInfo.driverDetails) {
        return;
    }
    
    _imageButton.layer.masksToBounds = YES;
    _imageButton.layer.cornerRadius = _imageButton.frame.size.width/2;
    _imageButton.layer.borderColor = [UIColor blackColor].CGColor;
    _imageButton.layer.borderWidth = 2.0f;
    
    
    _yearTextField.text = [_userInfo.driverDetails[@"carYear"]stringValue];
    _modelTextField.text = _userInfo.driverDetails[@"carModel"];
    _seatTextField.text = [_userInfo.driverDetails[@"carSeatCount"]stringValue];
    _makeTextField.text = _userInfo.driverDetails[@"carMake"];
    _mileageTextField.text = [_userInfo.driverDetails[@"carOdometer"]stringValue];
   // _maxMilesTextField.text = [_userInfo.driverDetails[@"maxExtraDistance"]stringValue];
    _licencePlateNumber.text = _userInfo.driverDetails[@"licenseNo"];
    
    if (_userInfo.driverDetails[@"carImage"]){
        PFFile *imageFile = _userInfo.driverDetails[@"carImage"];
        NSString *stringURL = imageFile.url;
        [_imageButton sd_setBackgroundImageWithURL:[NSURL URLWithString:stringURL] forState:UIControlStateNormal];
        [_imageButton setTitle:@"" forState:UIControlStateNormal];
    }
    
    _submitChangesButton = [UIButton muv_buttonUpdate:_submitChangesButton];
    _yearTextField = [UITextField muv_textFieldUpdate:_yearTextField];
    _modelTextField = [UITextField muv_textFieldUpdate:_modelTextField];
    _seatTextField = [UITextField muv_textFieldUpdate:_seatTextField];
    _makeTextField = [UITextField muv_textFieldUpdate:_makeTextField];
    _mileageTextField = [UITextField muv_textFieldUpdate:_mileageTextField];
    _licencePlateNumber = [UITextField muv_textFieldUpdate:_licencePlateNumber];
    
    [self disableFields];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [self disableFields];
}

- (void)disableFields {
    if (_userInfo.driverDetails) {
        _yearTextField.enabled = NO;
        _modelTextField.enabled = NO;
        _makeTextField.enabled = NO;
        _mileageTextField.enabled = NO;
        _licencePlateNumber.enabled = NO;
    }
}

- (void)viewDidUnload {
    [self setScrollView:nil];
    [super viewDidUnload];
}

- (IBAction)imageButtonTapped:(id)sender {
    [self takePicture];
}

- (IBAction)uploadButtonTapped:(id)sender {
    [self takePicture];
}

- (void)takePicture {
    if(!imagePicker){
        imagePicker = [[UIImagePickerController alloc]init];
        imagePicker.delegate = self;
        imagePicker.allowsEditing = YES;
    }
    
    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:imagePicker animated:YES completion:nil];
}

- (IBAction)submitChangesButtonTapped:(id)sender {
    
    if (_userInfo.user[@"driver"] == [NSNumber numberWithBool:YES]) {
        if (![self verifyEntry:_yearTextField] || ![self verifyEntry:_seatTextField] || ![self verifyEntry:_mileageTextField] || ![self verifyEntry:_maxMilesTextField]) {
            return;
        }
    }
    else {
        if (![self verifyEntry:_yearTextField] ||
            ![self verifyEntry:_modelTextField] ||
            ![self verifyEntry:_makeTextField] ||
            ![self verifyEntry:_licencePlateNumber] ||
            ![self verifyEntry:_mileageTextField] ||
            ![self verifyEntry:_seatTextField]
            ) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Could not submit. Please enter all the details." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            return;
        }
    }
    
    // need a filter on seatCount, year, and mileage
    if ([self checkNumeric:_yearTextField] && [self checkNumeric:_seatTextField] && [self checkNumeric:_mileageTextField] && [self checkNumeric:_maxMilesTextField]){
        
            if (_userInfo.user[@"pendingDriverApproval"] == [NSNumber numberWithBool:YES] || _userInfo.user[@"driver"] == [NSNumber numberWithBool:YES]) {
            [self addCarAttributes];
            [_userInfo.driverDetails saveInBackground];
            
        } else {
            
            _submitChangesButton.enabled = NO;
            _userInfo.driverDetails = [PFObject objectWithClassName:@"driverDetail"];
            [self addCarAttributes];
            _userInfo.driverDetails[@"userdetail"] = _userInfo.user;
            [_userInfo.driverDetails saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (!error){
    
                    _userInfo.user[@"driverdetail"] = _userInfo.driverDetails;
                    _userInfo.user[@"pendingDriverApproval"] = [NSNumber numberWithBool:YES];
                    [_userInfo.user saveInBackground];
                    
                    _submitChangesButton.enabled = YES;
                    
                    // SEND EMAIL TO ADMIN
                    MailManager *mailManager = [MailManager new];
                    [mailManager sendAdminWhenRiderWantsToBecomeADriverWithUser:_userInfo.user];
                    
                    dispatch_async((dispatch_get_main_queue()), ^{
                        [self performSegueWithIdentifier:@"finishApplyingSegue" sender:sender];
                    });
                }
                else {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Error in saving driver details. Please check network connection." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                    _submitChangesButton.enabled = YES;
                }
            }];
        }
        
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Could not submit. Please specific numeric values where ever necessary." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

-(void)addCarAttributes {
    
    _userInfo.driverDetails[@"carYear"] = [NSNumber numberWithInteger:[_yearTextField.text integerValue]];
    _userInfo.driverDetails[@"carModel"] =   _modelTextField.text;
    _userInfo.driverDetails[@"carSeatCount"] = [NSNumber numberWithInteger:[_seatTextField.text integerValue]];
    _userInfo.driverDetails[@"carMake"] = _makeTextField.text;
    _userInfo.driverDetails[@"carOdometer"] = [NSNumber numberWithInteger:[_mileageTextField.text integerValue]];
    _userInfo.driverDetails[@"licenseNo"] = _licencePlateNumber.text;
    
    if (selectedImage){
        NSData *imageData = UIImageJPEGRepresentation(selectedImage, 0.7f);;
        PFFile *carImage = [PFFile fileWithName:@"photo.jpg" data:imageData];
        
        _userInfo.driverDetails[@"carImage"] = carImage;
    }
    else {
        NSData *imageData = UIImageJPEGRepresentation([UIImage imageNamed:MUV_NO_CAR_IMAGE], 0.7f);
        PFFile *carImage = [PFFile fileWithName:@"noCarImage.jpg" data:imageData];
        _userInfo.driverDetails[@"carImage"] = carImage;
    }
}

- (BOOL)verifyEntry:(UITextField *)input {
    if (input.text.length <= 0) {
        return NO;
    }
    else return YES;
}

-(bool) checkNumeric: (UITextField *) input {
    
    BOOL valid;
    NSCharacterSet *alphaNums = [NSCharacterSet decimalDigitCharacterSet];
    NSCharacterSet *inStringSet = [NSCharacterSet characterSetWithCharactersInString:input.text];
    valid = [alphaNums isSupersetOfSet:inStringSet];
    
    return valid;
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    selectedImage = [[info objectForKey:UIImagePickerControllerOriginalImage] resizedImageByMagick:@"256x256#"];
    [_imageButton setBackgroundImage:selectedImage forState:UIControlStateNormal];
    [_imageButton setTitle:@"" forState:UIControlStateNormal];
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}


@end
