//
//  MuVDriverProfileViewController.m
//  MuV
//
//  Created by Hing Huynh on 1/12/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "MuVDriverProfileViewController.h"
#import "TPKeyboardAvoidingScrollView.h"
#import <Parse/Parse.h>
#import "NSString+MuVStringAdditions.h"
#import "UITextField+MuVTextFieldAdditions.h"
#import "UIButton+MuVUIButtonAdditions.h"
#import "UserProfileInformation.h"

@interface MuVDriverProfileViewController ()

@property (weak, nonatomic) IBOutlet UIButton *skipButton;
@property (weak, nonatomic) IBOutlet UIButton *submitChangesButton;
@property (weak, nonatomic) IBOutlet UIButton *riderModeButton;
@property (weak, nonatomic) IBOutlet UIButton *driverModeButton;

@property (weak, nonatomic) IBOutlet UITextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *companyNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *occupationTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordTextField;

@property (weak, nonatomic) IBOutlet UISegmentedControl *phoneSettingSegmentedControl;

@property (strong, nonatomic) UserProfileInformation *userInfo;

@end

@implementation MuVDriverProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [super viewDidLoad];
    [self.scrollView contentSizeToFit];
    
    _userInfo = [UserProfileInformation sharedMySingleton];
    
    _firstNameTextField.text = _userInfo.user[@"firstName"];
    _lastNameTextField.text = _userInfo.user[@"lastName"];
    _companyNameTextField.text = _userInfo.user[@"companyName"];
    _occupationTextField.text = _userInfo.user[@"occupation"];
    _phoneNumberTextField.text = _userInfo.user[@"phone"];
    _emailTextField.text = _userInfo.user[@"email"];
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"riderPreference"]){
        _riderModeButton.backgroundColor = [UIColor colorWithRed:0.557f green:0.694f blue:0.275f alpha:1.00f];
        _driverModeButton.backgroundColor = [UIColor colorWithRed:0.624f green:0.643f blue:0.733f alpha:1.00f];
        _segueIdentifier = @"homeLocationSegue";
    } else {
        _segueIdentifier = @"carPictureSegue";
    }
    
    _skipButton = [UIButton muv_buttonUpdate:_skipButton];
    _submitChangesButton = [UIButton muv_buttonUpdate:_submitChangesButton];
    _riderModeButton = [UIButton muv_buttonUpdate:_riderModeButton];
    _driverModeButton = [UIButton muv_buttonUpdate:_driverModeButton];

    _firstNameTextField = [UITextField muv_textFieldUpdate:_firstNameTextField];
    _lastNameTextField = [UITextField muv_textFieldUpdate:_lastNameTextField];
    _companyNameTextField = [UITextField muv_textFieldUpdate:_companyNameTextField];
    _occupationTextField = [UITextField muv_textFieldUpdate:_occupationTextField];
    _phoneNumberTextField = [UITextField muv_textFieldUpdate:_phoneNumberTextField];
    _emailTextField = [UITextField muv_textFieldUpdate:_emailTextField];
    _passwordTextField = [UITextField muv_textFieldUpdate:_passwordTextField];
    _confirmPasswordTextField = [UITextField muv_textFieldUpdate:_confirmPasswordTextField];
}

- (void)viewDidUnload {
    [self setScrollView:nil];
    [super viewDidUnload];
}

- (IBAction)forwardButtonTapped:(id)sender {
    
    if (self.hasValuesChanged) {
        if (self.validChanges){
            _userInfo.user[@"firstName"] = _firstNameTextField.text;
            _userInfo.user[@"lastName"] = _lastNameTextField.text;
            _userInfo.user[@"companyName"] = _companyNameTextField.text;
            _userInfo.user[@"occupation"] = _occupationTextField.text;
            _userInfo.user[@"phone"] = _phoneNumberTextField.text;
            _userInfo.user[@"email"] = _emailTextField.text;
            if (!_passwordTextField.text.muv_isEmpty){
                _userInfo.user[@"password"] = _passwordTextField.text;
            }
            [_userInfo.user saveInBackground];
            
           
            
        } else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message"
                                                            message:@"Missing fields or invalid input."
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            return;
        }
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self performSegueWithIdentifier:_segueIdentifier sender:sender];
    });
}

- (BOOL)hasValuesChanged {
    BOOL hasChanged = YES;
    if ((!_firstNameTextField.text.muv_isEmpty) && ![_firstNameTextField.text isEqualToString:_userInfo.user[@"firstName"]]) {
        return hasChanged;
    }
    else if (!_lastNameTextField.text.muv_isEmpty && ![_lastNameTextField.text isEqualToString:_userInfo.user[@"lastName"]]) {
        return hasChanged;
    }
    else if (!_companyNameTextField.text.muv_isEmpty && ![_companyNameTextField.text isEqualToString:_userInfo.user[@"companyName"]]) {
        return hasChanged;
    }
    else if (!_occupationTextField.text.muv_isEmpty && ![_occupationTextField.text isEqualToString:_userInfo.user[@"occupation"]]) {
        return hasChanged;
    }
    else if (!_phoneNumberTextField.text.muv_isEmpty && ![_phoneNumberTextField.text isEqualToString:_userInfo.user[@"phone"]]) {
        return hasChanged;
    }
    else if (!_emailTextField.text.muv_isEmpty && ![_emailTextField.text isEqualToString:_userInfo.user[@"email"]]) {
        return hasChanged;
    }
    else if (!_passwordTextField.text.muv_isEmpty && ![_passwordTextField.text isEqualToString:_userInfo.user[@"password"]]) {
        return hasChanged;
    }
    else {
        hasChanged = NO;
    }
    return hasChanged;
}

- (BOOL)validChanges {
    if (!_firstNameTextField.text.muv_isEmpty &&
        !_lastNameTextField.text.muv_isEmpty &&
        _emailTextField.text.muv_isValidEmailAddress &&
        _phoneNumberTextField.text.muv_isValidPhoneNumber &&
        self.validPasswords) {
        return true;
    }else {
        return false;
    }
}

- (BOOL)validPasswords {
    if (_passwordTextField.text.muv_isEmpty && _confirmPasswordTextField.text.muv_isEmpty){
        return true;
    }else if ([_passwordTextField.text isEqualToString:_confirmPasswordTextField.text]){
        return true;
    }else {
        return false;
    }
}


@end
