//
//  MuVDriverDetailsViewController.h
//  MuV
//
//  Created by Hing Huynh on 1/15/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TPKeyboardAvoidingScrollView;

@interface MuVDriverDetailsViewController : UIViewController

@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;

@end

