//
//  UserProfileInformation.m
//  MuV
//
//  Created by Hing Huynh on 1/21/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "UserProfileInformation.h"

@implementation UserProfileInformation

static UserProfileInformation* userInfo = nil;

+(UserProfileInformation*)sharedMySingleton
{
    @synchronized([UserProfileInformation class])
    {
        if (!userInfo)
            userInfo = [[self alloc] init];
        
        return userInfo;
    }
    
    return nil;
}

- (id)init {
    if (self = [super init]) {
    }
    return self;
}

@end
