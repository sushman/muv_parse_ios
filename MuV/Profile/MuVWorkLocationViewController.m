//
//  MuVWorkLocationViewController.m
//  MuV
//
//  Created by Hing Huynh on 1/7/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "MuVWorkLocationViewController.h"
#import "TPKeyboardAvoidingScrollView.h"
#import <Parse/Parse.h>
#import "NSString+MuVStringAdditions.h"
#import "UIButton+MuVUIButtonAdditions.h"
#import "UITextField+MuVTextFieldAdditions.h"
#import "UserProfileInformation.h"

@interface MuVWorkLocationViewController ()


@property (weak, nonatomic) IBOutlet UITextField *addressTextField;
@property (weak, nonatomic) IBOutlet UITextField *cityTextField;
@property (weak, nonatomic) IBOutlet UITextField *stateTextField;
@property (weak, nonatomic) IBOutlet UITextField *zipCodeTextField;

@property (weak, nonatomic) UserProfileInformation *userInfo;
@property (strong, nonatomic) CLPlacemark *validAddress;

@end

@implementation MuVWorkLocationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.scrollView contentSizeToFit];
    
    _userInfo = [UserProfileInformation sharedMySingleton];
    
    _addressTextField.text = _userInfo.user[@"workAddress"];
    _cityTextField.text = _userInfo.user[@"workCity"];
    _stateTextField.text = _userInfo.user[@"workState"];
    _zipCodeTextField.text = _userInfo.user[@"workZip"];
    _addressTextField = [UITextField muv_textFieldUpdate:_addressTextField];
    _cityTextField = [UITextField muv_textFieldUpdate:_cityTextField];
    _stateTextField = [UITextField muv_textFieldUpdate:_stateTextField];
    _zipCodeTextField = [UITextField muv_textFieldUpdate:_zipCodeTextField];
}

- (void)viewDidUnload {
    [self setScrollView:nil];
    [super viewDidUnload];
}

- (IBAction)skipButtonTapped:(id)sender {
    dispatch_async((dispatch_get_main_queue()), ^{
        [self performSegueWithIdentifier:@"profilePictureSegue" sender:sender];
    });
}


- (IBAction)gotoSetUpHomeLocation:(id)sender {
    
    [self compareAndSaveUserWithCompletionBlock:^(BOOL valid, NSError *error) {
        if (valid) {
            dispatch_async((dispatch_get_main_queue()), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
        }
    }];
}


- (IBAction)forwardButtonTapped:(id)sender {
    [self compareAndSaveUserWithCompletionBlock:^(BOOL valid, NSError *error) {
        if (valid) {
            dispatch_async((dispatch_get_main_queue()), ^{
                [self performSegueWithIdentifier:@"profilePictureSegue" sender:sender];
            });
        }
    }];
}

- (void)compareAndSaveUserWithCompletionBlock:(void (^)(BOOL valid, NSError *error))completionBlock {
    
    if ((_addressTextField.text.length <= 0) && (_cityTextField.text.length <=0) && (_stateTextField.text.length <=0) && (_zipCodeTextField.text.length <= 0)) {
        completionBlock (YES, nil);
        return;
    }
    
    if (!self.hasValuesChanged) {
        completionBlock (YES, nil);
        return;
    }

    
    NSString *fullAddress = [NSString stringWithFormat:@"%@, %@, %@, %@", _addressTextField.text, _cityTextField.text, _stateTextField.text, _zipCodeTextField.text];
    [self isAddressValid:fullAddress withCompletionBlock:^(BOOL valid, NSError *error){
        if (valid){
            _userInfo.user[@"workAddress"] = [NSString stringWithFormat:@"%@ %@",_validAddress.subThoroughfare, _validAddress.thoroughfare];
            _userInfo.user[@"workCity"] = _validAddress.locality;
            _userInfo.user[@"workState"] = _validAddress.administrativeArea;
            _userInfo.user[@"workZip"] = _validAddress.postalCode;
            [_userInfo.user saveInBackground];
            
            _addressTextField.text = _userInfo.user[@"workAddress"];
            _cityTextField.text = _userInfo.user[@"workCity"];
            _stateTextField.text = _userInfo.user[@"workState"];
            _zipCodeTextField.text = _userInfo.user[@"workZip"];
            
            completionBlock (valid, nil);
           
        } else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message"
                                                            message:@"Could not find location, please try again."
                                                           delegate:nil
                                                  cancelButtonTitle:nil
                                                  otherButtonTitles:@"Dismiss", nil];
            [alert show];
            completionBlock (NO, nil);
        }
    }];
}


- (BOOL)hasValuesChanged {
    BOOL hasChanged = YES;
    if ((!_addressTextField.text.muv_isEmpty) && ![_addressTextField.text isEqualToString:_userInfo.user[@"workAddress"]]) {
        return hasChanged;
    }
    else if (!_cityTextField.text.muv_isEmpty && ![_cityTextField.text isEqualToString:_userInfo.user[@"workCity"]]) {
        return hasChanged;
    }
    else if (!_stateTextField.text.muv_isEmpty && ![_stateTextField.text isEqualToString:_userInfo.user[@"workState"]]) {
        return hasChanged;
    }
    else if (!_zipCodeTextField.text.muv_isEmpty && ![_zipCodeTextField.text isEqualToString:_userInfo.user[@"workZip"]]) {
        return hasChanged;
    }
    else {
        hasChanged = NO;
    }
    return hasChanged;
}


- (BOOL)validChanges {
    if (!_addressTextField.text.muv_isEmpty &&
        !_cityTextField.text.muv_isEmpty &&
        !_stateTextField.text.muv_isEmpty &&
        !_zipCodeTextField.text.muv_isValidZip) {
        return true;
    }else {
        return false;
    }
}

-(void)isAddressValid:(NSString *) address withCompletionBlock:(void (^)(BOOL valid, NSError *error))completionBlock{
    
    CLGeocoder* geoCoder = [[CLGeocoder alloc]init];
    [geoCoder geocodeAddressString:address completionHandler:^(NSArray *placemarks, NSError *error){
        if (!error){
            _validAddress = [placemarks firstObject];
            completionBlock(YES, nil);
        } else {
            completionBlock(NO, error);
        }
    }];
}

@end
