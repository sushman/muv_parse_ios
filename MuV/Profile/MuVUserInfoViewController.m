//
//  MuVUserInfoViewController.m
//  MuV
//
//  Created by Hing Huynh on 1/7/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "MuVUserInfoViewController.h"
#import <Parse/Parse.h>
#import "UIButton+MuVUIButtonAdditions.h"
#import "UserProfileInformation.h"

@interface MuVUserInfoViewController ()<UIPickerViewDataSource, UIPickerViewDelegate>{
    NSArray *_genderData;
    NSMutableArray *_ageData;
    NSArray *_relationshipData;
    NSArray *_pickerData;
}

@property (weak, nonatomic) IBOutlet UIButton *genderSelectButton;
@property (weak, nonatomic) IBOutlet UIButton *ageSelectButton;
@property (weak, nonatomic) IBOutlet UIButton *relationshipSelectButton;
@property (weak, nonatomic) IBOutlet UIButton *submitChangesButton;
@property (weak, nonatomic) IBOutlet UIButton *selectButton;

@property (strong, nonatomic) IBOutlet UIView *transparentBackgroundView;
@property (strong, nonatomic) IBOutlet UIView *viewForPicker;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@property (strong, nonatomic) UserProfileInformation *userInfo;

@property (assign, nonatomic) BOOL valueChanged;


@end

@implementation MuVUserInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _userInfo = [UserProfileInformation sharedMySingleton];
    _genderData = @[@"male",@"female", @"N/A"];

    _ageData = [NSMutableArray new];
    for(int i = 18; i < 71; ++i) {
        [_ageData addObject:[NSString stringWithFormat:@"%d",i]];
    }
    
    _relationshipData = @[@"single",@"married",@"in a relationship", @"don't want to say"];

    self.pickerView.dataSource = self;
    self.pickerView.delegate = self;
    
    if (_userInfo.user[@"gender"]){
        [_genderSelectButton setTitle:_userInfo.user[@"gender"] forState:UIControlStateNormal];
    }
    if (_userInfo.user[@"age"]){
        [_ageSelectButton setTitle:_userInfo.user[@"age"] forState:UIControlStateNormal];
    }
    if (_userInfo.user[@"relationship"]){
        [_relationshipSelectButton setTitle:_userInfo.user[@"relationship"] forState:UIControlStateNormal];
    }
    
    _submitChangesButton = [UIButton muv_buttonUpdate:_submitChangesButton];
    _genderSelectButton = [UIButton muv_buttonUpdate:_genderSelectButton];
    _ageSelectButton = [UIButton muv_buttonUpdate:_ageSelectButton];
    _relationshipSelectButton = [UIButton muv_buttonUpdate:_relationshipSelectButton];
    _selectButton = [UIButton muv_buttonUpdate:_selectButton];

}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return _pickerData.count;
}

- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return _pickerData[row];
}

- (IBAction)genderSelectButtonTapped:(id)sender {
    _pickerData = _genderData;
    [self.pickerView reloadAllComponents];
    
    [self.view addSubview:_transparentBackgroundView];
    [self.view addSubview:_viewForPicker];
}

- (IBAction)ageSelectButtonTapped:(id)sender {
    _pickerData = _ageData;
    [self.pickerView reloadAllComponents];

    [self.view addSubview:_transparentBackgroundView];
    [self.view addSubview:_viewForPicker];
}

- (IBAction)relationshipSelectButtonTapped:(id)sender {
    _pickerData = _relationshipData;
    [self.pickerView reloadAllComponents];

    [self.view addSubview:_transparentBackgroundView];
    [self.view addSubview:_viewForPicker];
}

- (IBAction)forwardButtonTapped:(id)sender {
    
    [self compareAndSaveUser];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self performSegueWithIdentifier:@"userLikesSegue" sender:sender];
        _valueChanged = NO;
    });
}

- (IBAction)gotoProfilePic:(id)sender {
    
    [self compareAndSaveUser];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationController popViewControllerAnimated:YES];
        _valueChanged = NO;
    });
}

- (void)compareAndSaveUser {
    if (_valueChanged) {
        PFObject *object = _userInfo.user;
        [object saveInBackground];
    }
}



- (IBAction)selectButtonTapped:(id)sender {
    _valueChanged = YES;
    NSInteger row;
    row = [self.pickerView selectedRowInComponent:0];

    [_transparentBackgroundView removeFromSuperview];
    [_viewForPicker removeFromSuperview];
    
    if ([_pickerData isEqualToArray:_genderData]){
        _userInfo.user[@"gender"] = [_pickerData objectAtIndex:row];
        [_genderSelectButton setTitle:_userInfo.user[@"gender"] forState:UIControlStateNormal];

        
    } else if ([_pickerData isEqualToArray:_ageData]){
        _userInfo.user[@"age"] = [_pickerData objectAtIndex:row];
        [_ageSelectButton setTitle:_userInfo.user[@"age"] forState:UIControlStateNormal];
        
    } else {
        _userInfo.user[@"relationship"] = [_pickerData objectAtIndex:row];
        [_relationshipSelectButton setTitle:_userInfo.user[@"relationship"] forState:UIControlStateNormal];
    }
}
@end
