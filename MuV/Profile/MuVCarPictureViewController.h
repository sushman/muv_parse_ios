//
//  MuVCarPictureViewController.h
//  MuV
//
//  Created by Hing Huynh on 1/9/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TPKeyboardAvoidingScrollView;

@interface MuVCarPictureViewController : UIViewController

@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@end
