//
//  UserProfileInformation.h
//  MuV
//
//  Created by Hing Huynh on 1/21/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>

@interface UserProfileInformation : NSObject

@property(nonatomic,retain) PFUser *user;
@property(nonatomic,retain) PFObject  *driverDetails;

+ (UserProfileInformation *) sharedMySingleton;

@end
