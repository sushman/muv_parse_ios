//
//  MuVNotServicingViewController.m
//  MuV
//
//  Created by Hing Huynh on 1/15/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "MuVNotServicingViewController.h"
#import "UIButton+MuVUIButtonAdditions.h"
#import "MuVSidePanelController.h"
#import "CouponManager.h"
#import "UserProfileInformation.h"
#import "ActivityProvider.h"

@interface MuVNotServicingViewController ()

@property (weak, nonatomic) IBOutlet UIButton *inviteButton;
@property (weak, nonatomic) IBOutlet UIButton *driveButton;

@end

@implementation MuVNotServicingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _inviteButton = [UIButton muv_bigButton:_inviteButton];
    _driveButton = [UIButton muv_bigButton:_driveButton];
    
    if ([UserProfileInformation sharedMySingleton].driverDetails) {
        _driveButton.enabled = NO;
        [_driveButton setBackgroundColor:[UIColor grayColor]];
    }
}

- (IBAction)inviteButtonTapped:(id)sender {
    
    ActivityProvider *activityProvider = [[ActivityProvider alloc] init];
    
    NSArray *items = @[activityProvider];
    
    UIActivityViewController *ActivityView = [[UIActivityViewController alloc]
                                              initWithActivityItems:items
                                              applicationActivities:nil];
    [ActivityView setValue:@"Get The MüV App" forKey:@"subject"];
    
    
    
    [ActivityView setExcludedActivityTypes:
     @[UIActivityTypeAssignToContact,
       UIActivityTypeCopyToPasteboard,
       UIActivityTypePrint,
       UIActivityTypeSaveToCameraRoll,
       UIActivityTypePostToWeibo]];
    
    [self presentViewController:ActivityView animated:YES completion:nil];
    [ActivityView setCompletionHandler:^(NSString *act, BOOL done)
     {
         NSString *ServiceMsg = nil;
         if ( [act isEqualToString:UIActivityTypeMail] )           ServiceMsg = @"Mail sent!";
         if ( [act isEqualToString:UIActivityTypePostToTwitter] )  ServiceMsg = @"Post on twitter, ok!";
         if ( [act isEqualToString:UIActivityTypePostToFacebook] ) ServiceMsg = @"Post on facebook, ok!";
         if ( [act isEqualToString:UIActivityTypeMessage] )        ServiceMsg = @"SMS sended!";
         if ( done )
         {
             UIAlertView *Alert = [[UIAlertView alloc] initWithTitle:ServiceMsg message:@"" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
             [Alert show];
         }
     }];

    
}

- (IBAction)driveButtonTapped:(id)sender {
    MuVSidePanelController *sidePanelViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MuVSidePanelViewController"];
    sidePanelViewController.centerPanel = [self.storyboard instantiateViewControllerWithIdentifier:@"becomeADriverNav"];
    [self presentViewController:sidePanelViewController animated:YES completion:nil];
}

@end
