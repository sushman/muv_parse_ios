//
//  MuVArrivalViewController.h
//  MuV
//
//  Created by Hing Huynh on 1/15/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Ride.h"

@interface MuVArrivalViewController : UIViewController

@property (nonatomic, assign) Ride *requestedRide;

@end
