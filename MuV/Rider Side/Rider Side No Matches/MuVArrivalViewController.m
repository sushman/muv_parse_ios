//
//  MuVArrivalViewController.m
//  MuV
//
//  Created by Hing Huynh on 1/15/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "MuVArrivalViewController.h"
#import "UIButton+MuVUIButtonAdditions.h"
#import "MuVSidePanelController.h"
#import "MuVBestMatchesViewController.h"
#import "TripManager.h"


@interface MuVArrivalViewController ()

@property (weak, nonatomic) IBOutlet UIButton *continueButton;
@property (weak, nonatomic) IBOutlet UIButton *noThanksButton;

@end

@implementation MuVArrivalViewController

- (void)viewDidLoad {
    TripManager *tripManager = [TripManager sharedManager];
    [tripManager setParkAndRide:nil];
    [super viewDidLoad];
    _continueButton = [UIButton muv_buttonUpdate:_continueButton];
    _noThanksButton = [UIButton muv_buttonUpdate:_noThanksButton];
    
}

- (IBAction)continueButtonTapped:(id)sender {
    [self performSegueWithIdentifier:@"bestMatchesSegue" sender:sender];
}

- (IBAction)noThanksButtonTapped:(id)sender {
    MuVSidePanelController *sidePanelViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MuVSidePanelViewController"];
    sidePanelViewController.centerPanel = [self.storyboard instantiateViewControllerWithIdentifier:@"BookingNav"];
    [self presentViewController:sidePanelViewController animated:YES completion:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString: @"bestMatchesSegue"]) {
        MuVBestMatchesViewController *bestMatches = (MuVBestMatchesViewController *) [segue destinationViewController];
        bestMatches.arrivalRide = _requestedRide;
        bestMatches.isArrival = YES;
        bestMatches.isDeparture = NO;
    }
}
@end
