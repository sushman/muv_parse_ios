//
//  MuVBestMatchesViewController.h
//  MuV
//
//  Created by SushmaN on 1/20/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TableViewCellOfBestMatches.h"
#import "BestMatchedRides.h"
#import "Ride.h"

typedef enum{
    
    Driver = 0,
    RiderOne = 1,
    RiderTwo = 2,
    RiderThree = 3,
    RiderFour = 4,
    RiderFive = 5,
    DriverName = 6
    
}buttonType;

@interface MuVBestMatchesViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, assign) BOOL isArrival;
@property (nonatomic, assign) BOOL isDeparture;
@property (nonatomic, assign) BOOL arrivalBooked;

@property (nonatomic, assign) Ride *arrivalRide;
@property (nonatomic, assign) Ride *departureRide;

@end
