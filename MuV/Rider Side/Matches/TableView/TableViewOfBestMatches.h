//
//  TableViewOfBestMatches.h
//  MuV
//
//  Created by SushmaN on 1/20/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TVBestMatchedRidesProtocol;

@interface TableViewOfBestMatches : UITableView <UITableViewDelegate,UITableViewDataSource>

@property(strong, nonatomic) NSMutableArray *inputArray;
@property(assign, nonatomic) BOOL isArrival;
@property(assign, nonatomic) BOOL isDeparture;
@property (weak, nonatomic) id <TVBestMatchedRidesProtocol> tableAlertDelegate;

@end


@protocol TVBestMatchedRidesProtocol

- (void) tableView:(UITableView *)tableView setArrivalFlag:(BOOL)isArrival setDepartureFlag:(BOOL) isDeparture;

@end