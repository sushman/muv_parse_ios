//
//  TableViewOfBestMatches.m
//  MuV
//
//  Created by SushmaN on 1/20/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "TableViewOfBestMatches.h"
#import "TableViewCellOfBestMatches.h"
#import "BestMatchedRides.h"
#import "UIImageView+WebCache.h"
#import "MuVBestMatchesViewController.h"


@implementation TableViewOfBestMatches

NSInteger type;

- (id)initWithFrame:(CGRect)frame
{
    if ((self = [super initWithFrame:frame]))
    {
    }
    
    return self;
}


- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.delegate = self;
        self.dataSource = self;
        self.inputArray = [[NSMutableArray alloc] init];
    }
    
    return self;
    
}




- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"Best Matches >>>>>");
    TableViewCellOfBestMatches *newCell = (TableViewCellOfBestMatches *)[tableView dequeueReusableCellWithIdentifier: @"bestMatchesCell" forIndexPath:indexPath];
    BestMatchedRides *rides =[BestMatchedRides new];
    rides = (BestMatchedRides *) _inputArray[indexPath.row];
    [newCell.driverNameButton setTitle:rides.driverName forState:UIControlStateNormal];
    newCell.carName.text = rides.carMake;
    newCell.rideTime.text =  rides.rideTime;
    newCell.distBetweenRiderDriver.text = @"2.5";
    [newCell.bookaMuV setTag:indexPath.row];
    [newCell.bookaMuV addTarget:self action:@selector(bookMuV:) forControlEvents:UIControlEventTouchUpInside];
    return newCell;
}




- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSLog(@"Best Matches TableView %lu", (unsigned long)[_inputArray count]);
    return [_inputArray count];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
}

-(void)bookMuV:(UIButton *)sender {
    NSLog(@"Button Clicked %ld", (long)sender.tag);
    if (_isArrival && _isDeparture) {
        //record the data
        [_tableAlertDelegate tableView:self setArrivalFlag:NO setDepartureFlag:YES];
         NSLog(@"Arrival YES, Departure YES");
        type = 1;
        // show same screenpassin
    }
    if (_isArrival && !_isDeparture) {
        //show thank you screen
        NSLog(@"Arrival YES, Departure NO");
        type = 2;
    }
    if (!_isArrival && _isDeparture) {
         NSLog(@"Arrival NO, Departure NO");
        //record the date
        //show thank you screen
        type = 3;
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
    
}



@end
