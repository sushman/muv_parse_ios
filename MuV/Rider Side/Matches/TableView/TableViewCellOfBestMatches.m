//
//  TableViewCellOfBestMatches.m
//  MuV
//
//  Created by SushmaN on 1/20/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "TableViewCellOfBestMatches.h"
#import "UIButton+MuVUIButtonAdditions.h"

@implementation TableViewCellOfBestMatches

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)layoutSubviews
{
    _driverPhotoButton = [UIButton muv_buttonCircle:_driverPhotoButton];
    
    _riderOneButton = [UIButton muv_buttonCircle:_riderOneButton];
    _riderTwoButton = [UIButton muv_buttonCircle:_riderTwoButton];
    _riderThreeButton = [UIButton muv_buttonCircle:_riderThreeButton];
    _riderFourButton = [UIButton muv_buttonCircle:_riderFourButton];
    _riderFiveButton = [UIButton muv_buttonCircle:_riderFiveButton];
    
    _bookaMuV = [UIButton muv_buttonUpdate:_bookaMuV];
    
}

@end
