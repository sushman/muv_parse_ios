//
//  TableViewCellOfBestMatches.h
//  MuV
//
//  Created by SushmaN on 1/20/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewCellOfBestMatches : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *driverPhotoButton;
@property (weak, nonatomic) IBOutlet UIButton *driverNameButton;

@property (weak, nonatomic) IBOutlet UIButton *riderOneButton;
@property (weak, nonatomic) IBOutlet UIButton *riderTwoButton;
@property (weak, nonatomic) IBOutlet UIButton *riderThreeButton;
@property (weak, nonatomic) IBOutlet UIButton *riderFourButton;
@property (weak, nonatomic) IBOutlet UIButton *riderFiveButton;

@property (weak, nonatomic) IBOutlet UILabel *rideTime;
@property (weak, nonatomic) IBOutlet UILabel *carName;
@property (weak, nonatomic) IBOutlet UIButton *bookaMuV;
@property (weak, nonatomic) IBOutlet UILabel *distBetweenRiderDriver;
@property (weak, nonatomic) IBOutlet UILabel *farePrice;

//change later
@property (weak, nonatomic) IBOutlet UIImageView *driverProfilePic;
@property (weak, nonatomic) IBOutlet UILabel *driverName;

@property (weak, nonatomic) IBOutlet UIImageView *starOne;
@property (weak, nonatomic) IBOutlet UIImageView *starTwo;
@property (weak, nonatomic) IBOutlet UIImageView *starThree;
@property (weak, nonatomic) IBOutlet UIImageView *starFour;
@property (weak, nonatomic) IBOutlet UIImageView *starFive;


@end
