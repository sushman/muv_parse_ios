//
//  BestMatches.h
//  MuV
//
//  Created by SushmaN on 1/20/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>

@interface BestMatchedRides : NSObject

@property (nonatomic, strong) NSString *rideId;
@property (nonatomic, strong) NSString *rideTime;
@property (nonatomic, strong) NSString *rideType;
@property (nonatomic, strong) NSString *driverPicture;
@property (nonatomic, strong) NSString *driverName;
@property (nonatomic, strong) NSString *milesToPassengerLocation;
@property (nonatomic, strong) NSString *carMake;
@property (nonatomic, strong) NSString *fare;
@property (nonatomic, strong) NSNumber *totalSeats;
@property (nonatomic, strong) NSNumber *distance;
@property (nonatomic, strong) NSNumber *differenceInTimeFromIdeal;
@property (nonatomic, strong) NSNumber *driverRating;

@property (nonatomic, strong) NSArray  *otherPassengers;
@property (nonatomic, strong) PFObject *driverDetails;
@property (nonatomic, strong) PFObject *rideDetails;
@property (nonatomic, strong) PFObject *userDetails;
@property (nonatomic, strong) PFObject *route;

@end
