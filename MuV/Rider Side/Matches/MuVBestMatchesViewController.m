//
//  BestMatchesViewController.m
//  MuV
//
//  Created by SushmaN on 1/20/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "MuVBestMatchesViewController.h"
#import "RideManager.h"
#import "UIButton+WebCache.h"
#import "UIButton+MuVUIButtonAdditions.h"
#import "UIButton+WebCache.h"
#import "MuVBookingsViewController.h"
#import "MuVDriveProfileTableView.h"
#import "MuVRiderProfileTableView.h"
#import "UserProfileInformation.h"
#import "BookingInformation.h"
#import "UIViewController+JASidePanel.h"
#import "MuVSidePanelController.h"
#import "Helper.h"


@interface MuVBestMatchesViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tvOfBestMatches;
@property (strong, nonatomic) NSMutableArray *inputArray;
@property (nonatomic,assign) int riderNumber;
@property (strong, nonatomic) BestMatchedRides *selectedRide;
@property (strong, nonatomic) UserProfileInformation *userInfo;
@property (strong, nonatomic) BookingInformation *bookingInfo;

@end

@implementation MuVBestMatchesViewController
NSMutableArray *arrayRides;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _bookingInfo = [BookingInformation sharedMySingleton];
    _userInfo = [UserProfileInformation sharedMySingleton];
    
    NSLog(@"Booking Info %@", _bookingInfo.departureRide);
    
    NSLog(@"Booking Info %@", _bookingInfo.arrivalRide);
    
    _tvOfBestMatches.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"MuV_MainWhiteBG"]];
    RideManager *rideManager = [RideManager new];
    arrayRides = [NSMutableArray new];
    
    _tvOfBestMatches.delegate = self;
    _tvOfBestMatches.dataSource=self;
    
    if (_isArrival && _isDeparture && _arrivalBooked) {
        self.navigationItem.title = @"Best Matches Departing Work";
        [rideManager getDriversForRide:_departureRide withCompletionBlock:^(NSMutableArray *arrRides, NSError *error){
            if (!error){
                _inputArray  = arrRides;
                [_tvOfBestMatches reloadData];
            }
        }];
    } else if (_isArrival && _isDeparture){
        self.navigationItem.title = @"Best Matches to Work";
        [rideManager getDriversForRide:_arrivalRide withCompletionBlock:^(NSMutableArray *arrRides, NSError *error){
            if (!error){
                _inputArray  = arrRides;
                [_tvOfBestMatches reloadData];
            }
        }];
    } else if (_isArrival && !_isDeparture) {
        self.navigationItem.title = @"Best Matches to Work";
        [rideManager getDriversForRide:_arrivalRide withCompletionBlock:^(NSMutableArray *arrRides, NSError *error){
            if (!error){
                _inputArray  = arrRides;
                [_tvOfBestMatches reloadData];
            }
        }];
    } else {
        self.navigationItem.title = @"Best Matches Departing Work";
        [rideManager getDriversForRide:_departureRide withCompletionBlock:^(NSMutableArray *arrRides, NSError *error){
            if (!error){
                _inputArray  = arrRides;
                [_tvOfBestMatches reloadData];
            }
        }];
    }
}

- (IBAction)ButtonPressed:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:_tvOfBestMatches];
    NSIndexPath *indexPath = [_tvOfBestMatches indexPathForRowAtPoint:buttonPosition];
    _selectedRide =  _inputArray[indexPath.row];
    switch ([sender tag]) {
        case Driver:
            [self performSegueWithIdentifier:@"driverProfileSegue" sender:sender];
            break;
        case RiderOne:
            _riderNumber = 0;
            [self performSegueWithIdentifier:@"riderProfileSegue" sender:sender];
            break;
        case RiderTwo:
            _riderNumber = 1;
            [self performSegueWithIdentifier:@"riderProfileSegue" sender:sender];
            break;
        case RiderThree:
            _riderNumber = 2;
            [self performSegueWithIdentifier:@"riderProfileSegue" sender:sender];
            break;
        case RiderFour:
            _riderNumber = 3;
            [self performSegueWithIdentifier:@"riderProfileSegue" sender:sender];
            break;
        case RiderFive:
            _riderNumber = 4;
            [self performSegueWithIdentifier:@"riderProfileSegue" sender:sender];
            break;
        case DriverName:
            [self performSegueWithIdentifier:@"driverProfileSegue" sender:sender];
            break;
        default:
            break;
    }
}

- (IBAction)bookButtonTapped:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:_tvOfBestMatches];
    NSIndexPath *indexPath = [_tvOfBestMatches indexPathForRowAtPoint:buttonPosition];
    RideManager *rideManager = [RideManager new];
    
    if (_isArrival && _isDeparture &&_arrivalBooked ) {
        [self bookMUV:(BestMatchedRides *) _inputArray[indexPath.row]];
        [self saveBookingToSingleton];
        [self loadBookingView];
        
    } else if (_isArrival && _isDeparture){
        [self bookMUV:(BestMatchedRides *) _inputArray[indexPath.row]];
        [rideManager getDriversForRide:_departureRide withCompletionBlock:^(NSMutableArray *arrRides, NSError *error){
            if (!error){
                self.navigationItem.title = @"Best Matches Departing Work";
                _arrivalBooked = YES;
                _inputArray  = arrRides;
                [_tvOfBestMatches reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationLeft];
            }
        }];
    } else if (_isArrival && !_isDeparture) {
        [self bookMUV:(BestMatchedRides *) _inputArray[indexPath.row]];
        [self saveBookingToSingleton];
        [self loadBookingView];
        
    } else {
        [self bookMUV:(BestMatchedRides *) _inputArray[indexPath.row]];
        [self saveBookingToSingleton];
        [self loadBookingView];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TableViewCellOfBestMatches *newCell = (TableViewCellOfBestMatches *)[tableView dequeueReusableCellWithIdentifier: @"bestMatchesCell" forIndexPath:indexPath];
    
    BestMatchedRides *ride = _inputArray[indexPath.row];
    
    [newCell.driverNameButton setTitle:ride.driverName forState:UIControlStateNormal];
    newCell.carName.text = ride.carMake;
    [newCell.driverPhotoButton sd_setBackgroundImageWithURL:[NSURL URLWithString:ride.driverPicture] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"MuV_NOimageAvatar"]];
    
 //   newCell.distBetweenRiderDriver.text = [[ride.distance stringValue] stringByAppendingString:@" mi."];
    
    if (_isArrival && _isDeparture &&_arrivalBooked ) {
        newCell.farePrice.text = _departureRide.fare;
        newCell.distBetweenRiderDriver.text = [NSString stringWithFormat:@"%@ miles",_departureRide.mileage];
        newCell.rideTime.text = [NSString stringWithFormat:@"Depart: %@",ride.rideTime];
        
    } else if (_isArrival && _isDeparture){
        newCell.farePrice.text = _arrivalRide.fare;
        newCell.distBetweenRiderDriver.text = [NSString stringWithFormat:@"%@ miles",_arrivalRide.mileage];
        newCell.rideTime.text = [NSString stringWithFormat:@"Arrive: %@",ride.rideTime];
        
    } else if (_isArrival && !_isDeparture) {
        newCell.farePrice.text = _arrivalRide.fare;
        newCell.distBetweenRiderDriver.text = [NSString stringWithFormat:@"%@ miles",_arrivalRide.mileage];
        newCell.rideTime.text = [NSString stringWithFormat:@"Arrive: %@",ride.rideTime];
        
    } else {
        newCell.farePrice.text = _departureRide.fare;
        newCell.distBetweenRiderDriver.text = [NSString stringWithFormat:@"%@ miles",_departureRide.mileage];
        newCell.rideTime.text = [NSString stringWithFormat:@"Depart: %@",ride.rideTime];
        
    }
    
    if ([ride.totalSeats isEqual: @4]){
        newCell.riderFiveButton.hidden = YES;
    } else if ([ride.totalSeats isEqual: @3]){
        newCell.riderFiveButton.hidden = YES;
        newCell.riderFourButton.hidden = YES;
    } else if ([ride.totalSeats isEqual: @2]) {
        newCell.riderFiveButton.hidden = YES;
        newCell.riderFourButton.hidden = YES;
        newCell.riderThreeButton.hidden = YES;
    } else if ([ride.totalSeats isEqual: @1]) {
        newCell.riderFiveButton.hidden = YES;
        newCell.riderFourButton.hidden = YES;
        newCell.riderThreeButton.hidden = YES;
        newCell.riderTwoButton.hidden = YES;
    }
    
    // UPDATING UI TO REFLECT DRIVER RATING
    if ([ride.driverRating floatValue] > 0){
        [newCell.starOne setImage:[UIImage imageNamed:@"MuV_RateStar_HalfStar"]];
    }
    if ([ride.driverRating floatValue] > 0.5){
        [newCell.starOne setImage:[UIImage imageNamed:@"MuV_RateStar_Blu"]];
    }
    if ([ride.driverRating floatValue] > 1){
        [newCell.starTwo setImage:[UIImage imageNamed:@"MuV_RateStar_HalfStar"]];
    }
    if ([ride.driverRating floatValue] > 1.5){
        [newCell.starTwo setImage:[UIImage imageNamed:@"MuV_RateStar_Blu"]];
    }
    if ([ride.driverRating floatValue] > 2){
        [newCell.starThree setImage:[UIImage imageNamed:@"MuV_RateStar_HalfStar"]];
    }
    if ([ride.driverRating floatValue] > 2.5){
        [newCell.starThree setImage:[UIImage imageNamed:@"MuV_RateStar_Blu"]];
    }
    if ([ride.driverRating floatValue] > 3){
        [newCell.starFour setImage:[UIImage imageNamed:@"MuV_RateStar_HalfStar"]];
    }
    if ([ride.driverRating floatValue] > 3.5){
        [newCell.starFour setImage:[UIImage imageNamed:@"MuV_RateStar_Blu"]];
    }
    if ([ride.driverRating floatValue] > 4){
        [newCell.starFive setImage:[UIImage imageNamed:@"MuV_RateStar_HalfStar"]];
    }
    if ([ride.driverRating floatValue] > 4.5){
        [newCell.starFive setImage:[UIImage imageNamed:@"MuV_RateStar_Blu"]];
    }
    
    
    if (ride.otherPassengers.count > 0){
        int passengerCount = 1;
        for (PFObject *passengerRide in ride.otherPassengers) {
            PFObject *rider = passengerRide[@"rider"];
            PFFile *passengerImage =  rider[@"profileImage"];
            if (passengerCount == 1 ){
                [newCell.riderOneButton sd_setImageWithURL:[NSURL URLWithString:passengerImage.url] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"MuV_NOimageAvatar"]];
                newCell.riderOneButton.enabled = YES;
            }
            if (passengerCount == 2){
                [newCell.riderTwoButton sd_setImageWithURL:[NSURL URLWithString:passengerImage.url] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"MuV_NOimageAvatar"]];
                newCell.riderTwoButton.enabled = YES;
                
            }
            if (passengerCount == 3){
                [newCell.riderThreeButton sd_setImageWithURL:[NSURL URLWithString:passengerImage.url] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"MuV_NOimageAvatar"]];
                newCell.riderThreeButton.enabled = YES;
                
            }
            if (passengerCount == 4){
                [newCell.riderFourButton sd_setImageWithURL:[NSURL URLWithString:passengerImage.url] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"MuV_NOimageAvatar"]];
                newCell.riderFourButton.enabled = YES;
                
            }
            if (passengerCount == 5){
                [newCell.riderFiveButton sd_setImageWithURL:[NSURL URLWithString:passengerImage.url] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"MuV_NOimageAvatar"]];
                newCell.riderFiveButton.enabled = YES;
                
            }
            passengerCount++;
        }
    }
    
    return newCell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_inputArray count];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (void) loadBookingView {
    dispatch_async(dispatch_get_main_queue(),^{
        MuVSidePanelController *sidePanelViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MuVSidePanelViewController"];
        sidePanelViewController.centerPanel = [self.storyboard instantiateViewControllerWithIdentifier:@"BookingNav"];
        [self presentViewController:sidePanelViewController animated:YES completion:nil];
    });
}

- (void) saveBookingToSingleton {
    _bookingInfo.isArrival = _isArrival;
    _bookingInfo.isDeparture = _isDeparture;
    _bookingInfo.arrivalBooked = _arrivalBooked;
    _bookingInfo.arrivalRide = _arrivalRide;
    _bookingInfo.departureRide = _departureRide;
}


- (void)bookMUV:(BestMatchedRides *)rides {
    
    //record the data
    if (_isArrival && _isDeparture &&_arrivalBooked) {
        _departureRide.rideNo = rides.route;
        _departureRide.driverDetail = rides.driverDetails;
        _departureRide.userDetail = _userInfo.user;
        _departureRide.driverName = rides.driverName;
        _departureRide.passengers = [rides.otherPassengers mutableCopy];
        _departureRide.departTime = [Helper getTimeInStringFromDate:rides.route[@"rideDate"]];
    } else if ((_isArrival && _isDeparture)||(_isArrival && !_isDeparture)) {
        _arrivalRide.rideNo = rides.route;
        _arrivalRide.driverDetail = rides.driverDetails;
        _arrivalRide.userDetail = _userInfo.user;
        _arrivalRide.driverName = rides.driverName;
        _departureRide.passengers = [rides.otherPassengers mutableCopy];
        _arrivalRide.arriveTime = [Helper getTimeInStringFromDate:rides.route[@"rideDate"]];
        
    } else {
        _departureRide.rideNo = rides.route;
        _departureRide.driverDetail = rides.driverDetails;
        _departureRide.userDetail = _userInfo.user;
        _departureRide.driverName = [rides.driverName mutableCopy];
        _departureRide.departTime = [Helper getTimeInStringFromDate:rides.route[@"rideDate"]];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString: @"riderProfileSegue"]) {
        MuVRiderProfileTableView *profileViewController = (MuVRiderProfileTableView *) [segue destinationViewController];
        profileViewController.ride = _selectedRide;
        profileViewController.passenger = [_selectedRide.otherPassengers objectAtIndex:_riderNumber][@"rider"];
        profileViewController.isArrival = _isArrival;
        profileViewController.isDeparture = _isDeparture;
        profileViewController.arrivalBooked = _arrivalBooked;
        profileViewController.arrivalRide = _arrivalRide;
        profileViewController.departureRide = _departureRide;
    }
    if ([[segue identifier] isEqualToString: @"driverProfileSegue"]) {
        MuVDriveProfileTableView *profileViewController = (MuVDriveProfileTableView *) [segue destinationViewController];
        profileViewController.ride = _selectedRide;
        profileViewController.isArrival = _isArrival;
        profileViewController.isDeparture = _isDeparture;
        profileViewController.arrivalBooked = _arrivalBooked;
        profileViewController.arrivalRide = _arrivalRide;
        profileViewController.departureRide = _departureRide;
    }
}

@end
