//
//  Controlvariables.h
//  MuV
//
//  Created by SushmaN on 1/21/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#ifndef MuV_ControlVariables_h
#define MuV_ControlVariables_h

#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
#define ARRIVAL @"arrival"
#define DEPARTURE @"departure"
#define UICOLOR_PAST_TRIPS [UIColor colorWithRed:0.687 green:0.70 blue:0.78 alpha:1]
#define UICOLOR_REQUESTED_TRIPS [UIColor colorWithRed:0.73 green:0.43 blue:0.37 alpha:1]
#define UICOLOR_SCHEDULED_TRIPS [UIColor colorWithRed:0.56 green:0.69 blue:0.27 alpha:1]

#define MUV_GREEN_UICOLOR [UIColor colorWithRed:0.56 green:0.69 blue:0.27 alpha:1]
#define MUV_RED_UICOLOR [UIColor colorWithRed:0.73 green:0.43 blue:0.37 alpha:1]
#define MUV_GRAY_UICOLOR [UIColor colorWithRed:0.687 green:0.70 blue:0.78 alpha:1]
#define MUV_BLUE_UICOLOR [UIColor colorWithRed:0.25 green:0.31 blue:0.57 alpha:1]


#define UICOLOR_PENDING_TRIP [UIColor colorWithRed:0.87 green:0.68 blue:0.04 alpha:1]
#define UICOLOR_SCHEDULED_TRIP [UIColor colorWithRed:0.48 green:0.65 blue:0.18 alpha:1]

#define UICOLOR_RED_COLOR_USER [UIColor colorWithRed:0.67 green:0.15 blue:0.06 alpha:1]


#define NSUSERDEFAULT_FOR_RIDERMODE @"riderMode"

#define MUV_NO_CAR_IMAGE @"MuV_NoCarImage"
#define NOIMAGE_AVATAR @"MuV_NOimageAvatar"
#define MUV_ONE_WAY_IMAGE @"MuV_OneWayArrow_icon"
#define MUV_ROUND_TRIP_IMAGE @"MuV_RoundTripArrows_unSelected"
#define MUV_ROUND_TRIP_HIGHLIGHTED_IMAGE @"MuV_RoundTripArrow_HighlightedIcon"
#define MUV_ONE_WAY_HIGHLIGHTED_IMAGE @"MuV_OneWayArrow_HighlightedIcon"

#define MUV_DATE_TIME_FORMAT @"EEEE     MM / dd / yy hh:mm a"
#define MUV_DATE_FORMAT_TRIPS @"EEE,  MM / dd / yyyy"
#define MUV_TIME_FORMAT @"hh:mm a"

#define GOOGLE_APIKEY_SERVICES @"AIzaSyDPsmLnIRS5NxBuIEsOc1AppWwFssWCUEM"


#define MUV_BOLD_FONT(fontSize) [UIFont fontWithName:@"Quicksand-Bold" size:fontSize]

#define MUV_REGULAR_FONT(fontSize) [UIFont fontWithName:@"Quicksand-Regular" size:fontSize]


enum {
    Agenda = 1,
    Calendar = 2,
    Past = 3,
    PendingForDriver = 4,
    ScheduledForDriver = 5,
    PendingForRider = 6,
    ScheduledForRider = 7,
    Current = 8
};
typedef NSInteger TripsViewType;

enum EventType {
    NoEvent,
    Arrival_Departure_Scheduled,
    ArrivalScheduled_DepartureRequested,
    ArrivalRequested_DepartureScheduled,
    Arrival_Departure_Requested,
    ArrivalScheduled,
    DepartureScheduled,
    ArrivalRequested,
    DepartureRequested
};

enum {
    MoreThan24Hours = 1,
    Between4And24Hours = 2,
    LessThan4Hours = 3,
};


#endif
