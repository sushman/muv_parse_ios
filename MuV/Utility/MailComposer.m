//
//  MailComposer.m
//  MuV
//
//  Created by SushmaN on 4/13/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "MailComposer.h"
#import <MessageUI/MessageUI.h>

@implementation MailComposer


- (MFMailComposeViewController *)setUpEmailViewForRecipient:(NSString *)toEmail withSubject:(NSString *)subject {
    
    MFMailComposeViewController *mailComposer;
    if([MFMailComposeViewController canSendMail]) {
        mailComposer = [[MFMailComposeViewController alloc]init];
        [mailComposer setToRecipients:@[toEmail]];
        [mailComposer setSubject:subject];
        
    } else {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Your device's email client is not setup." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
    return mailComposer;
    
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result {
    switch (result) {
        case MessageComposeResultCancelled:
            break;
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed to send SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            break;
        }
        case MessageComposeResultSent:
            break;
        default:
            break;
    }
  //  [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)mailComposeController:(MFMailComposeViewController *)controller
         didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    if (result) {
        NSLog(@"Result : %d",result);
    }
    if (error) {
        NSLog(@"Error : %@",error);
    }
  //  [self dismissViewControllerAnimated:YES completion:nil];
    
}

@end
