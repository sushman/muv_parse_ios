//
//  Helper.m
//  MuV
//
//  Created by SushmaN on 1/7/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "Helper.h"
#import <MapKit/MapKit.h>
#import "ControlVariables.h"

@implementation Helper

+ (BOOL)isSameDay:(NSDate*)date1 otherDay:(NSDate*)date2 {
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay;
    NSDateComponents* comp1 = [calendar components:unitFlags fromDate:date1];
    NSDateComponents* comp2 = [calendar components:unitFlags fromDate:date2];
    
    return [comp1 day]   == [comp2 day] &&
    [comp1 month] == [comp2 month] &&
    [comp1 year]  == [comp2 year];
}


+ (BOOL)isPastDate:(NSDate*)dateA {
    NSComparisonResult result = [dateA compare:[NSDate date]];
    
    if(result == NSOrderedAscending) {
        // NSLog(@"%@ is greater than %@", dateA,[NSDate date]);
        return YES;
    }
    else if(result == NSOrderedDescending) {
        //NSLog(@"%@ is greater than %@", dateA,[NSDate date]);
        return NO; }
    else
        return YES;
}

+ (BOOL)compareDates:(NSDate*)dateA andDateB:(NSDate *)dateB {
    NSComparisonResult result = [dateA compare:dateB];
    
    if(result == NSOrderedAscending) {
        //Do your logic when date1 > date2
        return NO;
    }
    else if(result == NSOrderedDescending) {
        //NSLog(@"%@ is greater than %@", dateA,[NSDate date]);
        return YES;
    }
    else
        return NO;
}

+ (BOOL)isCurrentMonth:(NSDate*)date {
    
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    NSDateComponents* currentDate = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:[NSDate date]];
    
    NSDateComponents* reqDate = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:date];
    
    if ([currentDate month] == [reqDate month] && [currentDate year] == [reqDate year]) {
        return  YES;
    }
    else {
        return  NO;
    }
}

+ (BOOL)isPastDay: (NSDate*)date {
    
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    NSDateComponents* currentDate = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:[NSDate date]];
    
    NSDateComponents* reqDate = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:date];
    
    if ([reqDate year] < [currentDate year]) {
        return YES;
    }
    if ([reqDate year] <= [currentDate year] && [reqDate month] < [currentDate month]) {
        return YES;
    }
    if ([reqDate year] <= [currentDate year] && [reqDate month] <= [currentDate month] && [reqDate day] < [currentDate day]) {
        return YES;
    }
    
    return NO;
}




+ (NSDate *)getDateFromString:(NSString *)dateInString {
       // Convert string to date object
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:MUV_DATE_TIME_FORMAT];
    NSDate *date = [dateFormat dateFromString:dateInString];

    return date;
}

+ (NSString *)getFormattedDateInString:(NSDate *)date andFormat:(NSString *)format {
    // Convert string to date object
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:format];
    return [dateFormat stringFromDate:date];

}

+ (NSString *)getTimeInStringFromDate:(NSDate *)date {
    
    // Convert string to date object
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:MUV_TIME_FORMAT];
    return [dateFormat stringFromDate:date];
}


+ (NSString *)getStringFromDate:(NSDate *)date {
    // Convert string to date object
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:MUV_DATE_FORMAT_TRIPS];
    return [dateFormat stringFromDate:date];
}

//+ (double)getFormattedNumber:(NSString *) miles {
//    return [miles doubleValue];
//}


+ (CLLocationDistance) getDistanceBetweenLocationA:(CLLocation *)locA andLocationB: (CLLocation *)locB {
    
    CLLocationDistance dist = [locA distanceFromLocation:locB];
    //distance in miles
    dist = dist * 0.000621371192;
    return dist;
}

+ (CGFloat) getTotalFareOneWay:(CLLocationDistance) distance {
    return distance * 1.5;
}

+(NSMutableArray *)getAddressArray:(NSString *)addressinString {
    NSMutableArray *address;
    NSArray *components = [addressinString componentsSeparatedByString:@","];
    if (components.count > 1) {
        address = (NSMutableArray *)[[components objectAtIndex:0] componentsSeparatedByString:@" "];
        [address addObjectsFromArray:[[components objectAtIndex:1] componentsSeparatedByString:@" "]];
    }
    return address;
}

+ (NSDate *)firstSecondOfTheMonth :(NSDate *)date {
    
    
    NSString *string = @"23:59";
    
    NSDateFormatter *timeOnlyFormatter = [[NSDateFormatter alloc] init];
    [timeOnlyFormatter setDateFormat:@"HH:mm"];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDate *today = [NSDate date];
    NSDateComponents *todayComps = [calendar components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit) fromDate:today];
    
    [todayComps setDay:todayComps.day - 1];
    //  [todayComps setd]
    
    NSDateComponents *compsTime = [calendar components:(NSHourCalendarUnit | NSMinuteCalendarUnit) fromDate:[timeOnlyFormatter dateFromString:string]];
    
  
    
    
    NSDateComponents* comps = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSWeekCalendarUnit|NSWeekdayCalendarUnit fromDate:date]; // Get necessary date components

    // set last of month
    [comps setMonth:[comps month]];
    [comps setDay:0];
    [comps setTimeZone:SYSTEM_CLOCK];
    comps.hour = compsTime.hour;
    comps.minute = compsTime.minute;
    return [calendar dateFromComponents:comps];
}



+ (NSDate *)getFirstSecondOfTheMonth :(NSDate *)date {
    
    
    NSString *string = @"00:00";
    
    NSDateFormatter *timeOnlyFormatter = [[NSDateFormatter alloc] init];
    [timeOnlyFormatter setDateFormat:@"HH:mm"];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *compsTime = [calendar components:(NSHourCalendarUnit | NSMinuteCalendarUnit) fromDate:[timeOnlyFormatter dateFromString:string]];
    
    
    
    
    NSDateComponents* comps = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSWeekCalendarUnit|NSWeekdayCalendarUnit fromDate:date]; // Get necessary date components
    
    // set last of month
    [comps setMonth:[comps month]];
    [comps setDay:1];
    [comps setTimeZone:SYSTEM_CLOCK];
    comps.hour = compsTime.hour;
    comps.minute = compsTime.minute;
    return [calendar dateFromComponents:comps];
}

+ (NSDate *)getFirstSecondOfTheDay :(NSDate *)date {
    
    
    NSString *string = @"00:00";
    
    NSDateFormatter *timeOnlyFormatter = [[NSDateFormatter alloc] init];
    [timeOnlyFormatter setDateFormat:@"HH:mm"];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *compsTime = [calendar components:(NSHourCalendarUnit | NSMinuteCalendarUnit) fromDate:[timeOnlyFormatter dateFromString:string]];
    NSDateComponents* comps = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSWeekCalendarUnit|NSWeekdayCalendarUnit fromDate:date]; // Get necessary date components
    
    // set last of month
    [comps setMonth:[comps month]];
    [comps setDay:[comps day]];
    [comps setTimeZone:SYSTEM_CLOCK];
    comps.hour = compsTime.hour;
    comps.minute = compsTime.minute;
    return [calendar dateFromComponents:comps];
}

+ (NSDate *)lastSecondOfTheMonth:(NSDate *)date{
    
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSRange rng = [cal rangeOfUnit:NSDayCalendarUnit
                            inUnit:NSMonthCalendarUnit
                           forDate:date];
    NSUInteger numberOfDaysInMonth = rng.length;
    
    
    
    NSDateComponents* comps = [cal components:NSYearCalendarUnit|NSMonthCalendarUnit|NSWeekCalendarUnit|NSWeekdayCalendarUnit fromDate:date]; // Get necessary date components
    
    // set last of month
    [comps setMonth:[comps month]];
    [comps setDay:numberOfDaysInMonth];
    [comps setHour:23];
    [comps setMinute:59];
    [comps setSecond:59];
   // [comps setTimeZone:SYSTEM_CLOCK];
   
    return [cal dateFromComponents:comps];
 }


+ (NSDate *)lastSecondOfTheDay:(NSDate *)date {
    
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents* comps = [cal components:NSYearCalendarUnit|NSMonthCalendarUnit|NSWeekCalendarUnit|NSWeekdayCalendarUnit fromDate:date]; // Get necessary date components
    
    // set last of month
    [comps setMonth:[comps month]];
    // [comps setDay:numberOfDaysInMonth];
    [comps setHour:23];
    [comps setMinute:59];
    [comps setSecond:59];
    // [comps setTimeZone:SYSTEM_CLOCK];
    
    return [cal dateFromComponents:comps];
}

+ (NSInteger)daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime {
    NSDate *fromDate;
    NSDate *toDate;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    [calendar rangeOfUnit:NSDayCalendarUnit startDate:&fromDate
                 interval:NULL forDate:fromDateTime];
    [calendar rangeOfUnit:NSDayCalendarUnit startDate:&toDate
                 interval:NULL forDate:toDateTime];
    
    NSDateComponents *difference = [calendar components:NSDayCalendarUnit
                                               fromDate:fromDate toDate:toDate options:0];
    
    return [difference day];
}

+ (BOOL)isDate:(NSDate*)dateSelected WithinRange:(NSInteger) days {
    
    NSDate *fromDate;
    NSDate *toDate;
    NSDate *daysFromSelectedDate = [self getDaysFromNow:days];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    [calendar rangeOfUnit:NSDayCalendarUnit startDate:&fromDate
                 interval:NULL forDate:dateSelected];
    [calendar rangeOfUnit:NSDayCalendarUnit startDate:&toDate
                 interval:NULL forDate:daysFromSelectedDate];
    
    NSDateComponents *difference = [calendar components:NSDayCalendarUnit
                                               fromDate:fromDate toDate:toDate options:0];

    
    if ([difference day] > 0) {
        return YES;
    }
    else {
        return NO;
    }
}

+ (NSDate *)getDaysFromNow:(NSInteger) daysToAdd {
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setDay:daysToAdd];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    return [calendar dateByAddingComponents:dateComponents toDate:[NSDate date] options:0];
}

+ (CLLocationCoordinate2D)getPositionFromCommaSeperatedLatitude:(NSString *) locationInString {
    NSArray *latLong = [locationInString componentsSeparatedByString:@","];
    double lat = [latLong[0] doubleValue];
    double lng = [latLong[1] doubleValue];
    CLLocationCoordinate2D position = CLLocationCoordinate2DMake(lat, lng);
    return position;
}

+ (NSString *)getLatLongInCommaSeperatedString:(CLLocationCoordinate2D) location {
    
    return [NSString stringWithFormat:@"%0.8f,%0.8f", location.latitude, location.longitude];
}

+ (void) getDistanceBetweenLocationA:(CLLocation *)locA andLocationB: (CLLocation *)locB withCompletionBlock:(void (^)(CLLocationDistance distance, NSError *error))completionBlock {
    
   // NSLog(@"SMN Location is %@ %@", locA, locB);
    // Make a directions request
    MKDirectionsRequest *directionsRequest = [MKDirectionsRequest new];
    // Start at our current location
    CLLocationCoordinate2D sourceCoords = CLLocationCoordinate2DMake(locA.coordinate.latitude, locA.coordinate.longitude);
    MKPlacemark *sourcePlacemark = [[MKPlacemark alloc] initWithCoordinate:sourceCoords addressDictionary:nil];
    MKMapItem *source = [[MKMapItem alloc] initWithPlacemark:sourcePlacemark];
    [directionsRequest setSource:source];
    // Make the destination
    CLLocationCoordinate2D destinationCoords = CLLocationCoordinate2DMake(locB.coordinate.latitude, locB.coordinate.longitude);
    MKPlacemark *destinationPlacemark = [[MKPlacemark alloc] initWithCoordinate:destinationCoords addressDictionary:nil];
    MKMapItem *destination = [[MKMapItem alloc] initWithPlacemark:destinationPlacemark];
    [directionsRequest setDestination:destination];
    
    MKDirections *directions = [[MKDirections alloc] initWithRequest:directionsRequest];
    [directions calculateDirectionsWithCompletionHandler:^(MKDirectionsResponse *response, NSError *error) {
        // We're done
        // Now handle the result
        if (error) {
            NSLog(@"There was an error getting your directions");
            return;
        }
        
        // So there wasn't an error - let's plot those routes
        MKRoute *currentRoute = [response.routes firstObject];
        CLLocationDistance dist = currentRoute.distance * 0.000621371192;
        completionBlock (dist, nil);
    }];
}


+ (BOOL)isNumeric:(NSString *)val {
    
    BOOL valid = NO;
    NSCharacterSet *alphaNums = [NSCharacterSet decimalDigitCharacterSet];
    NSCharacterSet *inStringSet = [NSCharacterSet characterSetWithCharactersInString:val];
    valid = [alphaNums isSupersetOfSet:inStringSet];
    return valid;
    
}


@end
