//
//  EventKitManager.h
//  MuV
//
//  Created by SushmaN on 1/8/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <EventKit/EventKit.h>

@interface EventKitManager : NSObject

@property (nonatomic, strong) EKEventStore *eventStore;

@property (nonatomic, strong) NSString *selectedCalendarIdentifier;

@property (nonatomic, strong) NSString *selectedEventIdentifier;

@property (nonatomic) BOOL eventsAccessGranted;


-(NSArray *)getLocalEventCalendars;

-(void)saveCustomCalendarIdentifier:(NSString *)identifier;

-(BOOL)checkIfCalendarIsCustomWithIdentifier:(NSString *)identifier;

-(void)removeCalendarIdentifier:(NSString *)identifier;

-(NSString *)getStringFromDate:(NSDate *)date;

-(NSArray *)getEventsOfSelectedCalendar;

-(void)deleteEventWithIdentifier:(NSString *)identifier;

- (void) storeEventInLocalCalendarWithEventName:(NSString *)eventName
                                        forDate:(NSString *)dateSelected
                                        forTime:(NSString *)timeSelected
                                     forArrival:(BOOL)isArrival
                                    orDeparture:(BOOL)isDeparture
                            withCompletionBlock:(void (^)(NSString *eventId, NSError *error))completionBlock;

@end
