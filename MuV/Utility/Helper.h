//
//  Helper.h
//  MuV
//
//  Created by SushmaN on 1/7/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GoogleMaps/GoogleMaps.h>

@interface Helper : NSObject

+ (BOOL)isSameDay:(NSDate*)date1 otherDay:(NSDate*)date2;
+ (BOOL)isPastDay: (NSDate*)date;
+ (BOOL)isPastDate:(NSDate*)date;
+ (NSDate *)getDateFromString:(NSString *)dateInString;
+ (NSMutableArray *)getAddressArray:(NSString *)addressinString;
+ (CLLocationDistance) getDistanceBetweenLocationA:(CLLocation *)locA andLocationB: (CLLocation *)locB;
+ (NSString *)getStringFromDate:(NSDate *)date;
+ (NSString *)getFormattedDateInString:(NSDate *)date andFormat:(NSString *)format;
+ (NSDate *)firstSecondOfTheMonth :(NSDate *)date;
+ (NSDate *)getFirstSecondOfTheMonth :(NSDate *)date;
+ (NSDate *)lastSecondOfTheMonth :(NSDate *)date;
+ (NSInteger)daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime;
+ (NSDate *)getDaysFromNow:(NSInteger) daysToAdd;

+ (BOOL)isCurrentMonth:(NSDate*)date;

+ (BOOL)compareDates:(NSDate*)dateA andDateB:(NSDate *)dateB;
+ (CLLocationCoordinate2D)getPositionFromCommaSeperatedLatitude:(NSString *) locationInString;
+ (NSString *)getLatLongInCommaSeperatedString:(CLLocationCoordinate2D) location;
+ (BOOL)isDate:(NSDate*)dateSelected WithinRange:(NSInteger) days;
+ (void) getDistanceBetweenLocationA:(CLLocation *)locA andLocationB: (CLLocation *)locB withCompletionBlock:(void (^)(CLLocationDistance distance, NSError *error))completionBlock;

+ (NSDate *)lastSecondOfTheDay:(NSDate *)date;
+ (NSString *)getTimeInStringFromDate:(NSDate *)date;
+ (NSDate *)getFirstSecondOfTheDay :(NSDate *)date;

+ (BOOL)isNumeric:(NSString *)val;


@end
