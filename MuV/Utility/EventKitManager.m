//
//  EventKitManager.m
//  MuV
//
//  Created by SushmaN on 1/8/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "EventKitManager.h"

@interface EventKitManager()

@property (nonatomic, strong) NSMutableArray *arrCustomCalendarIdentifiers;

@end


@implementation EventKitManager

#pragma mark - Initialization

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.eventStore = [[EKEventStore alloc] init];
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        
        // Check if the access granted value for the events exists in the user defaults dictionary.
        if ([userDefaults valueForKey:@"eventkit_events_access_granted"] != nil) {
            // The value exists, so assign it to the property.
            self.eventsAccessGranted = [[userDefaults valueForKey:@"eventkit_events_access_granted"] intValue];
        }
        else{
            // Set the default value.
            self.eventsAccessGranted = NO;
        }
        
        
        // Load the selected calendar identifier.
        if ([userDefaults objectForKey:@"eventkit_selected_calendar"] != nil) {
            self.selectedCalendarIdentifier = [userDefaults objectForKey:@"eventkit_selected_calendar"];
        }
        else{
            self.selectedCalendarIdentifier = @"";
        }
        
        
        // Load the custom calendar identifiers (if exist).
        if ([userDefaults objectForKey:@"eventkit_cal_identifiers"] != nil) {
            self.arrCustomCalendarIdentifiers = [userDefaults objectForKey:@"eventkit_cal_identifiers"];
        }
        else{
            self.arrCustomCalendarIdentifiers = [[NSMutableArray alloc] init];
        }
    }
    return self;
}


#pragma mark - Setter method override

-(void)setEventsAccessGranted:(BOOL)eventsAccessGranted{
    _eventsAccessGranted = eventsAccessGranted;
    
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithBool:eventsAccessGranted] forKey:@"eventkit_events_access_granted"];
}


-(void)setSelectedCalendarIdentifier:(NSString *)selectedCalendarIdentifier{
    _selectedCalendarIdentifier = selectedCalendarIdentifier;
    
    [[NSUserDefaults standardUserDefaults] setObject:selectedCalendarIdentifier forKey:@"eventkit_selected_calendar"];
}


#pragma mark - Private method implementation

-(NSArray *)getLocalEventCalendars{
    NSArray *allCalendars = [self.eventStore calendarsForEntityType:EKEntityTypeEvent];
    NSMutableArray *localCalendars = [[NSMutableArray alloc] init];
    
    for (int i=0; i<allCalendars.count; i++) {
        EKCalendar *currentCalendar = [allCalendars objectAtIndex:i];
        if (currentCalendar.type == EKCalendarTypeLocal) {
            [localCalendars addObject:currentCalendar];
        }
    }
    
    return (NSArray *)localCalendars;
}


-(void)saveCustomCalendarIdentifier:(NSString *)identifier{
    [self.arrCustomCalendarIdentifiers addObject:identifier];
    
    [[NSUserDefaults standardUserDefaults] setObject:self.arrCustomCalendarIdentifiers forKey:@"eventkit_cal_identifiers"];
}


-(BOOL)checkIfCalendarIsCustomWithIdentifier:(NSString *)identifier{
    BOOL isCustomCalendar = NO;
    
    for (int i=0; i<self.arrCustomCalendarIdentifiers.count; i++) {
        if ([[self.arrCustomCalendarIdentifiers objectAtIndex:i] isEqualToString:identifier]) {
            isCustomCalendar = YES;
            break;
        }
    }
    
    return isCustomCalendar;
}


-(void)removeCalendarIdentifier:(NSString *)identifier{
    [self.arrCustomCalendarIdentifiers removeObject:identifier];
    
    [[NSUserDefaults standardUserDefaults] setObject:self.arrCustomCalendarIdentifiers forKey:@"eventkit_cal_identifiers"];
}


-(NSString *)getStringFromDate:(NSDate *)date{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.locale = [NSLocale currentLocale];
    [dateFormatter setDateFormat:@"d MMM yyyy, HH:mm"];
    NSString *stringFromDate = [dateFormatter stringFromDate:date];
    return stringFromDate;
}


-(NSArray *)getEventsOfSelectedCalendar{
    // Specify the calendar that will be used to get the events from.
    EKCalendar *calendar = nil;
    if (self.selectedCalendarIdentifier != nil && self.selectedCalendarIdentifier.length > 0) {
        calendar = [self.eventStore calendarWithIdentifier:self.selectedCalendarIdentifier];
    }
    
    // If no selected calendar identifier exists and the calendar variable has the nil value, then all calendars will be used for retrieving events.
    NSArray *calendarsArray = nil;
    if (calendar != nil) {
        calendarsArray = @[calendar];
    }
    
    // Create a predicate value with start date a year before and end date a year after the current date.
    // int yearSeconds = 365 * (60 * 60 * 24);
    
    
    NSDate *startDate = [NSDate date];
  
    
    NSCalendar *cal         = [NSCalendar currentCalendar];
    NSDateComponents *comps = [cal components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay
                                     fromDate:startDate];
    comps.month             -= 1;
    NSLog(@"Comps Month %ld", (long)comps.month);
    
    startDate          = [cal dateFromComponents:comps];
    NSLog(@"Comps Month %ld date %@", (long)comps.month, startDate);
    
    comps.month += 1;
   // endDate = [cal dateFromComponents:comps];
    NSLog(@"Comps Month %ld date %@ %@", (long)comps.month, [self firstDayOfMonthForDate:startDate], [self lastDayOfMonthForDate:startDate]);
    
   
    NSPredicate *predicate = [self.eventStore predicateForEventsWithStartDate:[self firstDayOfMonthForDate:startDate] endDate:[self lastDayOfMonthForDate:startDate] calendars:calendarsArray];
    
    
    
    // Get an array with all events.
    NSArray *eventsArray = [self.eventStore eventsMatchingPredicate:predicate];
    
    // Copy all objects one by one to a new mutable array, and make sure that the same event is not added twice.
    NSMutableArray *uniqueEventsArray = [[NSMutableArray alloc] init];
    for (int i=0; i<eventsArray.count; i++) {
        EKEvent *currentEvent = [eventsArray objectAtIndex:i];
        
        BOOL eventExists = NO;
        
        // Check if the current event has any recurring rules set. If not, no need to run the next loop.
        if (currentEvent.recurrenceRules != nil && currentEvent.recurrenceRules.count > 0) {
            for (int j=0; j<uniqueEventsArray.count; j++) {
                if ([[[uniqueEventsArray objectAtIndex:j] eventIdentifier] isEqualToString:currentEvent.eventIdentifier]) {
                    // The event already exists in the array.
                    eventExists = YES;
                    break;
                }
            }
        }
        
        // If the event does not exist to the new array, then add it now.
        if (!eventExists) {
            [uniqueEventsArray addObject:currentEvent];
        }
    }
    
    // Sort the array based on the start date.
    uniqueEventsArray = (NSMutableArray *)[uniqueEventsArray sortedArrayUsingSelector:@selector(compareStartDateWithEvent:)];
    
    // Return that array.
    return (NSArray *)uniqueEventsArray;
}


- (NSDate *)firstDayOfMonthForDate:(NSDate *)date
{
    NSCalendar *cal         = [NSCalendar currentCalendar];
    NSDateComponents *comps = [cal components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay
                                     fromDate:date];
    comps.day               = 1;
    return [cal dateFromComponents:comps];
}

// Return the last day of the month for the month that 'date' falls in:
- (NSDate *)lastDayOfMonthForDate:(NSDate *)date
{
    NSCalendar *cal         = [NSCalendar currentCalendar];
    NSDateComponents *comps = [cal components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay
                                     fromDate:date];
    comps.month             += 1;
    comps.day               = 0;
    
    return [[cal dateFromComponents:comps] dateByAddingTimeInterval:60*60*24*1];
}

-(void)deleteEventWithIdentifier:(NSString *)identifier{
    // Get the event that's about to be deleted.
    EKEvent *event = [self.eventStore eventWithIdentifier:identifier];
    
    // Delete it.
    NSError *error;
    if (![self.eventStore removeEvent:event span:EKSpanFutureEvents error:&error]) {
        // Display the error description.
        NSLog(@"%@", [error localizedDescription]);
    }
}

- (void) storeEventInLocalCalendarWithEventName:(NSString *)eventName
                                        forDate:(NSString *)dateSelected
                                        forTime:(NSString *)timeSelected
                                     forArrival:(BOOL)isArrival
                                    orDeparture:(BOOL)isDeparture
                            withCompletionBlock:(void (^)(NSString *eventId, NSError *error))completionBlock  {
    EKEventStore *eventStore = [[EKEventStore alloc] init];
    EKEvent *event = [EKEvent eventWithEventStore:eventStore];
    
    // title of the event
    event.title = eventName;
    
    NSString *dateArrive = [NSString stringWithFormat:@"%@ %@", dateSelected, timeSelected];
    NSString *dateDepart = [NSString stringWithFormat:@"%@ %@", dateSelected, timeSelected];
    
    
    // Convert string to date object
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"EEEE     MM / dd / yy hh:mm a"];
    
    if (isArrival) {
        event.startDate = [dateFormat dateFromString:dateArrive];
        event.endDate = [dateFormat dateFromString:dateArrive];
    }
    if (isDeparture) {
        event.startDate = [dateFormat dateFromString:dateDepart];
        event.endDate = [dateFormat dateFromString:dateDepart];
    }
    
    
    if ([eventStore respondsToSelector:@selector(requestAccessToEntityType:completion:)]) {
        // iOS 6 and later
        [eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
            if (granted) {
                // code here for when the user allows your app to access the calendar
                [event setCalendar:[eventStore defaultCalendarForNewEvents]];
                if ( [eventStore saveEvent:event span:EKSpanThisEvent error:&error]) {
                    NSString* str = [[NSString alloc] initWithFormat:@"%@", event.eventIdentifier];
                    completionBlock (str, nil);
                }
                else{
                    completionBlock (nil, error);
                    // An error occurred, so log the error description.
                    NSLog(@"%@", [error localizedDescription]);
                }
            } else {
                // code here for when the user does NOT allow your app to access the calendar
            }
        }];
    }
}


@end

