//
//  BaseContentViewController.h
//  MuV
//
//  Created by Hing Huynh on 6/11/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RootViewController.h"

@interface BaseContentViewController : UIViewController

#pragma mark - Properties
@property (nonatomic, strong) RootViewController *rootViewController;

@end
