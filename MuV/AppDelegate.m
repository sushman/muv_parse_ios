//
//  AppDelegate.m
//  MuV
//
//  Created by Hing Huynh on 1/7/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "AppDelegate.h"
#import <Parse/Parse.h>
//#import <ParseFacebookUtils/PFFacebookUtils.h>
#import <GoogleMaps/GoogleMaps.h>
#import <Braintree/Braintree.h>
#import "ControlVariables.h"
#import "LocationTracker.h"
#import <ParseCrashReporting/ParseCrashReporting.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>


@interface AppDelegate ()

@end

NSString *BraintreeAppDelegatePaymentsURLScheme = @"MuV.MuV.payments";

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // [Optional] Power your app with Local Datastore. For more info, go to
    [Parse enableLocalDatastore];
    //[self creatingInstanceForGlobalClasses];

    // Enable Crash Reporting
    [ParseCrashReporting enable];
    
    
    // Initialize Parse.
    [Parse setApplicationId:@"zPuYchs6RVIm35PMeKrk8mN0r7rfOZPUwLSeBzvw"
                  clientKey:@"265gm1jZ4JzeFsv16iSJQOJfT5jxmZPN7qcHQrcw"];
    
   
    [Fabric with:@[CrashlyticsKit]];
    
    // [Optional] Track statistics around application opens.
    [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
    
    [GMSServices provideAPIKey:@"AIzaSyBIqoIf10eN4E9ZB7uJfpsmTW0id-nmqQk"];
    
   // AIzaSyBIqoIf10eN4E9ZB7uJfpsmTW0id-nmqQk
    
    [self setupAppearance];
    
    [Braintree setReturnURLScheme:@"MuV.MuV.payments"];
    
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"navAddr"];
    
    
    return YES;
}

- (void)creatingInstanceForGlobalClasses{
    
    //main2 storyboard
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main2" bundle:nil];
    self.tripViewController = [storyboard instantiateViewControllerWithIdentifier:@"Tripsboard"];
    self.calendarViewController = [storyboard instantiateViewControllerWithIdentifier:@"CalendarView"];
    
    //main storyboard
    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    self.sidePanelViewController = [storyboard instantiateViewControllerWithIdentifier:@"MuVSidePanelViewController"];

}



- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    if ([url.scheme isEqualToString:BraintreeAppDelegatePaymentsURLScheme]) {
        return [Braintree handleOpenURL:url sourceApplication:sourceApplication];
    }
    return YES;
}



- (void)applicationWillTerminate:(UIApplication *)application {
   // [[PFFacebookUtils session] close];
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"navAddr"];
    [[LocationTracker shared] stopLocationTracking];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    NSLog(@"app entered foreground");
}

- (void)setupAppearance {
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:0.953f green:0.961f blue:0.973f alpha:1.00f]];
    [[UINavigationBar appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor colorWithRed:0.169f green:0.216f blue:0.439f alpha:1.00f], NSForegroundColorAttributeName,
      MUV_BOLD_FONT(16), NSFontAttributeName,nil]];
}



@end
