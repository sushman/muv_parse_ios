//
//  MuVSideMenuViewController.m
//  MuV
//
//  Created by Hing Huynh on 1/8/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "MuVSideMenuViewController.h"
#import "JASidePanelController.h"
#import "UIViewController+JASidePanel.h"
#import "MuVSidePanelCell.h"
#import "UIColor+MuVColorAdditions.h"
#import "MuVProfileSetupViewController.h"
#import "MuVRegisterViewController.h"
#import "SwitchToggleCell.h"
#import <Parse/Parse.h>
#import "UIButton+WebCache.h"
#import "UIImageView+WebCache.h"
#import "UIButton+MuVUIButtonAdditions.h"
#import "UIImageView+MuVUIImageViewAdditions.h"
#import "UserProfileInformation.h"
#import "ControlVariables.h"
#import "MuVLoginViewController.h"
#import "MuVSidePanelController.h"
#import "MuVTripEnRouteViewController.h"
#import <MessageUI/MessageUI.h>
#import "ActivityProvider.h"
#import "ActivityViewCustomActivity.h"

#import "RideManager.h"
#import "CouponManager.h"
#import "TripManager.h"
#import "BookingInformation.h"

#define MYTRIPS @"MY TRIPS"
#define MY_COUPONS @"MY COUPONS"
#define REQUEST_MUV @"REQUEST MÜV"
#define SCHEDULE_MUV @"SCHEDULE MÜV"
#define SUPPORT @"SUPPORT"
#define SUPPORT_EMAIL @"support@muv2work.com"




@implementation MuVSideMenuViewController

BOOL isDriver = NO;
RideManager *rideManager;
TripManager *tripManager;
Trip *currentTrip;
BOOL isCurrentForDriver;
BOOL isCurrentRider;


- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"riderMode"]) {
        isDriver = NO;
    }
    else {
        isDriver = YES;
    }
    isCurrentForDriver = NO;
    isCurrentRider = NO;
    _userInfo = [UserProfileInformation sharedMySingleton];
    
    rideManager = [RideManager new];
    
    if (_userInfo.driverDetails) {
        if (_userInfo.driverDetails[@"carImage"]) {
            PFFile *carDetails  = _userInfo.driverDetails[@"carImage"];
            if (carDetails) {
                _driverImageURL = carDetails.url;
            }
        }
    }
    
    
    if (_userInfo.user[@"profileImage"]) {
        PFFile *userImage  = _userInfo.user[@"profileImage"];
        
        if (userImage) {
            _riderImageURL = userImage.url;
        }
    }
    
    _tableViewMenu.delegate = self;
    _tableViewMenu.dataSource = self;
    
    [self showUserProfile];
    
    [self checkIfCurrentTrip];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tripStarted:) name:@"tripStarted" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(rideCompleted:) name:@"rideDone" object:nil];
}


- (void)viewWillAppear:(BOOL)animated {
    [self updateArray];
}

- (void)showUserProfile {
   
    _emailIdLabel.text = _userInfo.user[@"email"];
    _currentProfileImageView = [UIImageView muv_imageCircle:_currentProfileImageView];
    _rideToggle = [UIButton muv_buttonCircle:_rideToggle];
    [self toggleProfiles];
}

- (void)toggleProfiles {
    
    if (_userInfo.user[@"driver"] == [NSNumber numberWithBool:YES]) {
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"riderMode"]) {
            isDriver = NO;
            
            if (_riderImageURL) {
                [_currentProfileImageView sd_setImageWithURL:[NSURL URLWithString:_riderImageURL] placeholderImage:[UIImage imageNamed:NOIMAGE_AVATAR]];
            }
            else {
                [_currentProfileImageView setImage:[UIImage imageNamed:NOIMAGE_AVATAR]];
            }
            if (_driverImageURL) {
                [_rideToggle sd_setBackgroundImageWithURL:[NSURL URLWithString:_driverImageURL] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:MUV_NO_CAR_IMAGE]];
            }
            else {
               [_rideToggle setBackgroundImage:[UIImage imageNamed:MUV_NO_CAR_IMAGE] forState:UIControlStateNormal];
            }
            
        } else {
            isDriver = YES;
            if (_driverImageURL) {
                [_currentProfileImageView sd_setImageWithURL:[NSURL URLWithString:_driverImageURL] placeholderImage:[UIImage imageNamed:MUV_NO_CAR_IMAGE]];

            }
            else {
                [_currentProfileImageView setImage:[UIImage imageNamed:MUV_NO_CAR_IMAGE]];
            }

            
            if (_riderImageURL) {
                 [_rideToggle sd_setBackgroundImageWithURL:[NSURL URLWithString:_riderImageURL] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:NOIMAGE_AVATAR]];            }
            else {
                [_rideToggle setBackgroundImage:[UIImage imageNamed:NOIMAGE_AVATAR] forState:UIControlStateNormal];

            }
        }
    } else {
        isDriver = NO;
        if (_riderImageURL) {
            [_currentProfileImageView sd_setImageWithURL:[NSURL URLWithString:_riderImageURL] placeholderImage:[UIImage imageNamed:NOIMAGE_AVATAR]];
        }
        else {
            [_currentProfileImageView setImage:[UIImage imageNamed:NOIMAGE_AVATAR]];
        }
        _rideToggle.hidden = NO;
    }
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MuVSidePanelCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MuVSidePanelCell"];
    if (cell) {
        cell.menuTextLabel.font = MUV_REGULAR_FONT(14);
        cell.menuTextLabel.textColor = [UIColor muv_tableCellViewTextColor];
        cell.menuTextLabel.text = [_cellNames objectAtIndex:indexPath.row];

        NSString *imageName = [_cellNames objectAtIndex:indexPath.row];
        if ([imageName isEqualToString:REQUEST_MUV]) {
            imageName = @"REQUEST MUV";
        }

        if ([imageName isEqualToString:SCHEDULE_MUV]) {
            imageName = @"REQUEST MUV";
        }


        cell.iconImageView.image = [UIImage imageNamed:imageName];
        if (indexPath.row != 0){
            UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 0.5)];
            separatorLineView.backgroundColor = [UIColor lightGrayColor];
            [cell.contentView addSubview:separatorLineView];
        }
    }
    return cell;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellName = [_cellNames objectAtIndex:indexPath.row];
    if ([cellName isEqualToString:@"PROFILE"]) {
        if (![[NSUserDefaults standardUserDefaults] boolForKey:@"riderMode"]){
            self.sidePanelController.centerPanel = [self.storyboard instantiateViewControllerWithIdentifier:@"DriverProfileNavigation"];
        } else {
            self.sidePanelController.centerPanel = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileSetupNavigation"];
        }
        return;
    } else if ([cellName isEqualToString:@"SHARE"]) {
        
        ActivityProvider *activityProvider = [[ActivityProvider alloc] init];
        
        ActivityViewCustomActivity *ca = [ActivityViewCustomActivity new];
        
        NSArray *items = @[activityProvider];
        
        UIActivityViewController *ActivityView = [[UIActivityViewController alloc]
                                                   initWithActivityItems:items
                                                   applicationActivities:[NSArray arrayWithObject:ca]];
        [ActivityView setValue:@"Get The MüV App" forKey:@"subject"];
        
        
        
        [ActivityView setExcludedActivityTypes:
         @[UIActivityTypeAssignToContact,
           UIActivityTypeCopyToPasteboard,
           UIActivityTypePrint,
           UIActivityTypeSaveToCameraRoll,
           UIActivityTypePostToWeibo]];
        
        [self presentViewController:ActivityView animated:YES completion:nil];
        [ActivityView setCompletionHandler:^(NSString *act, BOOL done)
         {
             NSString *ServiceMsg = nil;
             if ( [act isEqualToString:UIActivityTypeMail] )           ServiceMsg = @"Mail sent!";
             if ( [act isEqualToString:UIActivityTypePostToTwitter] )  ServiceMsg = @"Post on twitter, ok!";
             if ( [act isEqualToString:UIActivityTypePostToFacebook] ) ServiceMsg = @"Post on facebook, ok!";
             if ( [act isEqualToString:UIActivityTypeMessage] )        ServiceMsg = @"SMS sended!";
             if ( done )
             {
                 UIAlertView *Alert = [[UIAlertView alloc] initWithTitle:ServiceMsg message:@"" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
                 [Alert show];
             }
         }];

    }
    else if ([cellName isEqualToString:MYTRIPS]) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main2" bundle:nil];
        if(!self.myTripsViewController)
            self.myTripsViewController = [storyboard instantiateViewControllerWithIdentifier:@"Tripsboard"];
        self.sidePanelController.centerPanel = self.myTripsViewController;
        [[TripManager sharedManager] setParkAndRide:nil];
        
    }
    else if ([cellName isEqualToString:MY_COUPONS]) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main2" bundle:nil];
        self.sidePanelController.centerPanel = [storyboard instantiateViewControllerWithIdentifier:@"CouponView"];
    }
    else if ([cellName isEqualToString:REQUEST_MUV] || [cellName isEqualToString:SCHEDULE_MUV]) {
        
        [BookingInformation sharedMySingleton].isArrival = NO;
        [BookingInformation sharedMySingleton].isDeparture = NO;
        self.sidePanelController.centerPanel = [self.storyboard instantiateViewControllerWithIdentifier:@"BookingNav"];
        [[TripManager sharedManager] setEditedRide:nil];
        [[TripManager sharedManager] setParkAndRide:nil];
    }
    else if ([cellName isEqualToString:@"RECEIVE PAYMENTS"]) {
        self.sidePanelController.centerPanel = [self.storyboard instantiateViewControllerWithIdentifier:@"recievePaymentsNav"];
    }
    else if ([cellName isEqualToString:@"PAYMENT"]) {
        self.sidePanelController.centerPanel = [self.storyboard instantiateViewControllerWithIdentifier:@"addPaymentNav"];
    }
    else if ([cellName isEqualToString:@"BECOME A DRIVER"]) {
        self.sidePanelController.centerPanel = [self.storyboard instantiateViewControllerWithIdentifier:@"driverApplicationNav"];
    }
    else if ([cellName isEqualToString:SUPPORT]) {
        [self showMailComposer];
    }
    
}

- (IBAction)gotoOnGoingTrip:(id)sender {
       if ([[NSUserDefaults standardUserDefaults]boolForKey:@"riderMode"]) {
        //rider
        [rideManager getCurrentRideForRiderwithCompletionBlock:^(Trip *trip, NSError *error) {
            if (trip) {
                [self loadTripDetailsPage:trip];
            }
            else {

            }
        }];
    }
    else {
        //driver
        [rideManager getCurrentRideForDriverwithCompletionBlock:^(Trip *trip, NSError *error) {
            if (trip) {
                [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"navAddr"];
                [self loadTripDetailsPage:trip];
            }
            else {

            }
        }];
    }
}


- (IBAction)logOut:(id)sender {
    [self prepareForLogout];
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
 
    return _cellNames.count;
}


- (void)tripStarted:(NSNotification *) notification  {
    if ([[TripManager sharedManager] currentTripDriver]) {
        if (isDriver) {
            isCurrentForDriver = YES;
            _onGoingTripView.hidden = NO;
        } else {
            _onGoingTripView.hidden = YES;
        }
    }
}

- (void)checkIfCurrentTrip {
    
    _onGoingTripView.hidden = YES;
    //for rider
    [rideManager getCurrentRideForRiderwithCompletionBlock:^(Trip *trip, NSError *error) {
        if (trip) {
            if (trip.route[@"currentLocation"]) {
                currentTrip = trip;
                isCurrentRider = YES;
            }
            else {
                currentTrip = nil;
                isCurrentRider = NO;
            }
        }
        if (_userInfo.user[@"driver"]) {
            if (_userInfo.driverDetails[@"active"] != [NSNumber numberWithBool:NO]) {
                NSInteger nextInLine = [[NSUserDefaults standardUserDefaults] integerForKey:@"navAddr"];
                //current trip
                if (nextInLine > 0) {
                    isCurrentForDriver = YES;
                    [self hideShowOngoingTripButton];

                }
                else {
                    [rideManager getCurrentRideForDriverwithCompletionBlock:^(Trip *trip, NSError *error) {
                        if (trip) {
                            if (trip.route[@"currentLocation"]) {
                                currentTrip = trip;
                                isCurrentForDriver = YES;
                            }
                            else {
                                currentTrip = nil;
                                isCurrentForDriver = NO;
                            }
                        }
                        [self hideShowOngoingTripButton];
                    }];
                }
            }
            else {
                [self hideShowOngoingTripButton];
            }
        }
        else {
            [self hideShowOngoingTripButton];
        }
    }];
}

- (void)hideShowOngoingTripButton {
    if ((isCurrentForDriver && isDriver) || (isCurrentRider && !isDriver)) {
        _onGoingTripView.hidden = NO;
    } else {
        _onGoingTripView.hidden = YES;
    }
}

- (void)rideCompleted:(NSNotification *)notification {
  //  [self checkIfCurrentTrip];
    _onGoingTripView.hidden = YES;
}

- (void) viewDidDisappear:(BOOL)animated {
    
  //  [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

- (void)viewDidUnload {
    [super viewDidUnload];
   // [[NSNotificationCenter defaultCenter] removeObserver:self forKeyPath:NSUSERDEFAULT_FOR_RIDERMODE];
}



- (IBAction)riderButtonTapped:(id)sender {
    
    BOOL isRider = [[NSUserDefaults standardUserDefaults]boolForKey:@"riderMode"];
    
    if (isRider && _userInfo.driverDetails[@"active"] == [NSNumber numberWithBool:NO]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message"
                                                        message:@"Your driver account has been disabled. Please contact 'Support MüV'"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    if (_userInfo.user[@"driver"] == [NSNumber numberWithBool:YES]) {
       
        if (isRider){
            [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"riderMode"];
            
        } else {
            [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"riderMode"];
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"riderMode" object:nil];
    }
    [self updateArray];
    [_tableViewMenu reloadData];
    [self toggleProfiles];
    if ((isCurrentForDriver && isDriver) || (isCurrentRider && !isDriver)) {
        _onGoingTripView.hidden = NO;
    } else {
        _onGoingTripView.hidden = YES;
    }
}


- (void) loadTripDetailsPage:(Trip *)trip {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main2" bundle:nil];
    MuVSidePanelController *sidePanelViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MuVSidePanelViewController"];
    
    sidePanelViewController.centerPanel = [storyboard instantiateViewControllerWithIdentifier:@"TripDetailNav"];
    
    [[TripManager sharedManager] setCurrentTrip:trip];
    
    [self presentViewController:sidePanelViewController animated:YES completion:nil];
}

- (void)prepareForLogout {
   // [[PFFacebookUtils session] closeAndClearTokenInformation];
    
    [UserProfileInformation sharedMySingleton].user = nil;
    [UserProfileInformation sharedMySingleton].driverDetails = nil;

    
    tripManager = [TripManager sharedManager];
    tripManager.tripDates = nil;
    [tripManager setEditedRide:nil];
    [tripManager setDateLoaded:nil];
    [tripManager setMonthLoaded:nil];
    [tripManager setCurrentTrip:nil];
    [tripManager setCurrentTripDriver:nil];
    [tripManager setCurrentTripRider:nil];
    [tripManager setParkAndRide:nil];
    
    [CouponManager sharedInstance].couponName = nil;
    
    [BookingInformation sharedMySingleton].isArrival = NO;
    [BookingInformation sharedMySingleton].isDeparture = NO;
    
    [[LocationTracker shared] stopLocationTracking];
    
    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"riderMode"];
    
    [PFUser logOut];
    
    MuVRegisterViewController *registerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MuVRegisterViewController"];
    [self presentViewController:registerViewController animated:YES completion:nil];

}


- (void)showMailComposer {
    
    if([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailComposer = [[MFMailComposeViewController alloc]init];
        [mailComposer setToRecipients:@[SUPPORT_EMAIL]];
        mailComposer.mailComposeDelegate = self;
        [mailComposer setSubject:@"MüV Support Inquiry"];
        [self presentViewController:mailComposer animated:YES completion:NULL];
    } else {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Your device's email client is not setup." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
    
}


- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result {
    switch (result) {
        case MessageComposeResultCancelled:
            break;
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed to send SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            break;
        }
        case MessageComposeResultSent:
            break;
        default:
            break;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)mailComposeController:(MFMailComposeViewController *)controller
         didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    if (result) {
        NSLog(@"Result : %d",result);
    }
    if (error) {
        NSLog(@"Error : %@",error);
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

////event handler when event occurs
//- (void)rideModeChanged: (NSNotification *) notification {
//    
//    [self updateArray];
//    [self.tableView reloadData];
//    
//}
//

-(void) updateArray {
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"riderMode"]) {
        
        if (_userInfo.user[@"pendingDriverApproval"] == [NSNumber numberWithBool:YES]) {
            
            if (_userInfo.user[@"driver"] == [NSNumber numberWithBool:YES]) {
                _cellNames = @[@"PROFILE",@"PAYMENT",MYTRIPS,REQUEST_MUV,@"SHARE",MY_COUPONS, SUPPORT];
  
            }
            else {
                _cellNames = @[@"PROFILE",@"PAYMENT",MYTRIPS,REQUEST_MUV,@"PENDING APPROVAL",@"SHARE",MY_COUPONS, SUPPORT];
            }
        }
        else {
            if (_userInfo.user[@"driver"] == [NSNumber numberWithBool:YES]) {
                 _cellNames = @[@"PROFILE",@"PAYMENT",MYTRIPS,REQUEST_MUV,@"SHARE",MY_COUPONS, SUPPORT];
            }
            else {
                _cellNames = @[@"PROFILE",@"PAYMENT",MYTRIPS,REQUEST_MUV,@"BECOME A DRIVER",@"SHARE",MY_COUPONS, SUPPORT];
            }
        }
    }
    else {
        _cellNames = @[@"PROFILE",@"RECEIVE PAYMENTS",MYTRIPS, SCHEDULE_MUV, @"SHARE", SUPPORT];
    }
}


@end
