//
//  MuVSideMenuViewController.h
//  MuV
//
//  Created by Hing Huynh on 1/8/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "UserProfileInformation.h"

@interface MuVSideMenuViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) UserProfileInformation *userInfo;
@property (strong, nonatomic) UIViewController *myTripsViewController;


@property (assign, nonatomic) BOOL driver;
@property (assign, nonatomic) int blankCellHeight;
@property (assign, nonatomic) int cellCount;
@property (strong, nonatomic) NSArray *cellNames;

@property (strong, nonatomic) NSString *riderImageURL;
@property (strong, nonatomic) NSString *driverImageURL;

@property (weak, nonatomic) IBOutlet UIButton *rideToggle;
@property (weak, nonatomic) IBOutlet UIImageView *currentProfileImageView;
@property (weak, nonatomic) IBOutlet UILabel *emailIdLabel;


@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UITableView *tableViewMenu;

@property (weak, nonatomic) IBOutlet UIView *onGoingTripView;


@end
