//
//  MuVSidePanelController.m
//  MuV
//
//  Created by Hing Huynh on 1/8/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "MuVSidePanelController.h"

@implementation MuVSidePanelController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)awakeFromNib {
    [self setLeftPanel:[self.storyboard instantiateViewControllerWithIdentifier:@"MuVLeftViewController"]];
    
    BOOL isRider = [[NSUserDefaults standardUserDefaults] boolForKey:@"riderMode"];
    
    if (isRider) {
        [self setCenterPanel:[self.storyboard instantiateViewControllerWithIdentifier:@"BookingNav"]];
    }
    else {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main2" bundle:nil];
        [self setCenterPanel:[storyboard instantiateViewControllerWithIdentifier:@"Tripsboard"]];
    }
}

@end
