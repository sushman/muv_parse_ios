//
//  OnGoingTripCell.h
//  MuV
//
//  Created by Hing Huynh on 1/15/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OnGoingTripCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *menuTextLabel;
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;

@end
