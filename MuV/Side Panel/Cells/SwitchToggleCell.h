//
//  SwitchToggleCell.h
//  MuV
//
//  Created by Hing Huynh on 1/20/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SwitchToggleCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *profileImageButton;
@property (weak, nonatomic) IBOutlet UIButton *riderImageButton;
@property (weak, nonatomic) IBOutlet UILabel *nameTextLabel;

@end
