//
//  MuVLoginViewController.m
//  MuV
//
//  Created by Hing Huynh on 1/7/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "MuVLoginViewController.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "MuVSidePanelController.h"
#import <Parse/Parse.h>
#import "NSString+MuVStringAdditions.h"
#import "UITextField+MuVTextFieldAdditions.h"
#import "MuVCreateProfileViewController.h"
#import "UIButton+MuVUIButtonAdditions.h"
#import "UITextField+MuVTextFieldAdditions.h"
#import "UserProfileInformation.h"
#import "CouponManager.h"
#import "RideManager.h"
#import "ControlVariables.h"
#import "TripManager.h"

@interface MuVLoginViewController ()

@property (weak, nonatomic) IBOutlet UITextField *userNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIButton *facebookButton;
@property (strong,nonatomic) UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) UserProfileInformation *userInfo;

@end

@implementation MuVLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _userInfo = [UserProfileInformation sharedMySingleton];
    
    [_userNameTextField addChangeListenerTarget:self action:@selector(textFieldValueChanged)];
    [_passwordTextField addChangeListenerTarget:self action:@selector(textFieldValueChanged)];
    
    _scrollView.frame = CGRectMake(0, 0, 320, self.view.bounds.size.height);
    
    _loginButton = [UIButton muv_buttonUpdate:_loginButton];
    _facebookButton = [UIButton muv_buttonUpdate:_facebookButton];
    
    _userNameTextField = [UITextField muv_textFieldUpdate:_userNameTextField];
    _passwordTextField = [UITextField muv_textFieldUpdate:_passwordTextField];
    
}

- (void)viewDidUnload {
    [self setScrollView:nil];
    [super viewDidUnload];
}

- (void)textFieldValueChanged {
    _loginButton.enabled = !_userNameTextField.text.muv_isEmpty &&
    !_passwordTextField.text.muv_isEmpty;
}

- (IBAction)forgotPasswordButtonTapped:(id)sender {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self performSegueWithIdentifier:@"forgotPasswordSegue" sender:sender];
    });
}

- (IBAction)loginButtonTapped:(id)sender {
    [PFUser logInWithUsernameInBackground:[_userNameTextField.text lowercaseString] password:_passwordTextField.text
                                    block:^(PFUser *user, NSError *error) {
        if (user) {
            [self saveUser:user];
            
        } else {
            
            NSString *errorString = [error userInfo][@"error"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message"
                                                            message:errorString
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
    }];
    
}

- (IBAction)facebookButtonTapped:(id)sender {
//  //  [PFFacebookUtils initializeFacebook];
//    // Set permissions required from the facebook user account
//    NSArray *permissionsArray = @[ @"user_about_me", @"user_relationships", @"user_birthday", @"user_location", @"email" ];
//    
//    // Login PFUser using Facebook
//    [PFFacebookUtils logInWithPermissions:permissionsArray block:^(PFUser *user, NSError *error) {
//        [_activityIndicator stopAnimating]; // Hide loading indicator
//        
//        NSLog(@"user: %@", user.username);
//        
//        if (!user) {
//            NSString *errorMessage = nil;
//            if (!error) {
//                NSLog(@"Uh oh. The user cancelled the Facebook login.");
//                errorMessage = @"Uh oh. The user cancelled the Facebook login.";
//            } else {
//                NSLog(@"Uh oh. An error occurred: %@", error);
//                errorMessage = [error localizedDescription];
//            }
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Log In Error"
//                                                            message:errorMessage
//                                                           delegate:nil
//                                                  cancelButtonTitle:nil
//                                                  otherButtonTitles:@"Dismiss", nil];
//            [alert show];
//        } else {
//            if (user.isNew) {
//                NSLog(@"User with facebook signed up and logged in!");
//                dispatch_async(dispatch_get_main_queue(),^{
//                    [self performSegueWithIdentifier:@"createProfileSegue" sender:sender];
//                });
//            } else {
//                NSLog(@"User with facebook logged in!");
//                [self saveUser:user];
//                
//            }
//        }
//    }];
//    
//    [_activityIndicator startAnimating]; // Show loading indicator until login is finished
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"createProfileSegue"]) {
        MuVCreateProfileViewController *createProfileViewController = (MuVCreateProfileViewController *)segue.destinationViewController;
        createProfileViewController.faceBook = YES;
    }
}

- (void)saveUser:(PFUser *)user {
    PFQuery *query = [PFUser query];
    [query whereKey:@"objectId" equalTo:user.objectId];
    [query includeKey:@"driverdetail"];
    [query includeKey:@"defaultToken"];
    [query includeKey:@"paymentTokens"];
    [query includeKey:@"driverdetail.merchantAccount"];
    
    
    [query getFirstObjectInBackgroundWithBlock:^(PFObject *currentUser, NSError *error){
        _userInfo.user = user;
        
        if (![_userInfo.user[@"driver"]boolValue]){
            [[NSUserDefaults standardUserDefaults]setBool:YES forKey :@"riderMode"];
        }
        _userInfo.driverDetails = user[@"driverdetail"];
        
        if (_userInfo.user[@"active"] == [NSNumber numberWithBool:YES]){
            dispatch_async(dispatch_get_main_queue(),^{
                MuVSidePanelController *sidePanelViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MuVSidePanelViewController"];
                [self presentViewController:sidePanelViewController animated:YES completion:nil];
            });
        } else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message"
                                                            message:@"Sorry, but your account has been disabled."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
        
        if (_userInfo.driverDetails[@"active"] == [NSNumber numberWithBool:NO]) {
            [[NSUserDefaults standardUserDefaults]setBool:YES forKey :@"riderMode"];
        }

        if ([CouponManager sharedInstance].couponName.length <= 0) {
            CouponManager *couponManager = [CouponManager new];
            [couponManager getMyCouponCodeForReferringFriendsWithCompletionBlock:^(NSString *couponCode, NSError *error) {
                [CouponManager sharedInstance].couponName = couponCode;
            }];
        }
    }];
}

@end
