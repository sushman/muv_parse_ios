//
//  MuVLoginViewController.h
//  MuV
//
//  Created by Hing Huynh on 1/7/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TPKeyboardAvoidingScrollView;

@interface MuVLoginViewController : UIViewController

@property (nonatomic, retain) IBOutlet TPKeyboardAvoidingScrollView *scrollView;

@end
