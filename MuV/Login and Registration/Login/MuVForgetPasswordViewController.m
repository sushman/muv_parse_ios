//
//  MuVForgetPasswordViewController.m
//  MuV
//
//  Created by Hing Huynh on 1/7/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "MuVForgetPasswordViewController.h"
#import "TPKeyboardAvoidingScrollView.h"
#import <Parse/Parse.h>
#import "UIButton+MuVUIButtonAdditions.h"
#import "UITextField+MuVTextFieldAdditions.h"


@interface MuVForgetPasswordViewController ()

@property (weak, nonatomic) IBOutlet UIButton *resetPasswordButton;

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;

@end

@implementation MuVForgetPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.scrollView contentSizeToFit];
    _resetPasswordButton = [UIButton muv_buttonUpdate:_resetPasswordButton];
    _emailTextField = [UITextField muv_textFieldUpdate:_emailTextField];
}


- (void)viewDidUnload
{
    [self setScrollView:nil];
    [super viewDidUnload];
}

- (IBAction)resetPasswordButtonTapped:(id)sender {
    [PFUser requestPasswordResetForEmailInBackground:_emailTextField.text];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self performSegueWithIdentifier:@"resetPasswordSegue" sender:sender];
    });
}

@end
