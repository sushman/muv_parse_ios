//
//  MuVCreateProfileViewController.m
//  MuV
//
//  Created by Hing Huynh on 1/7/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "MuVCreateProfileViewController.h"
#import "TPKeyboardAvoidingScrollView.h"
#import <Parse/Parse.h>
//#import <ParseFacebookUtils/PFFacebookUtils.h>
#import "NSString+MuVStringAdditions.h"
#import "UITextField+MuVTextFieldAdditions.h"
#import "MuVSidePanelController.h"
#import "JASidePanelController.h"
#import "UIViewController+JASidePanel.h"
#import "UIButton+MuVUIButtonAdditions.h"
#import "UITextField+MuVTextFieldAdditions.h"
#import "UserProfileInformation.h"
#import "UIView+UIViewAdditions.h"
#import "UIViewController+JASidePanel.h"
#import "MuVSidePanelController.h"
#import "CouponManager.h"
#import "AdminManager.h"
#import "ControlVariables.h"

@interface MuVCreateProfileViewController ()

- (void)textFieldValueChanged;

@property (weak, nonatomic) IBOutlet UITextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordTextField;

@property (weak, nonatomic) IBOutlet UIButton *joinButton;
@property (weak, nonatomic) IBOutlet UIButton *editProfileButton;
@property (weak, nonatomic) IBOutlet UIButton *bookAMuvButton;
@property (weak, nonatomic) IBOutlet UIButton *facebookButton;

@property (strong, nonatomic) IBOutlet UIView *transparentBackgroundView;
@property (strong, nonatomic) IBOutlet UIView *joinButtonTappedView;

@property (strong,nonatomic) UIActivityIndicatorView *activityIndicator;

@property (strong,nonatomic) UserProfileInformation *userInfo;


@end

@implementation MuVCreateProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.scrollView contentSizeToFit];
    
    _userInfo = [UserProfileInformation sharedMySingleton];
    
    self.faceBookData = [[NSDictionary alloc]init];
    
    [_firstNameTextField addChangeListenerTarget:self action:@selector(textFieldValueChanged)];
    [_lastNameTextField addChangeListenerTarget:self action:@selector(textFieldValueChanged)];
    [_emailTextField addChangeListenerTarget:self action:@selector(textFieldValueChanged)];
    [_phoneNumberTextField addChangeListenerTarget:self action:@selector(textFieldValueChanged)];
    [_passwordTextField addChangeListenerTarget:self action:@selector(textFieldValueChanged)];
    [_confirmPasswordTextField addChangeListenerTarget:self action:@selector(textFieldValueChanged)];
    
    if (_faceBook){
        [self _loadData];
    }
    
    _joinButton = [UIButton muv_buttonUpdate:_joinButton];
    _facebookButton = [UIButton muv_buttonUpdate:_facebookButton];
    _editProfileButton = [UIButton muv_buttonUpdate:_editProfileButton];
    _bookAMuvButton = [UIButton muv_buttonUpdate:_bookAMuvButton];
    
    _firstNameTextField = [UITextField muv_textFieldUpdate:_firstNameTextField];
    _lastNameTextField = [UITextField muv_textFieldUpdate:_lastNameTextField];
    _emailTextField = [UITextField muv_textFieldUpdate:_emailTextField];
    _phoneNumberTextField = [UITextField muv_textFieldUpdate:_phoneNumberTextField];
    _passwordTextField = [UITextField muv_textFieldUpdate:_passwordTextField];
    _confirmPasswordTextField = [UITextField muv_textFieldUpdate:_confirmPasswordTextField];
    
    _joinButtonTappedView = [UIView muv_viewUpdate:_joinButtonTappedView];
    
}

- (void)viewDidUnload
{
    [self setScrollView:nil];
    [super viewDidUnload];
}

- (void)textFieldValueChanged {
    if (_faceBook){
        if (!_firstNameTextField.text.muv_isEmpty &&
            !_lastNameTextField.text.muv_isEmpty &&
            _emailTextField.text.muv_isValidEmailAddress &&
            _phoneNumberTextField.text.muv_isValidPhoneNumber){
            _completed = YES;
        }else {
            _completed = NO;
        }
        
    }else {
        if(!_firstNameTextField.text.muv_isEmpty &&
           !_lastNameTextField.text.muv_isEmpty &&
           _emailTextField.text.muv_isValidEmailAddress &&
           _phoneNumberTextField.text.muv_isValidPhoneNumber &&
           !_passwordTextField.text.muv_isEmpty &&
           !_confirmPasswordTextField.text.muv_isEmpty){
            _completed = YES;
        } else {
            _completed = NO;
        }
    }
}

- (IBAction)joinButtonTapped:(id)sender {
    if (_completed){
        
        if (_faceBook){
            
            PFUser *user = [PFUser currentUser];
            
            UserProfileInformation *userInfo = [UserProfileInformation sharedMySingleton];
            
            userInfo.user = user;
            [user fetchIfNeeded];
            
            [self setupUserDetails:user];
            
        } else {
            if ([_passwordTextField.text isEqualToString:_confirmPasswordTextField.text]){
                
                PFUser *user = [PFUser user];
                user.username = [_emailTextField.text lowercaseString];
                user.email = _emailTextField.text;
                user.password = _passwordTextField.text;
                
                [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
                    if (!error){
                        
                        _userInfo.user = user;
                        // do installation for push notificiations
                        [self setupUserDetails:user];
                        
                    } else {
                        NSString *errorString = [error userInfo][@"error"];
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message"
                                                                        message:errorString
                                                                       delegate:self
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                        [alert show];
                    }
                }];
                
                
            }else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message"
                                                                message:@"Passwords do not match."
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }
            
        }
        
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message"
                                                        message:@"Invalid or missing fields."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    
}


- (void)setupUserDetails:(PFUser *)user {
    _userInfo.user[@"firstName"] = _firstNameTextField.text;
    _userInfo.user[@"lastName"] = _lastNameTextField.text;
    _userInfo.user[@"email"] = _emailTextField.text;
    _userInfo.user[@"phone"] = _phoneNumberTextField.text;
    _userInfo.user[@"active"] = [NSNumber numberWithBool:YES];
    _userInfo.user[@"driver"] = [NSNumber numberWithBool:NO];
    NSData *imageData = UIImageJPEGRepresentation([UIImage imageNamed:NOIMAGE_AVATAR], 0.7f);
    PFFile *profileImage = [PFFile fileWithName:@"photo.jpg" data:imageData];
    
    _userInfo.user[@"profileImage"] = profileImage;

    [_userInfo.user saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (succeeded) {
            [self createCouponForUserOnSignup];
        }
    }];
    [self.view addSubview:_transparentBackgroundView];
    [self.view addSubview:_joinButtonTappedView];
    
    for (UIView * txt in self.view.subviews){
        if ([txt isKindOfClass:[UITextField class]] && [txt isFirstResponder]) {
            [txt resignFirstResponder];
        }
    }
}

- (void)createCouponForUserOnSignup {
    CouponManager *couponManager = [CouponManager new];
    AdminManager *adminManager = [AdminManager sharedInstance];
    if (adminManager.adminSettings.userPromoAmount) {
        [couponManager createCouponCodeRecordforReferralForAmount:adminManager.adminSettings.userPromoAmount];
    }
    else {
        [adminManager getAdminSettingsWithCompletionBlock:^(AdminSettings *adminSetting, NSError *error) {
            if (adminSetting) {
                [couponManager createCouponCodeRecordforReferralForAmount:adminManager.adminSettings.userPromoAmount];
            }
            else {
                // [couponManager createCouponCodeRecordforReferralForAmount:amount];
            }
        }];
    }
}

- (IBAction)termsAndConditionButtonTapped:(id)sender {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self performSegueWithIdentifier:@"termsAndConditionSegue" sender:sender];
    });
}

- (IBAction)editProfileButtonTapped:(id)sender {
    dispatch_async(dispatch_get_main_queue(),^{
        [self performSegueWithIdentifier:@"profileSetupSegue" sender:sender];
        
    });
}

- (IBAction)bookAMuvButtonTapped:(id)sender {
    dispatch_async(dispatch_get_main_queue(),^{
        MuVSidePanelController *sidePanelViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MuVSidePanelViewController"];
        sidePanelViewController.centerPanel = [self.storyboard instantiateViewControllerWithIdentifier:@"BookingNav"];
        [self presentViewController:sidePanelViewController animated:YES completion:nil];
    });
}

- (IBAction)facebookButtonTapped:(id)sender {
//  //  [PFFacebookUtils initializeFacebook];
//    // Set permissions required from the facebook user account
//    NSArray *permissionsArray = @[ @"user_about_me", @"user_relationships", @"user_birthday", @"user_location"];
//    
//    // Login PFUser using Facebook
//    [PFFacebookUtils logInWithPermissions:permissionsArray block:^(PFUser *user, NSError *error) {
//        [_activityIndicator stopAnimating]; // Hide loading indicator
//        
//        if (!user) {
//            NSString *errorMessage = nil;
//            if (!error) {
//                NSLog(@"Uh oh. The user cancelled the Facebook login.");
//                errorMessage = @"Uh oh. The user cancelled the Facebook login.";
//            } else {
//                NSLog(@"Uh oh. An error occurred: %@", error);
//                errorMessage = [error localizedDescription];
//            }
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Log In Error"
//                                                            message:errorMessage
//                                                           delegate:nil
//                                                  cancelButtonTitle:nil
//                                                  otherButtonTitles:@"Dismiss", nil];
//            [alert show];
//        } else {
//            if (user.isNew) {
//                NSLog(@"User with facebook signed up and logged in!");
//                [self _loadData];
//            } else {
//                NSLog(@"User with facebook logged in!");
//                [self _loadData];
//            }
//        }
//    }];
//    
//    [_activityIndicator startAnimating]; // Show loading indicator until login is finished
}

#pragma mark - UITextFieldDelegate Methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == _firstNameTextField) {
        [textField resignFirstResponder];
        [_lastNameTextField becomeFirstResponder];
        
        return NO;
    } else if (textField == _lastNameTextField) {
        [textField resignFirstResponder];
        [_emailTextField becomeFirstResponder];
        
        return NO;
    } else if (textField == _emailTextField) {
        [textField resignFirstResponder];
        [_phoneNumberTextField becomeFirstResponder];
        
        return NO;
    }else if (textField == _emailTextField) {
        [textField resignFirstResponder];
        [_phoneNumberTextField becomeFirstResponder];
        
        return NO;
    }else if (textField == _phoneNumberTextField) {
        [textField resignFirstResponder];
        [_passwordTextField becomeFirstResponder];
        
        return NO;
    }else if (textField == _passwordTextField) {
        [textField resignFirstResponder];
        [_confirmPasswordTextField becomeFirstResponder];
        
        return NO;
    }else if (textField == _confirmPasswordTextField) {
        [textField resignFirstResponder];
        [self joinButtonTapped:self];
        
        return NO;
    }
    
    return YES;
}

- (void)_loadData {
//    // ...
//    FBRequest *request = [FBRequest requestForMe];
//    [request startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
//        // handle response
//        
//        _faceBook = YES;
//        
//        _firstNameTextField.text = result[@"first_name"];
//        _lastNameTextField.text = result[@"last_name"];
//        _emailTextField.text =  result[@"email"];
//        
//        [_passwordTextField setHidden:YES];
//        [_confirmPasswordTextField setHidden:YES];
//    }];
}


@end
