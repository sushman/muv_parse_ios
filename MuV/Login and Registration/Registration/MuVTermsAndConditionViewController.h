//
//  MuVTermsAndConditionViewController.h
//  MuV
//
//  Created by Hing Huynh on 1/7/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MuVTermsAndConditionViewController : UIViewController <UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UITextView *termsConditionTextView;

@end
