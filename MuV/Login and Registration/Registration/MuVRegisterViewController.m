//
//  MuVRegisterViewController.m
//  MuV
//
//  Created by Hing Huynh on 1/7/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "MuVRegisterViewController.h"
#import <Parse/Parse.h>
//#import <ParseFacebookUtils/PFFacebookUtils.h>
#import "MuVSidePanelController.h"
#import "UIButton+MuVUIButtonAdditions.h"
#import "UserProfileInformation.h"
#import "CouponManager.h"
#import "MailManager.h"
#import "AdminManager.h"

@interface MuVRegisterViewController ()

@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIButton *createAccountButton;
@property (strong, nonatomic) UserProfileInformation *userInfo;
@end

@implementation MuVRegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (![[NSUserDefaults standardUserDefaults]boolForKey:@"doneTutorial"]){
        [self performSegueWithIdentifier:@"tutorialSegue" sender:self];
    }
    
    _loginButton.hidden = YES;
    _createAccountButton.hidden = YES;
    _userInfo = [UserProfileInformation sharedMySingleton];
    
    PFUser *user = [PFUser currentUser];
    NSLog(@"user: %@", user.username);
    AdminManager *adminManager = [AdminManager sharedInstance];
    if (!adminManager.adminSettings.numberOfRecentLocations) {
        [adminManager getAdminSettingsWithCompletionBlock:^(AdminSettings *adminSetting, NSError *error) {
            NSLog(@"number of recent locations %@", adminManager.adminSettings.hoursBeforeArrivalForFee);
        }];
    }
    
    if (user) {
        
        PFQuery *query = [PFUser query];
        [query whereKey:@"objectId" equalTo:user.objectId];
        [query includeKey:@"driverdetail"];
        [query includeKey:@"defaultToken"];
        [query includeKey:@"paymentTokens"];
        [query includeKey:@"driverdetail.merchantAccount"];
        
        [query getFirstObjectInBackgroundWithBlock:^(PFObject *currentUser, NSError *error){
            _userInfo.user = user;
            if (![_userInfo.user[@"driver"]boolValue]){
                [[NSUserDefaults standardUserDefaults]setBool:YES forKey :@"riderMode"];
            }
           
            if ([CouponManager sharedInstance].couponName.length <= 0) {
                CouponManager *couponManager = [CouponManager new];
                [couponManager getMyCouponCodeForReferringFriendsWithCompletionBlock:^(NSString *couponCode, NSError *error) {
                    [CouponManager sharedInstance].couponName = couponCode;
                }];
            }
            
            
            _userInfo.driverDetails = user[@"driverdetail"];

            if (_userInfo.user[@"active"] == [NSNumber numberWithBool:YES]){
                dispatch_async(dispatch_get_main_queue(),^{
                    MuVSidePanelController *sidePanelViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MuVSidePanelViewController"];
                    [self presentViewController:sidePanelViewController animated:YES completion:nil];
                });
            } else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message"
                                                                message:@"Sorry, but your account has been disabled."
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];

            }
            
            if (_userInfo.driverDetails[@"active"] == [NSNumber numberWithBool:NO]) {
                [[NSUserDefaults standardUserDefaults]setBool:YES forKey :@"riderMode"];
            }
            
            if (error.code == -1005) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning"
                                                                message:@"Please check your network connection."
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];

            }
            if (error.code == -1009) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning"
                                                                message:@"Internet connection seems to be offline."
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
                
            }
            
        }];
        
    }
    else {
        _loginButton.hidden = NO;
        _createAccountButton.hidden = NO;

    }
    
    _loginButton = [UIButton muv_buttonUpdate:_loginButton];
    _createAccountButton = [UIButton muv_buttonUpdate:_createAccountButton];
    
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBarHidden = YES;
}

-(void)viewWillDisappear:(BOOL)animated{
    self.navigationController.navigationBarHidden = NO;
}

- (IBAction)loginButtonTapped:(id)sender {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self performSegueWithIdentifier:@"loginSegue" sender:sender];
    });
}


- (IBAction)createAccountButtonTapped:(id)sender {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self performSegueWithIdentifier:@"createAccountSegue" sender:sender];
    });
}


@end
