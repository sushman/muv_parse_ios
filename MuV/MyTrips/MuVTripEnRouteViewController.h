//
//  MuVTripEnRouteViewController.h
//  MuV
//
//  Created by SushmaN on 1/21/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoogleMapView.h"
#import "Ride.h"
#import "Trip.h"
#import "LocationTracker.h"
//#import 

@interface MuVTripEnRouteViewController : UIViewController <NSURLConnectionDelegate, GMSMapViewDelegate, CurrentLocationProtocol > {
    NSMutableData* _receivedData;
}

@property (nonatomic, strong) Ride *ride;
@property (nonatomic, strong) Trip *riderTrip;



@end
