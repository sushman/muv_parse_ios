//
//  MuVTripCalendarViewController.m
//  MuV
//
//  Created by SushmaN on 1/28/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "MuVTripCalendarViewController.h"
#import "MuVPassengerDetailsViewController.h"
#import "MuVDriverViewController.h"
#import "MuVTripEnRouteViewController.h"
#import "JTCalendar.h"
#import "RideManager.h"
#import "Helper.h"
#import "TripManager.h"
#import "ControlVariables.h"
#import "AgendaViewOfRides.h"
#import "UIImage+ResizeMagick.h"
#import "UIButton+MuVUIButtonAdditions.h"
#import "JASidePanelController.h"
#import "MuVSidePanelController.h"
#import "UIImageView+WebCache.h"
#import "UIImageView+MuVUIImageViewAdditions.h"
#import "RatingManager.h"
#import "EventKitManager.h"
#import "MailManager.h"
#import "AdminManager.h"
#import "PaymentManager.h"


@interface MuVTripCalendarViewController()
@property (strong, nonatomic) JTCalendar *calendar;
@property (weak, nonatomic) IBOutlet JTCalendarMenuView *calendarMenuView;
@property (weak, nonatomic) IBOutlet JTCalendarContentView *calendarContentView;
@property (weak, nonatomic) IBOutlet AgendaViewOfRides *tripsTable;
@property (weak, nonatomic) IBOutlet UIButton *scheduleMuVButton;
@property (weak, nonatomic) IBOutlet UIView *scheduleBookView;
@property (weak, nonatomic) IBOutlet UIButton *scheduleBookMuvBtn;

@property (strong, nonatomic) IBOutlet UIView *cancelPopUpView;
@property (strong, nonatomic) IBOutlet UIView *cancelPopUpBackgroundView;

@property (strong, nonatomic) IBOutlet UIView *cantCancelMuvBackground;
@property (strong, nonatomic) IBOutlet UIView *cantCancelMuvPopup;

@property (weak, nonatomic) IBOutlet UIView *ratingPopUpView;
@property (weak, nonatomic) IBOutlet UIView *ratingContainer;
@property (weak, nonatomic) IBOutlet UIImageView *ratingDriverImageView;
@property (weak, nonatomic) IBOutlet UILabel *ratingDriverNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *starOne;
@property (weak, nonatomic) IBOutlet UIButton *starTwo;
@property (weak, nonatomic) IBOutlet UIButton *starThree;
@property (weak, nonatomic) IBOutlet UIButton *starFour;
@property (weak, nonatomic) IBOutlet UIButton *starFive;
@property (weak, nonatomic) IBOutlet UITextField *reviewTextField;
@property (weak, nonatomic) IBOutlet UIButton *rateButton;

@property (strong, nonatomic) NSDate *currentDate;
@property (strong, nonatomic) NSNumber *selectedRating;

@property (weak, nonatomic) IBOutlet UILabel *currentMonth;

//farePopUp
@property (strong, nonatomic) IBOutlet UIView *farePopUpView;
@property (weak, nonatomic) IBOutlet UILabel *driverNameForFare;
@property (weak, nonatomic) IBOutlet UIImageView *driverPicForFare;
@property (weak, nonatomic) IBOutlet UIButton *requestReceipt;
@property (weak, nonatomic) IBOutlet UIImageView *creditCardLabel;
@property (weak, nonatomic) IBOutlet UILabel *creditCardInfo;
@property (weak, nonatomic) IBOutlet UILabel *fare;


@property (weak, nonatomic) IBOutlet UILabel *cancellationMessage;

@property (strong, nonatomic) NSNumber *cancelType;

@end


@implementation MuVTripCalendarViewController
RatingManager *ratingManager;
TripManager *tripManager;
Ride *ride;
Trip *trip;
PFObject *pfObject;
RideManager *rideManager;
UserDetail *userDetail;
MailManager *mailManager;
AdminManager *adminManager;
PaymentManager *paymentManager;
BOOL isDriver;
NSIndexPath *path;


NSArray *arrRides;



- (void)viewDidLoad {
    [super viewDidLoad];
    
    _activityIndicatorView.hidden = YES;
    _currentDate = [self getDateLoaded];
    if (_currentDate == nil) {
        _currentDate = [NSDate date];
    }
    
    ratingManager = [RatingManager new];
    tripManager = [TripManager sharedManager];
    adminManager = [AdminManager sharedInstance];
    
    if (!adminManager.adminSettings.hoursBeforeDepartureForFee) {
        [adminManager getAdminSettingsWithCompletionBlock:^(AdminSettings *adminSetting, NSError *error) {
            
        }];
    }
    
    [self customiseButtons];
    
    [self checkDriverMode];
    
    _tripsTable.hidden = YES;
    [_tripsTable setDelegate:_tripsTable];
    _tripsTable.tableAlertDelegate = self;
    _reviewTextField.delegate = self;
    
    [self initCalendarControl];
    
    [self getTripsWithReload:NO];
    
}

- (void)dealloc {
    
    //[[NSNotificationCenter defaultCenter] removeObserver:self name:NSUSERDEFAULT_FOR_RIDERMODE object:nil];
    
}

- (void)viewDidAppear:(BOOL)animated {
    [self.calendar reloadData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(eventHandler:) name:NSUSERDEFAULT_FOR_RIDERMODE object:nil];
    
}

- (void)viewDidDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


-(void)eventHandler: (NSNotification *) notification {
    
    [self checkDriverMode];
    
    _tripsTable.hidden = YES;
    
    [self getTripsWithReload:YES];
    
}


- (void)checkDriverMode {
    isDriver = ![[NSUserDefaults standardUserDefaults] boolForKey:@"riderMode"];
    
    if (isDriver)
    {
        [_scheduleMuVButton setTitle:@"Schedule a MüV" forState:UIControlStateNormal];
        [_scheduleBookMuvBtn setTitle:@"SCHEDULE A MüV" forState:UIControlStateNormal];
    }
    else {
        [_scheduleMuVButton setTitle:@"Request a MüV" forState:UIControlStateNormal];
        [_scheduleBookMuvBtn setTitle:@"REQUEST A MüV" forState:UIControlStateNormal];
    }
}

- (void)customiseButtons {
    _scheduleMuVButton = [UIButton muv_buttonUpdate:_scheduleMuVButton];
    _ratingDriverImageView = [UIImageView muv_imageCircle:_ratingDriverImageView];
    _rateButton = [UIButton muv_buttonUpdate:_rateButton];
    _requestReceipt =  [UIButton muv_buttonUpdate:_requestReceipt];
}

- (void) getTripsWithReload:(BOOL)reload {
    _activityIndicatorView.hidden = NO;
    [_activityIndicator startAnimating];
    TripManager *tripManager = [TripManager sharedManager];
    
    if ([Helper isCurrentMonth:_currentDate]) {
        reload = YES;
    }
    
    if (![[[TripManager sharedManager] dateLoaded] isEqualToString:_currentMonth.text] || reload) {
        RideManager *rideManager = [RideManager new];
        if (isDriver) {
            [rideManager getTripsForDriver:Calendar forDate:_currentDate withCompletionBlock:^(NSDictionary *rides, NSArray *tripDates, NSError *error) {
                if (error == nil) {
                    tripManager.rides = rides;
                    tripManager.tripDates = tripDates;
                    
                    [self.calendar setMenuMonthsView:_calendarMenuView];
                    [self.calendar setContentView:_calendarContentView];
                    [self.calendar setDataSource:self];

                    [self.calendar reloadData];
                    [self.calendar reloadAppearance];
                    _tripsTable.hidden = YES;
                }
                
                [self showNetworkAlert:error];
                
                _activityIndicatorView.hidden = YES;
                [_activityIndicator stopAnimating];
            }];
        }
        else {
            [rideManager getRidesForRiderForTripView:Calendar forDate:_currentDate withCompletionBlock:^(NSDictionary *rides, NSArray *tripDates, NSError *error) {
                if (error == nil) {
                    tripManager.rides = rides;
                    tripManager.tripDates = tripDates;
                    
                    [self.calendar setMenuMonthsView:_calendarMenuView];
                    [self.calendar setContentView:_calendarContentView];
                    [self.calendar setDataSource:self];

                    [self.calendar reloadData];
                    [self.calendar reloadAppearance];
                     _tripsTable.hidden = YES;
                    
                }
                
                [self showNetworkAlert:error];
                
                _activityIndicatorView.hidden = YES;
                [_activityIndicator stopAnimating];
            }];
        }
    }
    else {
        [self setCurrentDate:_currentDate];
        [self.calendar reloadData];
        _activityIndicatorView.hidden = YES;
        [_activityIndicator stopAnimating];
    }
   
    [[TripManager sharedManager] setDateLoaded:_currentMonth.text];
}

-(void)showNetworkAlert:(NSError *)error {
    if (error.code == 100 || error.code == -1001 || error.code == -1005) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning"
                                                        message:@"Please check your network connection"
                                                       delegate:nil
                                              cancelButtonTitle:nil
                                              otherButtonTitles:@"Dismiss", nil];
        
        [alert show];
    }
}

- (NSDate *)getDateLoaded {
    NSDateFormatter *dateFormatterA;
    if(!dateFormatterA){
        dateFormatterA = [NSDateFormatter new];
        dateFormatterA.timeZone = [NSTimeZone localTimeZone];
        [dateFormatterA setDateFormat:@"M, yyyy"];
    }
    
    if ([[TripManager sharedManager] dateLoaded]) {
        return [dateFormatterA dateFromString:[[TripManager sharedManager] dateLoaded]];
    }
    else {
        return nil;
    }
}


-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

- (void) initCalendarControl {
    self.calendar = [JTCalendar new];
    
    [self setCurrentDate:_currentDate];
    // All modifications on calendarAppearance have to be done before setMenuMonthsView and setContentView
    // Or you will have to call reloadAppearance
    {
        self.calendar.currentDate = _currentDate;
        self.calendar.calendarAppearance.calendar.firstWeekday = 1; // Sunday == 1, Saturday == 7
        self.calendar.calendarAppearance.dayCircleRatio = 9. / 10.;
        self.calendar.calendarAppearance.ratioContentMenu = 1.;
        self.calendar.calendarAppearance.showEvents = YES;
    }
    
    
    [self.calendar setMenuMonthsView:_calendarMenuView];
    [self.calendar setContentView:_calendarContentView];
    [self.calendar setDataSource:self];
}

- (void)setCurrentDate:(NSDate *)currentDate {
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    calendar.timeZone =[NSTimeZone localTimeZone];
    NSDateComponents *comps = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth fromDate:currentDate];
    
    NSInteger monthIndex = comps.month;
    NSInteger year = comps.year;
    
    while(monthIndex <= 0){
        monthIndex += 12;
    }
    
    static NSDateFormatter *dateFormatter;
    if(!dateFormatter){
        dateFormatter = [NSDateFormatter new];
        dateFormatter.timeZone = [NSTimeZone localTimeZone];
    }
    
    _currentMonth.text = [NSString stringWithFormat:@"%@ %ld",[[dateFormatter standaloneMonthSymbols][monthIndex - 1] capitalizedString], (long)year];
    
}

- (void)loadNextPrevMonth:(BOOL)isNextMonth {
    _activityIndicatorView.hidden = NO;
    [_activityIndicator startAnimating];
    if (isDriver) {
        [rideManager getTripsForDriver:Calendar forDate:_currentDate withCompletionBlock:^(NSDictionary *rides, NSArray *tripDates, NSError *error) {
            if (error == nil) {
                tripManager.rides = rides;
                tripManager.tripDates = tripDates;
                if (isNextMonth) {
                    [self.calendar loadNextMonth];
                }
                else {
                     [self.calendar loadPreviousMonth];
                }
            }
            [self showNetworkAlert:error];
            _activityIndicatorView.hidden = YES;
            [_activityIndicator stopAnimating];
        }];
    }
    else {
        [rideManager getRidesForRiderForTripView:Calendar forDate:_currentDate withCompletionBlock:^(NSDictionary *rides, NSArray *tripDates, NSError *error) {
            if (error == nil) {
                tripManager.rides = rides;
                tripManager.tripDates = tripDates;
                if (isNextMonth) {
                    [self.calendar loadNextMonth];
                }
                else {
                    [self.calendar loadPreviousMonth];
                }
            }
            [self showNetworkAlert:error];
            _activityIndicatorView.hidden = YES;
            [_activityIndicator stopAnimating];
        }];
    }

}

- (IBAction)loadPrevMonth:(id)sender {
    NSCalendar *cal         = [NSCalendar currentCalendar];
    NSDateComponents *comps = [cal components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit
                                     fromDate:_currentDate];
    comps.month             -= 1;
    _currentDate          = [cal dateFromComponents:comps];
    [self setCurrentDate:_currentDate];
    [self loadNextPrevMonth:NO];
    [[TripManager sharedManager] setDateLoaded:_currentMonth.text];
}

- (IBAction)loadNextMonth:(id)sender {
    NSCalendar *cal         = [NSCalendar currentCalendar];
    NSDateComponents *comps = [cal components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit
                                     fromDate:_currentDate];
    comps.month             += 1;
    _currentDate          = [cal dateFromComponents:comps];
    
    [self setCurrentDate:_currentDate];
    
    [self loadNextPrevMonth:YES];
    [[TripManager sharedManager] setDateLoaded:_currentMonth.text];
    
}

- (IBAction)startMuv:(id)sender {
    [self loadTripDetailsPage];
    [self.tripsTable deselectRowAtIndexPath:path animated:NO];
}

- (IBAction)dontStartMuv:(id)sender {
    
    [_cancelPopUpBackgroundView removeFromSuperview];
    [_cantCancelMuvPopup removeFromSuperview];
    [self.tripsTable deselectRowAtIndexPath:path animated:NO];
}


- (IBAction)removeRatingPopup:(id)sender {
    [_cantCancelMuvBackground removeFromSuperview];
    [_ratingPopUpView removeFromSuperview];
}


- (IBAction)removeFarePopup:(id)sender {
    [_cantCancelMuvBackground removeFromSuperview];
    [_farePopUpView removeFromSuperview];
}

// No Events - Return 0
// Start End Scheduled - Return 1
// Start Scheduled End Requested - Return 2
// Start Requested End Scheduled - Return 3
// Start End Requested - Return 4
- (NSInteger)calendarHaveEvent:(JTCalendar *)calendar date:(NSDate *)date {
    bool isArrivalScheduled = NO;
    bool isDepartureScheduled = NO;
    bool isArrivalRequested = NO;
    bool isDepartureRequested = NO;
    
    if (![tripManager.rides count] > 0) {
        return 0;
    }
    else {
        
        NSArray *arrayOfTrips = [tripManager.rides objectForKey:[Helper getStringFromDate:date]];
        
        if (arrayOfTrips.count > 0) {
        
            for (Ride *ride in arrayOfTrips) {
                if ([ride.routeType isEqualToString:ARRIVAL]) {
                    if (ride.isRequested) {
                        isArrivalRequested = YES;
                    }
                    else {
                        isArrivalScheduled = YES;
                    }
                }
                if ([ride.routeType isEqualToString:DEPARTURE]) {
                    if (ride.isRequested) {
                        isDepartureRequested = YES;
                    }
                    else {
                        isDepartureScheduled = YES;
                    }
                }
            }
            if (isArrivalScheduled && isDepartureScheduled) return Arrival_Departure_Scheduled;
            if (isArrivalScheduled && isDepartureRequested) return ArrivalScheduled_DepartureRequested;
            if (isArrivalRequested && isDepartureScheduled) return ArrivalRequested_DepartureScheduled;
            if (isArrivalRequested && isDepartureRequested) return Arrival_Departure_Requested;
            
            if (isArrivalScheduled) return ArrivalScheduled;
            if (isDepartureScheduled) return DepartureScheduled;
            
            if (isArrivalRequested) return ArrivalRequested;
            if (isDepartureRequested) return DepartureRequested;
        }
        return NoEvent;
    }
}

- (void)calendarDidDateSelected:(JTCalendar *)calendar date:(NSDate *)date
{
    NSString *dateInString = [Helper getStringFromDate:date];
    NSArray *arrayOfTrips = [tripManager.rides objectForKey:dateInString];
    if (arrayOfTrips.count > 0) {
        _tripsTable.inputArray = arrayOfTrips;
        _tripsTable.sectionTitle = dateInString;
        _tripsTable.hidden = NO;
        _scheduleBookView.hidden = YES;
        [_tripsTable reloadData];
    }
    else {
        _scheduleBookView.hidden = NO;
        _tripsTable.hidden = YES;
    }
}

- (IBAction)showAgendaView:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MuVSidePanelController *sidePanelViewController = [storyboard instantiateViewControllerWithIdentifier:@"MuVSidePanelViewController"];
    sidePanelViewController.centerPanel = [self.storyboard instantiateViewControllerWithIdentifier:@"Tripsboard"];
    [self presentViewController:sidePanelViewController animated:YES completion:nil];
    
}


- (IBAction)scheduleMUV:(id)sender {
    [[TripManager sharedManager] setEditedRide:nil];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MuVSidePanelController *sidePanelViewController = [storyboard instantiateViewControllerWithIdentifier:@"MuVSidePanelViewController"];
    sidePanelViewController.centerPanel = [storyboard instantiateViewControllerWithIdentifier:@"BookingNav"];
    [self presentViewController:sidePanelViewController animated:YES completion:nil];
}

//Agenda Table View Delegates
- (void)getPassengerDetail:(UserDetail *)userdetail andSender:(id)sender {
    
    if (userdetail != nil) {
        userDetail = userdetail;
        [self performSegueWithIdentifier:@"PassengerDetail" sender:sender];
    }
}

- (void)goToTripDetails:(Trip *)tripA andSender:(id)sender {
    
    if (tripA != nil) {
        trip = tripA;
        if (isDriver) {
            trip.isCurrent = NO;
        }
        
        [self loadTripDetailsPage];
    }
}

- (void)loadTripDetailsPage {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MuVSidePanelController *sidePanelViewController = [storyboard instantiateViewControllerWithIdentifier:@"MuVSidePanelViewController"];
    sidePanelViewController.centerPanel = [self.storyboard instantiateViewControllerWithIdentifier:@"TripDetailNav"];
    [[TripManager sharedManager] setCurrentTrip:trip];
    [[TripManager sharedManager] setCurrentTripDriver:trip];
    [self presentViewController:sidePanelViewController animated:YES completion:nil];
}


- (void)startMuv:(Trip *)tripA forIndexPath:indexPath andSender:(id)sender {
    
    if (tripA != nil) {
        trip = tripA;
        path = indexPath;
        [self.view addSubview:_cancelPopUpBackgroundView];
        [self.view addSubview:_cantCancelMuvPopup];
    }
}

- (void)getDriverDetail:(Ride *)rideA andSender:(id)sender {
    
    if (rideA != nil) {
        ride = rideA;
        if (ride.driverDetail) {
            [self performSegueWithIdentifier:@"DriverDet" sender:sender];
        }
    }
}

- (void) editTrip:(Ride *)ride andSender:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MuVSidePanelController *sidePanelViewController = [storyboard instantiateViewControllerWithIdentifier:@"MuVSidePanelViewController"];
    sidePanelViewController.centerPanel = [storyboard instantiateViewControllerWithIdentifier:@"BookingNav"];
    [self presentViewController:sidePanelViewController animated:YES completion:nil];
    
    [[TripManager sharedManager] setEditedRide:ride];
}

- (void)cancelMuVForPassenger:(Ride *)rideToCancel andSender:(id)sender {
    
    if (rideToCancel.rideNo[@"rideNo"]) {
        if (rideToCancel != nil) {
            BOOL isArrival = YES;
            ride = rideToCancel;
            
            
            if ([rideToCancel.routeType isEqualToString:ARRIVAL]) {
                isArrival = YES;
            }
            else {
                isArrival = NO;
            }
            
            NSString *cancellationMessage;
            NSInteger cancellationTime = [self checkTimeDifferenceBetweenDates:rideToCancel.rideDate andRouteType:isArrival];
            
            if (cancellationTime == MoreThan24Hours) {
                cancellationMessage = @"Are you sure you want to cancel your MüV? \n You are canceling more than 24 hrs before the ride, No cancellation fees will apply.";
                _cancelType = @1;
            }
            else if (cancellationTime == Between4And24Hours) {
                cancellationMessage = @"Are you sure you want to cancel your MüV? \n You are canceling less than 24 hrs before the ride. \n A cancellation fee of $5 will apply.";
                _cancelType = @2;
            }
            else  {
                cancellationMessage = @"Are you sure you want to cancel your MüV? \n You are canceling less than 4 hrs before the ride. \n Full Fare Charges will apply.";
                _cancelType = @3;
            }
            _cancellationMessage.text = cancellationMessage;
            
        }
    }
    else {
        ride = rideToCancel;
        NSString *cancellationMessage;
        cancellationMessage = @"Are you sure you want to cancel your MüV?";
        _cancellationMessage.text = cancellationMessage;
    }
    [self addCancelPopUpViewforDriver];
}


-(void)cancelMuvDriver:(Trip *)tripA forIndexPath:(NSIndexPath *)indexPath andSender:(id)sender {
    if (tripA != nil) {
        path = indexPath;
        BOOL isArrival;
        //has passengers
        NSString *cancellationMessage;
        
        if (tripA.userDetails.count > 0) {
            
            if ([tripA.route[@"routeType"] isEqualToString:ARRIVAL]) {
                isArrival = YES;
            }
            else {
                isArrival = NO;
            }
            
            NSInteger cancellationTime = [self checkTimeDifferenceBetweenDates:tripA.route[@"rideDate"] andRouteType:isArrival];
            
            if (cancellationTime == MoreThan24Hours) {
                cancellationMessage = @"Are you sure you want to cancel your MüV? \n You are canceling more than 24 hrs before the ride. \n No cancellation fees will apply.";
                _cancelType = @1;
            }
            else {
                cancellationMessage = @"Are you sure you want to cancel your MüV? \n You are canceling less than 24 hrs before the ride. \n A cancellation fee of $5/ passenger will apply.";
                _cancelType = @2;
            }
        }
        //no passengers
        else {
            cancellationMessage = @"Are you sure you want to cancel your MüV? \n You don't have any passengers booked for this ride. \n No cancellation fees will apply.";
            _cancelType = @1;
        }
        trip = tripA;
        _cancellationMessage.text = cancellationMessage;
        [self addCancelPopUpViewforDriver];
    }
}


- (NSInteger)checkTimeDifferenceBetweenDates:(NSDate *)rideDate andRouteType:(BOOL)isArrival {
    NSDate *currentDate = [NSDate date];
    
    NSDateComponents *components;
    NSInteger days;
    NSInteger hours;
    
    components = [[NSCalendar currentCalendar] components: NSDayCalendarUnit | NSHourCalendarUnit
                                                 fromDate: currentDate toDate: rideDate options: 0];
    
    days = [components day];
    hours = [components hour];
    NSInteger hoursBefore = 6;
    
    if (adminManager.adminSettings.hoursBeforeArrivalForFee) {
        if (isArrival) {
            hoursBefore = [adminManager.adminSettings.hoursBeforeArrivalForFee intValue];
        }
        else {
            hoursBefore = [adminManager.adminSettings.hoursBeforeDepartureForFee intValue];
        }
    }
    if (days >= 1) {
        return MoreThan24Hours;
    }
    else {
        if (hours > hoursBefore) {
            return Between4And24Hours;
        }
        else {
            return LessThan4Hours;
        }
    }
}



- (void)showFareView:(Ride *)rideObject andSender:(id)sender {
    
    ride = rideObject;
    
    PFFile *driverPhoto = ride.driverDetail[@"userdetail" ][@"profileImage"];
    [_driverPicForFare sd_setImageWithURL:[NSURL URLWithString:driverPhoto.url]];
    NSString *lastInitial = [[ride.driverDetail[@"userdetail"][@"lastName"] substringToIndex:1]uppercaseString];
    _driverNameForFare.text = [NSString stringWithFormat:@"%@ %@.", ride.driverDetail[@"userdetail"][@"firstName"],lastInitial];
    
   // NSLog(@"Ride .rideNo %@", ride.rideNo);
    if (ride.rideNo[@"payment"]) {
        PFObject *payment = ride.rideNo[@"payment"];
     //   NSLog(@"payment Object %@", payment);
        
        if (payment[@"paymentDetail"]) {
            _creditCardInfo.text = [NSString stringWithFormat:@"Credit Card xxxx %@", ride.rideNo[@"payment"][@"paymentDetail"][@"lastFourDigits"]];
            _creditCardLabel.image = [UIImage imageNamed:ride.rideNo[@"payment"][@"paymentDetail"][@"cardType"]];
            float amountCharged = [payment[@"AmountCharged"] floatValue];
            _fare.text = [NSString stringWithFormat:@"$ %0.2f", amountCharged];
        }
        else {
            _creditCardInfo.hidden = YES;
            _creditCardLabel.hidden = YES;
            _fare.hidden = YES;
        }
    }
    
    [self.view addSubview:_cantCancelMuvBackground];
    [self.view addSubview:_farePopUpView];
}

- (IBAction)requestReceipt:(id)sender {
    // SEND EMAIL TO RIDER OF HIS RECEIPT
    [mailManager sendRiderRequestsReceiptWithRide:ride.rideNo];
    [_cantCancelMuvBackground removeFromSuperview];
    [_farePopUpView removeFromSuperview];
    
}


- (IBAction)ratingViewButtonTapped:(id)sender {
    switch ([sender tag]) {
        case RateButton1:
            
            if (_selectedRating) {
                [ratingManager createRatingWithReview:_reviewTextField.text andRating:_selectedRating ForRide:ride];
            }
            
            [[TripManager sharedManager] setDateLoaded:nil];
            
            [self getTripsWithReload:NO];
            [_cantCancelMuvBackground removeFromSuperview];
            [_ratingPopUpView removeFromSuperview];
            break;
        case starOne1:
            [self updateStarsWithRating:@1];
            break;
        case starTwo1:
            [self updateStarsWithRating:@2];
            break;
        case starThree1:
            [self updateStarsWithRating:@3];
            break;
        case starFour1:
            [self updateStarsWithRating:@4];
            break;
        case starFive1:
            [self updateStarsWithRating:@5];
            break;
        default:
            break;
    }
    
}

- (void)goToRatingView:(Ride *)rideObject andSender:(id)sender {
    
    ride = rideObject;
    
    PFFile *driverPhoto = ride.driverDetail[@"userdetail" ][@"profileImage"];
    [_ratingDriverImageView sd_setImageWithURL:[NSURL URLWithString:driverPhoto.url]];
    NSString *lastInitial = [[ride.driverDetail[@"userdetail"][@"lastName"] substringToIndex:1]uppercaseString];
    _ratingDriverNameLabel.text = [NSString stringWithFormat:@"%@ %@.", ride.driverDetail[@"userdetail"][@"firstName"],lastInitial];
    
    [self.view addSubview:_cantCancelMuvBackground];
    [self.view addSubview:_ratingPopUpView];
    
}

- (void) updateStarsWithRating:(NSNumber *)rating{
    NSLog(@"%@", rating);
    _selectedRating = rating;
    
    [_starOne setBackgroundImage:[UIImage imageNamed:@"MuV_RateStar_Gry"] forState:UIControlStateNormal];
    [_starTwo setBackgroundImage:[UIImage imageNamed:@"MuV_RateStar_Gry"] forState:UIControlStateNormal];
    [_starThree setBackgroundImage:[UIImage imageNamed:@"MuV_RateStar_Gry"] forState:UIControlStateNormal];
    [_starFour setBackgroundImage:[UIImage imageNamed:@"MuV_RateStar_Gry"] forState:UIControlStateNormal];
    [_starFive setBackgroundImage:[UIImage imageNamed:@"MuV_RateStar_Gry"] forState:UIControlStateNormal];
    
    if ([rating floatValue] > 0){
        [_starOne setBackgroundImage:[UIImage imageNamed:@"MuV_RateStar_Blu"] forState:UIControlStateNormal];
    }
    if ([rating floatValue] > 1) {
        [_starTwo setBackgroundImage:[UIImage imageNamed:@"MuV_RateStar_Blu"] forState:UIControlStateNormal];
    }
    if ([rating floatValue] > 2) {
        [_starThree setBackgroundImage:[UIImage imageNamed:@"MuV_RateStar_Blu"] forState:UIControlStateNormal];
    }
    if ([rating floatValue] > 3) {
        [_starFour setBackgroundImage:[UIImage imageNamed:@"MuV_RateStar_Blu"] forState:UIControlStateNormal];
    }
    if ([rating floatValue] > 4) {
        [_starFive setBackgroundImage:[UIImage imageNamed:@"MuV_RateStar_Blu"] forState:UIControlStateNormal];
    }
}



- (IBAction)contactMuvSupport:(id)sender {
    [self removePopUpViewforDriver];
}

- (IBAction)cancelRide:(id)sender {
    
    mailManager = [MailManager new];
    
    if (isDriver) {
        //mark drivers ride as cancelled
        trip.route[@"isCancelled"] = [NSNumber numberWithBool:YES];
        
         //SMN test
        trip.route[@"cancelTime"] = [NSDate date];
        [trip.route setObject:[NSNull null] forKey:@"calendarEventId"];
        
        //mark his passengers ride as cancelled
        [rideManager updatePFObject:trip.route withCompletionBlock:^(BOOL succeeded, NSError *error) {
            if (succeeded) {
                for (UserDetail *userDetail in trip.userDetails) {
                    userDetail.rideObj[@"isCancelledByDriver"] = [NSNumber numberWithBool:YES];
                    [userDetail.rideObj setObject:[NSNull null] forKey:@"calendarEventId"];
                    [rideManager updateRideObject:userDetail.rideObj];
                }
                
                // SEND EMAIL RIDER NOTIFICATION OF DRIVER'S CANCELATION
                [mailManager sendRiderNotificationWhenDriverCancelsWithRoute:trip.route];
                
                // SEND EMAIL TO ADMIN OF DRIVER'S CANCELATION
                [mailManager sendDriverCancelsAdminAlertWithRoute:trip.route];
                
                // SEND EMAIL TO DRIVER OF CANELATION DEPENDING ON SCENARIO
                if ([_cancelType  isEqual: @1]){
                    [mailManager sendRideCancelingByDriver1WithRoute:trip.route];
                } else {
                    [mailManager sendRideCancelingByDriver2WithRoute:trip.route];
                }
            }
            
          //  [[TripManager sharedManager] setDateFormat:nil];
            [self getTripsWithReload:NO];
        }];
    }
    else {
        if (ride.rideNo[@"rideNo"]) {
          //  isRoundTripEmailSent = NO;
            
            if ([_cancelType isEqual:@1]) {
                [mailManager sendRiderCancelsMuVGetsRefundWithRide:ride.rideNo];
                
            } else if ([_cancelType isEqual:@2]) {
                [mailManager sendRiderCancelsMuVGetsChargedFiveWithRide:ride.rideNo];
                [paymentManager createCancellationChargeWithRide:ride.rideNo withCompletionBlock:^(BOOL succeeded, NSError *error){
                    NSLog(@"done: %@", error);
                }];
            } else {
                NSLog(@"3 cancel");
                [mailManager sendRiderCancelsMuVDoesNotGetRefundWithRide:ride.rideNo];
                [paymentManager chargePaymentWithRide:ride.rideNo withCompletionBlock:^(BOOL succeeded, NSError *error){
                    NSLog(@"done: %@", error);
                }];
            }
            
            // SEND EMAIL TO DRIVER OF RIDER'S CANCELATION
            [mailManager sendDriverNotificationForRiderCancelationWithRide:ride.rideNo];
            
            [self cancelTripAndCreditUser:ride.rideNo];
        }
        else {
            [self cancelRequestedTrip:ride.rideNo];
        }
        
    }

    
    
    [self removePopUpViewforDriver];
    
}

- (void)cancelRequestedTrip:(PFObject *)rideObj  {
    rideObj[@"cancelled"] = [NSNumber numberWithBool:YES];
    [rideManager cancelRide:rideObj withCompletionBlock:^(BOOL succeeded, NSError *error) {
        
    }];
    
    [[TripManager sharedManager] setDateLoaded:nil];
    [self getTripsWithReload:NO];
}


- (void)cancelTripAndCreditUser:(PFObject *)rideObj {
    
    rideObj[@"cancelled"] = [NSNumber numberWithBool:YES];
    NSString *calendarEvent = rideObj[@"calendarEventId"];
    [rideObj setObject:[NSNull null] forKey:@"calendarEventId"];
    
    NSDate *currentDate = [NSDate date];
    NSDate *rideDate = rideObj[@"rideDate"];
    
    NSDateComponents *components;
    NSInteger days;
    NSInteger hours;
    double fare;
    NSInteger hoursBefore = 6;
    
    components = [[NSCalendar currentCalendar] components: NSDayCalendarUnit | NSHourCalendarUnit
                                                 fromDate: currentDate toDate: rideDate options: 0];
    days = [components day];
    hours = [components hour];
    
    fare = [rideObj[@"fare"] floatValue];
    
    if (adminManager.adminSettings.hoursBeforeArrivalForFee) {
        if (rideObj[@"endTime"]) {
            hoursBefore = [adminManager.adminSettings.hoursBeforeArrivalForFee intValue];
        }
        else {
            hoursBefore = [adminManager.adminSettings.hoursBeforeDepartureForFee intValue];
        }
    }
    
    if (days >= 1) {
        //more than 24 hours. no charges to user
    }
    else {
        if (hours > hoursBefore) {
            //greater than 4 hours, less than 24 hours
            
        }
        else {
            //charge fare amount to user
        }
    }
    
    [rideManager cancelRide:rideObj withCompletionBlock:^(BOOL succeeded, NSError *error) {
        if (succeeded) {
            
            [rideObj[@"rideNo"] removeObject:rideObj forKey:@"rides"];
            //update Occupied Seat
            NSNumber *occupiedSeats = rideObj[@"rideNo"][@"occupiedSeats"];
            int value = [occupiedSeats intValue];
            occupiedSeats = [NSNumber numberWithInt:value - 1];
            rideObj[@"rideNo"][@"occupiedSeats"] = occupiedSeats;
            [rideManager updatePFObject:rideObj[@"rideNo"]];
            
            if (calendarEvent) {
                EventKitManager *eventKit = [EventKitManager new];
                [eventKit deleteEventWithIdentifier:calendarEvent];
            }
            
            //  [[TripManager sharedManager] setDateFormat:nil];
            [[TripManager sharedManager] setDateLoaded:nil];
            [self getTripsWithReload:NO];
        }
    }];
}



- (IBAction)dontCancelRide:(id)sender {
    [self removePopUpViewforDriver];
    [self.tripsTable deselectRowAtIndexPath:path animated:NO];
}


- (void)removePopUpViewforDriver {
    [_cancelPopUpBackgroundView removeFromSuperview];
    [_cancelPopUpView removeFromSuperview];
}

- (void)addCancelPopUpViewforDriver {
    
    [self.view addSubview:_cancelPopUpBackgroundView];
    [self.view addSubview:_cancelPopUpView];
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString: @"PassengerDetail"]) {
        MuVPassengerDetailsViewController *passDetails = [segue destinationViewController];
        passDetails.userDetail = userDetail;
        return;
    }
    if ([[segue identifier] isEqualToString: @"TripsDetail"]) {
        MuVTripEnRouteViewController *tripDetails = [segue destinationViewController];
        tripDetails.riderTrip = trip;
        
        return;
    }
    if ([[segue identifier] isEqualToString: @"DriverDet"]) {
        MuVDriverViewController *driverDetails = [segue destinationViewController];
        driverDetails.ride = ride;
        return;
    }
    
}


@end
