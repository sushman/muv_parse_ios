//
//  MuVTripEnRouteViewController.m
//  MuV
//
//  Created by SushmaN on 1/21/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "MuVTripEnRouteViewController.h"
#import "GoogleMapView.h"
#import "ControlVariables.h"
#import "RideManager.h"
#import "UIImage+ResizeMagick.h"
#import "UIImageView+WebCache.h"
#import "UIImage+WebP.h"
#import "UIImage+GIF.h"
#import "UIImage+MultiFormat.h"
#import "UserDetail.h"
#import "LocationTracker.h"
#import "RouteManager.h"
#import "UIButton+MuVUIButtonAdditions.h"
#import "UIImageView+MuVUIImageViewAdditions.h"
#import "UIImage+MuVImageAdditions.h"
#import <MapKit/MapKit.h>
#import "Helper.h"
#import "MuVSideMenuViewController.h"
#import "MuVSidePanelController.h"
#import "MuVPassengerDetailsViewController.h"
#import "MuVDriverViewController.h"
#import "MailManager.h"
#import "AdminManager.h"
#import "PaymentManager.h"
#import "TripManager.h"
#import "GoogleMapView.h"
#import "UIImageView+MuVUIImageViewAdditions.h"

#define END_MUV @"End MuV"

@interface MuVTripEnRouteViewController ()
@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (nonatomic, retain) CLLocationManager *locationManager;
@property (nonatomic, retain) GMSMarker *marker;
@property (nonatomic) BOOL draggingPing;
@property(nonatomic)  float zoomLevel;
@property (strong, nonatomic) IBOutlet UIView *riderProfileView;

@property (weak, nonatomic) IBOutlet UIView *riderStartMuvView;
@property (weak, nonatomic) IBOutlet UILabel *riderName;
@property (weak, nonatomic) IBOutlet UIImageView *riderProfilePicture;
@property (weak, nonatomic) IBOutlet UILabel *riderStartAddress;


@property (weak, nonatomic) IBOutlet UIButton *startNavBtn;
@property (weak, nonatomic) IBOutlet UIButton *noShowBtn;
@property (weak, nonatomic) IBOutlet UIButton *startMuvBtn;
@property (weak, nonatomic) IBOutlet UILabel *startLocationHidden;
@property (weak, nonatomic) IBOutlet UIButton *endNavButton;


@property (weak, nonatomic) IBOutlet UIView *driverContactView;
@property (weak, nonatomic) IBOutlet UIImageView *driverPic;
@property (weak, nonatomic) IBOutlet UIButton *driverEmail;
@property (weak, nonatomic) IBOutlet UILabel *driverName;
@property (weak, nonatomic) IBOutlet UILabel *driverArrivalTime;
@property (weak, nonatomic) IBOutlet UIButton *driverPhone;
@property (weak, nonatomic) IBOutlet UIButton *driverSMS;

@property (weak, nonatomic) IBOutlet UIButton *startNavigation;

@property (weak, nonatomic) IBOutlet UIButton *navigate;

@end

@implementation MuVTripEnRouteViewController

GMSCameraPosition *camera;
GMSMarker *marker;
GMSMarker *currentLocationMarker;
GMSPolyline *polyline;
GMSCircle *circ;

CLLocation *currentLocation;
CLLocationCoordinate2D nextAddress;

MailManager *mailManager;
PaymentManager *paymentManager;
RideManager *rideManager;
Trip *_trip;
TripManager *tripManager;
UserDetail *userDetail;
AdminManager *adminManager;


BOOL isDriver;

NSMutableArray *arrayOfLocations;
NSMutableDictionary *dictOfWayPointOrderAndStartOrEnd;
NSArray *wayPointOrder;
CLLocation *locNext;
NSTimer *timer;


enum UserActivityTypeForDriver {
    NoShow,
    PickedUp,
    DroppedOff
};


- (void)viewDidLoad {
    [super viewDidLoad];
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:37.7833
                                                            longitude:-122.4167
                                                                 zoom:5];
    _mapView.camera = camera;
    
    [self initializeVariables];
    
    [self hideButtonsOnLoad];
    
    [self.navigationItem setTitle:@"Trip Details"];
    
    if (tripManager.currentTrip != nil) {
        _trip = tripManager.currentTrip;
        
        if (isDriver &&  _trip.isCurrent) {
            //start tracking location
            [[LocationTracker shared] startLocationTracking];
        }
        [self getGoogleApiResponseForStartLocation:_trip.startLocation];
    }
}

- (void)loadDriversPassengersDetails {
    isDriver = ![[NSUserDefaults standardUserDefaults] boolForKey:@"riderMode"];
    if (isDriver) {
        if(_trip.isCurrent) {
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"tripStarted" object:nil];
            [RouteManager sharedInstance].currentRide = _trip.route;
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(eventHandler:) name:@"eventType" object:nil];
            [self.navigationItem setTitle:@"Trip Enroute"];
            if (!rideManager) {
                rideManager = [RideManager new];
            }
            [rideManager updateTripTimes:YES :_trip.route];
            
        }
        else {
            [[LocationTracker shared] stopLocationTracking];
        }
    }
    else if (!isDriver){
        if (_trip.isCurrent) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"tripStarted" object:nil];
            [[TripManager sharedManager] setCurrentTripRider:_trip];
            
            [self.navigationItem setTitle:@"Trip Enroute"];
            
            [self setUpDriversContactView];
            [rideManager getDriversCurrentLocationWithCompletionBlock:^(PFGeoPoint *geoPoint, NSError *error) {
                if (geoPoint != nil) {
                    NSLog(@"geopoint %f %f", geoPoint.latitude, geoPoint.longitude);
                    [self plotDriversCurrentLocation:geoPoint isFirstLoad:YES];
                }
                else {
                    
                }
            }];
        }
    }

}

- (void)initializeVariables {
    
    mailManager = [MailManager new];
    rideManager = [RideManager new];
    paymentManager = [PaymentManager new];
    currentLocationMarker = [[GMSMarker alloc] init];
    tripManager = [TripManager sharedManager];
    adminManager = [AdminManager sharedInstance];
    
    if (!adminManager.adminSettings.driverPaymentPercentage) {
        [adminManager getAdminSettingsWithCompletionBlock:^(AdminSettings *adminSetting, NSError *error) {
            //
        }];
    }
}

- (void)hideButtonsOnLoad {
    _driverContactView.hidden = YES;
    _startNavigation.hidden = YES;
    _navigate.hidden = YES;
}

- (void)setUpDriversContactView {
    
    _driverContactView.hidden = NO;
    PFObject *driver = _trip.driverDetails;
    if (driver) {
        PFFile *driverPicURL = driver[@"profileImage"];
        if (driverPicURL.url != nil) {
            [_driverPic sd_setImageWithURL:[NSURL URLWithString:driverPicURL.url] placeholderImage:[UIImage imageNamed:NOIMAGE_AVATAR]];
        }
        else {
            [_driverPic setImage:[UIImage imageNamed:NOIMAGE_AVATAR]];
        }
        _driverPic = [UIImageView muv_imageCircle:_driverPic];
        _driverName.text = driver[@"firstName"];
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"h:mm a"];
        
        _driverArrivalTime.text = [dateFormat stringFromDate:_trip.route[@"rideDate"]];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    
    if(timer) {
        [timer invalidate];
        timer = nil;
    }
}

- (void)addPopUpView {
    [self.view addSubview:_riderStartMuvView];
    [self.view addSubview:_riderProfileView];
}

- (void)beautifyButtons {
    _startNavBtn = [UIButton muv_buttonUpdate:_startNavBtn];
    _startNavigation = [UIButton muv_buttonUpdate:_startNavigation];
    _navigate = [UIButton muv_buttonUpdate:_startNavigation];
}

- (void)checkIfDriverIsArrivingLocation:(CLLocationCoordinate2D) currentCoord {
//     NSLog(@"Array is %@", arrayOfLocations);
    if (!arrayOfLocations) {
        return;
    }
    
    currentLocation = [[CLLocation alloc] initWithLatitude:currentCoord.latitude longitude:currentCoord.longitude];
    NSInteger nextInLine = [[NSUserDefaults standardUserDefaults] integerForKey:@"navAddr"];
    
    //at start location
    if(nextInLine == 0) {
        [[NSUserDefaults standardUserDefaults] setInteger:nextInLine + 1 forKey:@"navAddr"];
        _startNavigation.hidden = NO;
        return;
    }
    nextInLine = nextInLine + 1;
    BOOL isStart = YES;
    NSInteger userDetailNumber = 0;
    
   // NSLog(@"Way Point is %@", wayPointOrder);
    
    if (nextInLine - 2 < wayPointOrder.count) {
        //get next location from waypoint order
        NSInteger nextLocationPoint = [wayPointOrder[nextInLine - 2] intValue];
        NSInteger arrLoc = nextInLine - 1;
      //  NSLog(@"nextLocation Point %ld", (long)nextLocationPoint);
        //myLocationIndex = nextLocationPoint/2;

        NSString *dictValue = [dictOfWayPointOrderAndStartOrEnd objectForKey:[NSString stringWithFormat:@"%ld",nextLocationPoint]];
        
        NSString *startEnd = [self getValueFromCommaSeperatedString:dictValue forPosition:0];
        userDetailNumber = [[self getValueFromCommaSeperatedString:dictValue forPosition:1] integerValue];
        
        if ([startEnd isEqualToString:@"Start"]) {
            isStart = YES;
            locNext = arrayOfLocations[arrLoc];
        }
        else {
            isStart = NO;
            locNext = arrayOfLocations[arrLoc];
        }
    }
    else if (nextInLine == arrayOfLocations.count) {
        locNext = arrayOfLocations[arrayOfLocations.count - 1];
        [self prepareToEndDriversMuV];
    }
    
    CLLocation *locCurrentDestination = locNext;
    
    CLLocationDistance dist = [Helper getDistanceBetweenLocationA:locCurrentDestination andLocationB:currentLocation];
    
    if (dist < [adminManager.adminSettings.geoFenceInMiles floatValue]) {
        [self.navigationItem setTitle:@"Arriving"];
        
        if (nextInLine - 2 < wayPointOrder.count) {
            userDetail = _trip.userDetails[userDetailNumber];
            
            if (isStart) {
                if (!userDetail.isNextInLineForPickUp) {
                    _startNavigation.hidden = YES;
                    userDetail.isNextInLineForPickUp = YES;
                    [self prepareforDrivingToStartLocation];
                }
            }
            else {
                
                if (!userDetail.isNextInLineForDropOff) {
                    userDetail.isNextInLineForDropOff = YES;
                    [self prepareforDrivingToEndLocation];
                }
            }
        }
        //last leg of the ride
        else {
            if (nextInLine == arrayOfLocations.count) {
                [self prepareToEndDriversMuV];
            }
        }
    }
}


- (void)determineNextLocation {
    NSInteger nextInLine = [[NSUserDefaults standardUserDefaults] integerForKey:@"navAddr"];
    nextInLine = nextInLine + 1; //get the next location
    //if the next location is drivers end address
    if (arrayOfLocations.count -1 == nextInLine) {
        //navigate to drivers end location
        //show end muv for driver
        [self prepareToEndDriversMuV];
        [self updateStateForPassenger:nextInLine - 2];
        CLLocation *location = arrayOfLocations[nextInLine];
        [self loadAppleMap:location.coordinate];
    }
    else if (arrayOfLocations.count == nextInLine) {
        [self endMuVForDriver];
    }
    //driver has to still pick up or drop off passengers
    else {
        //either start or end address of passenger
        if (nextInLine > 1 && nextInLine < arrayOfLocations.count - 1) {
            //check whether the previous address was start or end location
            //update the users status
            [self updateStateForPassenger:nextInLine - 2];
        }
        
        //get next location from waypoint order
        NSInteger nextLocationPoint = [wayPointOrder[nextInLine - 1] intValue];
        
        NSInteger arrLoc = nextInLine;
        //find the index in the address arrays diving the number by 2
      //  NSInteger myLocationIndex = nextLocationPoint/2;
      //  userDetail = _trip.userDetails[myLocationIndex];
        
        
        NSString *dictValue = [dictOfWayPointOrderAndStartOrEnd objectForKey:[NSString stringWithFormat:@"%ld", (long)nextLocationPoint]];
        
        NSString *startEnd = [self getValueFromCommaSeperatedString:dictValue forPosition:0];
        
        if ([startEnd isEqualToString:@"Start"]) {
            locNext = arrayOfLocations[arrLoc];
            [self removePopUpView];
        }
        else {
            locNext = arrayOfLocations[arrLoc];
            [self removePopUpView];
        }
        
        //navigate to next location
        [self loadAppleMap:locNext.coordinate];
    }
}

- (void)endMuVForDriver {
    //reset counters before quitting
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"navAddr"];
    [[LocationTracker shared] stopLocationTracking];
    [rideManager setDriversLocationToNull];
    [[TripManager sharedManager] setCurrentTripDriver:nil];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MuVSidePanelController *sidePanelViewController = [storyboard instantiateViewControllerWithIdentifier:@"MuVSidePanelViewController"];
    sidePanelViewController.centerPanel = [self.storyboard instantiateViewControllerWithIdentifier:@"Tripsboard"];
    [self presentViewController:sidePanelViewController animated:YES completion:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"riderMode" object:nil];
}

//event handler when event occurs
- (void)eventHandler: (NSNotification *) notification {
    CLLocationCoordinate2D coord = [[LocationTracker shared] myLastLocation];
    currentLocationMarker.position = coord;
    currentLocationMarker.appearAnimation = kGMSMarkerAnimationPop;
    currentLocationMarker.icon = [UIImage imageNamed:@"driverCurrentLocation"];
    currentLocationMarker.map = _mapView;
    //check if the current location is close to start end location of the passenger
    [self checkIfDriverIsArrivingLocation:coord];
}

- (void)loadAppleMap:(CLLocationCoordinate2D) coord {
   
    NSURL *testURL = [NSURL URLWithString:@"comgooglemaps-x-callback://"];
    
    if ([[UIApplication sharedApplication] canOpenURL:testURL]) {
        NSString *destinationAddr = [Helper getLatLongInCommaSeperatedString:coord];
        NSString *directionsRequest = [NSString stringWithFormat:@"comgooglemaps-x-callback://?daddr=%@&x-success=MuV://?resume=true&x-source=MuV", destinationAddr];
        NSURL *directionsURL = [NSURL URLWithString:directionsRequest];
        [[UIApplication sharedApplication] openURL:directionsURL];
    
    } else {
        
        CLLocationCoordinate2D endingCoord = coord;
        MKPlacemark *endLocation = [[MKPlacemark alloc] initWithCoordinate:endingCoord addressDictionary:nil];
        MKMapItem *endingItem = [[MKMapItem alloc] initWithPlacemark:endLocation];
        
        NSMutableDictionary *launchOptions = [[NSMutableDictionary alloc] init];
        [launchOptions setObject:MKLaunchOptionsDirectionsModeDriving forKey:MKLaunchOptionsDirectionsModeKey];
        [endingItem openInMapsWithLaunchOptions:launchOptions];
    }
 }

- (BOOL)mapView:(GMSMapView *) mapView didTapMarker:(GMSMarker *)marker {
    
    if (isDriver && _trip.isCurrent) {
        [self showNavigationView:marker];
    }
    else {
        [self showProfileView:marker];
    }
    return YES;
}


- (void)passengerPickedUpView {
    
    _startNavBtn.enabled = NO;
    [_startNavBtn setBackgroundColor:UICOLOR_PAST_TRIPS];
    
    _startMuvBtn.enabled = NO;
    [_startMuvBtn setBackgroundColor:UICOLOR_PAST_TRIPS];
    
    _noShowBtn.hidden = YES;
    _noShowBtn.enabled = NO;
   // [_noShowBtn setBackgroundColor:UICOLOR_REQUESTED_TRIPS];
   // [_noShowBtn setTitle:END_MUV forState:UIControlStateNormal];
    
  //  _endNavButton.hidden = NO;
   // _endNavButton.enabled = YES;
    
    _riderName.hidden = NO;
    _riderProfilePicture.hidden = NO;
    _riderStartAddress.hidden = NO;
    
}

- (void)passengerPickedUpAndNextInLineForDropOffView {
    
    _startNavBtn.enabled = NO;
    [_startNavBtn setBackgroundColor:UICOLOR_PAST_TRIPS];
    
    _startMuvBtn.enabled = NO;
    [_startMuvBtn setBackgroundColor:UICOLOR_PAST_TRIPS];
    
    _noShowBtn.hidden = YES;
    _noShowBtn.enabled = NO;
    // [_noShowBtn setBackgroundColor:UICOLOR_REQUESTED_TRIPS];
    // [_noShowBtn setTitle:END_MUV forState:UIControlStateNormal];
    
    _endNavButton.hidden = NO;
    _endNavButton.enabled = YES;
    
    _riderName.hidden = NO;
    _riderProfilePicture.hidden = NO;
    _riderStartAddress.hidden = NO;
    
}


- (void)passengerDroppedOffView {
    
    _startNavBtn.enabled = NO;
    [_startNavBtn setBackgroundColor:UICOLOR_PAST_TRIPS];
    [_startNavBtn setTitle:@"Start With Navigation" forState:UIControlStateNormal];
    
    _startMuvBtn.enabled = NO;
    [_startMuvBtn setBackgroundColor:UICOLOR_PAST_TRIPS];
    
    _noShowBtn.enabled = NO;
    [_noShowBtn setBackgroundColor:UICOLOR_PAST_TRIPS];
    
    _endNavButton.hidden = YES;
    _endNavButton.enabled = NO;
    [_endNavButton setBackgroundColor:UICOLOR_PAST_TRIPS];
    
    _riderName.hidden = NO;
    _riderProfilePicture.hidden = NO;
    _riderStartAddress.hidden = NO;
}

- (void)plotDriversCurrentLocation:(PFGeoPoint *)geoPoint isFirstLoad:(BOOL)firstLoad {
    if (firstLoad) {
        float pingInterval = 60;
        if (adminManager.adminSettings.currentLocationPingIntervalInSeconds) {
            pingInterval = [adminManager.adminSettings.currentLocationPingIntervalInSeconds floatValue];
        }
        
        timer = [NSTimer scheduledTimerWithTimeInterval:pingInterval target:self selector:@selector(reloadDriversLocation) userInfo:nil repeats:YES];
        currentLocationMarker = [GMSMarker markerWithPosition:CLLocationCoordinate2DMake(geoPoint.latitude, geoPoint.longitude)];
        currentLocationMarker.appearAnimation = kGMSMarkerAnimationPop;
        currentLocationMarker.icon =  [UIImage imageNamed:@"driverCurrentLocation"];
        currentLocationMarker.map = _mapView;
    }
    else {
        if (!currentLocationMarker) {
            currentLocationMarker = [[GMSMarker alloc] init];
        }
        currentLocationMarker.appearAnimation = kGMSMarkerAnimationPop;
        currentLocationMarker.position = CLLocationCoordinate2DMake(geoPoint.latitude, geoPoint.longitude);
        currentLocationMarker.icon =  [UIImage imageNamed:@"driverCurrentLocation"];
        currentLocationMarker.map = _mapView;
    }
}

- (void)prepareToEndDriversMuV {
    [self removePopUpView];
    _startNavigation.hidden = NO;
    [_startNavigation setBackgroundColor:UICOLOR_REQUESTED_TRIPS];
    [_startNavigation setTitle:END_MUV forState:UIControlStateNormal];
}

- (void)prepareforDrivingToStartLocation {
    _riderStartAddress.hidden = NO;
    _riderProfilePicture.hidden = NO;
    _startNavBtn.enabled = YES;
    _startMuvBtn.enabled = YES;
    [_noShowBtn setTitle:@"No Show" forState:UIControlStateNormal ];
    [self removePopUpView];
}

- (void)prepareforDrivingToEndLocation {
    //show end muv PopUp
    //hide navigation button, start Muv Button. Only keep No Show Button. CHnage text to EndMuv
    _startNavBtn.hidden = YES;
    _startMuvBtn.hidden = YES;
    _riderStartAddress.hidden = NO;
    _riderProfilePicture.hidden = NO;
    [_noShowBtn setTitle:END_MUV forState:UIControlStateNormal];
    
    _endNavButton.hidden = NO;
    _endNavButton.enabled = YES;
    [_endNavButton setBackgroundColor:UICOLOR_REQUESTED_TRIPS];
    
    _noShowBtn.hidden = NO;
    _noShowBtn.enabled = YES;
    [_noShowBtn setBackgroundColor:UICOLOR_REQUESTED_TRIPS];
    [_noShowBtn setTitle:END_MUV forState:UIControlStateNormal];
   // [self addPopUpView];
}

- (void)prepareStartNavigationViewForDriver {
    [_startNavBtn setBackgroundColor:UICOLOR_SCHEDULED_TRIPS];
    _startMuvBtn.enabled = YES;
    _startNavBtn.enabled = YES;
    
    _riderName.hidden = YES;
    _riderProfilePicture.hidden = YES;
    _riderStartAddress.hidden = YES;
    
    _startMuvBtn.hidden = YES;
    _noShowBtn.hidden = YES;
    _endNavButton.hidden = YES;
    //make the view smaller
    [_startNavBtn setTitle:@"Start Navigation" forState:UIControlStateNormal];
}

- (void)reloadDriversLocation {
    [rideManager getDriversCurrentLocationWithCompletionBlock:^(PFGeoPoint *geoPoint, NSError *error) {
        if (geoPoint == nil) {
            if(timer) {
                [timer invalidate];
                timer = nil;
            }
        }
        else {
            [self plotDriversCurrentLocation:geoPoint isFirstLoad:NO];
        }
    }];
}

- (void)removePopUpView {
    [_riderProfileView removeFromSuperview];
    [_riderStartMuvView removeFromSuperview];
}


- (void)showNavigationView:(GMSMarker *)marker {
    if (marker.userData != nil) {
        if ([marker.userData isKindOfClass:[UserDetail class]]) {
            UserDetail *userDetail = marker.userData;
            _startLocationHidden.text = [NSString stringWithFormat:@"%ld",(long)userDetail.rank]; //store this for noshow
            
            _riderStartMuvView.hidden = NO;
            _riderName.text = userDetail.displayName;
            _riderStartAddress.text = marker.title;
            
            PFFile *riderPhoto = userDetail.profileImage;
            if (riderPhoto != nil) {
                [_riderProfilePicture sd_setImageWithURL:[NSURL URLWithString:riderPhoto.url]];
            }
            else {
                [_riderProfilePicture setImage:[UIImage imageNamed:@"MuV_NOimageAvatar"]];
            }
            
            _riderProfilePicture  =  [UIImageView muv_imageCircle:_riderProfilePicture];
            
            if (userDetail.isNextInLineForPickUp && !userDetail.isPickedUp) {
                [self nextInLinePassengersPickUpView];
            }
            else {
                [self otherPassengersPickUpView];
            }
            
            if (userDetail.isPickedUp) {
                [self passengerPickedUpView];
            }
            
            if (userDetail.isPickedUp && userDetail.isNextInLineForDropOff) {
                [self prepareforDrivingToEndLocation];
            }
            
            if (userDetail.isDroppedOff) {
                [self passengerDroppedOffView];
            }
            
            if (userDetail.isNoShow) {
                [self preparePopUpAfterNoShow];
            }
            else {
                if (!userDetail.isPickedUp) {
                    _noShowBtn.enabled = YES;
                    [_noShowBtn setBackgroundColor:UICOLOR_REQUESTED_TRIPS];
                }
            }
        }
        else {
            [self prepareStartNavigationViewForDriver];
        }
        [self addPopUpView];
    }
}

- (void)preparePopUpAfterNoShow {
    _noShowBtn.enabled = NO;
    [_noShowBtn setBackgroundColor:UICOLOR_PAST_TRIPS];
    _startNavBtn.enabled = NO;
    [_startNavBtn setBackgroundColor:UICOLOR_PAST_TRIPS];
    _startMuvBtn.enabled = NO;
    [_startMuvBtn setBackgroundColor:UICOLOR_PAST_TRIPS];

}

- (void)showProfileView:(GMSMarker *)marker {
    if (marker.userData != nil) {
        if ([marker.userData isKindOfClass:[UserDetail class]]) {
            userDetail = marker.userData;
            [self performSegueWithIdentifier:@"PassDetail" sender:nil];
            
        }
    }
}

- (void)updateStateForPassenger:(NSInteger)index {
    //get next location from waypoint order
    NSInteger nextLocationPoint = [wayPointOrder[index] intValue];
    
    
    
    NSString *dictValue = [dictOfWayPointOrderAndStartOrEnd objectForKey:[NSString stringWithFormat:@"%ld", nextLocationPoint]];
    
    NSString *startEnd = [self getValueFromCommaSeperatedString:dictValue forPosition:0];
    NSInteger userDetailNumber = [[self getValueFromCommaSeperatedString:dictValue forPosition:1] integerValue];
    userDetail = _trip.userDetails[userDetailNumber];
    
    if ([startEnd isEqualToString:@"Start"]) {
        userDetail.isPickedUp = YES;
        userDetail.rideObj[@"pickUpTime"] = [NSDate date];
        [self.navigationItem setTitle:@"Trip Enroute"];
        [rideManager updateRideObject:userDetail.rideObj];
        [[NSUserDefaults standardUserDefaults] setInteger:index + 2 forKey:@"navAddr"];

    }
    else {
        [self chargePayment:userDetail.rideObj withIndex:index + 2];
    }
}

- (void)chargePayment:(PFObject *)rideObj withIndex:(NSInteger)index {
    [paymentManager chargePaymentWithRide:rideObj withCompletionBlock:^(BOOL succeeded, NSError *error) {
        
        if (!error) {
            [[NSUserDefaults standardUserDefaults] setInteger:index forKey:@"navAddr"];
            userDetail.isDroppedOff = YES;
            userDetail.rideObj[@"dropOffTime"] = [NSDate date];
            
            [rideManager updatePFObject:userDetail.rideObj withCompletionBlock:^(BOOL succeeded, NSError *error) {
                // Email for Rider Trip Completion
                [mailManager sendRiderTripCompletionRoute:userDetail.rideObj];
            }];
        }
        else {
            if ([error.description rangeOfString:@"Request failed: request timed out (408)" options:NSCaseInsensitiveSearch].location != NSNotFound) {
                [self chargePayment:rideObj withIndex:index];
            }
            else {
                UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Error ending MüV! Please try again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [warningAlert show];
            }
        }
    }];
}

- (void)updatePassengerStatusToNoShowAndReloadMap {
 //   NSLog(@"user location %@", _startLocationHidden.text);
    userDetail = _trip.userDetails[[_startLocationHidden.text intValue]];

       // RIDER MAKES PAYMENT FOR NO SHOW
    [paymentManager chargePaymentWithRide:userDetail.rideObj withCompletionBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            userDetail.isNoShow = YES;
            userDetail.rideObj[@"noShow"] = [NSNumber numberWithBool:YES];
            CLLocationCoordinate2D coord = [[LocationTracker shared] myLastLocation];
            [self getGoogleApiResponseForStartLocation:[NSString stringWithFormat:@"%f,%f",coord.latitude,coord.longitude]];
            
            [rideManager updatePFObject:userDetail.rideObj withCompletionBlock:^(BOOL succeeded, NSError *error) {
                // SEND EMAIL RIDER NO SHOW
                [mailManager sendRiderNoShowWithRide:userDetail.rideObj];
            }];
        }
    }];
    
}

- (void)chargePaymentNoShow:(PFObject *)rideObj {
    [paymentManager chargePaymentWithRide:rideObj withCompletionBlock:^(BOOL succeeded, NSError *error) {
        
        if (!error) {
            userDetail.isNoShow = YES;
            userDetail.rideObj[@"noShow"] = [NSNumber numberWithBool:YES];
            CLLocationCoordinate2D coord = [[LocationTracker shared] myLastLocation];
            [self getGoogleApiResponseForStartLocation:[NSString stringWithFormat:@"%f,%f",coord.latitude,coord.longitude]];
            
            [rideManager updatePFObject:userDetail.rideObj withCompletionBlock:^(BOOL succeeded, NSError *error) {
                // SEND EMAIL RIDER NO SHOW
                [mailManager sendRiderNoShowWithRide:userDetail.rideObj];
            }];
        }
        else {
            if ([error.description rangeOfString:@"Request failed: request timed out (408)" options:NSCaseInsensitiveSearch].location != NSNotFound) {
                [self chargePaymentNoShow:rideObj];
            }
            else {
                UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Error with No Show. Please check your network connection and try again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [warningAlert show];
            }
        }
    }];

}

- (void)nextInLinePassengersPickUpView {
    _startNavBtn.hidden = NO;
    _startNavBtn.enabled = YES;
    [_startNavBtn setBackgroundColor:UICOLOR_SCHEDULED_TRIPS];
    [_startNavBtn setTitle:@"Start with Navigation" forState:UIControlStateNormal];
    
    _startMuvBtn.hidden = NO;
    _startMuvBtn.enabled = YES;
    [_startMuvBtn setBackgroundColor:UICOLOR_SCHEDULED_TRIPS];
    
    _noShowBtn.hidden = NO;
    _riderProfilePicture.hidden = NO;
    _riderStartAddress.hidden = NO;
    
    _endNavButton.hidden = YES;
    
    _riderName.hidden = NO;
    _riderProfilePicture.hidden = NO;
    _riderStartAddress.hidden = NO;
}

- (void)otherPassengersPickUpView {
    [_startMuvBtn setBackgroundColor:UICOLOR_PAST_TRIPS];
    _startMuvBtn.enabled = NO;
    
    [_startNavBtn setBackgroundColor:UICOLOR_PAST_TRIPS];
    _startNavBtn.enabled = NO;
    
    _riderName.hidden = NO;
    _riderProfilePicture.hidden = NO;
    _riderStartAddress.hidden = NO;
    _startMuvBtn.hidden = NO;
    _noShowBtn.hidden = NO;
    
    _endNavButton.hidden = YES;
    
    //_riderName.hidden = YES;
    //_riderProfilePicture.hidden = YES;
    //_riderStartAddress.hidden = YES;
}

- (void)getGoogleApiResponseForStartLocation:(NSString *)startLocation {
    
    NSString *queryString = [self createGoogleMapApiUQueryStringWithStartLocation:startLocation];
    
    GoogleMapView *gmap = [GoogleMapView new];
    [gmap getGoogleDirectionForQuery:queryString withCompletionBlock:^(NSArray *places, NSError *error) {
        [self directionLegsFromJSonResponse:places];
        CLLocationCoordinate2D coord = [self getPositionFromCommaSeperatedLatitude:_trip.startLocation];
        [self plotUsersOnMap:NO withCoordinates: coord];
        [self loadDriversPassengersDetails];
    }];
}

- (NSMutableString *)createGoogleMapApiUQueryStringWithStartLocation:(NSString *)startLocation {
    
    NSMutableString *queryString = [NSMutableString string];
    [queryString appendString: @"https://maps.googleapis.com/maps/api/directions/json?origin="];
    [queryString appendString:startLocation];
    [queryString appendString:@"&destination="];
    [queryString appendString:_trip.endLocation];
    [queryString appendString:@"&waypoints=optimize:true%7C"];
    
    UserDetail *userDetail;
    
    dictOfWayPointOrderAndStartOrEnd = [NSMutableDictionary new];
    int wayPointCount = 0;
    for (int i = 0; i < _trip.wayPointStartAddress.count; i++) {
        userDetail  = (UserDetail *)_trip.userDetails[i];
        userDetail.rank = i;
        NSString *dictStartEnd;
       // NSLog(@"SMN Log userdetail %d %d",userDetail.isPickedUp, userDetail.isNoShow);
        if (userDetail != nil && (!userDetail.isPickedUp && !userDetail.isNoShow)) {
            [queryString appendString:_trip.wayPointStartAddress[i]];
            [queryString appendString:@"%7C"]; //this is separator
            dictStartEnd = [NSString stringWithFormat:@"%@,%ld", @"Start",(long)userDetail.rank];
            [dictOfWayPointOrderAndStartOrEnd setObject:dictStartEnd forKey:[NSString stringWithFormat:@"%d",wayPointCount]];
            wayPointCount++;
        }
        
        if (userDetail != nil && (!userDetail.isDroppedOff && !userDetail.isNoShow)) {
            [queryString appendString:_trip.wayPointEndAddress[i]];
             dictStartEnd = [NSString stringWithFormat:@"%@,%ld", @"End",(long)userDetail.rank];
            [dictOfWayPointOrderAndStartOrEnd setObject:dictStartEnd forKey:[NSString stringWithFormat:@"%d",wayPointCount]];
            wayPointCount++;
            if(i!=_trip.wayPointStartAddress.count - 1) {
                [queryString appendString:@"%7C"];
            }
        }
    }
    
    [queryString appendString:@"&key=AIzaSyDfLC2TAJWFlw83x9HcepH4WgAJk_TwuAk"];
    return queryString;
    
}

- (void)plotUsersOnMap:(BOOL) isCurrent withCoordinates:(CLLocationCoordinate2D) currentLocation {
    
    _mapView.delegate = self;
    
    //plot start location
    GMSMarker *marker = [GMSMarker markerWithPosition:currentLocation];
    marker.title = _trip.startLocation;
    marker.icon = [UIImage imageNamed:@"StartMrker"];
    marker.zIndex = 0;
    marker.map = _mapView;
    
    //plot endLocation location
    marker = [GMSMarker markerWithPosition:[self getPositionFromCommaSeperatedLatitude:_trip.endLocation]];
    marker.title = _trip.endAddress;
    marker.icon = [UIImage imageNamed:@"EndMrker"];
    marker.zIndex = 0;
    marker.map = _mapView;
    
    UserDetail *userDetail;
    UIImageView *imageView;
    
    for (int i = 0; i<_trip.wayPointStartAddress.count; i++) {
        //plot start location
        
        marker = [GMSMarker markerWithPosition:[self getPositionFromCommaSeperatedLatitude:_trip.wayPointStartAddress[i]]];
        //change this to get the title
        userDetail = (UserDetail *) _trip.userDetails[i];
        marker.title = userDetail.startAddress;
        marker.userData = userDetail;
        marker.zIndex = 1;
        imageView = [[UIImageView alloc] init];
        if (userDetail.profileImage.url != nil) {
            [imageView setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL: [NSURL URLWithString:userDetail.profileImage.url]]]];
        }
        else {
            [imageView setImage:[UIImage imageNamed:NOIMAGE_AVATAR]];
        }
        
        marker.icon  = [UIImage circularScaleNCrop:[imageView.image resizedImageByMagick:@"35x35#"] withColor:UICOLOR_SCHEDULED_TRIPS];
        marker.map = _mapView;
        
    }
    for (int i = 0; i<_trip.wayPointEndAddress.count; i++) {
        //plot start location
        
        marker = [GMSMarker markerWithPosition:[self getPositionFromCommaSeperatedLatitude:_trip.wayPointEndAddress[i]]];
        //change this to get the title
        userDetail = (UserDetail *) _trip.userDetails[i];
        marker.title = userDetail.endAddress;
        marker.userData = userDetail;
        marker.zIndex = 1;
        imageView = [[UIImageView alloc] init];
        if (userDetail.profileImage.url != nil) {
          //  [imageView sd_setImageWithURL:[NSURL URLWithString:userDetail.profileImage.url]];
            [imageView setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL: [NSURL URLWithString:userDetail.profileImage.url]]]];
        }
        else {
            [imageView setImage:[UIImage imageNamed:NOIMAGE_AVATAR]];
        }
        marker.icon  = [UIImage circularScaleNCrop:[imageView.image resizedImageByMagick:@"35x35#"] withColor:UICOLOR_RED_COLOR_USER];
        marker.map = _mapView;
    }
}

- (void)getCurrentLocation:(CLLocationCoordinate2D) myLastLocation {
    [self plotUsersOnMap:NO withCoordinates:myLastLocation];
}

- (UIImage *)resizeImage:(NSString *)imageName withSize:(NSString *)size {
    UIImage *image = [UIImage imageNamed:imageName];
    return [image resizedImageByMagick:size];
}

- (CLLocationCoordinate2D)getPositionFromCommaSeperatedLatitude:(NSString *) locationInString {
    NSArray *latLong = [locationInString componentsSeparatedByString:@","];
    double lat = [latLong[0] doubleValue];
    double lng = [latLong[1] doubleValue];
    CLLocationCoordinate2D position = CLLocationCoordinate2DMake(lat, lng);
    return position;
}

- (NSString *)getValueFromCommaSeperatedString:(NSString *)stringVal forPosition:(NSInteger)position {
    NSArray *seperatedValues = [stringVal componentsSeparatedByString:@","];
    if (position < seperatedValues.count) {
        return seperatedValues[position];
    }
    else {
        return @"";
    }
}

- (CLLocationCoordinate2D)getPositionForPicture:(NSString *) locationInString {
    NSArray *latLong = [locationInString componentsSeparatedByString:@","];
    double lat = [latLong[0] doubleValue];
    double lng = [latLong[1] doubleValue];
    CLLocationCoordinate2D position = CLLocationCoordinate2DMake(lat + 0.02, lng - 0.03);
    return position;
}

- (void)directionLegsFromJSonResponse :(NSArray *)routeResults {
    
    CLLocationCoordinate2D coordinate = [self getPositionFromCommaSeperatedLatitude:_trip.startLocation];
        camera = [GMSCameraPosition cameraWithLatitude:coordinate.latitude
                                             longitude:coordinate.longitude
                                                  zoom:11];
    
    _mapView.camera = camera;

    GMSMutablePath *path = [GMSMutablePath new];
    
    arrayOfLocations = [[NSMutableArray alloc] init];
    
    for (NSDictionary *routeDict in routeResults) {
        NSArray *legResults = [routeDict valueForKey:@"legs"];
        wayPointOrder = [routeDict valueForKey:@"waypoint_order"];
        
        int leg = 0;
        
        for (NSDictionary *legDict in legResults) {
            //get the start location
            NSDictionary *startLocResults = [legDict valueForKey:@"start_location"];
            
            [path addLatitude:[startLocResults [@"lat"] doubleValue ]longitude:[startLocResults [@"lng"] doubleValue]];
            
            CLLocationCoordinate2D position = CLLocationCoordinate2DMake([startLocResults[@"lat"] doubleValue], [startLocResults[@"lng"] doubleValue]);
            
            CLLocation *location = [[CLLocation alloc] initWithLatitude:position.latitude longitude:position.longitude];
            [arrayOfLocations addObject:location];
            
            marker = [GMSMarker markerWithPosition:position];
            
            if (leg == legResults.count - 1) {
                NSDictionary *endLocResults = [legDict valueForKey:@"end_location"];
                CLLocationCoordinate2D positionEnd = CLLocationCoordinate2DMake([endLocResults[@"lat"] doubleValue], [endLocResults[@"lng"] doubleValue]);
                CLLocation *location = [[CLLocation alloc] initWithLatitude:positionEnd.latitude longitude:positionEnd.longitude];
                [arrayOfLocations addObject:location];
            }
            
            //get the start location
            NSArray *stepResults = [legDict valueForKey:@"steps"];
            
            int i = 0;
            for (NSDictionary *keyStepsDict in stepResults) {
                NSDictionary  *endLocationResults = [keyStepsDict valueForKey:@"end_location"];
                NSString *polyLinePoints = [[keyStepsDict objectForKey:@"polyline"] objectForKey:@"points"];
                GMSPath *polyLinePath = [GMSPath pathFromEncodedPath:polyLinePoints];
                for (int p=0; p<polyLinePath.count; p++) {
                    [path addCoordinate:[polyLinePath coordinateAtIndex:p]];
                }
                if (stepResults.count - 1 == i) {
                    [path addLatitude:[endLocationResults[@"lat"] doubleValue ]longitude:[endLocationResults[@"lng"] doubleValue]];
                }
                i++;
            }
            leg++;
        }  //end for legDict
    }
    
    polyline = nil;
    [_mapView clear];
    polyline = [GMSPolyline polylineWithPath:path];
    polyline.strokeColor = [UIColor blueColor];
    polyline.strokeWidth = 3.f;
    polyline.map = _mapView;
    
}

- (CLLocationCoordinate2D)coordinateWithLocation:(NSDictionary*)location{
    double latitude = [[location objectForKey:@"lat"] doubleValue];
    double longitude = [[location objectForKey:@"lng"] doubleValue];
    return CLLocationCoordinate2DMake(latitude, longitude);
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString: @"PassDetail"]) {
        MuVPassengerDetailsViewController *passDetails = [segue destinationViewController];
        passDetails.userDetail = userDetail;
        return;
    }
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result {
    switch (result) {
        case MessageComposeResultCancelled:
            break;
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed to send SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            break;
        }
        case MessageComposeResultSent:
            break;
        default:
            break;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)mailComposeController:(MFMailComposeViewController *)controller
         didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    if (result) {
        NSLog(@"Result : %d",result);
    }
    if (error) {
        NSLog(@"Error : %@",error);
    }
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

#pragma IBActions

- (IBAction)startEndNavigation:(id)sender {
        
    if ([_startNavigation.titleLabel.text isEqualToString:END_MUV]) {
        [self endMuVForDriver];
    }
    else {
        
        NSInteger nextInLine = [[NSUserDefaults standardUserDefaults] integerForKey:@"navAddr"];
        CLLocation *location = arrayOfLocations[nextInLine];
        [self loadAppleMap:location.coordinate];
    }
}


- (IBAction)startNavigation:(id)sender {
    if(isDriver) {
        [self determineNextLocation];
    }
}

- (IBAction)cancel:(id)sender {
    [self removePopUpView];
}

- (IBAction)navigate:(id)sender {
    
    NSInteger nextInLine = [[NSUserDefaults standardUserDefaults] integerForKey:@"navAddr"];
    nextInLine = nextInLine + 1; //get the next location
    CLLocation *location = arrayOfLocations[nextInLine];
    [self loadAppleMap:location.coordinate];
}


- (IBAction)startMuV:(id)sender {
    
    if (isDriver && _trip.isCurrent) {
        NSInteger nextInLine = [[NSUserDefaults standardUserDefaults] integerForKey:@"navAddr"];
        nextInLine = nextInLine + 1;
       // NSLog(@"NextInLine in Start MUV %ld", (long)nextInLine);
        [self updateStateForPassenger:nextInLine - 2];
        [self removePopUpView];
    }
    //[[NSUserDefaults standardUserDefaults] setInteger:nextInLine forKey:@"navAddr"];
}

//this ends Muv Plus Navigates
- (IBAction)endNav:(id)sender {
    [self determineNextLocation];
}

- (IBAction)noShow:(id)sender {
    if (isDriver) {
        // when driver clicks end muv, navigate to the next location if any and also update the drop off location of the user
        if ([_noShowBtn.titleLabel.text isEqualToString:END_MUV]) {
            //[self navigationSteps];
            NSInteger nextInLine = [[NSUserDefaults standardUserDefaults] integerForKey:@"navAddr"];
            nextInLine = nextInLine + 1;
          //  NSLog(@"NextInLine in END MUV %ld", (long)nextInLine);
            if (nextInLine - 2 < wayPointOrder.count) {
                [self updateStateForPassenger:nextInLine - 2];
                [self removePopUpView];
            }
            else {
                [self endMuVForDriver];
            }
        }
        //no show - update the databse and also remove the entries from the array
        else {
            //get
            [self updatePassengerStatusToNoShowAndReloadMap];
            
            //update the ride object that the passenger has been picked up or dropped off
            //  [self findLocInDictAndUpdateRideObj:_startLocationHidden.text withDate:nil];
            [self removePopUpView];
        }
    }
}

- (IBAction)callDriver:(id)sender {
    
    NSString *driverPhone = _trip.driverDetails[@"phone"];
    if (driverPhone) {
        NSString *phoneNumber = [[driverPhone componentsSeparatedByCharactersInSet:
                                  [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                                 componentsJoinedByString:@""];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[@"tel:" stringByAppendingString:phoneNumber]]];
    }
}

- (IBAction)smsDriver:(id)sender {
    
    NSString *driverPhone = _trip.driverDetails[@"phone"];
    
    if (driverPhone) {
        if(![MFMessageComposeViewController canSendText]) {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Your device doesn't support SMS." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            return;
        }
        MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
        messageController.messageComposeDelegate = self;
        NSString *phoneNumber = [[driverPhone componentsSeparatedByCharactersInSet:
                                  [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                                 componentsJoinedByString:@""];
        [messageController setRecipients:@[phoneNumber]];
        [self presentViewController:messageController animated:YES completion:nil];
    }
}

- (IBAction)emailDriver:(id)sender {
    
    NSString *driverEmail = _trip.driverDetails[@"email"];
    
    if (driverEmail) {
        if([MFMailComposeViewController canSendMail]) {
            MFMailComposeViewController *mailComposer = [[MFMailComposeViewController alloc]init];
            [mailComposer setToRecipients:@[driverEmail]];
            mailComposer.mailComposeDelegate = self;
            [mailComposer setSubject:@"Message from MüV User"];
            [self presentViewController:mailComposer animated:YES completion:NULL];
        }else{
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Your device's email client is not setup." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}



@end
