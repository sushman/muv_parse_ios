//
//  TripViewController.m
//  MuV
//
//  Created by SushmaN on 1/8/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "MuVTripViewController.h"
#import "RideManager.h"

#import "JASidePanelController.h"
#import "UIButton+MuVUIButtonAdditions.h"
#import "UserDetail.h"
#import "MuVPassengerDetailsViewController.h"
#import "MuVTripEnRouteViewController.h"
#import "MuVDriverViewController.h"
#import "Helper.h"
#import "Trip.h"
#import "ControlVariables.h"
#import "MuVSidePanelController.h"
#import "SectionInfo.h"
#import "ControlVariables.h"
#import "RatingManager.h"
#import "UIImageView+WebCache.h"
#import "UIImageView+MuVUIImageViewAdditions.h"
#import "UIView+UIViewAdditions.h"
#import "EventKitManager.h"
#import "MailManager.h"
#import "TripManager.h"
#import "AdminManager.h"
#import "PaymentManager.h"


@interface MuVTripViewController ()

{
    // ivar
}


@property (weak, nonatomic) IBOutlet AgendaViewOfRides *tripsAgendaView;
@property (weak, nonatomic) IBOutlet UIButton *calendarAgendaSwitch;
@property (weak, nonatomic) IBOutlet UIButton *scheduleMuVButton;
@property (weak, nonatomic) IBOutlet UIButton *pastTripsButton;
@property (weak, nonatomic) IBOutlet UIButton *pendingButton;
@property (weak, nonatomic) IBOutlet UIButton *scheduledButton;

@property (strong, nonatomic) IBOutlet UIView *cancelPopUpView;
@property (strong, nonatomic) IBOutlet UIView *popUpBackgroundView;

@property (strong, nonatomic) IBOutlet UIView *cantCancelMuvPopup;

// RATING VIEW
@property (weak, nonatomic) IBOutlet UIView *ratingPopUpView;
@property (weak, nonatomic) IBOutlet UIImageView *ratingDriverImageView;
@property (weak, nonatomic) IBOutlet UILabel *ratingDriverNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *starOne;
@property (weak, nonatomic) IBOutlet UIButton *starTwo;
@property (weak, nonatomic) IBOutlet UIButton *starThree;
@property (weak, nonatomic) IBOutlet UIButton *starFour;
@property (weak, nonatomic) IBOutlet UIButton *starFive;
@property (weak, nonatomic) IBOutlet UITextField *reviewTextField;
@property (weak, nonatomic) IBOutlet UIButton *ratingButton;
@property (strong, nonatomic) NSNumber *selectedRating;

@property (weak, nonatomic) IBOutlet UIButton *loadPrevMonth;
@property (weak, nonatomic) IBOutlet UIButton *loadNextMonth;
@property (weak, nonatomic) IBOutlet UILabel *monthAndYear;
@property (strong, nonatomic) NSDate *currentDate;

//farePopUp
@property (strong, nonatomic) IBOutlet UIView *farePopUpView;
@property (weak, nonatomic) IBOutlet UILabel *driverNameForFare;
@property (weak, nonatomic) IBOutlet UIImageView *driverPicForFare;
@property (weak, nonatomic) IBOutlet UIButton *requestReceipt;
@property (weak, nonatomic) IBOutlet UIImageView *creditCardLabel;
@property (weak, nonatomic) IBOutlet UILabel *creditCardInfo;
@property (weak, nonatomic) IBOutlet UILabel *fare;
@property (weak, nonatomic) IBOutlet UIView *requestReceiptView;

@property (weak, nonatomic) IBOutlet UIView *noTripsView;

@property (weak, nonatomic) IBOutlet UILabel *cancellationMessage;
@property (strong, nonatomic) NSNumber *cancelType;

@end

@implementation MuVTripViewController

BOOL isDriver;
BOOL reloadNextData;
BOOL isAgendaView;
BOOL isRoundTripEmailSent;

UserDetail *userDetail;

Ride *ride;
Trip *trip;
PFObject *pfObject;
RideManager *rideManager;
RatingManager *ratingManager;
MailManager *mailManager;
AdminManager *adminManager;
PaymentManager *paymentManager;

NSArray *arrRides;
NSIndexPath *path;
UITapGestureRecognizer *singleFingerTap;



- (void)viewDidLoad {
    [super viewDidLoad];
    
    _activityIndicatorView.hidden = YES;
    
    ratingManager = [RatingManager new];
    rideManager = [RideManager new];
    adminManager = [AdminManager sharedInstance];
    paymentManager = [PaymentManager new];
    mailManager = [MailManager new];
    
    if (!adminManager.adminSettings.hoursBeforeDepartureForFee) {
        [adminManager getAdminSettingsWithCompletionBlock:^(AdminSettings *adminSetting, NSError *error) {
            
        }];
    }
    
    _currentDate = [self getDateLoaded];
    if (_currentDate == nil) {
        _currentDate = [NSDate date];
    }
    [self setCurrentDate:_currentDate];
    
    [self customiseControls];
    
    [self checkDriverMode];
    
    [_tripsAgendaView setDelegate:_tripsAgendaView];
    _tripsAgendaView.tableAlertDelegate = self;
    
    _reviewTextField.delegate = self;
    
    _requestReceiptView.hidden = YES;
    //The setup code (in viewDidLoad in your view controller)
   
    _noTripsView.hidden = YES;

    [self getTripsWithReload:NO];
    
}


- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
  //  NSLog(@" testing singkle tap");
    [_popUpBackgroundView removeFromSuperview];
  //  [_cancelPopUpView removeFromSuperview];
}

- (void)viewDidAppear:(BOOL)animated {
    
    if (!singleFingerTap) {
        singleFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
        singleFingerTap.numberOfTapsRequired = 1;
        //[_popUpBackgroundView setUserInteractionEnabled:YES];
        [_farePopUpView addGestureRecognizer:singleFingerTap];
        singleFingerTap.cancelsTouchesInView = NO;
        singleFingerTap.delegate = self;
    }

    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(eventHandler:) name:NSUSERDEFAULT_FOR_RIDERMODE object:nil];
    
}

- (void)viewDidDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)dealloc {
   
}

-(void)eventHandler: (NSNotification *) notification {
    
    [self checkDriverMode];
    [self getTripsWithReload:YES];
}


- (void)checkDriverMode {
    isDriver = ![[NSUserDefaults standardUserDefaults] boolForKey:@"riderMode"];
    
    if (isDriver)
    {
        [_scheduleMuVButton setTitle:@"Schedule a MüV" forState:UIControlStateNormal];
    }
    else {
        [_scheduleMuVButton setTitle:@"Request a MüV" forState:UIControlStateNormal];
    }
}




- (void)customiseControls {
    _scheduleMuVButton = [UIButton muv_buttonUpdate:_scheduleMuVButton];
    _pastTripsButton = [UIButton muv_buttonUpdate:_pastTripsButton];
    _pendingButton = [UIButton muv_buttonUpdate:_pendingButton];
    _scheduleMuVButton = [UIButton muv_buttonUpdate:_scheduleMuVButton];
    _ratingDriverImageView = [UIImageView muv_imageCircle:_ratingDriverImageView];
    _driverPicForFare = [UIImageView muv_imageCircle:_driverPicForFare];
    _ratingButton = [UIButton muv_buttonUpdate:_ratingButton];
    _requestReceipt =  [UIButton muv_buttonUpdate:_requestReceipt];
}



- (void)setCurrentDate:(NSDate *)currentDate {
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    calendar.timeZone =[NSTimeZone localTimeZone];
    NSDateComponents *comps = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth fromDate:currentDate];
    
    NSInteger monthIndex = comps.month;
    NSInteger year = comps.year;
    
    while(monthIndex <= 0){
        monthIndex += 12;
    }
    
    static NSDateFormatter *dateFormatter;
    if(!dateFormatter){
        dateFormatter = [NSDateFormatter new];
        dateFormatter.timeZone = [NSTimeZone localTimeZone];
    }
    
    _monthAndYear.text = [NSString stringWithFormat:@"%@ %ld",[[dateFormatter standaloneMonthSymbols][monthIndex - 1] capitalizedString], (long)year];
    
}

- (NSDate *)getDateLoaded {
    NSDateFormatter *dateFormatterA;
    if(!dateFormatterA){
        dateFormatterA = [NSDateFormatter new];
        dateFormatterA.timeZone = [NSTimeZone localTimeZone];
        [dateFormatterA setDateFormat:@"M, yyyy"];
    }
    
    if ([[TripManager sharedManager] dateLoaded]) {
        return [dateFormatterA dateFromString:[[TripManager sharedManager] dateLoaded]];
    }
    else {
        return nil;
    }
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma IBActions
- (IBAction)toggleCalendarAgenda:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MuVSidePanelController *sidePanelViewController = [storyboard instantiateViewControllerWithIdentifier:@"MuVSidePanelViewController"];
    sidePanelViewController.centerPanel = [self.storyboard instantiateViewControllerWithIdentifier:@"CalendarView"];
    [self presentViewController:sidePanelViewController animated:YES completion:nil];
}

- (IBAction)showPastTrips:(id)sender {
    _activityIndicatorView.hidden = NO;
    [_activityIndictor startAnimating];

    RideManager *rideManager = [RideManager new];
    if (isDriver) {
        [rideManager getTripsForDriver:Past forDate:_currentDate withCompletionBlock:^(NSDictionary* trips, NSArray *tripDates, NSError* error) {
            _tripsAgendaView.inputDictionary  = trips;
            _tripsAgendaView.inputSectionArray = tripDates;
            if (!error || trips.count > 0) {
                _tripsAgendaView.sectionInfoArray = nil;
                
                [self setUpForCollapsibleView:YES];
                _noTripsView.hidden = YES;
            }
            else {
                _noTripsView.hidden = NO;
            }
            
            [self showNetworkAlert:error];
            _activityIndicatorView.hidden = YES;
            [_activityIndictor stopAnimating];
        }];
    }
    else {
        [rideManager getRidesForRiderForTripView:Past forDate:_currentDate withCompletionBlock:^(NSDictionary *trips,NSArray *tripDates, NSError *error) {
            _tripsAgendaView.inputDictionary  = trips;
            _tripsAgendaView.inputSectionArray = tripDates;
            if (!error || trips.count > 0) {
                _tripsAgendaView.sectionInfoArray = nil;
                
                [self setUpForCollapsibleView:YES];
                _noTripsView.hidden = YES;
            }
            else {
                _noTripsView.hidden = NO;
            }
            
             [self showNetworkAlert:error];
            _activityIndicatorView.hidden = YES;
            [_activityIndictor stopAnimating];

        }];
    }
}


- (IBAction)loadPrev:(id)sender {
    
    NSCalendar *cal         = [NSCalendar currentCalendar];
    NSDateComponents *comps = [cal components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit
                                     fromDate:_currentDate];
    comps.month             -= 1;
    _currentDate          = [cal dateFromComponents:comps];
    [self setCurrentDate:_currentDate];
    [self getTripsWithReload:NO];
    
}

- (IBAction)loadNext:(id)sender {
    NSCalendar *cal         = [NSCalendar currentCalendar];
    NSDateComponents *comps = [cal components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit
                                     fromDate:_currentDate];
    comps.month             += 1;
    _currentDate          = [cal dateFromComponents:comps];
    [self setCurrentDate:_currentDate];
    [self getTripsWithReload:NO];
}


- (IBAction)startMuv:(id)sender {
    
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"navAddr"];
  
    
    [self loadTripDetailsPage];
    
    [self.tripsAgendaView deselectRowAtIndexPath:path animated:NO];
}

- (IBAction)dontStartMuv:(id)sender {
    [_popUpBackgroundView removeFromSuperview];
    [_cantCancelMuvPopup removeFromSuperview];
    [self.tripsAgendaView deselectRowAtIndexPath:path animated:NO];
}

//Agenda Table View Delegates
- (void)getPassengerDetail:(UserDetail *)userDetailA andSender:(id)sender {
    
    if (userDetailA != nil) {
        userDetail = userDetailA;
        [self performSegueWithIdentifier:@"PassengerDetail" sender:sender];
    }
}

- (void)goToTripDetails:(Trip *)tripA andSender:(id)sender {
    
    if (tripA != nil) {
        trip = tripA;
        if (isDriver) {
            trip.isCurrent = NO;
        }
        
        [self loadTripDetailsPage];
    }
}

- (void)loadTripDetailsPage {
    [[TripManager sharedManager] setCurrentTrip:trip];
    [[TripManager sharedManager] setCurrentTripDriver:trip];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MuVSidePanelController *sidePanelViewController = [storyboard instantiateViewControllerWithIdentifier:@"MuVSidePanelViewController"];
    sidePanelViewController.centerPanel = [self.storyboard instantiateViewControllerWithIdentifier:@"TripDetailNav"];
    [self presentViewController:sidePanelViewController animated:YES completion:nil];
}



- (void)startMuv:(Trip *)tripA forIndexPath:indexPath andSender:(id)sender {
    if (tripA != nil) {
        
        trip = tripA;
        trip.isCurrent = YES;
        path = indexPath;
        
        [self.view addSubview:_popUpBackgroundView];
        [self.view addSubview:_cantCancelMuvPopup];
    }
}


- (void)getDriverDetail:(Ride *)rideA andSender:(id)sender {
    
    if (rideA != nil) {
        ride = rideA;
        if (ride.driverDetail) {
            [self performSegueWithIdentifier:@"DriverDet" sender:sender];
        }
    }
}

- (void)cancelMuVForPassenger:(Ride *)rideToCancel andSender:(id)sender {
    
    NSLog(@"ride to cancel is %@", rideToCancel.rideNo);
    
    if (rideToCancel.rideNo[@"rideNo"]) {
        if (rideToCancel != nil) {
            BOOL isArrival = YES;
            ride = rideToCancel;
            
            
            if ([rideToCancel.routeType isEqualToString:ARRIVAL]) {
                isArrival = YES;
            }
            else {
                isArrival = NO;
            }
            
            NSString *cancellationMessage;
            NSInteger cancellationTime = [self checkTimeDifferenceBetweenDates:rideToCancel.rideDate andRouteType:isArrival];
            
            if (cancellationTime == MoreThan24Hours) {
                cancellationMessage = @"Are you sure you want to cancel your MüV? \n You are canceling more than 24 hrs before the ride, No cancellation fees will apply.";
                _cancelType = @1;
            }
            else if (cancellationTime == Between4And24Hours) {
                cancellationMessage = @"Are you sure you want to cancel your MüV? \n You are canceling less than 24 hrs before the ride. \n A cancellation fee of $5 will apply.";
                _cancelType = @2;
            }
            else  {
                cancellationMessage = @"Are you sure you want to cancel your MüV? \n You are canceling less than 4 hrs before the ride. \n Full Fare Charges will apply.";
                _cancelType = @3;
            }
            _cancellationMessage.text = cancellationMessage;
          
        }
    }
    else {
        ride = rideToCancel;
        NSString *cancellationMessage;
        cancellationMessage = @"Are you sure you want to cancel your MüV?";
        _cancellationMessage.text = cancellationMessage;
    }
    [self addCancelPopUpViewforDriver];
}

-(void)cancelMuvDriver:(Trip *)tripA forIndexPath:(NSIndexPath *)indexPath andSender:(id)sender {
    if (tripA != nil) {
        path = indexPath;
        BOOL isArrival;
        //has passengers
        NSString *cancellationMessage;
        
        if (tripA.userDetails.count > 0) {
            
            if ([tripA.route[@"routeType"] isEqualToString:ARRIVAL]) {
                isArrival = YES;
            }
            else {
                isArrival = NO;
            }
            
            NSInteger cancellationTime = [self checkTimeDifferenceBetweenDates:tripA.route[@"rideDate"] andRouteType:isArrival];
            
            if (cancellationTime == MoreThan24Hours) {
                cancellationMessage = @"Are you sure you want to cancel your MüV? \n You are canceling more than 24 hrs before the ride, No cancellation fees will apply.";
                _cancelType = @1;
            }
            else {
                cancellationMessage = @"Are you sure you want to cancel your MüV? \n You are canceling less than 24 hrs before the ride. \n A cancellation fee of $5/ passenger will apply.";
                _cancelType = @2;
            }
        }
        //no passengers
        else {
            cancellationMessage = @"Are you sure you want to cancel your MüV? \n You don't have any passengers booked for this ride. \n No cancellation fees will apply.";
            _cancelType = @1;
        }
        trip = tripA;
        _cancellationMessage.text = cancellationMessage;
        [self addCancelPopUpViewforDriver];
    }
}


- (NSInteger)checkTimeDifferenceBetweenDates:(NSDate *)rideDate andRouteType:(BOOL)isArrival {
    NSDate *currentDate = [NSDate date];
    
    NSDateComponents *components;
    NSInteger days;
    NSInteger hours;
    
    components = [[NSCalendar currentCalendar] components: NSDayCalendarUnit | NSHourCalendarUnit
                                                 fromDate: currentDate toDate: rideDate options: 0];
    
    days = [components day];
    hours = [components hour];
    NSInteger hoursBefore = 6;
    
    if (adminManager.adminSettings.hoursBeforeArrivalForFee) {
        if (isArrival) {
            hoursBefore = [adminManager.adminSettings.hoursBeforeArrivalForFee intValue];
        }
        else {
            hoursBefore = [adminManager.adminSettings.hoursBeforeDepartureForFee intValue];
        }
    }
    if (days >= 1) {
        return MoreThan24Hours;
    }
    else {
        if (hours > hoursBefore) {
            return Between4And24Hours;
        }
        else {
            return LessThan4Hours;
        }
    }
}


- (IBAction)contactMuvSupport:(id)sender {
    [self removePopUpViewforDriver];
}

- (IBAction)cancelRide:(id)sender {
    
    mailManager = [MailManager new];
    
    if (isDriver) {
        //mark drivers ride as cancelled
        trip.route[@"isCancelled"] = [NSNumber numberWithBool:YES];
        
        //SMN test
        trip.route[@"cancelTime"] = [NSDate date];
        [trip.route setObject:[NSNull null] forKey:@"calendarEventId"];
        
        //mark his passengers ride as cancelled
        [rideManager updatePFObject:trip.route withCompletionBlock:^(BOOL succeeded, NSError *error) {
            if (succeeded) {
                for (UserDetail *userDetail in trip.userDetails) {
                    userDetail.rideObj[@"isCancelledByDriver"] = [NSNumber numberWithBool:YES];
                    [userDetail.rideObj setObject:[NSNull null] forKey:@"calendarEventId"];
                    [rideManager updateRideObject:userDetail.rideObj];
                }
                
                // SEND EMAIL RIDER NOTIFICATION OF DRIVER'S CANCELATION
                [mailManager sendRiderNotificationWhenDriverCancelsWithRoute:trip.route];
                
                // SEND EMAIL TO ADMIN OF DRIVER'S CANCELATION
                [mailManager sendDriverCancelsAdminAlertWithRoute:trip.route];
                
                // SEND EMAIL TO DRIVER OF CANCELATION DEPENDING ON SCENARIO
                if ([_cancelType  isEqual: @1]){
                    [mailManager sendRideCancelingByDriver1WithRoute:trip.route];
                } else {
                    [mailManager sendRideCancelingByDriver2WithRoute:trip.route];
                }
                
               // [[TripManager sharedManager] setDateFormat:nil];
                [self getTripsWithReload:NO];
            }
        }];
    }
    else {
        if (ride.rideNo[@"rideNo"]) {
            isRoundTripEmailSent = NO;
            
            if ([_cancelType isEqual:@1]) {
                [mailManager sendRiderCancelsMuVGetsRefundWithRide:ride.rideNo];
                
            } else if ([_cancelType isEqual:@2]) {
                [mailManager sendRiderCancelsMuVGetsChargedFiveWithRide:ride.rideNo];
                [paymentManager createCancellationChargeWithRide:ride.rideNo withCompletionBlock:^(BOOL succeeded, NSError *error){
                    NSLog(@"done: %@", error);
                }];
            } else {
                NSLog(@"3 cancel");
                [mailManager sendRiderCancelsMuVDoesNotGetRefundWithRide:ride.rideNo];
                [paymentManager chargePaymentWithRide:ride.rideNo withCompletionBlock:^(BOOL succeeded, NSError *error){
                    NSLog(@"done: %@", error);
                }];
            }
            
            // SEND EMAIL TO DRIVER OF RIDER'S CANCELATION
            [mailManager sendDriverNotificationForRiderCancelationWithRide:ride.rideNo];
            
            [self cancelTripAndCreditUser:ride.rideNo];
        }
        else {
            [self cancelRequestedTrip:ride.rideNo];
        }
        
    }
    [self removePopUpViewforDriver];
}

- (void)cancelRequestedTrip:(PFObject *)rideObj  {
    rideObj[@"cancelled"] = [NSNumber numberWithBool:YES];
    [rideManager cancelRide:rideObj withCompletionBlock:^(BOOL succeeded, NSError *error) {
        
    }];
     
    [[TripManager sharedManager] setDateLoaded:nil];
    [self getTripsWithReload:NO];
}

- (void)cancelTripAndCreditUser:(PFObject *)rideObj {
    
    rideObj[@"cancelled"] = [NSNumber numberWithBool:YES];
    NSString *calendarEvent = rideObj[@"calendarEventId"];
    [rideObj setObject:[NSNull null] forKey:@"calendarEventId"];
    
    NSDate *currentDate = [NSDate date];
    NSDate *rideDate = rideObj[@"rideDate"];
    
    NSDateComponents *components;
    NSInteger days;
    NSInteger hours;
    double fare;
    NSInteger hoursBefore = 6;
    
    components = [[NSCalendar currentCalendar] components: NSDayCalendarUnit | NSHourCalendarUnit
                                                 fromDate: currentDate toDate: rideDate options: 0];
    days = [components day];
    hours = [components hour];
    
    fare = [rideObj[@"fare"] floatValue];
    
    if (adminManager.adminSettings.hoursBeforeArrivalForFee) {
        if (rideObj[@"endTime"]) {
            hoursBefore = [adminManager.adminSettings.hoursBeforeArrivalForFee intValue];
        }
        else {
            hoursBefore = [adminManager.adminSettings.hoursBeforeDepartureForFee intValue];
        }
    }
    
    if (days >= 1) {
        //more than 24 hours. no charges to user
    }
    else {
        if (hours > hoursBefore) {
            //greater than 4 hours, less than 24 hours
            
        }
        else {
            //charge fare amount to user
        }
    }
    
    [rideManager cancelRide:rideObj withCompletionBlock:^(BOOL succeeded, NSError *error) {
        if (succeeded) {
            
            [rideObj[@"rideNo"] removeObject:rideObj forKey:@"rides"];
            //update Occupied Seat
            NSNumber *occupiedSeats = rideObj[@"rideNo"][@"occupiedSeats"];
            int value = [occupiedSeats intValue];
            occupiedSeats = [NSNumber numberWithInt:value - 1];
            rideObj[@"rideNo"][@"occupiedSeats"] = occupiedSeats;
            [rideManager updatePFObject:rideObj[@"rideNo"]];
            
            if (calendarEvent) {
                EventKitManager *eventKit = [EventKitManager new];
                [eventKit deleteEventWithIdentifier:calendarEvent];
            }
            
          //  [[TripManager sharedManager] setDateFormat:nil];
            [[TripManager sharedManager] setDateLoaded:nil];
            [self getTripsWithReload:NO];
        }
    }];
}

- (IBAction)removeRatingPopup:(id)sender {
    [_popUpBackgroundView removeFromSuperview];
    [_ratingPopUpView removeFromSuperview];
}


- (IBAction)removeFarePopup:(id)sender {
    [_popUpBackgroundView removeFromSuperview];
    [_farePopUpView removeFromSuperview];
}

- (IBAction)dontCancelRide:(id)sender {
    [self removePopUpViewforDriver];
    [self.tripsAgendaView deselectRowAtIndexPath:path animated:NO];
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)ratingViewButtonTapped:(id)sender {
    
    switch ([sender tag]) {
        case RateButton:
            
            if (_selectedRating) {
                [ratingManager createRatingWithReview:_reviewTextField.text andRating:_selectedRating ForRide:ride];
            }
            
            [[TripManager sharedManager] setDateLoaded:nil];
            [self getTripsWithReload:NO];
            [_popUpBackgroundView removeFromSuperview];
            [_ratingPopUpView removeFromSuperview];

            break;
        case starOne:
            [self updateStarsWithRating:@1];
            break;
        case starTwo:
            [self updateStarsWithRating:@2];
            break;
        case starThree:
            [self updateStarsWithRating:@3];
            break;
        case starFour:
            [self updateStarsWithRating:@4];
            break;
        case starFive:
            [self updateStarsWithRating:@5];
            break;
        default:
            break;
    }
    
}

- (void)goToRatingView:(Ride *)rideObject andSender:(id)sender {
    
    ride = rideObject;
    
    PFFile *driverPhoto = ride.driverDetail[@"userdetail" ][@"profileImage"];
    [_ratingDriverImageView sd_setImageWithURL:[NSURL URLWithString:driverPhoto.url]];
    NSString *lastInitial = [[ride.driverDetail[@"userdetail"][@"lastName"] substringToIndex:1]uppercaseString];
    _ratingDriverNameLabel.text = [NSString stringWithFormat:@"%@ %@.", ride.driverDetail[@"userdetail"][@"firstName"],lastInitial];
    
    [self.view addSubview:_popUpBackgroundView];
    [self.view addSubview:_ratingPopUpView];
    
}

- (IBAction)requestReceipt:(id)sender {
    // SEND EMAIL TO RIDER OF HIS RECEIPT
    [mailManager sendRiderRequestsReceiptWithRide:ride.rideNo];
    [_popUpBackgroundView removeFromSuperview];
    [_farePopUpView removeFromSuperview];
}


- (void)showFareView:(Ride *)rideObject andSender:(id)sender {
    
    ride = rideObject;
    
    PFFile *driverPhoto = ride.driverDetail[@"userdetail" ][@"profileImage"];
    [_driverPicForFare sd_setImageWithURL:[NSURL URLWithString:driverPhoto.url] placeholderImage:[UIImage imageNamed:NOIMAGE_AVATAR]];
    NSString *lastInitial = [[ride.driverDetail[@"userdetail"][@"lastName"] substringToIndex:1]uppercaseString];
    _driverNameForFare.text = [NSString stringWithFormat:@"%@ %@.", ride.driverDetail[@"userdetail"][@"firstName"],lastInitial];
    
 //   NSLog(@"Ride .rideNo %@", ride.rideNo);
    if (ride.rideNo[@"payment"]) {
        PFObject *payment = ride.rideNo[@"payment"];
    //    NSLog(@"payment Object %@", payment);
        
        if (payment[@"paymentDetail"]) {
            _creditCardInfo.text = [NSString stringWithFormat:@"Credit Card xxxx %@", ride.rideNo[@"payment"][@"paymentDetail"][@"lastFourDigits"]];
            _creditCardLabel.image = [UIImage imageNamed:ride.rideNo[@"payment"][@"paymentDetail"][@"cardType"]];
            
            if (payment[@"AmountCharged"]) {
                NSLog(@"Amount Charged");
            }
            float amountCharged = [payment[@"AmountCharged"] floatValue];
    
            _fare.text = [NSString stringWithFormat:@"$ %0.2f", amountCharged];
            
        }
    }
    
    
    [self.view addSubview:_popUpBackgroundView];
    [self.view addSubview:_farePopUpView];
    
}

- (void) editTrip:(Ride *)ride andSender:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MuVSidePanelController *sidePanelViewController = [storyboard instantiateViewControllerWithIdentifier:@"MuVSidePanelViewController"];
    sidePanelViewController.centerPanel = [storyboard instantiateViewControllerWithIdentifier:@"BookingNav"];
    [self presentViewController:sidePanelViewController animated:YES completion:nil];
    
    [[TripManager sharedManager] setEditedRide:ride];
}

- (void) updateStarsWithRating:(NSNumber *)rating{
    NSLog(@"%@", rating);
    _selectedRating = rating;
    
    [_starOne setBackgroundImage:[UIImage imageNamed:@"MuV_RateStar_Gry"] forState:UIControlStateNormal];
    [_starTwo setBackgroundImage:[UIImage imageNamed:@"MuV_RateStar_Gry"] forState:UIControlStateNormal];
    [_starThree setBackgroundImage:[UIImage imageNamed:@"MuV_RateStar_Gry"] forState:UIControlStateNormal];
    [_starFour setBackgroundImage:[UIImage imageNamed:@"MuV_RateStar_Gry"] forState:UIControlStateNormal];
    [_starFive setBackgroundImage:[UIImage imageNamed:@"MuV_RateStar_Gry"] forState:UIControlStateNormal];
    
    if ([rating floatValue] > 0){
        [_starOne setBackgroundImage:[UIImage imageNamed:@"MuV_RateStar_Blu"] forState:UIControlStateNormal];
    }
    if ([rating floatValue] > 1) {
        [_starTwo setBackgroundImage:[UIImage imageNamed:@"MuV_RateStar_Blu"] forState:UIControlStateNormal];
    }
    if ([rating floatValue] > 2) {
        [_starThree setBackgroundImage:[UIImage imageNamed:@"MuV_RateStar_Blu"] forState:UIControlStateNormal];
    }
    if ([rating floatValue] > 3) {
        [_starFour setBackgroundImage:[UIImage imageNamed:@"MuV_RateStar_Blu"] forState:UIControlStateNormal];
    }
    if ([rating floatValue] > 4) {
        [_starFive setBackgroundImage:[UIImage imageNamed:@"MuV_RateStar_Blu"] forState:UIControlStateNormal];
    }
}

- (void)removePopUpViewforDriver {
    
    [_popUpBackgroundView removeFromSuperview];
    [_cancelPopUpView removeFromSuperview];
    
}

- (void)addCancelPopUpViewforDriver {
    
    [self.view addSubview:_popUpBackgroundView];
    [self.view addSubview:_cancelPopUpView];
}


- (IBAction)showPendingTrips:(id)sender {
    RideManager *rideManager = [RideManager new];
    if (isDriver) {
        [rideManager getTripsForDriver:PendingForDriver forDate: _currentDate withCompletionBlock:^(NSDictionary* trips,NSArray *tripDates,  NSError* error) {
            _tripsAgendaView.inputDictionary  = trips;
            _tripsAgendaView.inputSectionArray = tripDates;
            if (!error || trips.count > 0) {
                _tripsAgendaView.sectionInfoArray = nil;
                [self setUpForCollapsibleView:YES];
                
            }
        }];
    }
    else {
        
        [rideManager getRidesForRiderForTripView:PendingForRider forDate:_currentDate withCompletionBlock:^(NSDictionary *trips,NSArray *tripDates, NSError *error) {
            _tripsAgendaView.inputDictionary  = trips;
            _tripsAgendaView.inputSectionArray = tripDates;
            if (!error || trips.count > 0) {
                _tripsAgendaView.sectionInfoArray = nil;

                [self setUpForCollapsibleView:YES];
            }
        }];
    }
}

- (IBAction)showScheduledTrips:(id)sender {
    _activityIndicatorView.hidden = NO;
    [_activityIndictor startAnimating];

    RideManager *rideManager = [RideManager new];
    if (isDriver) {
        [rideManager getTripsForDriver:ScheduledForDriver forDate:_currentDate withCompletionBlock:^(NSDictionary* trips, NSArray *tripDates, NSError* error) {
            _tripsAgendaView.inputDictionary  = trips;
            _tripsAgendaView.inputSectionArray = tripDates;
            
            if (!error || trips.count > 0) {
                _tripsAgendaView.sectionInfoArray = nil;

                [self setUpForCollapsibleView:YES];
            }
            
             [self showNetworkAlert:error];
            _activityIndicatorView.hidden = YES;
            [_activityIndictor stopAnimating];

        }];
    }
    else {
        [rideManager getRidesForRiderForTripView:ScheduledForRider forDate:_currentDate withCompletionBlock:^(NSDictionary *trips,NSArray *tripDates, NSError *error) {
            _tripsAgendaView.inputDictionary  = trips;
            _tripsAgendaView.inputSectionArray = tripDates;
            if (!error || trips.count > 0) {
                _tripsAgendaView.sectionInfoArray = nil;

                [self setUpForCollapsibleView:YES];
            }
            
             [self showNetworkAlert:error];
            _activityIndicatorView.hidden = YES;
            [_activityIndictor stopAnimating];

        }];
    }
}



// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString: @"PassengerDetail"]) {
        MuVPassengerDetailsViewController *passDetails = [segue destinationViewController];
        passDetails.userDetail = userDetail;
        return;
    }
    if ([[segue identifier] isEqualToString: @"TripsDetail"]) {
        
        
        MuVTripEnRouteViewController *tripDetails = [segue destinationViewController];
        tripDetails.riderTrip = trip;
        
        return;
    }
    if ([[segue identifier] isEqualToString: @"DriverDet"]) {
        MuVDriverViewController *driverDetails = [segue destinationViewController];
        driverDetails.ride = ride;
        return;
    }
}



- (void) getTripsWithReload:(BOOL)reload {
    _activityIndicatorView.hidden = NO;
    [_activityIndictor startAnimating];
    TripManager *tripManager = [TripManager sharedManager];
    
    if ([Helper isCurrentMonth:_currentDate]) {
        reload = YES;
    }
    
    if (![[[TripManager sharedManager] dateLoaded] isEqualToString:_monthAndYear.text] || reload) {
        RideManager *rideManager = [RideManager new];
        
        if (isDriver){
            [rideManager getTripsForDriver:Agenda forDate:_currentDate withCompletionBlock:^(NSDictionary* trips,NSArray *tripDates, NSError* error) {
                
                tripManager.rides = trips;
                tripManager.tripDates = tripDates;
                
                _tripsAgendaView.inputDictionary  = trips;
                _tripsAgendaView.inputSectionArray = tripDates;
                
                if (reload) {
                    [self setUpForCollapsibleView:NO];

                }
                else {
                    [self setUpForCollapsibleView:YES];
                }
                
                if (trips.count > 0) {
                    _noTripsView.hidden = YES;
                }
                else {
                    _noTripsView.hidden = NO;
                }
                 [self showNetworkAlert:error];
                [_activityIndictor stopAnimating];
                _activityIndicatorView.hidden = YES;
            }];
        } else {
            [rideManager getRidesForRiderForTripView:Agenda forDate:_currentDate withCompletionBlock:^(NSDictionary *trips,NSArray *tripDates, NSError *error) {
                
                tripManager.rides = trips;
                tripManager.tripDates = tripDates;
                
                _tripsAgendaView.inputDictionary  = trips;
                _tripsAgendaView.inputSectionArray = tripDates;
                
                if (reload) {
                    [self setUpForCollapsibleView:NO];
                    
                }
                else {
                    [self setUpForCollapsibleView:YES];
                }
                if (trips.count > 0) {
                    _noTripsView.hidden = YES;
                }
                else {
                    _noTripsView.hidden = NO;
                }
                 [self showNetworkAlert:error];
                [_activityIndictor stopAnimating];
                _activityIndicatorView.hidden = YES;
            }];
        }
    }
    else {
        _tripsAgendaView.inputDictionary  =  tripManager.rides;
        _tripsAgendaView.inputSectionArray = tripManager.tripDates;
        [self setUpForCollapsibleView:NO];
        [_activityIndictor stopAnimating];
        _activityIndicatorView.hidden = YES;
        
    }
    [[TripManager sharedManager] setDateLoaded:_monthAndYear.text];
}

- (void) setUpForCollapsibleView:(BOOL)isPastPendingOrScheduled {
   // if (_tripsAgendaView.sectionInfoArray == nil) {
        
        int row = 0;
        NSMutableArray *array = [[NSMutableArray alloc] init];
        for (NSString *title in _tripsAgendaView.inputSectionArray) {
            SectionInfo *section = [[SectionInfo alloc] init];
            section.sectionTitle = title;
            
            if (row == 0 || row == 1) {
                section.open = YES;
            }
            else {
                section.open = NO;
            }
            
            NSNumber *defaultHeight = [NSNumber numberWithInt:63];
            
            NSArray *sectionInputArray = [_tripsAgendaView.inputDictionary objectForKey:title];
            NSInteger count = sectionInputArray.count;
            
            for (NSInteger i= 0; i<count; i++) {
                [section insertObject:defaultHeight inRowHeightsAtIndex:i];
            }
            [array addObject:section];
            row++;
        }
        _tripsAgendaView.sectionInfoArray = array;
   // }
//    else {
//        if (!isPastPendingOrScheduled) {
//            _tripsAgendaView.sectionInfoArray = nil;
//        }
//    }
    
    [_tripsAgendaView reloadData];
}



- (IBAction)scheduleBookMuv:(id)sender {
    
    [[TripManager sharedManager] setEditedRide:nil];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MuVSidePanelController *sidePanelViewController = [storyboard instantiateViewControllerWithIdentifier:@"MuVSidePanelViewController"];
    sidePanelViewController.centerPanel = [storyboard instantiateViewControllerWithIdentifier:@"BookingNav"];
    [self presentViewController:sidePanelViewController animated:YES completion:nil];
    
}

-(void)showNetworkAlert:(NSError *)error {
    if (error.code == 100 || error.code == -1001 || error.code == -1005) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning"
                                                        message:@"Please check your network connection"
                                                       delegate:nil
                                              cancelButtonTitle:nil
                                              otherButtonTitles:@"Dismiss", nil];
        
        [alert show];
    }
}



@end
