//
//  MuVTripCalendarViewController.h
//  MuV
//
//  Created by SushmaN on 1/28/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JTCalendar.h"
#import "AgendaViewOfRides.h"

typedef enum{
    
    RateButton1 = -6,
    starOne1 = -1,
    starTwo1 = -2,
    starThree1 = -3,
    starFour1 = -4,
    starFive1 = -5,
    
}calendarRatingButtons;

@interface MuVTripCalendarViewController : UIViewController <JTCalendarDataSource, TableViewOfTripsProtocol,UITextFieldDelegate>
//@property (strong, nonatomic) UIViewController *myTripsViewController;

@property (weak, nonatomic) IBOutlet UIView *activityIndicatorView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end
