//
//  TableViewCellOfAgendaRides.h
//  MuV
//
//  Created by SushmaN on 1/16/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewCellOfAgendaRides : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *rideType;
@property (weak, nonatomic) IBOutlet UILabel *rideTime;
@property (weak, nonatomic) IBOutlet UILabel *routeType;


@property (weak, nonatomic) IBOutlet UIButton *driverImageButton;
@property (weak, nonatomic) IBOutlet UILabel *driverName;
@property (weak, nonatomic) IBOutlet UIButton *cancelRateButton;
@property (weak, nonatomic) IBOutlet UIButton *tripDetailsButton;


@property (weak, nonatomic) IBOutlet UIButton *passenger1Button;
@property (weak, nonatomic) IBOutlet UILabel *passenger1ArrivalTime;

@property (weak, nonatomic) IBOutlet UIButton *passenger2Button;
@property (weak, nonatomic) IBOutlet UILabel *passenger2ArrivalTime;

@property (weak, nonatomic) IBOutlet UIButton *passenger3Button;
@property (weak, nonatomic) IBOutlet UILabel *passenger3ArrivalTime;

@property (weak, nonatomic) IBOutlet UIButton *passenger4Button;
@property (weak, nonatomic) IBOutlet UILabel *passenger4ArrivalTime;

@property (weak, nonatomic) IBOutlet UIButton *passenger5Button;
@property (weak, nonatomic) IBOutlet UILabel *passenger5ArrivalTime;



@end
