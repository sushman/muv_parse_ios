//
//  TableViewOfRides.h
//  MuV
//
//  Created by SushmaN on 1/7/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface TableViewOfRides : UITableView <UITableViewDataSource, UITableViewDelegate>


@property(strong, nonatomic) NSArray *inputArray;
@property(assign, nonatomic) BOOL isDriver;


@end
