//
//  AgendaViewOfRides.m
//  MuV
//
//  Created by SushmaN on 1/9/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "AgendaViewOfRides.h"
#import "TableViewCellOfAgendaRides.h"
#import "RideManager.h"
#import "Ride.h"
#import "SectionHeaderCell.h"
#import "Helper.h"
#import "ControlVariables.h"
#import "UIButton+MuVUIButtonAdditions.h"
#import "UIImageView+MuVUIImageViewAdditions.h"
#import "UILabel+MuVFontColorSize.h"
#import "UIImageView+WebCache.h"
#import "UIButton+WebCache.h"
#import "UserDetail.h"


@implementation AgendaViewOfRides


enum{
    
    Driver = 0,
    PassOne = 1,
    PassTwo = 2,
    PassThree = 3,
    PassFour = 4,
    PassFive = 5,
    DriverName = 6
    
} userDetailButton;

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

Ride *ride;
RideManager *ridemanager;
NSMutableArray *ridesForDate;
BOOL isDriver;
NSMutableDictionary *dictOfUserDetails;
NSDateFormatter *dateFormat;
SectionInfo *sectionInfo;
BOOL rowSelected;

long sectionNumber;
long rowNumber;
NSIndexPath *path;

- (id)initWithFrame:(CGRect)frame
{
    if ((self = [super initWithFrame:frame]))
    {

    }
    
    return self;
}


- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.delegate = self;
        self.dataSource = self;
        self.inputArray = [[NSMutableArray alloc] init];
        self.inputSectionArray = [[NSMutableArray alloc] init];
        ridemanager = [RideManager new];
        dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"h:mm a"];
        isDriver = ![[NSUserDefaults standardUserDefaults] boolForKey:@"riderMode"];
    }
    return self;
}


- (Ride *)getRideObjectForIndexPath:(NSIndexPath *)indexPath {
    Ride* ride;
    if (_inputSectionArray != nil && _inputSectionArray.count > 0) {
        NSString *sectionTitle = [_inputSectionArray objectAtIndex:indexPath.section];
        NSArray *sectionData = [_inputDictionary objectForKey:sectionTitle];
        ride = (Ride *)  [sectionData objectAtIndex:indexPath.row];
    }
    else {
        ride = (Ride *)  [_inputArray objectAtIndex:indexPath.row];
    }
    return ride;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TableViewCellOfAgendaRides *newCell = (TableViewCellOfAgendaRides *)[tableView dequeueReusableCellWithIdentifier: @"agendaRideCell" forIndexPath:indexPath];
    
    Ride* ride = [self getRideObjectForIndexPath:indexPath];
    
    newCell.rideTime.text = [NSString stringWithFormat:@"%@",ride.routeType];
    if (ride.trip.tripDate) {
        newCell.rideTime.text = [dateFormat stringFromDate:ride.trip.tripDate];
    }
    else {
        newCell.rideTime.text = [dateFormat stringFromDate:ride.rideDate];
    }
    newCell.routeType.text = ride.routeType;
    
    //these cells can be seen depending on wther user is driver or rider.
    if (isDriver) {
        newCell.driverImageButton.hidden = YES;
        newCell.driverName.hidden = YES;
        newCell.tripDetailsButton.hidden = NO;
    }
    else {
        newCell.driverImageButton.hidden = NO;
        newCell.driverName.hidden = NO;
        newCell.cancelRateButton.hidden = NO;
        newCell.tripDetailsButton.hidden = NO;
    }
    
    if ([Helper isPastDay:ride.rideDate ]) {
        
        newCell.rideType.backgroundColor = UICOLOR_PAST_TRIPS;
        
        if (isDriver) {
            [self setUpForDriversPastRide:newCell];
        }
        else {
            if (ride.isRequested) {
                [self setUpForRidersPastRequestedRide:newCell];
            }
            else {
                [self setUpForRidersPastScheduledRide:newCell andRide:ride];
            }
        }
    }
    else {
        if (ride.isRequested) {
            [self setUpCellsForRequestedRide:newCell];
        }
        else {
            [self setUpCellsForScheduledRide:newCell andRide:ride];
        }
    }
    
    // show hide passenger buttons depending on whether a driver or rider.
    NSArray *arrayOfPassengerButtons = [NSArray arrayWithObjects:newCell.passenger1Button,newCell.passenger2Button,newCell.passenger3Button,newCell.passenger4Button,newCell.passenger5Button, nil];
    NSArray *arrayOfPassengerTimeLabels = [NSArray arrayWithObjects:newCell.passenger1ArrivalTime,newCell.passenger2ArrivalTime,newCell.passenger3ArrivalTime,newCell.passenger4ArrivalTime,newCell.passenger5ArrivalTime, nil];
    
    [self showHidePassDetailsForRide:ride andPassBtns:arrayOfPassengerButtons andPassLbls:arrayOfPassengerTimeLabels];
    
    return newCell;
}

- (void)setUpForDriversPastRide:(TableViewCellOfAgendaRides *)newCell {
    newCell.cancelRateButton.hidden = YES;
    [newCell.tripDetailsButton setTitle:@"Trip Details" forState:UIControlStateNormal];
    [newCell.tripDetailsButton setBackgroundColor:MUV_BLUE_UICOLOR];
}

- (void)setUpForRidersPastRequestedRide:(TableViewCellOfAgendaRides *)newCell {
    newCell.cancelRateButton.hidden = YES;
    newCell.tripDetailsButton.hidden = YES;
    [newCell.driverImageButton setBackgroundImage:[UIImage imageNamed:@"MuV_NoMatchIcon"] forState:UIControlStateNormal];
    newCell.driverName.text = @"";
}

- (void)setUpForRidersPastScheduledRide:(TableViewCellOfAgendaRides *)newCell andRide:(Ride *)ride {
    newCell.cancelRateButton.hidden = NO;
    newCell.tripDetailsButton.hidden = NO;
    [newCell.tripDetailsButton setTitle:@"Fare" forState:UIControlStateNormal];
    [newCell.tripDetailsButton setBackgroundColor:MUV_GREEN_UICOLOR];

    [self showDriversDetails:newCell forRide:ride andIsPast:YES andScheduled:!ride.isRequested];
    
    if (ride.hasBeenRated) {
        newCell.cancelRateButton.hidden = YES;
    }
    else {
        [newCell.cancelRateButton setTitle:@"Rate Driver" forState:UIControlStateNormal];
    }
}

- (void)setUpCellsForRequestedRide:(TableViewCellOfAgendaRides *)newCell {
    
    newCell.rideType.backgroundColor = UICOLOR_PENDING_TRIP;
    
    if (!isDriver) {
        newCell.cancelRateButton.hidden = NO;
        [newCell.cancelRateButton setTitle:@"Cancel" forState:UIControlStateNormal];
    }
    else {
        newCell.cancelRateButton.hidden = YES;
    }
   
    [newCell.tripDetailsButton setTitle:@"Edit" forState:UIControlStateNormal];
    [newCell.tripDetailsButton setBackgroundColor:UICOLOR_PENDING_TRIP];
    [newCell.driverImageButton setBackgroundImage:[UIImage imageNamed:@"MuV_NoMatchIcon"] forState:UIControlStateNormal];
    newCell.driverName.text = @"";
    
}

- (void)setUpCellsForScheduledRide:(TableViewCellOfAgendaRides *)newCell andRide:(Ride *)ride {
    newCell.rideType.backgroundColor = UICOLOR_SCHEDULED_TRIP;
    newCell.cancelRateButton.hidden = NO;
    [newCell.tripDetailsButton setTitle:@"Trip Details" forState:UIControlStateNormal];
    [newCell.tripDetailsButton setBackgroundColor:MUV_BLUE_UICOLOR];
    [newCell.cancelRateButton setTitle:@"Cancel" forState:UIControlStateNormal];
    
    if (!isDriver) {
        [self showDriversDetails:newCell forRide:ride andIsPast:NO andScheduled:YES];
        
        if (ride.rideNo[@"pickUpTime"] || ride.trip.route[@"hasEnded"]) {
            
            if (ride.hasBeenRated) {
                newCell.cancelRateButton.hidden = YES;
            }
            else {
                newCell.cancelRateButton.hidden = NO;
                [newCell.cancelRateButton setTitle:@"Rate Driver" forState:UIControlStateNormal];
            }
            
            [newCell.tripDetailsButton setTitle:@"Fare" forState:UIControlStateNormal];
            [newCell.tripDetailsButton setBackgroundColor:MUV_GREEN_UICOLOR];

        }
        else {
            [newCell.cancelRateButton setTitle:@"Cancel" forState:UIControlStateNormal];
            [newCell.tripDetailsButton setTitle:@"Trip Details" forState:UIControlStateNormal];
            [newCell.tripDetailsButton setBackgroundColor:MUV_BLUE_UICOLOR];

        }
    }
    else {
        newCell.cancelRateButton.hidden = YES;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    sectionNumber = indexPath.section;
    rowNumber = indexPath.row;
    rowSelected = YES;
    path = indexPath;
}

- (void)showDriversDetails:(TableViewCellOfAgendaRides *)newCell forRide:(Ride *)ride andIsPast:(BOOL)isPast andScheduled:(BOOL)isScheduled {
    
    newCell.driverImageButton = [UIButton muv_buttonCircle:newCell.driverImageButton];
    if (ride.driverDetail[@"userdetail"]) {
        newCell.driverName.text = [NSString stringWithFormat:@"%@ %@.", ride.driverDetail[@"userdetail"][@"firstName"], [ride.driverDetail[@"userdetail"][@"lastName"] substringToIndex:1]];
        PFFile *imageFile = ride.driverDetail[@"userdetail"][@"profileImage"];
        
        if (imageFile) {
            [newCell.driverImageButton sd_setBackgroundImageWithURL:[NSURL URLWithString:imageFile.url] forState:UIControlStateNormal];
        }
        else {
            [newCell.driverImageButton setBackgroundImage:[UIImage imageNamed:NOIMAGE_AVATAR] forState:UIControlStateNormal];
        }
        newCell.driverName = [UILabel muv_CustomFontColorSize:newCell.driverName];
    }
}

- (void) showHidePassDetailsForRide:(Ride *)rideA andPassBtns:(NSArray *)passBtns andPassLbls:(NSArray *)passLbls {
    if (isDriver){
        //show all the passenger buttons
        dictOfUserDetails = [[NSMutableDictionary alloc] init];
        int totalPassSeats = [rideA.totalSeats intValue];
        if (totalPassSeats > 5) {
            totalPassSeats = 5;
        }
        
        for (int totalButtonCount = totalPassSeats; totalButtonCount < 5; totalButtonCount ++) {
            UIButton *button = passBtns[totalButtonCount];
            button.hidden = YES;
            UILabel *label = passLbls[totalButtonCount];
            label.hidden = YES;
        }
        
        for (int passengers = 0; passengers < rideA.trip.userDetails.count; passengers++) {
            [self setPassengerButton:passBtns[passengers] andLabel:passLbls[passengers] withPassengerDetail:rideA.trip.userDetails[passengers]];
        }
        
        for (NSUInteger numEmptySeats = rideA.trip.userDetails.count; numEmptySeats < totalPassSeats; numEmptySeats++) {
            [self setPassengerButton:passBtns[numEmptySeats] andLabel:passLbls[numEmptySeats] withPassengerDetail:nil];
        }
      

        
        
    }
    //else passenger - hide all the passenger buttons
    else {
        
        //NSLog(@"Rider Hiding passenegr buttons");
        for (int passengerBtn = 0; passengerBtn < 5; passengerBtn++) {
            UIButton *button = passBtns[passengerBtn];
            button.hidden = YES;
            UILabel *label = passLbls[passengerBtn];
            label.hidden = YES;
        }
    }
    
}

- (void)setPassengerButton:(UIButton *)btnPassenger andLabel:(UILabel *)lblArrivalTime withPassengerDetail:(UserDetail *) passengerDetail {
    
    btnPassenger = [UIButton muv_buttonCircle:btnPassenger];
    btnPassenger.hidden = NO;
    lblArrivalTime = [UILabel muv_CustomFontColorSize:lblArrivalTime];
    if (passengerDetail != nil) {
        
        if (passengerDetail.profileImage!=nil) {
            PFFile *imageFile = passengerDetail.profileImage;
            [btnPassenger sd_setBackgroundImageWithURL:[NSURL URLWithString:imageFile.url] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"MuV_NOimageAvatar"]];
        }
        else {
            [btnPassenger setBackgroundImage:[UIImage imageNamed:@"MuV_NOimageAvatar"] forState:UIControlStateNormal];
        }
        
        if (passengerDetail.startTime) {
            [lblArrivalTime  setText:[dateFormat stringFromDate:passengerDetail.startTime]];
        }
        else {
            [lblArrivalTime  setText:[dateFormat stringFromDate:passengerDetail.endTime]];
        }
        
    }
    else {
        [btnPassenger setBackgroundImage:nil forState:UIControlStateNormal];
        [lblArrivalTime  setText:@"N/A"];
    }
}

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath {
    if (_inputSectionArray != nil && _inputSectionArray.count > 0) {
        // Return the number of rows in the section.
        SectionInfo *array = [self.sectionInfoArray objectAtIndex:indexPath.section];
        
        return [[array objectInRowHeightsAtIndex:indexPath.row] floatValue];
    }
    else {
        return 63;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (_inputSectionArray != nil && _inputSectionArray.count > 0) {
        // Return the number of rows in the section.
        sectionInfo = [_sectionInfoArray objectAtIndex:section];
        NSArray *sectionInputArray = [_inputDictionary objectForKey:sectionInfo.sectionTitle];
        
        return(sectionInfo.open) ? sectionInputArray.count : 0;
    }
    else {
        return _inputArray.count;
    }
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40.0;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSString *title;
    
    if (_inputSectionArray != nil && _inputSectionArray.count > 0) {
        // Return the number of rows in the section.
        sectionInfo = [_sectionInfoArray objectAtIndex:section];
        if (!sectionInfo.sectionView)
        {
            title = sectionInfo.sectionTitle;
            
            sectionInfo.sectionView = [[SectionHeaderCell alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, 40) WithTitle:title Section:section delegate:self expand:YES];
            if (section == 0  || section == 1) {
                [sectionInfo.sectionView.expandBtn setSelected:YES];
            }
        }
    }
    else if (_inputArray.count > 0) {
        title = _sectionTitle;
        sectionInfo = [SectionInfo new];
        sectionInfo.sectionView = [[SectionHeaderCell alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, 40) WithTitle:title Section:section delegate:self expand:NO];
    }
    else {
        return nil;
    }
    
    NSDateFormatter *dateFormatA = [[NSDateFormatter alloc] init];
    [dateFormatA setDateFormat:MUV_DATE_FORMAT_TRIPS];
    
    NSDate *date = [dateFormatA dateFromString:title];
    
    if (date != nil) {
        
        if ([Helper compareDates:date andDateB:[NSDate date]]) {
            sectionInfo.sectionView.cancelMuVBtn.hidden = NO;
        }
        else {
            sectionInfo.sectionView.cancelMuVBtn.hidden = YES;
        }
        
        if ([Helper isSameDay:date otherDay:[NSDate date]]) {
            sectionInfo.sectionView.startMuVBtn.hidden = NO;
            sectionInfo.sectionView.cancelMuVBtn.hidden = NO;
        }
        else {
            sectionInfo.sectionView.startMuVBtn.hidden = YES;
        }
        
    }
    if (!isDriver) {
        sectionInfo.sectionView.startMuVBtn.hidden = YES;
        sectionInfo.sectionView.cancelMuVBtn.hidden = YES;
    }
    
    return sectionInfo.sectionView;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (_inputSectionArray != nil && _inputSectionArray.count > 0) {
        // Return the number of sections.
        return [_inputSectionArray count];
    }
    else {
        return 1;
    }
}

- (IBAction)showEditTripDetails:(id)sender {
    UIButton *button = (UIButton *)sender;
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self];
    NSIndexPath *indexPath = [self indexPathForRowAtPoint:buttonPosition];
    
    Ride *ride = [self getRideObjectForIndexPath:indexPath];
    
    if ([button.titleLabel.text isEqualToString:@"Edit"]) {
        [_tableAlertDelegate editTrip:ride andSender:sender];
    }
    else if ([button.titleLabel.text isEqualToString:@"Fare"]) {
        [_tableAlertDelegate showFareView:ride andSender:sender];
    }
    else {
        [_tableAlertDelegate goToTripDetails:ride.trip andSender:sender];
    }
}

- (IBAction)cancelRateMuvForPassenger:(id)sender {
    
    UIButton *button = (UIButton *)sender;
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self];
    NSIndexPath *indexPath = [self indexPathForRowAtPoint:buttonPosition];
    
    Ride *ride = [self getRideObjectForIndexPath:indexPath];
    
    //if Cancel : Ask user to confirm that the trip is going to be cancelled
    if ([button.titleLabel.text isEqualToString:@"Cancel"]) {
    
        [_tableAlertDelegate cancelMuVForPassenger:ride andSender:sender];
    }
    //else if past trip rate the driver
    else {
        // takes you to rating view
        [_tableAlertDelegate goToRatingView:ride andSender:sender];
    }
}

- (IBAction)showPassengerDetailsButton:(id)sender {
    
    UserDetail *userDetail;
    
    
    UITableViewCell *clickedCell = (UITableViewCell *)[[sender superview] superview];
    NSIndexPath *indexPath = [self indexPathForCell:clickedCell];
    
    ride = [self getRideObjectForIndexPath:indexPath];
    
    switch ([sender tag]) {
        case Driver:
            if (ride != nil) {
                [_tableAlertDelegate getDriverDetail:ride andSender:sender];
            }
            break;
        case PassOne:
            if (ride.trip.userDetails.count > 0) {
                userDetail = ride.trip.userDetails[0];
                if (userDetail != nil) {
                    [_tableAlertDelegate getPassengerDetail:userDetail andSender:sender];
                }
            }
            break;
        case PassTwo:
            if (ride.trip.userDetails.count > 1) {
                userDetail = ride.trip.userDetails[1];
                if (userDetail != nil) {
                    [_tableAlertDelegate getPassengerDetail:userDetail andSender:sender];
                }
            }
            break;
        case PassThree:
            if (ride.trip.userDetails.count > 2) {
                userDetail = ride.trip.userDetails[2];
                if (userDetail != nil) {
                    [_tableAlertDelegate getPassengerDetail:userDetail andSender:sender];
                }
            }
            break;
        case PassFour:
            if (ride.trip.userDetails.count > 3) {
                userDetail = ride.trip.userDetails[3];
                if (userDetail != nil) {
                    [_tableAlertDelegate getPassengerDetail:userDetail andSender:sender];
                }
            }
            break;
        case PassFive:
            if (ride.trip.userDetails.count > 4) {
                userDetail = ride.trip.userDetails[4];
                if (userDetail != nil) {
                    [_tableAlertDelegate getPassengerDetail:userDetail andSender:sender];
                }
            }
            break;
        case DriverName:
            
            break;
        default:
            break;
    }
}


- (void) sectionOpened : (NSInteger) section {
    sectionInfo = [self.sectionInfoArray objectAtIndex:section];
    sectionInfo.open = YES;
    NSArray *sectionInputArray = [_inputDictionary objectForKey:sectionInfo.sectionTitle];
    NSInteger count = sectionInputArray.count;
    NSMutableArray *indexPathToInsert = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i<count;i++)
    {
        [indexPathToInsert addObject:[NSIndexPath indexPathForRow:i inSection:section]];
    }
    
    [self beginUpdates];
    [self insertRowsAtIndexPaths:indexPathToInsert withRowAnimation:UITableViewRowAnimationNone];
    [self endUpdates];
}

- (void) sectionClosed : (NSInteger) section{
    SectionInfo *sectionInfo = [self.sectionInfoArray objectAtIndex:section];
    sectionInfo.open = NO;
    NSInteger countOfRowsToDelete = [self numberOfRowsInSection:section];
    
    if (countOfRowsToDelete > 0) {
        NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
        for (NSInteger i = 0; i < countOfRowsToDelete; i++) {
            [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i inSection:section]];
        }
        
        [self deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:UITableViewRowAnimationTop];
    }
}

- (void) startMuVPressed:(NSInteger)section {
    
    NSString *sectionTitle;
    if (_inputSectionArray != nil && _inputSectionArray.count > 0) {
        sectionTitle = [_inputSectionArray objectAtIndex:sectionNumber];
        NSArray *sectionData = [_inputDictionary objectForKey:sectionTitle];
        ride = (Ride *)  [sectionData objectAtIndex:rowNumber];
    }
    else {
        ride = (Ride *)  [_inputArray objectAtIndex:rowNumber];
    }
    
    
    if (section != sectionNumber) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please select an appropriate row" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }
    else {
        if (rowSelected) {
            if (ride != nil) {
                if (ride.trip.route[@"hasEnded"]) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Selected trip has ended." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                }
                else if (ride.trip.userDetails.count > 0) {
                    [_tableAlertDelegate startMuv:ride.trip forIndexPath:path andSender:nil];
                    ride = nil;
                    rowSelected = NO;
                }
                else {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Sorry! You have no passengers booked for this trip." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                }
            }
        }
        else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please select a row" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            return;
        }
    }
 }

- (void) cancelMuVPressed:(NSInteger)section andSender:(id)sender {
    
    NSString *sectionTitle;
    if (_inputSectionArray != nil && _inputSectionArray.count > 0) {
        sectionTitle = [_inputSectionArray objectAtIndex:sectionNumber];
        NSArray *sectionData = [_inputDictionary objectForKey:sectionTitle];
        ride = (Ride *)  [sectionData objectAtIndex:rowNumber];
    }
    else {
        ride = (Ride *)  [_inputArray objectAtIndex:rowNumber];
    }

    
    if (section != sectionNumber) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please select an appropriate row" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }
    else {
        if (rowSelected) {
            
            if (ride != nil) {
                if (ride.trip.route[@"hasEnded"]) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Selected trip has ended." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                }
                else {
                    [_tableAlertDelegate cancelMuvDriver:ride.trip forIndexPath:path andSender:sender];
                    ride = nil;
                    rowSelected = NO;
                }
            }
        }
        else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please select a row" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            return;
        }
    }
}



@end


