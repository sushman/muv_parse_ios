//
//  SectionHeaderCell.h
//  MuV
//
//  Created by SushmaN on 1/29/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UILabel+MuVFontColorSize.h"

@protocol SectionHeaderCell;

@interface SectionHeaderCell : UIView
@property (weak, nonatomic) IBOutlet UILabel *sectionHeaderTitle;
@property (weak, nonatomic) IBOutlet UIButton *startMuVBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelMuVBtn;
@property (weak, nonatomic) IBOutlet UIButton *expandBtn;
@property (nonatomic, assign) NSInteger section;
@property (nonatomic, weak) id<SectionHeaderCell> delegate;

- (id)initWithFrame:(CGRect)frame WithTitle: (NSString *) title Section:(NSInteger)sectionNumber delegate: (id <SectionHeaderCell>) delegate expand:(BOOL)shouldExpand;
- (void) expandBtnPressed : (id) sender;
- (void) toggleButtonPressed : (BOOL) flag;

@end

@protocol SectionHeaderCell <NSObject>

@optional
- (void) sectionClosed : (NSInteger) section;
- (void) sectionOpened : (NSInteger) section;
- (void) startMuVPressed : (NSInteger) section;
- (void) cancelMuVPressed : (NSInteger) section andSender:(id)sender;

@end
