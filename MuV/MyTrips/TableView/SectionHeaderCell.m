//
//  SectionHeaderCell.m
//  MuV
//
//  Created by SushmaN on 1/29/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "SectionHeaderCell.h"
#import "UIButton+MuVUIButtonAdditions.h"
#import "ControlVariables.h"
#import "UIImage+ResizeMagick.h"

@implementation SectionHeaderCell

//- (void)awakeFromNib {
// 
//}

//- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
//    [super setSelected:selected animated:animated];
//    
//    // Configure the view for the selected state
//}

+ (Class)layerClass {
    
    return [CAGradientLayer class];
}

- (id)initWithFrame: (CGRect)frame
          WithTitle: (NSString *)title
            Section: (NSInteger)sectionNumber
           delegate: (id <SectionHeaderCell>) Delegate
             expand: (BOOL)shouldExpand {
    
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = UICOLOR_PAST_TRIPS;
        
        if (Delegate != nil) {
            if (shouldExpand) {
                UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(expandBtnPressed:)];
                [self addGestureRecognizer:tapGesture];
            }
            
            self.userInteractionEnabled = YES;
        }
        
        
        self.section = sectionNumber;
        self.delegate = Delegate;
        
        CGRect LabelFrame = CGRectMake(5, 5, 165, 30);;
        CGRectInset(LabelFrame, 0.0, 5.0);
        
        
        CGRect viewTop = CGRectMake(0, 0, 320, 1);
        CGRect viewBottom = CGRectMake(0, 40, 320, 1);

        UIView *topView = [[UILabel alloc] initWithFrame:viewTop];
        topView.backgroundColor = [UIColor darkGrayColor];
        [self addSubview:topView];
        
        UIView *bottomView = [[UILabel alloc] initWithFrame:viewBottom];
        bottomView.backgroundColor = [UIColor darkGrayColor];
        [self addSubview:bottomView];

      
        
        UILabel *label = [[UILabel alloc] initWithFrame:LabelFrame];
        label.text = title;
        label = [UILabel muv_QuicksandFontWithSize:12.0f andColor:[UIColor whiteColor] forLabel:label];
        label.backgroundColor = [UIColor clearColor];
        [self addSubview:label];
        self.sectionHeaderTitle = label;
        
        CGRect startMuvButtonFrame = CGRectMake(170, 5, 60, 30);
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.backgroundColor = UICOLOR_SCHEDULED_TRIP;
        button.frame = startMuvButtonFrame;
        [button setTitle:@"Start" forState:UIControlStateNormal];
        //button.enabled = NO;
        _startMuVBtn = [UIButton muv_smallButtonUpdate:_startMuVBtn];
        [button addTarget:self action:@selector(startMuvBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button];
        self.startMuVBtn = button;
        
        CGRect cancelMuvButtonFrame = CGRectMake(230, 5, 60, 30);
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.backgroundColor = UICOLOR_REQUESTED_TRIPS;
        button.frame = cancelMuvButtonFrame;
        //button.enabled = NO;
        [button setTitle:@"Cancel" forState:UIControlStateNormal];
        _cancelMuVBtn = [UIButton muv_smallButtonUpdate:_cancelMuVBtn];
        [button addTarget:self action:@selector(cancelBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button];
        self.cancelMuVBtn = button;
        
        if (shouldExpand) {
            CGRect buttonFrame = CGRectMake(290, 5, 30, 30);
            button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = buttonFrame;
            
            UIImage *image = [UIImage imageNamed:@"muvRight"];
            [button setImage:[image resizedImageByMagick:@"22x22#"] forState:UIControlStateNormal];
            image = [UIImage imageNamed:@"Muv_Down_Arrow"];
            [button setImage:[image resizedImageByMagick:@"22x22#"] forState:UIControlStateSelected];

            [button addTarget:self action:@selector(expandBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:button];
            self.expandBtn = button;
        }
    }
    return self;
}

-(void)layoutSubviews {
    _startMuVBtn = [UIButton muv_smallButtonUpdate:_startMuVBtn];
    _cancelMuVBtn = [UIButton muv_smallButtonUpdate:_cancelMuVBtn];
}


- (void)expandBtnPressed:(id)sender {
    [self toggleButtonPressed:TRUE];
}

- (void)startMuvBtnPressed:(id)sender {
    [self.delegate startMuVPressed:self.section];
}

- (void)cancelBtnPressed:(id)sender {
   [self.delegate cancelMuVPressed:self.section andSender:sender];
}



- (void)toggleButtonPressed : (BOOL) flag {
    _expandBtn.selected = !_expandBtn.selected;
    if(flag) {
        if (_expandBtn.selected) {
            if ([self.delegate respondsToSelector:@selector(sectionOpened:)])
            {
                [self.delegate sectionOpened:self.section];
            }
        }
        else {
            if ([self.delegate respondsToSelector:@selector(sectionClosed:)]) {
                [self.delegate sectionClosed:self.section];
            }
        }
    }
}

@end


