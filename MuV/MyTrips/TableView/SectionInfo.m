//
//  SectionInfo.m
//  MuV
//
//  Created by SushmaN on 2/18/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "SectionInfo.h"

@implementation SectionInfo

- init {
    
    self = [super init];
    if (self) {
        _rowHeights = [[NSMutableArray alloc] init];
    }
    return self;
}


- (NSUInteger)countOfRowHeights {
    return [_rowHeights count];
}

- (id)objectInRowHeightsAtIndex:(NSUInteger)idx {
    return [_rowHeights objectAtIndex:idx];
}

- (void)insertObject:(id)anObject inRowHeightsAtIndex:(NSUInteger)idx {
    [_rowHeights insertObject:anObject atIndex:idx];
}

- (void)insertRowHeights:(NSArray *)rowHeightArray atIndexes:(NSIndexSet *)indexes {
    [_rowHeights insertObjects:rowHeightArray atIndexes:indexes];
}

- (void)removeObjectFromRowHeightsAtIndex:(NSUInteger)idx {
    [_rowHeights removeObjectAtIndex:idx];
}

- (void)removeRowHeightsAtIndexes:(NSIndexSet *)indexes {
    [_rowHeights removeObjectsAtIndexes:indexes];
}

- (void)replaceObjectInRowHeightsAtIndex:(NSUInteger)idx withObject:(id)anObject {
    [_rowHeights replaceObjectAtIndex:idx withObject:anObject];
}

- (void)replaceRowHeightsAtIndexes:(NSIndexSet *)indexes withRowHeights:(NSArray *)rowHeightArray {
    [_rowHeights replaceObjectsAtIndexes:indexes withObjects:rowHeightArray];
}


@end
