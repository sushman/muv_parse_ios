//
//  AgendaViewOfRides.h
//  MuV
//
//  Created by SushmaN on 1/9/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserDetail.h"
#import "Ride.h"
#import "Trip.h"
#import "SectionInfo.h"
#import "SectionHeaderCell.h"



@protocol TableViewOfTripsProtocol;



@interface AgendaViewOfRides : UITableView <UITableViewDelegate,UITableViewDataSource, SectionHeaderCell>


@property(strong, nonatomic) NSArray *inputArray;
@property(strong, nonatomic) NSString *sectionTitle;
@property(strong, nonatomic) NSDictionary *inputDictionary;
@property(strong, nonatomic) NSArray *inputSectionArray;
@property(strong, nonatomic) NSMutableArray *sectionInfoArray;
@property(assign, nonatomic) NSInteger openSectionIndex;
@property (weak, nonatomic) id <TableViewOfTripsProtocol> tableAlertDelegate;


@end

@protocol TableViewOfTripsProtocol

- (void)getPassengerDetail:(UserDetail *)userDetail andSender:(id)sender;
- (void)goToTripDetails:(Trip *)ride andSender:(id)sender;
//- (void)startMuv:(Trip *)ride andSender:(id)sender;
- (void)getDriverDetail:(Ride *)ride andSender:(id)sender;
- (void)cancelMuVForPassenger:(Ride *)rideToCancel andSender:(id)sender;
//- (void)cancelMuvDriver:(Trip *)route andSender:(id)sender;
- (void)goToRatingView:(Ride *)rideObject andSender:(id)sender;
- (void)showFareView:(Ride *)rideObject andSender:(id)sender;
- (void)editTrip:(Ride *)ride andSender:(id)sender;
- (void)cancelMuvDriver:(Trip *)route forIndexPath:indexPath andSender:(id)sender;
- (void)startMuv:(Trip *)route forIndexPath:indexPath andSender:(id)sender;

@end
