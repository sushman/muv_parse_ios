//
//  TableViewOfRides.m
//  MuV
//
//  Created by SushmaN on 1/7/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "TableViewOfRides.h"
#import "TableViewCellOfRides.h"
#import "Ride.h"

@implementation TableViewOfRides

NSInteger numSections;
//
//  TableViewOfEvents.m
//  MuV
//
//  Created by SushmaN on 1/6/15.
//  Copyright (c) 2015 SushmaN. All rights reserved.
//



- (id)initWithFrame:(CGRect)frame
{
    if ((self = [super initWithFrame:frame]))
    {
    }
    
    return self;
}


- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.delegate = self;
        self.dataSource = self;
        self.inputArray = [[NSMutableArray alloc] init];
    }
    
    return self;
    
}




- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TableViewCellOfRides *newCell = (TableViewCellOfRides *)[tableView dequeueReusableCellWithIdentifier: @"rideCell" forIndexPath:indexPath];
    Ride *ride =[Ride new];
    ride = (Ride *) _inputArray[indexPath.row];
    
    newCell.rideTime.text = [NSString stringWithFormat:@"%@ - %@",ride.type,ride.time];
    newCell.driverInfo.text = ride.driverInfo;
    
//    if ([ride.isScheduled  isEqual: @"YES"]) {
//        [newCell.rideTimeLine setBackgroundColor:[UIColor greenColor]];
//    }
//    else {
//        [newCell.rideTimeLine setBackgroundColor:[UIColor redColor]];
//    }
   
    
    return newCell;
}




- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSLog(@"Count %lu", (unsigned long)self.inputArray.count);
    return [self.inputArray count];
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    ContentDetail *content =[ContentDetail new];
    //    content = (ContentDetail *) _inputArray[indexPath.row];
    //    [_tableAlertDelegate tableView:self didPassIndexPath:content.contentId andType:content.type andURL:content.url];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return numSections;
}

- (NSString *)getDurationInMins: (NSNumber *)seconds {
    //check if number is less than 60 seconds
    if (seconds <  [NSNumber numberWithInt:60]) {
        return [seconds stringValue];;
    }
    else {
        //  sec = seconds % ([NSNumber numberWithInt:60]);
        //  seconds = seconds / [NSNumber numberWithInt:60];
    }
    
    return @"";
}

- (void) toggleAgendarCalendar:(NSInteger)index {
    if (index == 0) {
        
    }
    else {
        numSections = 1;
    }
}

@end



