//
//  TableViewCellOfAgendaRides.m
//  MuV
//
//  Created by SushmaN on 1/16/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "TableViewCellOfAgendaRides.h"
#import "UIButton+MuVUIButtonAdditions.h"

@implementation TableViewCellOfAgendaRides

-(void)layoutSubviews
{
    _tripDetailsButton = [UIButton muv_smallButtonUpdate:_tripDetailsButton];
    _cancelRateButton =  [UIButton muv_smallButtonUpdate:_cancelRateButton];

//    _riderOneButton = [UIButton muv_buttonCircle:_riderOneButton];
//    _riderTwoButton = [UIButton muv_buttonCircle:_riderTwoButton];
//    _riderThreeButton = [UIButton muv_buttonCircle:_riderThreeButton];
//    _riderFourButton = [UIButton muv_buttonCircle:_riderFourButton];
//    _riderFiveButton = [UIButton muv_buttonCircle:_riderFiveButton];
}


@end
