//
//  MuVTripViewController.h
//  MuV
//
//  Created by SushmaN on 1/8/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AgendaViewOfRides.h"

typedef enum{
    
    RateButton = -6,
    starOne = -1,
    starTwo = -2,
    starThree = -3,
    starFour = -4,
    starFive = -5,
    
}tripsRatingButtons;


@interface MuVTripViewController : UIViewController<TableViewOfTripsProtocol, UITextFieldDelegate, UIGestureRecognizerDelegate>
//@property (strong, nonatomic) UIViewController *myCalenderViewController;

@property (weak, nonatomic) IBOutlet UIView *activityIndicatorView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndictor;

@end


