//
//  MuVDriverDetailsViewController.m
//  MuV
//
//  Created by SushmaN on 2/5/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "MuVDriverViewController.h"
#import "UIImageView+WebCache.h"
#import "UIButton+MuVUIButtonAdditions.h"
#import "UIImageView+MuVUIImageViewAdditions.h"
#import "UserDetail.h"
#import "UIButton+WebCache.h"
#import "ControlVariables.h"
#import "MuVPassengersProfileViewController.m"
#import "UserProfileInformation.h"

@interface MuVDriverViewController ()
@property (weak, nonatomic) IBOutlet UILabel *driverName;
@property (weak, nonatomic) IBOutlet UIImageView *driverProfileImage;
@property (weak, nonatomic) IBOutlet UIButton *driverPhone;
@property (weak, nonatomic) IBOutlet UIButton *driverEmail;
@property (weak, nonatomic) IBOutlet UIButton *driverSMS;

@property (weak, nonatomic) IBOutlet UIImageView *carPicture;
@property (weak, nonatomic) IBOutlet UILabel *carMake;
@property (weak, nonatomic) IBOutlet UIButton *passenger1;
@property (weak, nonatomic) IBOutlet UIButton *passenger2;
@property (weak, nonatomic) IBOutlet UIButton *passenger3;
@property (weak, nonatomic) IBOutlet UIButton *passenger4;
@property (weak, nonatomic) IBOutlet UIButton *passenger5;
@property (weak, nonatomic) IBOutlet UITextView *aboutMeTxtV;
@property (weak, nonatomic) IBOutlet UILabel *driverDesc;
@property (weak, nonatomic) IBOutlet UILabel *driverProfession;
@property (weak, nonatomic) IBOutlet UILabel *driverCompany;

@property (weak, nonatomic) IBOutlet UIImageView *starOne;
@property (weak, nonatomic) IBOutlet UIImageView *starTwo;
@property (weak, nonatomic) IBOutlet UIImageView *starThree;
@property (weak, nonatomic) IBOutlet UIImageView *starFour;
@property (weak, nonatomic) IBOutlet UIImageView *starFive;


@end

@implementation MuVDriverViewController
enum{
    Driver = 0,
    PassOne = 1,
    PassTwo = 2,
    PassThree = 3,
    PassFour = 4,
    PassFive = 5,
    DriverName = 6
    
} userDetailButton;


UserDetail *userdetail;
PFObject *driverDetail;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    driverDetail = _ride.driverDetail;
    
    // UPDATING UI TO REFLECT DRIVER RATING
    if ([_ride.driverRating floatValue] > 0){
        [_starOne setImage:[UIImage imageNamed:@"MuV_RateStar_HalfStar"]];
    }
    if ([_ride.driverRating floatValue] > 0.5){
        [_starOne setImage:[UIImage imageNamed:@"MuV_RateStar_Blu"]];
    }
    if ([_ride.driverRating floatValue] > 1){
        [_starTwo setImage:[UIImage imageNamed:@"MuV_RateStar_HalfStar"]];
    }
    if ([_ride.driverRating floatValue] > 1.5){
        [_starTwo setImage:[UIImage imageNamed:@"MuV_RateStar_Blu"]];
    }
    if ([_ride.driverRating floatValue] > 2){
        [_starThree setImage:[UIImage imageNamed:@"MuV_RateStar_HalfStar"]];
    }
    if ([_ride.driverRating floatValue] > 2.5){
        [_starThree setImage:[UIImage imageNamed:@"MuV_RateStar_Blu"]];
    }
    if ([_ride.driverRating floatValue] > 3){
        [_starFour setImage:[UIImage imageNamed:@"MuV_RateStar_HalfStar"]];
    }
    if ([_ride.driverRating floatValue] > 3.5){
        [_starFour setImage:[UIImage imageNamed:@"MuV_RateStar_Blu"]];
    }
    if ([_ride.driverRating floatValue] > 4){
        [_starFive setImage:[UIImage imageNamed:@"MuV_RateStar_HalfStar"]];
    }
    if ([_ride.driverRating floatValue] > 4.5){
        [_starFive setImage:[UIImage imageNamed:@"MuV_RateStar_Blu"]];
    }
    
   // NSLog(@")
    _driverName.text = [NSString stringWithFormat:@"%@ %@.", driverDetail[@"userdetail"][@"firstName"], [driverDetail[@"userdetail"][@"lastName"] substringToIndex:1]];
    _driverDesc.text = [NSString stringWithFormat:@"%@ / %@ / %@", driverDetail[@"userdetail"][@"age"], [driverDetail[@"userdetail"][@"gender"] substringToIndex:1], driverDetail[@"userdetail"][@"relationship"] ];
    _driverProfession.text = driverDetail[@"userdetail"][@"occupation"];
    _driverCompany.text = driverDetail[@"userdetail"][@"companyName"];
    _aboutMeTxtV.text = driverDetail[@"userdetail"][@"profile"];
    
    _driverPhone.titleLabel.text = driverDetail[@"userdetail"][@"phone"];
    _driverEmail.titleLabel.text = driverDetail[@"userdetail"][@"email"];
    
    PFFile *imageFile = driverDetail[@"userdetail"][@"profileImage"];
    [_driverProfileImage sd_setImageWithURL:[NSURL URLWithString:imageFile.url]];
    _driverProfileImage = [UIImageView muv_imageCircle:_driverProfileImage];
    
    if (!imageFile) {
        [_driverProfileImage setImage:[UIImage imageNamed:NOIMAGE_AVATAR]];
    }
    
    imageFile = driverDetail[@"carImage"];
    [_carPicture sd_setImageWithURL:[NSURL URLWithString:imageFile.url]];
    _carPicture = [UIImageView muv_imageCircle:_carPicture];
    
    if (!imageFile) {
        [_carPicture setImage:[UIImage imageNamed:MUV_NO_CAR_IMAGE]];
    }
    
    _carMake.text = [NSString stringWithFormat:@"%@ %@ %@", driverDetail[@"carYear"], driverDetail[@"carModel"], driverDetail[@"carMake"]];
    
    //get passenegers for that day for driver
    [self getPassengersForDriver];
    
}

- (void)getPassengersForDriver {
    PFFile *imageFile;
    UIButton *button;
    UserDetail *userdetail;
    NSArray *arrayOfPassengerButtons = [NSArray arrayWithObjects:_passenger1,_passenger2,_passenger3,_passenger4,_passenger5, nil];
    for(int count = 0; count < _ride.trip.userDetails.count; count++) {
        userdetail = _ride.trip.userDetails[count];
        if (count < arrayOfPassengerButtons.count) {
            if (userdetail) {
                imageFile = userdetail.profileImage;
                button = arrayOfPassengerButtons[count];
                button = [UIButton muv_buttonCircle:button];
                [button sd_setImageWithURL:[NSURL URLWithString:imageFile.url] forState:UIControlStateNormal];
                [button setBackgroundColor:nil];
            }
        }
    }
    
    for (NSUInteger numEmptySeats = _ride.trip.userDetails.count; numEmptySeats < arrayOfPassengerButtons.count ; numEmptySeats++) {
        button = arrayOfPassengerButtons[numEmptySeats];
        button.hidden = YES;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)callDriver:(id)sender {
    NSString *driverPhone = driverDetail[@"userdetail"][@"phone"];
    
    NSString *phoneNumber = [[driverPhone componentsSeparatedByCharactersInSet:
                              [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                             componentsJoinedByString:@""];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[@"tel:" stringByAppendingString:phoneNumber]]];
    
}


- (IBAction)showPassengerProfile:(id)sender {
    
    switch ([sender tag]) {
        case PassOne:
            if (_ride.trip.userDetails.count > 0) {
                userdetail = _ride.trip.userDetails[0];
            }
            break;
        case PassTwo:
            if (_ride.trip.userDetails.count > 1) {
                userdetail = _ride.trip.userDetails[1];
            }
            break;
        case PassThree:
            if (_ride.trip.userDetails.count > 2) {
                userdetail = _ride.trip.userDetails[2];
            }
            break;
        case PassFour:
            if (_ride.trip.userDetails.count > 3) {
                userdetail = _ride.trip.userDetails[3];
            }
            break;
        case PassFive:
            if (_ride.trip.userDetails.count > 4) {
                userdetail = _ride.trip.userDetails[4];
            }
            break;
        default:
            break;
    }
    
    PFUser *user = [UserProfileInformation sharedMySingleton].user;
    if (userdetail != nil && userdetail.userId != user) {
        [self performSegueWithIdentifier:@"PassengerProfile" sender:sender];
    }
}




- (IBAction)emailDriver:(id)sender {
    if([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailComposer = [[MFMailComposeViewController alloc]init];
        NSString *driverEmail = driverDetail[@"userdetail"][@"email"];
        [mailComposer setToRecipients:@[driverEmail]];
        mailComposer.mailComposeDelegate = self;
        [mailComposer setSubject:@"Message from MüV User"];
        [self presentViewController:mailComposer animated:YES completion:NULL];
    }else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Your device's email client is not setup." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
}

- (IBAction)textDriver:(id)sender {
    if(![MFMessageComposeViewController canSendText]) {
        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Your device doesn't support SMS." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [warningAlert show];
        return;
    }
    
    NSString *driverPhone = driverDetail[@"userdetail"][@"phone"];
    
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    NSString *phoneNumber = [[driverPhone componentsSeparatedByCharactersInSet:
                              [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                             componentsJoinedByString:@""];
    [messageController setRecipients:@[phoneNumber]];
    [self presentViewController:messageController animated:YES completion:nil];
    
}


- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result {
    switch (result) {
        case MessageComposeResultCancelled:
            break;
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed to send SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            break;
        }
        case MessageComposeResultSent:
            break;
        default:
            break;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)mailComposeController:(MFMailComposeViewController *)controller
         didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    if (result) {
        NSLog(@"Result : %d",result);
    }
    if (error) {
        NSLog(@"Error : %@",error);
    }
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString: @"PassengerProfile"]) {
        MuVPassengersProfileViewController  *passProfile = [segue destinationViewController];
        passProfile.userDetail = userdetail;
        return;
    }
}


@end
