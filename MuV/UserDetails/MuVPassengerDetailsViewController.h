//
//  PassengerDetailsViewController.h
//  MuV
//
//  Created by SushmaN on 2/4/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserDetail.h"

@interface MuVPassengerDetailsViewController : UIViewController

@property (nonatomic, strong) UserDetail *userDetail;

@end
