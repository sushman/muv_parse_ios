//
//  PassengerDetailsViewController.m
//  MuV
//
//  Created by SushmaN on 2/4/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "MuVPassengerDetailsViewController.h"
#import "UIImageView+WebCache.h"
#import "UIButton+MuVUIButtonAdditions.h"
#import "UIImageView+MuVUIImageViewAdditions.h"
#import <MessageUI/MessageUI.h>

@interface MuVPassengerDetailsViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *profileImage;
@property (weak, nonatomic) IBOutlet UIButton *phoneNumber;
@property (weak, nonatomic) IBOutlet UIButton *email;
@property (weak, nonatomic) IBOutlet UIButton *sms;
@property (weak, nonatomic) IBOutlet UILabel *startLocation;
@property (weak, nonatomic) IBOutlet UILabel *endLocation;
@property (weak, nonatomic) IBOutlet UILabel *time;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *routeType;


@end

@implementation MuVPassengerDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    PFObject *user = _userDetail.userId;
    _phoneNumber.titleLabel.text = user[@"phone"];
    _email.titleLabel.text = user[@"email"];
    _startLocation.text = _userDetail.startAddress;
    _endLocation.text = _userDetail.endAddress;
    _name.text = _userDetail.displayName;
                             
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"h:mm a"];
    
    if (_userDetail.startTime) {
        _routeType.text = @"Depart Work";
        _time.text = [dateFormat stringFromDate:_userDetail.startTime];
    }
    else {
        _routeType.text = @"Arrive at work";
        _time.text = [dateFormat stringFromDate:_userDetail.endTime];
    }
   
  
    PFFile *imageFile = _userDetail.profileImage;
    [_profileImage sd_setImageWithURL:[NSURL URLWithString:imageFile.url]];
    _profileImage = [UIImageView muv_imageCircle:_profileImage];
 }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)callDriver:(id)sender {
    NSString *phone = _phoneNumber.titleLabel.text;
    
    NSString *phoneNumber = [[phone componentsSeparatedByCharactersInSet:
                              [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                             componentsJoinedByString:@""];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[@"tel:" stringByAppendingString:phoneNumber]]];
    
}



- (IBAction)emailDriver:(id)sender {
    if([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailComposer = [[MFMailComposeViewController alloc]init];
        NSString *email = _email.titleLabel.text;
        [mailComposer setToRecipients:@[email]];
        mailComposer.mailComposeDelegate = self;
        [mailComposer setSubject:@"Message from MüV User"];
        [self presentViewController:mailComposer animated:YES completion:NULL];
    }else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Your device's email client is not setup." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
}

- (IBAction)textDriver:(id)sender {
    if(![MFMessageComposeViewController canSendText]) {
        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Your device doesn't support SMS." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [warningAlert show];
        return;
    }
    
    NSString *phone = _phoneNumber.titleLabel.text;
    
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    NSString *phoneNumber = [[phone componentsSeparatedByCharactersInSet:
                              [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                             componentsJoinedByString:@""];
    [messageController setRecipients:@[phoneNumber]];
    [self presentViewController:messageController animated:YES completion:nil];
    
}


- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result {
    switch (result) {
        case MessageComposeResultCancelled:
            break;
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed to send SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            break;
        }
        case MessageComposeResultSent:
            break;
        default:
            break;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)mailComposeController:(MFMailComposeViewController *)controller
         didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    if (result) {
        NSLog(@"Result : %d",result);
    }
    if (error) {
        NSLog(@"Error : %@",error);
    }
    [self dismissViewControllerAnimated:YES completion:nil];
    
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
