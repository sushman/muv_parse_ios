//
//  MuVCoRiderProfileViewController.m
//  MuV
//
//  Created by SushmaN on 2/19/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "MuVCoRiderProfileViewController.h"
#import "UIButton+MuVUIButtonAdditions.h"
#import "UIImageView+MuVUIImageViewAdditions.h"
#import "UIImageView+WebCache.h"
#import <Parse/Parse.h>
#import <MessageUI/MessageUI.h>

@implementation MuVCoRiderProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    PFObject *userdetail = _userDetail.userId;
    
    _name.text = _userDetail.displayName;
    
    _desc.text = [NSString stringWithFormat:@"%@ / %@ / %@", userdetail[@"age"], [userdetail[@"gender"] substringToIndex:1], userdetail[@"relationship"]];
    _profession.text = userdetail[@"occupation"];
    _organisation.text = userdetail[@"companyName"];
    _aboutMe.text = userdetail[@"profile"];
    
    if (userdetail[@"isContactDetailPublic"] == [NSNumber numberWithBool:YES]) {
        _phone.titleLabel.text = userdetail[@"phone"];
        _email.titleLabel.text = userdetail[@"email"];
    }
    else {
        _phone.titleLabel.text = @"";
        _email.titleLabel.text = @"";
    }
  
    PFFile *imageFile = _userDetail.profileImage;
    [_profilePic sd_setImageWithURL:[NSURL URLWithString:imageFile.url]];
    _profilePic = [UIImageView muv_imageCircle:_profilePic];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)callDriver:(id)sender {
    
    if (_phone.titleLabel.text.length <= 0) {
        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"" message:@"User doesn't wish to share the contact information." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [warningAlert show];
        return;
    }
    
    NSString *phone = _phone.titleLabel.text;
    
    NSString *phoneNumber = [[phone componentsSeparatedByCharactersInSet:
                              [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                             componentsJoinedByString:@""];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[@"tel:" stringByAppendingString:phoneNumber]]];
    
}



- (IBAction)emailDriver:(id)sender {
    if (_email.titleLabel.text.length <= 0) {
        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"" message:@"User doesn't wish to share the contact information." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [warningAlert show];
        return;
    }
    
    if([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailComposer = [[MFMailComposeViewController alloc]init];
        NSString *email = _email.titleLabel.text;
        [mailComposer setToRecipients:@[email]];
        mailComposer.mailComposeDelegate = self;
        [mailComposer setSubject:@"Message from MüV User"];
        [self presentViewController:mailComposer animated:YES completion:NULL];
    }else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Your device's email client is not setup." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
}

- (IBAction)textDriver:(id)sender {
    
    if (_phone.titleLabel.text.length <= 0) {
        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"" message:@"User doesn't wish to share the contact information." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [warningAlert show];
        return;
    }
    
    if(![MFMessageComposeViewController canSendText]) {
        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Your device doesn't support SMS." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [warningAlert show];
        return;
    }
    
    NSString *phone = _phone.titleLabel.text;
    
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    NSString *phoneNumber = [[phone componentsSeparatedByCharactersInSet:
                              [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                             componentsJoinedByString:@""];
    [messageController setRecipients:@[phoneNumber]];
    [self presentViewController:messageController animated:YES completion:nil];
    
}


- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result {
    switch (result) {
        case MessageComposeResultCancelled:
            break;
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed to send SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            break;
        }
        case MessageComposeResultSent:
            break;
        default:
            break;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)mailComposeController:(MFMailComposeViewController *)controller
         didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    if (result) {
        NSLog(@"Result : %d",result);
    }
    if (error) {
        NSLog(@"Error : %@",error);
    }
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


@end
