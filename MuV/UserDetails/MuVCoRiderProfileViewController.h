//
//  MuVCoRiderProfileViewController.h
//  MuV
//
//  Created by SushmaN on 2/19/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserDetail.h"

@interface MuVCoRiderProfileViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *profilePic;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *desc;
@property (weak, nonatomic) IBOutlet UILabel *profession;
@property (weak, nonatomic) IBOutlet UILabel *organisation;
@property (weak, nonatomic) IBOutlet UIButton *email;
@property (weak, nonatomic) IBOutlet UITextView *aboutMe;
@property (weak, nonatomic) IBOutlet UIButton *phone;
@property (weak, nonatomic) IBOutlet UIButton *sms;

@property (nonatomic, strong) UserDetail *userDetail;


@end
