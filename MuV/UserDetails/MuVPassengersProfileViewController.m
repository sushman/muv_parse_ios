//
//  MuVPassengerProfileViewController.m
//  MuV
//
//  Created by SushmaN on 2/18/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import "MuVPassengersProfileViewController.h"

@interface MuVPassengersProfileViewController ()

@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UIImageView *profileImage;
@property (weak, nonatomic) IBOutlet UIButton *phone;
@property (weak, nonatomic) IBOutlet UIButton *email;
@property (weak, nonatomic) IBOutlet UIButton *SMS;

@property (weak, nonatomic) IBOutlet UIImageView *carPicture;
@property (weak, nonatomic) IBOutlet UILabel *carMake;
@property (weak, nonatomic) IBOutlet UITextView *aboutMeTxtV;
@property (weak, nonatomic) IBOutlet UILabel *desc;
@property (weak, nonatomic) IBOutlet UILabel *profession;
@property (weak, nonatomic) IBOutlet UILabel *company;


@end

@implementation MuVPassengersProfileViewController




@end
