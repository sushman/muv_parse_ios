//
//  MuVDriverDetailsViewController.h
//  MuV
//
//  Created by SushmaN on 2/5/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Ride.h"
#import <MessageUI/MessageUI.h>

@interface MuVDriverViewController : UIViewController<MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate>

@property (nonatomic, strong) Ride *ride;

@end
