//
//  Page4ViewController.h
//  MuV
//
//  Created by Hing Huynh on 6/11/15.
//  Copyright (c) 2015 moback. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseContentViewController.h"

@interface Page4ViewController : BaseContentViewController

- (IBAction)actionSkip:(id)sender;
@end
